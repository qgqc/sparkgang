package com.gcl.corporatewalletmanagement.service;

import java.util.List;
import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;

import javax.servlet.http.HttpServletResponse;

/**
 * BPS文件处理管理Service接口
 * 
 * @author yada
 * @date 2022-01-13
 */
public interface IBusBpsFilehandlService 
{
    /**
     * 查询BPS文件处理管理
     * 
     * @param id BPS文件处理管理主键
     * @return BPS文件处理管理
     */
    public BusBpsFilehandl selectBusBpsFilehandlById(String id);

    /**
     * 查询BPS文件处理管理列表
     * 
     * @param busBpsFilehandl BPS文件处理管理
     * @return BPS文件处理管理集合
     */
    public List<BusBpsFilehandl> selectBusBpsFilehandlList(BusBpsFilehandl busBpsFilehandl);

    /**
     * 新增BPS文件处理管理
     * 
     * @param busBpsFilehandl BPS文件处理管理
     * @return 结果
     */
    public int insertBusBpsFilehandl(BusBpsFilehandl busBpsFilehandl);

    /**
     * 修改BPS文件处理管理
     * 
     * @param busBpsFilehandl BPS文件处理管理
     * @return 结果
     */
    public int updateBusBpsFilehandl(BusBpsFilehandl busBpsFilehandl);

    /**
     * 批量删除BPS文件处理管理
     * 
     * @param ids 需要删除的BPS文件处理管理主键集合
     * @return 结果
     */
    public int deleteBusBpsFilehandlByIds(String[] ids);

    /**
     * 删除BPS文件处理管理信息
     * 
     * @param id BPS文件处理管理主键
     * @return 结果
     */
    public int deleteBusBpsFilehandlById(String id);



    void downloadDATFile(String id,String type, HttpServletResponse response) throws Exception;


    void clearDATAndRETFileBefoerDays(String days);

}
