package com.gcl.corporatewalletmanagement.service;

import java.util.List;

import com.gcl.common.constant.DataStatuEnum;
import com.gcl.corporatewalletmanagement.domain.BusMerWalletInfo;
import com.gcl.corporatewalletmanagement.domain.vo.BusMerWalletInfoVo;

/**
 * 对公钱包开立申请Service接口
 *
 * @author yada
 * @date 2022-01-18
 */
public interface IBusMerWalletInfoService
{
    /**
     * 查询对公钱包开立申请
     *
     * @param id 对公钱包开立申请主键
     * @return 对公钱包开立申请
     */
    public BusMerWalletInfo selectBusMerWalletInfoById(String id);

    /**
     * 查询对公钱包开立申请列表
     *
     * @param busMerWalletInfo 对公钱包开立申请
     * @return 对公钱包开立申请集合
     */
    public List<BusMerWalletInfo> selectBusMerWalletInfoList(BusMerWalletInfo busMerWalletInfo);

    public List<BusMerWalletInfo> selectBusMerWalletInfoListVo(BusMerWalletInfoVo busMerWalletInfo);


    public List<BusMerWalletInfo> selectInitStatusBusMerWalletInfoList(BusMerWalletInfo busMerWalletInfo);

    public List<BusMerWalletInfo> selectExamineBusMerWalletInfoList(BusMerWalletInfo busMerWalletInfo);

    /**
     * 新增对公钱包开立申请
     *
     * @param busMerWalletInfo 对公钱包开立申请
     * @return 结果
     */
    public int insertBusMerWalletInfo(BusMerWalletInfo busMerWalletInfo);

    /**
     * 修改对公钱包开立申请
     *
     * @param busMerWalletInfo 对公钱包开立申请
     * @return 结果
     */
    public int updateBusMerWalletInfo(BusMerWalletInfo busMerWalletInfo);


    /**
     * 更新数据
     * @param busMerWalletInfo 商户钱包信息
     * @return 保存结果
     */
    int update(BusMerWalletInfo busMerWalletInfo);


    int updateDataStatus(String id, DataStatuEnum dataStatusEnum);
    /**
     * 批量删除对公钱包开立申请
     *
     * @param ids 需要删除的对公钱包开立申请主键集合
     * @return 结果
     */
    public int deleteBusMerWalletInfoByIds(String[] ids);

    /**
     * 删除对公钱包开立申请信息
     *
     * @param id 对公钱包开立申请主键
     * @return 结果
     */
    public int deleteBusMerWalletInfoById(String id);

    /**
     *
     * @param busMerWalletInfoList 存量信息
     * @param isStockInfoImport 是否存量信息导入
     * @return 返回信息
     */
    public String importerWalletInfo(List<BusMerWalletInfo> busMerWalletInfoList,boolean isStockInfoImport,String operName);

    /**
     * 根据统一社会信用代码和营业执照号码进行企业信息的唯一性校验
     * @param busMerWalletInfo 统一社会信用代码和营业执照号码
     * @return 企业信息是否已经存在
     */
    public boolean busMerWalletInfoAlreadyExists(BusMerWalletInfo busMerWalletInfo);


    /**
     * @return 客户号是否已经存在
     */
    boolean busMerWalletInfoCustNoAlreadyExists(String custNo);

    /**
     * 对数据导入模板中的数据进行初始化
     * @param busMerWalletInfo 模板中的数据
     * @param isStockInfoImport 是否存量信息导入
     * @return 初始化后的数据
     */
    BusMerWalletInfo importDataInit(BusMerWalletInfo busMerWalletInfo,boolean isStockInfoImport);

    List<BusMerWalletInfo> selectApplyPageBusMerWalletInfoList(BusMerWalletInfo busMerWalletInfo);

    boolean vertifyLoginName(String account);

    boolean vertifyEditLoginName(String account);

    void auditPass(BusMerWalletInfoVo busMerWalletInfo);

}
