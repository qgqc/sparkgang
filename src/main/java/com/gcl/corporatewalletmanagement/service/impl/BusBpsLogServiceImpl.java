package com.gcl.corporatewalletmanagement.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gcl.corporatewalletmanagement.mapper.BusBpsLogMapper;
import com.gcl.corporatewalletmanagement.domain.BusBpsLog;
import com.gcl.corporatewalletmanagement.service.IBusBpsLogService;

/**
 * BPS日志Service业务层处理
 * 
 * @author zhaihb
 * @date 2022-01-13
 */
@Service
public class BusBpsLogServiceImpl implements IBusBpsLogService 
{
    @Autowired
    private BusBpsLogMapper busBpsLogMapper;

    /**
     * 查询BPS日志
     * 
     * @param logId BPS日志主键
     * @return BPS日志
     */
    @Override
    public BusBpsLog selectBusBpsLogByLogId(String logId)
    {
        return busBpsLogMapper.selectBusBpsLogByLogId(logId);
    }

    /**
     * 查询BPS日志列表
     * 
     * @param busBpsLog BPS日志
     * @return BPS日志
     */
    @Override
    public List<BusBpsLog> selectBusBpsLogList(BusBpsLog busBpsLog)
    {
        return busBpsLogMapper.selectBusBpsLogList(busBpsLog);
    }

    /**
     * 新增BPS日志
     * 
     * @param busBpsLog BPS日志
     * @return 结果
     */
    @Override
    public int insertBusBpsLog(BusBpsLog busBpsLog)
    {
        return busBpsLogMapper.insertBusBpsLog(busBpsLog);
    }

    /**
     * 修改BPS日志
     * 
     * @param busBpsLog BPS日志
     * @return 结果
     */
    @Override
    public int updateBusBpsLog(BusBpsLog busBpsLog)
    {
        return busBpsLogMapper.updateBusBpsLog(busBpsLog);
    }

    /**
     * 批量删除BPS日志
     * 
     * @param logIds 需要删除的BPS日志主键
     * @return 结果
     */
    @Override
    public int deleteBusBpsLogByLogIds(String[] logIds)
    {
        return busBpsLogMapper.deleteBusBpsLogByLogIds(logIds);
    }

    /**
     * 删除BPS日志信息
     * 
     * @param logId BPS日志主键
     * @return 结果
     */
    @Override
    public int deleteBusBpsLogByLogId(String logId)
    {
        return busBpsLogMapper.deleteBusBpsLogByLogId(logId);
    }
}
