package com.gcl.corporatewalletmanagement.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.gcl.bps.constant.BpsTypeConstant;
import com.gcl.bps.util.DateUtil;
import com.gcl.common.utils.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.gcl.corporatewalletmanagement.mapper.BusBpsFilehandlMapper;
import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;
import com.gcl.corporatewalletmanagement.service.IBusBpsFilehandlService;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;

/**
 * BPS文件处理管理Service业务层处理
 * 
 * @author yada
 * @date 2022-01-13
 */
@Service
public class BusBpsFilehandlServiceImpl implements IBusBpsFilehandlService 
{
    private static Logger logger = LoggerFactory.getLogger(BusBpsFilehandlServiceImpl.class);

    @Autowired
    private BusBpsFilehandlMapper busBpsFilehandlMapper;


    @Value("${bpsCreatLocalPath}")
    private String bpsDatFilePath;

    @Value("${bpsDownFileLocalPath}")
    private String bpsDownFileLocalPath;


    /**
     * 查询BPS文件处理管理
     * 
     * @param id BPS文件处理管理主键
     * @return BPS文件处理管理
     */
    @Override
    public BusBpsFilehandl selectBusBpsFilehandlById(String id)
    {
        return busBpsFilehandlMapper.selectBusBpsFilehandlById(id);
    }

    /**
     * 查询BPS文件处理管理列表
     * 
     * @param busBpsFilehandl BPS文件处理管理
     * @return BPS文件处理管理
     */
    @Override
    public List<BusBpsFilehandl> selectBusBpsFilehandlList(BusBpsFilehandl busBpsFilehandl)
    {
        return busBpsFilehandlMapper.selectBusBpsFilehandlList(busBpsFilehandl);
    }

    /**
     * 新增BPS文件处理管理
     * 
     * @param busBpsFilehandl BPS文件处理管理
     * @return 结果
     */
    @Override
    public int insertBusBpsFilehandl(BusBpsFilehandl busBpsFilehandl)
    {
        return busBpsFilehandlMapper.insertBusBpsFilehandl(busBpsFilehandl);
    }

    /**
     * 修改BPS文件处理管理
     * 
     * @param busBpsFilehandl BPS文件处理管理
     * @return 结果
     */
    @Override
    public int updateBusBpsFilehandl(BusBpsFilehandl busBpsFilehandl)
    {
        return busBpsFilehandlMapper.updateBusBpsFilehandl(busBpsFilehandl);
    }

    /**
     * 批量删除BPS文件处理管理
     * 
     * @param ids 需要删除的BPS文件处理管理主键
     * @return 结果
     */
    @Override
    public int deleteBusBpsFilehandlByIds(String[] ids)
    {
        return busBpsFilehandlMapper.deleteBusBpsFilehandlByIds(ids);
    }

    /**
     * 删除BPS文件处理管理信息
     * 
     * @param id BPS文件处理管理主键
     * @return 结果
     */
    @Override
    public int deleteBusBpsFilehandlById(String id)
    {
        return busBpsFilehandlMapper.deleteBusBpsFilehandlById(id);
    }

    /*****
     * 下载DAT文件
     * @param id
     * @param response
     * @throws Exception
     */
    @Override
    public void downloadDATFile(String id,String type, HttpServletResponse response) throws Exception {

        BusBpsFilehandl head = busBpsFilehandlMapper.selectBusBpsFilehandlById(id);
        if(head != null && StringUtils.isNotBlank(head.getSourceFile())){
            String datFilePath = bpsDatFilePath + type + File.separator + head.getSourceFile();
            File file = new File(datFilePath);
            if (!file.exists()) {
                throw new Exception("文件不存在");
            }
            InputStream inputStream = new FileInputStream(datFilePath);
            OutputStream outputStream = response.getOutputStream();
            byte[] b = new byte[4096];
            int len;
            while ((len = inputStream.read(b)) > 0) {
                outputStream.write(b, 0, len);
            }
            inputStream.close();
        }else {
            throw new Exception("head信息不存在");
        }
    }

    @Transactional
    @Override
    public void clearDATAndRETFileBefoerDays(String days) {
        //1.查询出 days 天以前生成的DAT和RET文件
        //Date clearDate = DateUtils.addDays(new Date(),(0 - Integer.parseInt(days)));
        String clearDate = DateUtil.getOldDay(Integer.parseInt(days));

        ArrayList<BusBpsFilehandl> readyClearDatData = busBpsFilehandlMapper.selectReadyClearDATBusBpsFilehandlByDate(clearDate);
        String datFilePath = bpsDatFilePath + BpsTypeConstant.CWFS_TYPE + File.separator;

        if(CollectionUtils.isNotEmpty(readyClearDatData)){
            for(BusBpsFilehandl fileInfo : readyClearDatData){
                String fileName = fileInfo.getSourceFile();
                if(StringUtils.isNotBlank(fileName)){
                    datFilePath  = datFilePath + fileName;
                    File file = new File(datFilePath);
                    if (file.exists()) {
                        file.delete();
                        logger.info("文件名为：" + fileName + " 的DAT文件已经被 自动清理定时任务 删除！");
                    }else{
                        //logger.warn("文件名为：" + fileName + "DAT文件已经不存在，所以不再进行删除！");
                    }
                }
            }

        }


        String retFilePath = bpsDownFileLocalPath + BpsTypeConstant.CWFS_TYPE + File.separator;

        ArrayList<BusBpsFilehandl> readyClearRETData = busBpsFilehandlMapper.selectReadyClearERTBusBpsFilehandlByDate(clearDate);
        if(CollectionUtils.isNotEmpty(readyClearRETData)){
            for(BusBpsFilehandl fileInfo : readyClearRETData){
                String fileName = fileInfo.getSourceFile();
                fileName = fileName.replace(".DAT", ".RET");
                if(StringUtils.isNotBlank(fileName)){
                    retFilePath  = retFilePath + fileName;
                    File file = new File(retFilePath);
                    if (file.exists()) {
                        file.delete();
                        logger.info("文件名为：" + fileName + " 的RET文件已经被 自动清理定时任务 删除！");

                    }else{
                        //logger.warn("文件名为：" + fileName + " RET文件已经不存在，所以不再进行删除！");
                    }
                }
            }

        }
    }
}
