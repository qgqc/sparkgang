package com.gcl.corporatewalletmanagement.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gcl.corporatewalletmanagement.mapper.BusMerFileUploadRecordMapper;
import com.gcl.corporatewalletmanagement.domain.BusMerFileUploadRecord;
import com.gcl.corporatewalletmanagement.service.IBusMerFileUploadRecordService;

/**
 * 资料上传日志Service业务层处理
 *
 * @author zhaihb
 * @date 2022-01-14
 */
@Service
public class BusMerFileUploadRecordServiceImpl implements IBusMerFileUploadRecordService
{
    @Autowired
    private BusMerFileUploadRecordMapper busMerFileUploadRecordMapper;

    /**
     * 查询资料上传日志
     *
     * @param id 资料上传日志主键
     * @return 资料上传日志
     */
    @Override
    public BusMerFileUploadRecord selectBusMerFileUploadRecordById(String id)
    {
        return busMerFileUploadRecordMapper.selectBusMerFileUploadRecordById(id);
    }

    /**
     * 查询资料上传日志列表
     *
     * @param busMerFileUploadRecord 资料上传日志
     * @return 资料上传日志
     */
    @Override
    public List<BusMerFileUploadRecord> selectBusMerFileUploadRecordList(BusMerFileUploadRecord busMerFileUploadRecord)
    {
        return busMerFileUploadRecordMapper.selectBusMerFileUploadRecordList(busMerFileUploadRecord);
    }

    /**
     * 新增资料上传日志
     *
     * @param busMerFileUploadRecord 资料上传日志
     * @return 结果
     */
    @Override
    public int insertBusMerFileUploadRecord(BusMerFileUploadRecord busMerFileUploadRecord)
    {
        return busMerFileUploadRecordMapper.insertBusMerFileUploadRecord(busMerFileUploadRecord);
    }

    /**
     * 修改资料上传日志
     *
     * @param busMerFileUploadRecord 资料上传日志
     * @return 结果
     */
    @Override
    public int updateBusMerFileUploadRecord(BusMerFileUploadRecord busMerFileUploadRecord)
    {
        return busMerFileUploadRecordMapper.updateBusMerFileUploadRecord(busMerFileUploadRecord);
    }

    /**
     * 批量删除资料上传日志
     *
     * @param ids 需要删除的资料上传日志主键
     * @return 结果
     */
    @Override
    public int deleteBusMerFileUploadRecordByIds(String[] ids)
    {
        return busMerFileUploadRecordMapper.deleteBusMerFileUploadRecordByIds(ids);
    }

    /**
     * 删除资料上传日志信息
     *
     * @param id 资料上传日志主键
     * @return 结果
     */
    @Override
    public int deleteBusMerFileUploadRecordById(String id)
    {
        return busMerFileUploadRecordMapper.deleteBusMerFileUploadRecordById(id);
    }
}
