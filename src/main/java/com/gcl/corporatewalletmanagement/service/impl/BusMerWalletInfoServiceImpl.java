package com.gcl.corporatewalletmanagement.service.impl;

import java.math.BigDecimal;
import java.util.*;

import javax.validation.Validator;

import com.gcl.bps.domain.BusMerWalletDeal;
import com.gcl.bps.service.IBusMerWalletDealService;
import com.gcl.bps.util.DateUtil;
import com.gcl.common.constant.ResultStatus;
import com.gcl.corporatewalletmanagement.domain.BusMerWalletReturnInfo;
import com.gcl.corporatewalletmanagement.domain.vo.BusMerWalletInfoVo;
import com.gcl.corporatewalletmanagement.mapper.BusMerWalletInfoTempMapper;
import com.gcl.corporatewalletmanagement.service.*;
import com.gcl.util.UpdateTool;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcl.common.constant.DataSourcesEnum;
import com.gcl.common.constant.DataStatuEnum;
import com.gcl.common.exception.ServiceException;
import com.gcl.common.utils.DateUtils;
import com.gcl.common.utils.StringUtils;
import com.gcl.common.utils.bean.VerificationField;
import com.gcl.corporatewalletmanagement.domain.BusMerWalletInfo;
import com.gcl.corporatewalletmanagement.mapper.BusMerWalletInfoMapper;
import com.gcl.system.mapper.SysUserMapper;
import com.gcl.system.service.ISysConfigService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 对公钱包开立申请Service业务层处理
 *
 * @author yada
 * @date 2022-01-18
 */
@Service
public class BusMerWalletInfoServiceImpl implements IBusMerWalletInfoService {
    private static final Logger log = LoggerFactory.getLogger(BusMerWalletInfoServiceImpl.class);
    @Autowired
    private BusMerWalletInfoMapper busMerWalletInfoMapper;

    @Autowired
    private BusMerWalletInfoTempMapper busMerWalletInfoTempMapper;

    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    protected Validator validator;






    @Autowired
    private IBusMerWalletReturnInfoService busMerWalletReturnInfoService;

    @Autowired
    private IBusMerWalletDealService busMerWalletDealService;





    /**
     * 查询对公钱包开立申请
     *
     * @param id 对公钱包开立申请主键
     * @return 对公钱包开立申请
     */
    @Override
    public BusMerWalletInfo selectBusMerWalletInfoById(String id) {
        return busMerWalletInfoMapper.selectBusMerWalletInfoById(id);
    }

    /**
     * 查询对公钱包开立申请列表
     *
     * @param busMerWalletInfo 对公钱包开立申请
     * @return 对公钱包开立申请
     */
    @Override
    public List<BusMerWalletInfo> selectBusMerWalletInfoList(BusMerWalletInfo busMerWalletInfo) {
        return busMerWalletInfoMapper.selectBusMerWalletInfoList(busMerWalletInfo);
    }

    @Override
    public List<BusMerWalletInfo> selectBusMerWalletInfoListVo(BusMerWalletInfoVo busMerWalletInfo) {
        return busMerWalletInfoMapper.selectBusMerWalletInfoListVo(busMerWalletInfo);

    }

    @Override
    public List<BusMerWalletInfo> selectInitStatusBusMerWalletInfoList(BusMerWalletInfo busMerWalletInfo) {
        return busMerWalletInfoMapper.selectInitStatusBusMerWalletInfoList(busMerWalletInfo);
    }

    @Override
    public List<BusMerWalletInfo> selectExamineBusMerWalletInfoList(BusMerWalletInfo busMerWalletInfo) {
        return busMerWalletInfoMapper.selectExamineBusMerWalletInfoList(busMerWalletInfo);
    }

    /**
     * 新增对公钱包开立申请
     *
     * @param busMerWalletInfo 对公钱包开立申请
     * @return 结果
     */
    @Override
    public int insertBusMerWalletInfo(BusMerWalletInfo busMerWalletInfo) {
        return busMerWalletInfoMapper.insertBusMerWalletInfo(busMerWalletInfo);
    }

    /**
     * 修改对公钱包开立申请
     *
     * @param busMerWalletInfo 对公钱包开立申请
     * @return 结果
     */
    @Override
    public int updateBusMerWalletInfo(BusMerWalletInfo busMerWalletInfo) {
        busMerWalletInfo.setUpdateTime(DateUtils.getNowDate());
        return busMerWalletInfoMapper.updateBusMerWalletInfo(busMerWalletInfo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(BusMerWalletInfo busMerWalletInfo) {
        try {
            //更新数据
            //1.删除临时表中暂存的数据
            busMerWalletInfoTempMapper.deleteBusMerWalletInfoTempById(busMerWalletInfo.getId());
            //2.更新至正式表中
            if(DataStatuEnum.REVIEW_REJECTION.getStatus().equals(busMerWalletInfo.getDataStatus())){
                //解决审核被退回后再编辑需要将状态改为待审核而不是审核驳回的问题
                busMerWalletInfo.setDataStatus(DataStatuEnum.SUBMIT_REVIEW.getStatus());
            }
            if(DataStatuEnum.INITIAL_DATA.getStatus().equals(busMerWalletInfo.getDataStatus())){
                //初始数据->数据已确认
                busMerWalletInfo.setDataStatus(DataStatuEnum.DATA_CONFIRMED.getStatus());
            }
            //解决更新值为null的问题
            BusMerWalletInfo merWalletInfo = busMerWalletInfoMapper.selectBusMerWalletInfoById(busMerWalletInfo.getId());
            UpdateTool.copyNullProperties(merWalletInfo,busMerWalletInfo);
            busMerWalletInfoMapper.updateBusMerWalletInfo(busMerWalletInfo);
        }catch (Exception e){
            e.printStackTrace();
            return ResultStatus.FAIL;
        }
        return ResultStatus.SUCCESS;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateDataStatus(String id, DataStatuEnum dataStatusEnum) {
        BusMerWalletInfo merWalletInfo = busMerWalletInfoMapper.selectBusMerWalletInfoById(id);
        if(merWalletInfo != null){
            merWalletInfo.setDataStatus(dataStatusEnum.getStatus());
            try {
                merWalletInfo.setUpdateTime(new Date());
                busMerWalletInfoMapper.updateBusMerWalletInfo(merWalletInfo);
                return ResultStatus.SUCCESS;
            }catch (Exception e){
                e.printStackTrace();
                return ResultStatus.FAIL;
            }
        }
        return ResultStatus.FAIL;
    }

    /**
     * 批量删除对公钱包开立申请
     *
     * @param ids 需要删除的对公钱包开立申请主键
     * @return 结果
     */
    @Override
    public int deleteBusMerWalletInfoByIds(String[] ids) {
        return busMerWalletInfoMapper.deleteBusMerWalletInfoByIds(ids);
    }

    /**
     * 删除对公钱包开立申请信息
     *
     * @param id 对公钱包开立申请主键
     * @return 结果
     */
    @Override
    public int deleteBusMerWalletInfoById(String id) {
        return busMerWalletInfoMapper.deleteBusMerWalletInfoById(id);
    }

    /**
     * 导入企业数据(有重复信息不能导入，导入前校验，有错误所有信息都不能导入)
     *
     * @param merWalletInfos 企业数据列表
     * @return 结果
     */
    @Override
    public String importerWalletInfo(List<BusMerWalletInfo> merWalletInfos, boolean isStockInfoImport, String operName) {
        if (StringUtils.isNull(merWalletInfos) || merWalletInfos.size() == 0) {
            throw new ServiceException("导入企业数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        int lineNum = 1;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();

        Set<String> thisBatchCustNos = new HashSet<String>();

        //先校验
        for (BusMerWalletInfo merWalletInfo : merWalletInfos) {
            lineNum = lineNum + 1;


            //已存在就不进行字段校验

            /*****
             * 已存在字段校验会校验 businessLicenseNo（营业执照号码） 和 uniformSocialCreditCode（统一社会信用代码） 两个字段
             * 所以这两个字段不能再数据库中已存在
             * 注：这里连个字段为空 就会和为空的重复 是否需要做处理？
             */



            //校验客户号是否已存在(验证数据库和本批次是否存在重复的)
            if (busMerWalletInfoCustNoAlreadyExists(merWalletInfo.getCustNo()) || thisBatchCustNos.contains(merWalletInfo.getCustNo())) {
                failureNum++;
                failureMsg.append("<br/><b>第" + lineNum + "行：</b><br/>客户号 " + merWalletInfo.getCustNo() + " 已存在");
            } else {
                //2022-03-04 去除企业存在校验
                thisBatchCustNos.add(merWalletInfo.getCustNo());
                String errorString = VerificationField.verificationField(merWalletInfo);
                if (!errorString.isEmpty()) {
                    failureNum++;
                    failureMsg.append("<br/><b>第" + lineNum + "行:</b>" + errorString);
                }
            }

        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        } else {
            for (BusMerWalletInfo merWalletInfo : merWalletInfos) {
                BusMerWalletInfo busMerWalletInfo = importDataInit(merWalletInfo,isStockInfoImport);
                //导入数据插入数据库前进行处理
                processDataBeforInsert(busMerWalletInfo);
                //设置最后更新时间

                busMerWalletInfo.setCreateBy(operName);

                busMerWalletInfo.setUpdateTime(new Date());
                insertBusMerWalletInfo(busMerWalletInfo);
                successNum++;
            }
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }


    private void processDataBeforInsert(BusMerWalletInfo busMerWalletInfo ){
        //修复钱包名称不存在bug 公共钱包导入信息,如果钱包名称不存在，则使用【EntName】作为钱包名称
        //钱包名称为空 则使用EntName
        if(StringUtils.isBlank(busMerWalletInfo.getWalletName())){
            busMerWalletInfo.setWalletName(busMerWalletInfo.getEntName());
        }
    }

    /**
     * 根据统一社会信用代码和营业执照号码进行企业信息的唯一性校验
     *
     * @param busMerWalletInfo 统一社会信用代码和营业执照号码
     * @return 企业信息是否已经存在
     */
    @Override
    public boolean busMerWalletInfoAlreadyExists(BusMerWalletInfo busMerWalletInfo) {
        return busMerWalletInfoMapper.busMerWalletInfoCheck(busMerWalletInfo).size() > 0;
    }

    @Override
    public boolean busMerWalletInfoCustNoAlreadyExists(String custNo) {
        return busMerWalletInfoMapper.busMerWalletInfoCustNoAlreadyExists(custNo).size() > 0;
    }

    /**
     * 对数据导入模板中的数据进行初始化
     *
     * @param merWalletInfo 模板中的数据
     * @return 初始化后的数据
     */
    @Override
    public BusMerWalletInfo importDataInit(BusMerWalletInfo merWalletInfo, boolean isStockInfoImport) {
        String id = UUID.randomUUID().toString().replace("-", "");
        merWalletInfo.setId(id);
        if (isStockInfoImport) {
            merWalletInfo.setDataSources(DataSourcesEnum.STOCK_DATA.getStatus());
        } else {
            merWalletInfo.setDataSources(DataSourcesEnum.NEW_DATA.getStatus());
        }
        merWalletInfo.setDataStatus(DataStatuEnum.INITIAL_DATA.getStatus());
        //wjw 2022-03-09 初始化的时候不再设置核心交易结构号 使用编辑或开通页面提交的客户经理所属机构号
        //merWalletInfo.setTradingOrganizationNo(configService.selectConfigByKey("bus.mer.walletinfo.tradingorganizationno"));
        //merWalletInfo.setTradingOrganizationNo(BusMerWalletInfo.DEFAULT_TRADING_ORGANIZATION_NO);

        merWalletInfo.setLoginType(configService.selectConfigByKey("bus.mer.walletinfo.logintype"));
        merWalletInfo.setWalletType(configService.selectConfigByKey("bus.mer.walletinfo.wallettype"));
        merWalletInfo.setAccountBank(configService.selectConfigByKey("bus.mer.walletinfo.accountbank"));
        merWalletInfo.setOperatingOrganization(configService.selectConfigByKey("bus.mer.walletinfo.operatingorganization"));
        merWalletInfo.setAgencyCode(configService.selectConfigByKey("bus.mer.walletinfo.agencycode"));
        merWalletInfo.setMerchantLevel(configService.selectConfigByKey("bus.mer.walletinfo.merchantlevel"));
        merWalletInfo.setMerchantAttributes(configService.selectConfigByKey("bus.mer.walletinfo.merchantattributes"));
        merWalletInfo.setAccessId(configService.selectConfigByKey("bus.mer.walletinfo.accessid"));
        merWalletInfo.setSupAgreeWithholding(configService.selectConfigByKey("bus.mer.walletinfo.supagreewithholding"));
        merWalletInfo.setAccountType(configService.selectConfigByKey("bus.mer.walletinfo.accounttype"));

        merWalletInfo.setSettleCycleType(configService.selectConfigByKey("bus.mer.walletinfo.settlecycletype"));
        merWalletInfo.setReceivePaymentBankCode(configService.selectConfigByKey("bus.mer.walletinfo.receivepaymentbankcode"));
        merWalletInfo.setReceivingBankName(configService.selectConfigByKey("bus.mer.walletinfo.receivingbankname"));
        merWalletInfo.setSettleCycle(configService.selectConfigByKey("bus.mer.walletinfo.settlecycle"));
        merWalletInfo.setSettleTreatmentMethod(configService.selectConfigByKey("bus.mer.walletinfo.settletreatmentmethod"));
        merWalletInfo.setMerchantRegisterChannels(configService.selectConfigByKey("bus.mer.walletinfo.merchantregisterchannels"));


        //合同号： TODO 暂时使用随机数，规则确定后再重写
        // DCEP + 交易机构号 + YYYYMMDD + 7位顺序号(000000)
        if(StringUtils.isNotBlank(merWalletInfo.getTradingOrganizationNo())){
            merWalletInfo.setPaperContractNo(createPaperContractNo(merWalletInfo.getTradingOrganizationNo()));
        }else{

            //生成的合同号 是否使用 客户经理所属机构号？
            if(StringUtils.isNotBlank(merWalletInfo.getCustMngOrgNo())){
                merWalletInfo.setPaperContractNo(createPaperContractNo(merWalletInfo.getCustMngOrgNo()));
                merWalletInfo.setTradingOrganizationNo(merWalletInfo.getCustMngOrgNo());
            }else{
                merWalletInfo.setPaperContractNo(createPaperContractNo(BusMerWalletInfo.DEFAULT_TRADING_ORGANIZATION_NO));
            }


            //merWalletInfo.setPaperContractNo(createPaperContractNo(BusMerWalletInfo.DEFAULT_TRADING_ORGANIZATION_NO));

        }
        merWalletInfo.setContractEffectiveDate(DateUtil.getCurDate());
        merWalletInfo.setContractExpireDate(DateUtil.modifyMon(DateUtil.getCurDate(),12));


        return merWalletInfo;
    }

    //生成纸质合同号
    // DCEP + 交易机构号 + YYYYMMDD + 7位顺序号(000000)
    private String createPaperContractNo(String tradingOrganizationNo){

        Long seqPaperContractNo = busMerWalletInfoMapper.getSeqPaperContractNo();
        String no =  BusMerWalletInfo.PAPER_CONTRACT_NO_PREFIX + tradingOrganizationNo + DateFormatUtils.format(new Date(),"yyyyMMdd") + getNumberFormat(7,String.valueOf(seqPaperContractNo));
        //System.out.println("纸质合同号：" + no);
        return no;
    }

    protected String getNumberFormat(int len, String number) {
        BigDecimal num = new BigDecimal(number);
        return String.format("%0"+(len)+"d", num.longValue());
    }

    @Override
    public List<BusMerWalletInfo> selectApplyPageBusMerWalletInfoList(BusMerWalletInfo busMerWalletInfo) {
        return busMerWalletInfoMapper.selectApplyPageBusMerWalletInfoList(busMerWalletInfo);
    }

    @Override
    public boolean vertifyLoginName(String account) {
        return (busMerWalletInfoMapper.countAllByLoginName(account) > 0);    }

    @Override
    public boolean vertifyEditLoginName(String account) {
        return (busMerWalletInfoMapper.countAllByLoginName(account) >=2);    }


    @Override
    @Transactional
    public void auditPass(BusMerWalletInfoVo busMerWalletInfo) {
        String id = busMerWalletInfo.getId();
        //如果存在审核退回信息则删除
        List<BusMerWalletReturnInfo> merWalletReturnInfoList = busMerWalletReturnInfoService.findByCustomerInfoId(id);
        if(merWalletReturnInfoList.size()>0){
            busMerWalletReturnInfoService.deleteByCustomerInfoId(id);
        }

        /////////////保存审核反馈信息
        BusMerWalletReturnInfo returnInfo = new BusMerWalletReturnInfo();
        String uuid = UUID.randomUUID().toString().replace("-", "");

        returnInfo.setId(uuid);
        returnInfo.setCustomerInfoId(busMerWalletInfo.getId());
        returnInfo.setFeedbackInfor(busMerWalletInfo.getFeedbackInfo());
        returnInfo.setReviewDate(DateUtil.getCurDate());
        busMerWalletReturnInfoService.insertBusMerWalletReturnInfo(returnInfo);
        //end

        BusMerWalletInfo merWalletInfo = this.selectBusMerWalletInfoById(id);
        merWalletInfo.setDataStatus(DataStatuEnum.REVIEW_PASSED.getStatus());
        merWalletInfo.setExamineSuccessDate(DateUtil.getCurDate());
        //审核通过，将信息存入BPS处理表
        BusMerWalletDeal merWalletDeal = new BusMerWalletDeal();
        BeanUtils.copyProperties(merWalletInfo,merWalletDeal);
        BusMerWalletDeal exitDbDeal = busMerWalletDealService.selectBusMerWalletDealById(merWalletDeal.getId());
        if(exitDbDeal == null){
            busMerWalletDealService.insertBusMerWalletDeal(merWalletDeal);
        }else {
            busMerWalletDealService.updateBusMerWalletDeal(merWalletDeal);

        }
        merWalletInfo.setDealId(merWalletDeal.getId());
        this.updateBusMerWalletInfo(merWalletInfo);
    }
}
