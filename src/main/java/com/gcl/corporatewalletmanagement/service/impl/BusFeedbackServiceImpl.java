package com.gcl.corporatewalletmanagement.service.impl;

import java.util.List;
import com.gcl.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gcl.corporatewalletmanagement.mapper.BusFeedbackMapper;
import com.gcl.corporatewalletmanagement.domain.BusFeedback;
import com.gcl.corporatewalletmanagement.service.IBusFeedbackService;

/**
 * 意见反馈Service业务层处理
 * 
 * @author zhaihb
 * @date 2022-01-14
 */
@Service
public class BusFeedbackServiceImpl implements IBusFeedbackService 
{
    @Autowired
    private BusFeedbackMapper busFeedbackMapper;

    /**
     * 查询意见反馈
     * 
     * @param id 意见反馈主键
     * @return 意见反馈
     */
    @Override
    public BusFeedback selectBusFeedbackById(String id)
    {
        return busFeedbackMapper.selectBusFeedbackById(id);
    }

    /**
     * 查询意见反馈列表
     * 
     * @param busFeedback 意见反馈
     * @return 意见反馈
     */
    @Override
    public List<BusFeedback> selectBusFeedbackList(BusFeedback busFeedback)
    {
        return busFeedbackMapper.selectBusFeedbackList(busFeedback);
    }

    /**
     * 新增意见反馈
     * 
     * @param busFeedback 意见反馈
     * @return 结果
     */
    @Override
    public int insertBusFeedback(BusFeedback busFeedback)
    {
        busFeedback.setCreateTime(DateUtils.getNowDate());
        return busFeedbackMapper.insertBusFeedback(busFeedback);
    }

    /**
     * 修改意见反馈
     * 
     * @param busFeedback 意见反馈
     * @return 结果
     */
    @Override
    public int updateBusFeedback(BusFeedback busFeedback)
    {
        return busFeedbackMapper.updateBusFeedback(busFeedback);
    }

    /**
     * 批量删除意见反馈
     * 
     * @param ids 需要删除的意见反馈主键
     * @return 结果
     */
    @Override
    public int deleteBusFeedbackByIds(String[] ids)
    {
        return busFeedbackMapper.deleteBusFeedbackByIds(ids);
    }

    /**
     * 删除意见反馈信息
     * 
     * @param id 意见反馈主键
     * @return 结果
     */
    @Override
    public int deleteBusFeedbackById(String id)
    {
        return busFeedbackMapper.deleteBusFeedbackById(id);
    }
}
