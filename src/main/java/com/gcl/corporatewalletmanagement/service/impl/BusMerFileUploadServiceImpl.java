package com.gcl.corporatewalletmanagement.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gcl.corporatewalletmanagement.domain.BusMerFileUpload;
import com.gcl.corporatewalletmanagement.domain.BusMerFileUploadRecord;
import com.gcl.corporatewalletmanagement.mapper.BusMerFileUploadMapper;
import com.gcl.corporatewalletmanagement.mapper.BusMerFileUploadRecordMapper;
import com.gcl.corporatewalletmanagement.service.IBusMerFileUploadService;
import com.gcl.util.DateTimeFormatUtil;

/**
 * 企业资料信息Service业务层处理
 * 
 * @author yada
 * @date 2022-01-12
 */
@Service
public class BusMerFileUploadServiceImpl implements IBusMerFileUploadService 
{
    @Autowired
    private BusMerFileUploadMapper busMerFileUploadMapper;


    @Autowired
    private BusMerFileUploadRecordMapper busMerFileUploadRecordMapper;
    /**
     * 查询企业资料信息
     * 
     * @param id 企业资料信息主键
     * @return 企业资料信息
     */
    @Override
    public BusMerFileUpload selectBusMerFileUploadById(String id)
    {
        return busMerFileUploadMapper.selectBusMerFileUploadById(id);
    }

    /**
     * 查询企业资料信息列表
     * 
     * @param busMerFileUpload 企业资料信息
     * @return 企业资料信息
     */
    @Override
    public List<BusMerFileUpload> selectBusMerFileUploadList(BusMerFileUpload busMerFileUpload)
    {
        return busMerFileUploadMapper.selectBusMerFileUploadList(busMerFileUpload);
    }

    /**
     * 新增企业资料信息
     * 
     * @param busMerFileUpload 企业资料信息
     * @return 结果
     */
    @Override
    public int insertBusMerFileUpload(BusMerFileUpload busMerFileUpload)
    {
        return busMerFileUploadMapper.insertBusMerFileUpload(busMerFileUpload);
    }

    /**
     * 修改企业资料信息
     * 
     * @param busMerFileUpload 企业资料信息
     * @return 结果
     */
    @Override
    public int updateBusMerFileUpload(BusMerFileUpload busMerFileUpload)
    {
        return busMerFileUploadMapper.updateBusMerFileUpload(busMerFileUpload);
    }

    /**
     * 批量删除企业资料信息
     * 
     * @param ids 需要删除的企业资料信息主键
     * @return 结果
     */
    @Override
    public int deleteBusMerFileUploadByIds(String[] ids)
    {
        return busMerFileUploadMapper.deleteBusMerFileUploadByIds(ids);
    }

    /**
     * 删除企业资料信息信息
     * 
     * @param id 企业资料信息主键
     * @return 结果
     */
    @Override
    public int deleteBusMerFileUploadById(String id)
    {
        return busMerFileUploadMapper.deleteBusMerFileUploadById(id);
    }

    @Override
    public BusMerFileUpload findFileByoriginalFileNameAndMerchantId(String originalFileName, String merchantId) {
        Map param = new HashMap();
        param.put("originalFileName", originalFileName);
        param.put("merchantId", merchantId);
        return busMerFileUploadMapper.findByOriginalFileNameAndMerchantId(param);
    }

    @Override
    public List<BusMerFileUpload> findFileLoadByMerchantId(String id) {
        return busMerFileUploadMapper.findFileLoadByMerchantId(id);

    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public String deleteFile(String path, String fileid) {
        String result = "1";
        try {
            //删除文件信息
            BusMerFileUpload merchantsFileUpload = busMerFileUploadMapper.selectBusMerFileUploadById(fileid);
            String abPath = path + "/" + merchantsFileUpload.getRelativePath();
            File f = new File(abPath);
            f.delete();
            busMerFileUploadMapper.deleteBusMerFileUploadById(fileid);
            //日志
            BusMerFileUploadRecord merFileUploadRecord = new BusMerFileUploadRecord();
            BeanUtils.copyProperties(merchantsFileUpload, merFileUploadRecord);
            merFileUploadRecord.setDeletedTime(DateTimeFormatUtil.getHHMMSSTime());
            merFileUploadRecord.setDeletedDate(DateTimeFormatUtil.getYYYYMMDDDate());
			merFileUploadRecord.setFileDeleteTime(new Date());
			merFileUploadRecord.setUpdateTime(new Date());
			busMerFileUploadRecordMapper.insertBusMerFileUploadRecord(merFileUploadRecord);
        } catch (Exception e) {

            e.printStackTrace();
            result = "0";
        }
        return result;
    }

    @Override
    public void downloadFile(String path, String id, String filename, HttpServletResponse response) throws Exception {
        //服务器存放的绝对路径
        String abPath = path + "/" + id + "/" + filename;
        File file = new File(abPath);
        if (!file.exists()) {
            throw new Exception(abPath + "文件名不存在");
        }
        InputStream inputStream = new FileInputStream(abPath);
        OutputStream outputStream = response.getOutputStream();
        byte[] b = new byte[4096];
        int len;
        while ((len = inputStream.read(b)) > 0) {
            outputStream.write(b, 0, len);
        }
        inputStream.close();
    }


}
