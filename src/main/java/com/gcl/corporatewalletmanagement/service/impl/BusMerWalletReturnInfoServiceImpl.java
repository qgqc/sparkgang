package com.gcl.corporatewalletmanagement.service.impl;

import java.util.List;

import com.gcl.corporatewalletmanagement.domain.BusMerWalletReturnInfo;
import com.gcl.corporatewalletmanagement.mapper.BusMerWalletReturnInfoMapper;
import com.gcl.corporatewalletmanagement.service.IBusMerWalletReturnInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 商户开钱包退回信息Service业务层处理
 * 
 * @author yada
 * @date 2022-02-16
 */
@Service
public class BusMerWalletReturnInfoServiceImpl implements IBusMerWalletReturnInfoService
{
    @Autowired
    private BusMerWalletReturnInfoMapper busMerWalletReturnInfoMapper;

    /**
     * 查询商户开钱包退回信息
     * 
     * @param id 商户开钱包退回信息主键
     * @return 商户开钱包退回信息
     */
    @Override
    public BusMerWalletReturnInfo selectBusMerWalletReturnInfoById(String id)
    {
        return busMerWalletReturnInfoMapper.selectBusMerWalletReturnInfoById(id);
    }

    /**
     * 查询商户开钱包退回信息列表
     * 
     * @param busMerWalletReturnInfo 商户开钱包退回信息
     * @return 商户开钱包退回信息
     */
    @Override
    public List<BusMerWalletReturnInfo> selectBusMerWalletReturnInfoList(BusMerWalletReturnInfo busMerWalletReturnInfo)
    {
        return busMerWalletReturnInfoMapper.selectBusMerWalletReturnInfoList(busMerWalletReturnInfo);
    }

    /**
     * 新增商户开钱包退回信息
     * 
     * @param busMerWalletReturnInfo 商户开钱包退回信息
     * @return 结果
     */
    @Override
    public int insertBusMerWalletReturnInfo(BusMerWalletReturnInfo busMerWalletReturnInfo)
    {
        return busMerWalletReturnInfoMapper.insertBusMerWalletReturnInfo(busMerWalletReturnInfo);
    }

    /**
     * 修改商户开钱包退回信息
     * 
     * @param busMerWalletReturnInfo 商户开钱包退回信息
     * @return 结果
     */
    @Override
    public int updateBusMerWalletReturnInfo(BusMerWalletReturnInfo busMerWalletReturnInfo)
    {
        return busMerWalletReturnInfoMapper.updateBusMerWalletReturnInfo(busMerWalletReturnInfo);
    }

    /**
     * 批量删除商户开钱包退回信息
     * 
     * @param ids 需要删除的商户开钱包退回信息主键
     * @return 结果
     */
    @Override
    public int deleteBusMerWalletReturnInfoByIds(String[] ids)
    {
        return busMerWalletReturnInfoMapper.deleteBusMerWalletReturnInfoByIds(ids);
    }

    /**
     * 删除商户开钱包退回信息信息
     * 
     * @param id 商户开钱包退回信息主键
     * @return 结果
     */
    @Override
    public int deleteBusMerWalletReturnInfoById(String id)
    {
        return busMerWalletReturnInfoMapper.deleteBusMerWalletReturnInfoById(id);
    }

    @Override
    public List<BusMerWalletReturnInfo> findByCustomerInfoId(String customerInfoId) {
        return busMerWalletReturnInfoMapper.findByCustomerInfoId(customerInfoId);
    }

    @Override
    public void deleteByCustomerInfoId(String customerInfoId) {
        busMerWalletReturnInfoMapper.deleteByCustomerInfoId(customerInfoId);
    }
}
