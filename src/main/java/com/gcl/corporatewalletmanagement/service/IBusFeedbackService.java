package com.gcl.corporatewalletmanagement.service;

import java.util.List;
import com.gcl.corporatewalletmanagement.domain.BusFeedback;

/**
 * 意见反馈Service接口
 * 
 * @author zhaihb
 * @date 2022-01-14
 */
public interface IBusFeedbackService 
{
    /**
     * 查询意见反馈
     * 
     * @param id 意见反馈主键
     * @return 意见反馈
     */
    public BusFeedback selectBusFeedbackById(String id);

    /**
     * 查询意见反馈列表
     * 
     * @param busFeedback 意见反馈
     * @return 意见反馈集合
     */
    public List<BusFeedback> selectBusFeedbackList(BusFeedback busFeedback);

    /**
     * 新增意见反馈
     * 
     * @param busFeedback 意见反馈
     * @return 结果
     */
    public int insertBusFeedback(BusFeedback busFeedback);

    /**
     * 修改意见反馈
     * 
     * @param busFeedback 意见反馈
     * @return 结果
     */
    public int updateBusFeedback(BusFeedback busFeedback);

    /**
     * 批量删除意见反馈
     * 
     * @param ids 需要删除的意见反馈主键集合
     * @return 结果
     */
    public int deleteBusFeedbackByIds(String[] ids);

    /**
     * 删除意见反馈信息
     * 
     * @param id 意见反馈主键
     * @return 结果
     */
    public int deleteBusFeedbackById(String id);
}
