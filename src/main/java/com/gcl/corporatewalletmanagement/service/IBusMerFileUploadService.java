package com.gcl.corporatewalletmanagement.service;

import java.util.List;
import com.gcl.corporatewalletmanagement.domain.BusMerFileUpload;

import javax.servlet.http.HttpServletResponse;

/**
 * 企业资料信息Service接口
 * 
 * @author yada
 * @date 2022-01-12
 */
public interface IBusMerFileUploadService 
{
    /**
     * 查询企业资料信息
     * 
     * @param id 企业资料信息主键
     * @return 企业资料信息
     */
    public BusMerFileUpload selectBusMerFileUploadById(String id);

    /**
     * 查询企业资料信息列表
     * 
     * @param busMerFileUpload 企业资料信息
     * @return 企业资料信息集合
     */
    public List<BusMerFileUpload> selectBusMerFileUploadList(BusMerFileUpload busMerFileUpload);

    /**
     * 新增企业资料信息
     * 
     * @param busMerFileUpload 企业资料信息
     * @return 结果
     */
    public int insertBusMerFileUpload(BusMerFileUpload busMerFileUpload);

    /**
     * 修改企业资料信息
     * 
     * @param busMerFileUpload 企业资料信息
     * @return 结果
     */
    public int updateBusMerFileUpload(BusMerFileUpload busMerFileUpload);

    /**
     * 批量删除企业资料信息
     * 
     * @param ids 需要删除的企业资料信息主键集合
     * @return 结果
     */
    public int deleteBusMerFileUploadByIds(String[] ids);

    /**
     * 删除企业资料信息信息
     * 
     * @param id 企业资料信息主键
     * @return 结果
     */
    public int deleteBusMerFileUploadById(String id);



    BusMerFileUpload findFileByoriginalFileNameAndMerchantId(String originalFileName, String merchantId);

    List<BusMerFileUpload> findFileLoadByMerchantId(String id);

    String deleteFile(String path, String fileid);

    void downloadFile(String path, String id, String filename, HttpServletResponse response) throws Exception;

}
