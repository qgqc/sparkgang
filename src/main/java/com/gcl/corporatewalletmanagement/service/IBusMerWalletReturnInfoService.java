package com.gcl.corporatewalletmanagement.service;

import java.util.List;

import com.gcl.corporatewalletmanagement.domain.BusMerWalletReturnInfo;

/**
 * 商户开钱包退回信息Service接口
 * 
 * @author yada
 * @date 2022-02-16
 */
public interface IBusMerWalletReturnInfoService 
{
    /**
     * 查询商户开钱包退回信息
     * 
     * @param id 商户开钱包退回信息主键
     * @return 商户开钱包退回信息
     */
    public BusMerWalletReturnInfo selectBusMerWalletReturnInfoById(String id);

    /**
     * 查询商户开钱包退回信息列表
     * 
     * @param busMerWalletReturnInfo 商户开钱包退回信息
     * @return 商户开钱包退回信息集合
     */
    public List<BusMerWalletReturnInfo> selectBusMerWalletReturnInfoList(BusMerWalletReturnInfo busMerWalletReturnInfo);

    /**
     * 新增商户开钱包退回信息
     * 
     * @param busMerWalletReturnInfo 商户开钱包退回信息
     * @return 结果
     */
    public int insertBusMerWalletReturnInfo(BusMerWalletReturnInfo busMerWalletReturnInfo);

    /**
     * 修改商户开钱包退回信息
     * 
     * @param busMerWalletReturnInfo 商户开钱包退回信息
     * @return 结果
     */
    public int updateBusMerWalletReturnInfo(BusMerWalletReturnInfo busMerWalletReturnInfo);

    /**
     * 批量删除商户开钱包退回信息
     * 
     * @param ids 需要删除的商户开钱包退回信息主键集合
     * @return 结果
     */
    public int deleteBusMerWalletReturnInfoByIds(String[] ids);

    /**
     * 删除商户开钱包退回信息信息
     * 
     * @param id 商户开钱包退回信息主键
     * @return 结果
     */
    public int deleteBusMerWalletReturnInfoById(String id);

    List<BusMerWalletReturnInfo> findByCustomerInfoId(String id);

    void deleteByCustomerInfoId(String id);
}
