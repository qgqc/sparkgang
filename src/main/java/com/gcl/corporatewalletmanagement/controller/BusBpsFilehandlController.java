package com.gcl.corporatewalletmanagement.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.corporatewalletmanagement.service.IBusBpsFilehandlService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;


/**
 * BPS文件处理管理Controller
 * 
 * @author yada
 * @date 2022-01-13
 */
@RestController
@RequestMapping("/corporatewalletmanagement/filehandl")
public class BusBpsFilehandlController extends BaseController
{
    @Autowired
    private IBusBpsFilehandlService busBpsFilehandlService;

    /**
     * 查询BPS文件处理管理列表
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:filehandl:list')")
    @GetMapping("/list")
    public TableDataInfo list(String proNo,String sourceFile)
    {
        BusBpsFilehandl busBpsFilehandl = new BusBpsFilehandl(proNo,sourceFile);
        startPage();
        List<BusBpsFilehandl> list = busBpsFilehandlService.selectBusBpsFilehandlList(busBpsFilehandl);
        return getDataTable(list);
    }

    /**
     * 导出BPS文件处理管理列表
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:filehandl:export')")
    @Log(title = "BPS文件处理管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusBpsFilehandl busBpsFilehandl)
    {
        List<BusBpsFilehandl> list = busBpsFilehandlService.selectBusBpsFilehandlList(busBpsFilehandl);
        ExcelUtil<BusBpsFilehandl> util = new ExcelUtil<BusBpsFilehandl>(BusBpsFilehandl.class);
        util.exportExcel(response, list, "BPS文件处理管理数据");
    }

    /**
     * 获取BPS文件处理管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:filehandl:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(busBpsFilehandlService.selectBusBpsFilehandlById(id));
    }

    /**
     * 新增BPS文件处理管理
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:filehandl:add')")
    @Log(title = "BPS文件处理管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusBpsFilehandl busBpsFilehandl)
    {
        return toAjax(busBpsFilehandlService.insertBusBpsFilehandl(busBpsFilehandl));
    }

    /**
     * 修改BPS文件处理管理
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:filehandl:edit')")
    @Log(title = "BPS文件处理管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusBpsFilehandl busBpsFilehandl)
    {
        return toAjax(busBpsFilehandlService.updateBusBpsFilehandl(busBpsFilehandl));
    }

    /**
     * 删除BPS文件处理管理
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:filehandl:remove')")
    @Log(title = "BPS文件处理管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(busBpsFilehandlService.deleteBusBpsFilehandlByIds(ids));
    }
}
