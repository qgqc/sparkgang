package com.gcl.corporatewalletmanagement.controller;

import java.util.List;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcl.corporatewalletmanagement.domain.BusBpsLog;
import com.gcl.corporatewalletmanagement.service.IBusBpsLogService;

/**
 * BPS日志Controller
 * 
 * @author zhaihb
 * @date 2022-01-13
 */
@RestController
@RequestMapping("/corporatewalletmanagement/bpslog")
public class BusBpsLogController extends BaseController {
    @Autowired
    private IBusBpsLogService busBpsLogService;

    /**
     * 查询BPS日志列表
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:bpslog:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusBpsLog busBpsLog)
    {
        startPage();
        List<BusBpsLog> list = busBpsLogService.selectBusBpsLogList(busBpsLog);
        return getDataTable(list);
    }

    /**
     * 导出BPS日志列表
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:bpslog:export')")
    @Log(title = "BPS日志", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BusBpsLog busBpsLog)
    {
        List<BusBpsLog> list = busBpsLogService.selectBusBpsLogList(busBpsLog);
        ExcelUtil<BusBpsLog> util = new ExcelUtil<BusBpsLog>(BusBpsLog.class);
        return util.exportExcel(list, "BPS日志数据");
    }

    /**
     * 获取BPS日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:bpslog:query')")
    @GetMapping(value = "/{logId}")
    public AjaxResult getInfo(@PathVariable("logId") String logId)
    {
        return AjaxResult.success(busBpsLogService.selectBusBpsLogByLogId(logId));
    }

    /**
     * 新增BPS日志
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:bpslog:add')")
    @Log(title = "BPS日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusBpsLog busBpsLog)
    {
        return toAjax(busBpsLogService.insertBusBpsLog(busBpsLog));
    }

    /**
     * 修改BPS日志
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:bpslog:edit')")
    @Log(title = "BPS日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusBpsLog busBpsLog)
    {
        return toAjax(busBpsLogService.updateBusBpsLog(busBpsLog));
    }

    /**
     * 删除BPS日志
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:bpslog:remove')")
    @Log(title = "BPS日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{logIds}")
    public AjaxResult remove(@PathVariable String[] logIds)
    {
        return toAjax(busBpsLogService.deleteBusBpsLogByLogIds(logIds));
    }
}
