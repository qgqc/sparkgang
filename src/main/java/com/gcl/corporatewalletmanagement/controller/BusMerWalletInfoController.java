package com.gcl.corporatewalletmanagement.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.gcl.bps.constant.BpsTypeConstant;
import com.gcl.bps.service.IBusMerWalletDealService;
import com.gcl.bps.util.DateUtil;
import com.gcl.common.annotation.Log;
import com.gcl.common.constant.BusMerAndWalletConstants;
import com.gcl.common.constant.DataSourcesEnum;
import com.gcl.common.constant.DataStatuEnum;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.domain.entity.SysDept;
import com.gcl.common.core.domain.entity.SysRole;
import com.gcl.common.core.domain.entity.SysUser;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.DateUtils;
import com.gcl.common.utils.StringUtils;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;
import com.gcl.corporatewalletmanagement.domain.BusMerFileUpload;
import com.gcl.corporatewalletmanagement.domain.BusMerFileUploadRecord;
import com.gcl.corporatewalletmanagement.domain.BusMerWalletInfo;
import com.gcl.corporatewalletmanagement.domain.BusMerWalletReturnInfo;
import com.gcl.corporatewalletmanagement.domain.vo.BusMerFileUploadVo;
import com.gcl.corporatewalletmanagement.domain.vo.BusMerWalletInfoVo;
import com.gcl.corporatewalletmanagement.service.IBusBpsFilehandlService;
import com.gcl.corporatewalletmanagement.service.IBusMerFileUploadRecordService;
import com.gcl.corporatewalletmanagement.service.IBusMerFileUploadService;
import com.gcl.corporatewalletmanagement.service.IBusMerWalletInfoService;
import com.gcl.corporatewalletmanagement.service.IBusMerWalletReturnInfoService;
import com.gcl.system.service.ISysConfigService;
import com.gcl.system.service.ISysDeptService;
import com.gcl.system.service.ISysRoleService;
import com.gcl.system.service.ISysUserService;
import com.gcl.util.DateTimeFormatUtil;
import com.gcl.util.FilePortUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**
 * 对公钱包开立申请Controller
 *
 * @author yada
 * @date 2022-01-18
 */
@Api(value = "对公钱包开立申请",description = "对公钱包开立申请")
@RestController
@RequestMapping("/corporatewalletmanagement/busmerwalletinfo")
public class BusMerWalletInfoController extends BaseController
{


    //文件上传路径信息
    @Value("${file.upload.path}")
    private String path;
    @Autowired
    private IBusMerWalletInfoService busMerWalletInfoService;


    @Autowired
    private IBusMerFileUploadService busMerFileUploadService;

    @Autowired
    private IBusBpsFilehandlService busBpsFilehandlService;


    @Autowired
    private IBusMerFileUploadRecordService busMerFileUploadRecordService;


    @Autowired
    private IBusMerWalletReturnInfoService busMerWalletReturnInfoService;

    @Autowired
    private IBusMerWalletDealService busMerWalletDealService;


    @Autowired
private ISysUserService sysUserService;


    @Autowired
    private ISysDeptService sysDeptService;

    @Autowired
    private ISysRoleService sysRoleService;
    @Autowired
    private ISysConfigService configService;

    /**
     * 存量信息导入列表页
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:stocklist')")
    @GetMapping("/stocklist")
    public TableDataInfo stockList(BusMerWalletInfo busMerWalletInfo)
    {
        if(isCanSeeAllInfo()){
            busMerWalletInfo.setCanSeeAllInfo(BusMerAndWalletConstants.CAN_SEE_ALL_INFO_YES);
        }else{
            busMerWalletInfo.setCanSeeAllInfo(BusMerAndWalletConstants.CAN_SEE_ALL_INFO_NO);
            List<SysDept> canSeeDepts =  sysDeptService.getCurrentDeptAndAllSonDeptByCurrentLoginUserDept(getUsername());
            busMerWalletInfo.setDepts(canSeeDepts);
        }
        //List<String> currentDeptUserNames =   getCurrentDeptUserNames();

        startPage();
        busMerWalletInfo.setDataSources(DataSourcesEnum.STOCK_DATA.getStatus());
        List<BusMerWalletInfo> list = busMerWalletInfoService.selectInitStatusBusMerWalletInfoList(busMerWalletInfo);
        return getDataTable(list);
    }

    /**
     * 获取存量企业信息导入模板
     * @param response response
     */
    @PostMapping("/importTemplate")
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:stocklist')")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<BusMerWalletInfo> util = new ExcelUtil<BusMerWalletInfo>(BusMerWalletInfo.class);
        util.importTemplateExcel(response, "存量企业信息");
    }

    @Log(title = "存量企业信息导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:stocklist')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception {



        String operName = getUsername();
        ExcelUtil<BusMerWalletInfo> util = new ExcelUtil<BusMerWalletInfo>(BusMerWalletInfo.class);
        List<BusMerWalletInfo> busMerWalletInfoList = util.importExcel(file.getInputStream());
        String message = busMerWalletInfoService.importerWalletInfo(busMerWalletInfoList,true,operName);
        return AjaxResult.success(message);
    }

/*    //校验模板是否合规
    private boolean importDataModelIsStandard() throws ClassNotFoundException {


        Class busMerWalletInfoClass = Class.forName("com.yada.corporatewalletmanagement.domain.BusMerWalletInfo");
        busMerWalletInfoClass.getAnnotationsByType()

        return true;
    }*/

    /**
     * 新增信息导入列表页
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:newlyaddedlist')")
    @GetMapping("/newlyaddedlist")
    public TableDataInfo newlyAddedList(BusMerWalletInfo busMerWalletInfo)
    {
        if(isCanSeeAllInfo()){
            busMerWalletInfo.setCanSeeAllInfo(BusMerAndWalletConstants.CAN_SEE_ALL_INFO_YES);
        }else{
            busMerWalletInfo.setCanSeeAllInfo(BusMerAndWalletConstants.CAN_SEE_ALL_INFO_NO);
            List<SysDept> canSeeDepts =  sysDeptService.getCurrentDeptAndAllSonDeptByCurrentLoginUserDept(getUsername());
            busMerWalletInfo.setDepts(canSeeDepts);
        }

        startPage();
        busMerWalletInfo.setDataSources(DataSourcesEnum.NEW_DATA.getStatus());
        List<BusMerWalletInfo> list = busMerWalletInfoService.selectInitStatusBusMerWalletInfoList(busMerWalletInfo);
        return getDataTable(list);
    }

    /**
     * 获取新增企业信息导入模板
     * @param response response
     */
    @PostMapping("/importNewaddTemplate")
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:newlyaddedlist')")
    public void importNewaddTemplate(HttpServletResponse response)
    {
        ExcelUtil<BusMerWalletInfo> util = new ExcelUtil<BusMerWalletInfo>(BusMerWalletInfo.class);
        util.importTemplateExcel(response, "新增企业信息");
    }

    @Log(title = "新增企业信息导入", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:newlyaddedlist')")
    @PostMapping("/importNewAddData")
    public AjaxResult importNewaddData(MultipartFile file) throws Exception {
        String operName = getUsername();
        ExcelUtil<BusMerWalletInfo> util = new ExcelUtil<BusMerWalletInfo>(BusMerWalletInfo.class);
        List<BusMerWalletInfo> busMerWalletInfoList = util.importExcel(file.getInputStream());
        String message = busMerWalletInfoService.importerWalletInfo(busMerWalletInfoList,false,operName);
        return AjaxResult.success(message);
    }

    /**
     * 查询对公钱包开立申请列表
     * 公共接口不做权限限制
     */
    //@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusMerWalletInfo busMerWalletInfo)
    {
        startPage();
        List<BusMerWalletInfo> list = busMerWalletInfoService.selectBusMerWalletInfoList(busMerWalletInfo);
        return getDataTable(list);
    }


    @GetMapping("/listVo")
    public TableDataInfo listVo(BusMerWalletInfoVo busMerWalletInfoVo)
    {
        startPage();
        List<BusMerWalletInfo> list = busMerWalletInfoService.selectBusMerWalletInfoListVo(busMerWalletInfoVo);
        return getDataTable(list);
    }

    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:list')")
    @GetMapping("/applyList")
    public TableDataInfo applyList(BusMerWalletInfo busMerWalletInfo)
    {
        //bankBranchNo
        if(isCanSeeAllInfo()){
            busMerWalletInfo.setCanSeeAllInfo(BusMerAndWalletConstants.CAN_SEE_ALL_INFO_YES);
        }else{
            busMerWalletInfo.setCanSeeAllInfo(BusMerAndWalletConstants.CAN_SEE_ALL_INFO_NO);
            List<SysDept> canSeeDepts =  sysDeptService.getCurrentDeptAndAllSonDeptByCurrentLoginUserDept(getUsername());
            busMerWalletInfo.setDepts(canSeeDepts);
        }
        startPage();
        List<BusMerWalletInfo> list = busMerWalletInfoService.selectApplyPageBusMerWalletInfoList(busMerWalletInfo);
        return getDataTable(list);
    }


    /**
     * 查询审核列表
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:examinelist')")
    @GetMapping("/examinelist")
    public TableDataInfo examineList(BusMerWalletInfo busMerWalletInfo)
    {
        if(isCanSeeAllInfo()){
            busMerWalletInfo.setCanSeeAllInfo(BusMerAndWalletConstants.CAN_SEE_ALL_INFO_YES);
        }else{
            busMerWalletInfo.setCanSeeAllInfo(BusMerAndWalletConstants.CAN_SEE_ALL_INFO_NO);
            List<SysDept> canSeeDepts =  sysDeptService.getCurrentDeptAndAllSonDeptByCurrentLoginUserDept(getUsername());
            busMerWalletInfo.setDepts(canSeeDepts);
        }

        startPage();
        List<BusMerWalletInfo> list = busMerWalletInfoService.selectExamineBusMerWalletInfoList(busMerWalletInfo);
        return getDataTable(list);
    }

    /**
     * 导出对公钱包开立申请列表
     */
    //@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:export')")
    @Log(title = "对公钱包开立申请", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BusMerWalletInfo busMerWalletInfo)
    {
        List<BusMerWalletInfo> list = busMerWalletInfoService.selectBusMerWalletInfoList(busMerWalletInfo);
        ExcelUtil<BusMerWalletInfo> util = new ExcelUtil<BusMerWalletInfo>(BusMerWalletInfo.class);
        return util.exportExcel(list, "对公钱包开立申请数据");
    }

    /**
     * 获取对公钱包开立申请详细信息
     */
    @ApiOperation(value = "对公钱包开立申请信息-获取", notes = "对公钱包信息-获取")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "申请信息ID(以 /id 形式传参)", required = true, dataType = "String", paramType = "query")

    })
    //@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {


        //AjaxResult ajaxResult=AjaxResult.success(busMerWalletInfoService.selectBusMerWalletInfoById(id));
        BusMerWalletInfo busMerWalletInfo = busMerWalletInfoService.selectBusMerWalletInfoById(id);


        return AjaxResult.success(busMerWalletInfo);
    }


    /*****
     * 携带登录客户信息查询请求  为了不影响其他功能，只在需要携带改信息时请求改接口
     * 正常查询信息还是访问 getInfo 接口
     * @param id
     * @return
     */
    @GetMapping(value = "/withCustInfo/{id}")
    public AjaxResult getInfowithCustInfo(@PathVariable("id") String id)
    {


        //AjaxResult ajaxResult=AjaxResult.success(busMerWalletInfoService.selectBusMerWalletInfoById(id));
        BusMerWalletInfo busMerWalletInfo = busMerWalletInfoService.selectBusMerWalletInfoById(id);
        //由于编辑或开立时，需要将当前登录用户信息赋给客户经理信息
        setCustInfo(busMerWalletInfo);

        return AjaxResult.success(busMerWalletInfo);
    }



    @GetMapping(value = "/loginOrgInfo")
    public AjaxResult getInfowithCustInfo()
    {
        //System.out.println("loginOrgInfo");
        String loginUserName = getUsername();
        SysUser sysUser = sysUserService.selectUserByUserName(loginUserName);
        if(sysUser.getDeptId() != null) {
            SysDept dept = sysDeptService.selectDeptById(sysUser.getDeptId());
            if (dept != null) {
               return AjaxResult.success(dept);
            }
        }

        return AjaxResult.success(null);

    }


    /*******
     * 为开立信息中设置当前登录客户经理信息
     * @param busMerWalletInfo
     */
    private void setCustInfo(BusMerWalletInfo busMerWalletInfo){
        if(busMerWalletInfo != null){
            //如果客户经理信息 三个条目中有一个为空，则设置为当前登录用户信息
            if(StringUtils.isBlank(busMerWalletInfo.getCustMngOrgNo())
                    || StringUtils.isBlank(busMerWalletInfo.getCustMngEmpNum())
                    || StringUtils.isBlank(busMerWalletInfo.getAccountManager())
            ){
                //查询当前登录用户机构信息
                String loginUserName = getUsername();
                SysUser sysUser = sysUserService.selectUserByUserName(loginUserName);
                if(sysUser.getDeptId() != null){
                    SysDept dept = sysDeptService.selectDeptById(sysUser.getDeptId());
                    if(dept != null){
                        busMerWalletInfo.setAccountManager(sysUser.getNickName());
                        busMerWalletInfo.setCustMngOrgNo(dept.getDeptNo());
                        busMerWalletInfo.setCustMngEmpNum(sysUser.getUserName());
                    }

                }
            }
        }
    }
    /**
     * 新增对公钱包开立申请
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:add')")
    @Log(title = "对公钱包开立申请", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusMerWalletInfo busMerWalletInfo)
    {
        String id = UUID.randomUUID().toString().replace("-", "");
        busMerWalletInfo.setId(id);
        return toAjax(busMerWalletInfoService.insertBusMerWalletInfo(busMerWalletInfo));
    }

    /**
     * 修改对公钱包开立申请
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:edit')")
    @Log(title = "对公钱包开立申请", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusMerWalletInfo busMerWalletInfo)
    {
        busMerWalletInfo.setTradingOrganizationNo(busMerWalletInfo.getCustMngOrgNo());
        busMerWalletInfo.setTellerNo(busMerWalletInfo.getCustMngEmpNum());
        busMerWalletInfo.setOpenAccountName(busMerWalletInfo.getAccountName());
        //商户所属省市取开户行省市，前端不用显示。
        busMerWalletInfo.setProvinceCode(busMerWalletInfo.getAccountOpeningProvinceCode());
        busMerWalletInfo.setCityCode(busMerWalletInfo.getAccountOpeningCityCode());
        //删除“银行所属省、银行所属市”输入框，后台取值同“开户行省、开户行市”
        busMerWalletInfo.setProvinceOfBank(busMerWalletInfo.getAccountOpeningProvinceCode());
        busMerWalletInfo.setCityOfBank(busMerWalletInfo.getAccountOpeningCityCode());

        //营业执照期限，取 企业证件到期日
        busMerWalletInfo.setDurationBusinessLicense(busMerWalletInfo.getEntCertTypeExpireDate());
        //签约人设置为商家责任人
        //busMerWalletInfo.setSignatory(busMerWalletInfo.getMerchantResponsiblePerson());





        return toAjax(busMerWalletInfoService.updateBusMerWalletInfo(busMerWalletInfo));
    }

    /**
     * 删除对公钱包开立申请
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:remove')")
    @Log(title = "对公钱包开立申请", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(busMerWalletInfoService.deleteBusMerWalletInfoByIds(ids));
    }


    /******
     * 1 开钱包开商户
     * 2 只开钱包
     * 3 已开钱包开商户
     * 开钱包开商户 需要更新 transactionCode 用户操作交易码 根据该参数确定开通类型
     * 提交开通申请后，dataStatus更新为2（已确认状态）
     * 前台页面显示  编辑 打印申请表  上传资料 按钮三个按钮功能
     *
     * @param busMerWalletInfo
     * @return
     */
    @ApiOperation(value = "[开钱包开商户][只开钱包][只开商户]接口", notes = "开钱包开商户 只开钱包 只开商户 接口")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "busMerWalletInfo", value = "申请信息", required = true, dataType = "String", paramType = "query"),
    })
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:open')")
    @Log(title = "对公钱包开立申请", businessType = BusinessType.UPDATE)
    @PostMapping("/open")
    public AjaxResult open(@RequestBody  BusMerWalletInfo busMerWalletInfo)
    {
        //System.out.println(busMerWalletInfo);
        busMerWalletInfo.setTradingOrganizationNo(busMerWalletInfo.getCustMngOrgNo());
        busMerWalletInfo.setTellerNo(busMerWalletInfo.getCustMngEmpNum());
        busMerWalletInfo.setUpdateTime(new Date());
        //商户所属省市取开户行省市，前端不用显示。
        busMerWalletInfo.setProvinceCode(busMerWalletInfo.getAccountOpeningProvinceCode());
        busMerWalletInfo.setCityCode(busMerWalletInfo.getAccountOpeningCityCode());

        //删除“银行所属省、银行所属市”输入框，后台取值同“开户行省、开户行市”
        busMerWalletInfo.setProvinceOfBank(busMerWalletInfo.getAccountOpeningProvinceCode());
        busMerWalletInfo.setCityOfBank(busMerWalletInfo.getAccountOpeningCityCode());

        //营业执照期限，取 企业证件到期日
        busMerWalletInfo.setDurationBusinessLicense(busMerWalletInfo.getEntCertTypeExpireDate());


        //开户名称设置为账户名称
        busMerWalletInfo.setOpenAccountName(busMerWalletInfo.getAccountName());
        //签约人设置为商家责任人
        //busMerWalletInfo.setSignatory(busMerWalletInfo.getMerchantResponsiblePerson());


        return toAjax(busMerWalletInfoService.update(busMerWalletInfo));
        //return toAjax(1);
    }


    /******
     * 提交审核
     * @param busMerWalletInfo.id  提交审核数据ID
     * @return
     */
    @ApiOperation(value = "提交审核", notes = "提交审核")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "申请信息ID", required = true, dataType = "String", paramType = "query"),
    })
    //@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:submitreview')")
    @Log(title = "对公钱包开立申请", businessType = BusinessType.UPDATE)
    @PostMapping("/submitReview")
    public AjaxResult submitReview(@RequestBody  BusMerWalletInfo busMerWalletInfo)
    {
        //System.out.println("id:" + busMerWalletInfo.getId());
        busMerWalletInfoService.updateDataStatus(busMerWalletInfo.getId(), DataStatuEnum.SUBMIT_REVIEW);
        return AjaxResult.success("执行成功");
        //return AjaxResult.error("提交失败");
    }


    /*****
     * 对公钱包信息审核-通过
     * @param id  提交审核数据ID
     * @return
     */
    @ApiOperation(value = "对公钱包信息审核-通过", notes = "对公钱包信息审核-通过")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "申请信息ID", required = true, dataType = "String", paramType = "query"),
    })
    //@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:review')")
    @Log(title = "对公钱包开立申请", businessType = BusinessType.UPDATE)
    @PostMapping("/agreeInf")
    public AjaxResult agreeInf(@RequestBody BusMerWalletInfoVo busMerWalletInfo)
    {
        busMerWalletInfo.setUpdateTime(new Date());

        busMerWalletInfoService.auditPass(busMerWalletInfo);
        return AjaxResult.success("审核完成");



    }



    /******
     * 对公钱包信息审核-驳回
     * @param id  提交审核数据ID
     * @return
     */
    @ApiOperation(value = "对公钱包信息审核-驳回", notes = "对公钱包信息审核-驳回")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "申请信息ID", required = true, dataType = "String", paramType = "query"),
            @ApiImplicitParam(name = "feedbackInfo", value = "驳回反馈信息", required = true, dataType = "String", paramType = "query"),

    })
    //@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:review')")
    @Log(title = "对公钱包开立申请", businessType = BusinessType.UPDATE)
    @PostMapping("/reject")
    public AjaxResult reject(@RequestBody  BusMerWalletInfoVo busMerWalletInfoVo)
    {

        //System.out.println(busMerWalletInfoVo);
        //先检测该商户信息是否已经有退回信息，有的话就删除
        List<BusMerWalletReturnInfo> merWalletReturnInfoList = busMerWalletReturnInfoService.findByCustomerInfoId(busMerWalletInfoVo.getId());
        if(merWalletReturnInfoList.size()>0){
            busMerWalletReturnInfoService.deleteByCustomerInfoId(busMerWalletInfoVo.getId());
        }
        BusMerWalletReturnInfo returnInfo = new BusMerWalletReturnInfo();
        String uuid = UUID.randomUUID().toString().replace("-", "");

        returnInfo.setId(uuid);
        returnInfo.setCustomerInfoId(busMerWalletInfoVo.getId());
        returnInfo.setFeedbackInfor(busMerWalletInfoVo.getFeedbackInfo());
        returnInfo.setReviewDate(DateUtil.getCurDate());

        busMerWalletInfoVo.setUpdateTime(new Date());
        busMerWalletReturnInfoService.insertBusMerWalletReturnInfo(returnInfo);
        busMerWalletInfoService.updateDataStatus(busMerWalletInfoVo.getId(),DataStatuEnum.REVIEW_REJECTION);
        return AjaxResult.success("审核拒绝完成");
    }

    /*  *//******
 * 打印开立申请表
 * @param id 信息ID
 * @return
 *//*
    @ApiOperation("获取打印开立申请表信息")
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:printApplyFrom')")
    @PostMapping("/getPrintApplyFormInfo")
    public AjaxResult print_application_forms(String id) {
        BusMerWalletInfo printForm = busMerWalletInfoService.selectBusMerWalletInfoById(id);
        if(printForm == null){
            return AjaxResult.error("查询不到符合条件的信息");
        }
        return AjaxResult.success(printForm);
    }
*/


@ApiOperation("上传申请资料")
@ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "申请信息ID", required = true, dataType = "String", paramType = "query"),
})
@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:uploadApplyFile')")
@PostMapping("/uploadApplyFile")
public AjaxResult upload(MultipartHttpServletRequest request, String id) {
    //System.out.println("-------------id:" + id);
    Map<String, Object> json = new HashMap<String, Object>();
    try {
        MultiValueMap<String, MultipartFile> map = request.getMultiFileMap();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("text", "plain", Charset.forName("UTF-8")));
        List<MultipartFile> list = map.get("file");// 获取到文件的列表
        File localpath = new File(path + "/" + id);
        if (!localpath.exists() || null == localpath) {
            localpath.mkdirs();
        }
        BusMerWalletInfo printForm = busMerWalletInfoService.selectBusMerWalletInfoById(id);


        for (MultipartFile mFile : list) {
            String uuid = UUID.randomUUID().toString().replace("-", "");
            //获取文件名称
            String originalFileName = mFile.getOriginalFilename();
            String fileRelativePath = id + "/" + originalFileName;
            byte[] bytes = mFile.getBytes();
            String filePath = path + "/" + fileRelativePath;

            BusMerFileUpload merchantsFileUpload = new BusMerFileUpload();
            BusMerFileUploadRecord merchantsFileUploadRecord = new BusMerFileUploadRecord();


            merchantsFileUploadRecord.setId(uuid);
            merchantsFileUpload.setId(uuid);
            //商户id
            merchantsFileUpload.setMerchantId(id);
            //原始文件名
            merchantsFileUpload.setOriginalFielName(originalFileName);

            //文件路径
            merchantsFileUpload.setRelativePath(fileRelativePath);
            //创建时间
            merchantsFileUpload.setCreatedTime(DateTimeFormatUtil.getHHMMSSTime());
            //创建日期
            merchantsFileUpload.setCreatedDate(DateTimeFormatUtil.getYYYYMMDDDate());
            //下载URL    /downLoadFile/{id}/{filename}
            merchantsFileUpload.setDownloadUrl("/corporatewalletmanagement/busmerwalletinfo/downLoadFile" + "?id=" + id + "&filename=" + originalFileName);
            //删除URL
            merchantsFileUpload.setDeleteUrl("/corporatewalletmanagement/busmerwalletinfo/deleteFileLoad" + "?fileid=" + uuid);
            //大小
            merchantsFileUpload.setFileSize(String.valueOf(mFile.getSize()));
            if (printForm != null) {
                //企业名称
                merchantsFileUpload.setEntName(printForm.getEntName());
                //客户经理姓名
                merchantsFileUpload.setAccountManagerName(printForm.getAccountManager());
                merchantsFileUploadRecord.setEntName(printForm.getEntName());
                merchantsFileUploadRecord.setAccountManagerName(printForm.getAccountManager());
            }
            //上传资料日志表记录
            merchantsFileUploadRecord.setMerchantId(id);
            merchantsFileUploadRecord.setOriginalFielName(originalFileName);
            merchantsFileUploadRecord.setRelativePath(fileRelativePath);
            merchantsFileUploadRecord.setCreatedTime(DateTimeFormatUtil.getHHMMSSTime());
            merchantsFileUploadRecord.setCreatedDate(DateTimeFormatUtil.getYYYYMMDDDate());
				merchantsFileUploadRecord.setFileCreateTime(new Date());
            BusMerFileUpload merFileUpload = busMerFileUploadService.findFileByoriginalFileNameAndMerchantId(originalFileName, id);
            if (merFileUpload != null) {
                merchantsFileUpload.setId(merFileUpload.getId());
                //更新时间
                merchantsFileUpload.setUpdatedTime(DateTimeFormatUtil.getHHMMSSTime());
                //更新日期
                merchantsFileUpload.setUpdatedDate(DateTimeFormatUtil.getYYYYMMDDDate());
					merchantsFileUpload.setUpdateTime(new Date());
                //上传资料日志表记录
                merchantsFileUploadRecord.setUpdatedTime(DateTimeFormatUtil.getHHMMSSTime());
                merchantsFileUploadRecord.setUpdatedDate(DateTimeFormatUtil.getYYYYMMDDDate());
					merchantsFileUploadRecord.setFileUpdateTime(new Date());
                //merchantsOpenWalletsInBatchesService.deleteFileLoad(merchantsFileUpload.getId());
                busMerFileUploadService.updateBusMerFileUpload(merchantsFileUpload);
            }else{
                busMerFileUploadService.insertBusMerFileUpload(merchantsFileUpload);

            }
            //保存日志表
				merchantsFileUploadRecord.setUpdateTime(new Date());
            busMerFileUploadRecordService.insertBusMerFileUploadRecord(merchantsFileUploadRecord);
            //修改状态
            busMerWalletInfoService.updateDataStatus(id, DataStatuEnum.DATA_UPLOADED);
            List<BusMerFileUpload> fileUploads = busMerFileUploadService.findFileLoadByMerchantId(id);
            json.put("fileLoad", fileUploads);
            FileOutputStream fos = new FileOutputStream(new File(filePath)); //写出到文件
            fos.write(bytes);
            fos.flush();
            fos.close();
        }
    } catch (Exception e) {
        e.printStackTrace();
        return AjaxResult.error();
    }
    return AjaxResult.success(json);
}



    /**
     * 删除文件数据
     **/
    @ApiOperation("删除文件数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "busMerWalletInfo", value = "搜索参数", required = true, dataType = "String", paramType = "query"),
    })
    @RequestMapping(value = "/deleteFileLoad", method = RequestMethod.POST)
    @ResponseBody
    public String deleteFileLoad(@RequestBody BusMerFileUploadVo busMerFileUploadVo) {
        String fileId = busMerFileUploadVo.getFileId();
        return  busMerFileUploadService.deleteFile(path,fileId);
    }

    @ApiOperation("下载文件数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "", required = true, dataType = "String", paramType = "query"),
    })
    @PostMapping(value = "/downLoadFile")
    public void downloadFile(String id, String filename, HttpServletResponse response)  {
        try {
            busMerFileUploadService.downloadFile(path,id,filename,response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }







    @ApiOperation("获取BPS-DAT文件名")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "申请信息ID", required = true, dataType = "String", paramType = "query"),
    })
    @PostMapping(value = "/getDATFileName")
    public AjaxResult getDATFileName(BusMerWalletInfo busMerWalletInfo)  {

        BusMerWalletInfo info = busMerWalletInfoService.selectBusMerWalletInfoById(busMerWalletInfo.getId());
        if(info != null){

            String headFileId = info.getHeadId();
            if(StringUtils.isNotBlank(headFileId)){
                //存在headFileId，表示DAT文件已生成
                BusBpsFilehandl head = busBpsFilehandlService.selectBusBpsFilehandlById(headFileId);
                return AjaxResult.success(head.getSourceFile());
            }
        }
        return AjaxResult.error();

    }

    @ApiOperation("下载BPS-DAT文件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "申请信息ID", required = true, dataType = "String", paramType = "query"),
    })
    @PostMapping(value = "/downloadDATFile")
    public void downloadDATFile(BusMerWalletInfo busMerWalletInfo, HttpServletResponse response) throws Exception {

        BusMerWalletInfo info = busMerWalletInfoService.selectBusMerWalletInfoById(busMerWalletInfo.getId());
        if(info != null){
            String headFileId = info.getHeadId();
            if(StringUtils.isNotBlank(headFileId)){
                    //存在headFileId，表示DAT文件已生成
               busBpsFilehandlService.downloadDATFile(headFileId, BpsTypeConstant.CWFS_TYPE,response);
            }
        }

    }

    @ApiOperation("下载BPS-DAT文件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fileHandleId", value = "文件信息ID", required = true, dataType = "String", paramType = "query"),
    })
    @PostMapping(value = "/downloadDATFileByFileHandleId")
    public void downloadDATFileFileHandleId(BusBpsFilehandl busBpsFilehandl, HttpServletResponse response) throws Exception {
        //存在headFileId，表示DAT文件已生成
        busBpsFilehandlService.downloadDATFile(busBpsFilehandl.getId(), BpsTypeConstant.CWFS_TYPE,response);

    }




    @ApiOperation("获取已上传文件列表(根据申请信息ID)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "申请信息ID", required = true, dataType = "String", paramType = "query"),
    })
    @PostMapping(value = "/getFileListById")
    public AjaxResult getFileListById(@RequestBody  BusMerWalletInfo busMerWalletInfo)  {

        List<BusMerFileUpload> files =  busMerFileUploadService.findFileLoadByMerchantId(busMerWalletInfo.getId());
        return AjaxResult.success(files);
    }


    /**
     * 登录名称唯一性校验
     */
    @ApiOperation("登录名称唯一性校验")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "loginName", value = "登录名称", required = true, dataType = "String", paramType = "query"),
    })
    //该接口功能为开通功能的子功能，有开通权限即可访问该接口
    //@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:open')")
    @PostMapping("/vertifyLoginname")
    @ResponseBody
    public boolean vertifyLoginname(@RequestBody BusMerWalletInfo busMerWalletInfo) {
        boolean result =  busMerWalletInfoService.vertifyLoginName(busMerWalletInfo.getLoginName());
        return result;
    }


    @ApiOperation("登录名称唯一性校验")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "loginName", value = "登录名称", required = true, dataType = "String", paramType = "query"),
    })
    //该接口功能为开通功能的子功能，有开通权限即可访问该接口
    //@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:open')")
    @PostMapping("/vertifyEditLoginname")
    @ResponseBody
    public boolean vertifyEditLoginname(@RequestBody BusMerWalletInfo busMerWalletInfo) {
        boolean result =  busMerWalletInfoService.vertifyEditLoginName(busMerWalletInfo.getLoginName());
        return result;
    }

    @ApiOperation("查询对公钱包开立申请报表列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "busMerWalletInfo", value = "搜索参数", required = true, dataType = "String", paramType = "query"),
    })
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:busmerwalletreportlist')")
    @GetMapping("/reportList")
    public TableDataInfo merOpenWalForm(BusMerWalletInfoVo busMerWalletInfo) {

        if(isCanSeeAllInfo()){
            busMerWalletInfo.setCanSeeAllInfo(BusMerAndWalletConstants.CAN_SEE_ALL_INFO_YES);
        }else{
            busMerWalletInfo.setCanSeeAllInfo(BusMerAndWalletConstants.CAN_SEE_ALL_INFO_NO);
            List<SysDept> canSeeDepts =  sysDeptService.getCurrentDeptAndAllSonDeptByCurrentLoginUserDept(getUsername());
            busMerWalletInfo.setDepts(canSeeDepts);
        }

        if(busMerWalletInfo.getUpdateTimeRange() != null){
            //System.out.println("time:" + busMerWalletInfo.getUpdateTimeRange()[0] + busMerWalletInfo.getUpdateTimeRange()[1]);
            busMerWalletInfo.setUpdateTimeStart(DateUtils.parseDate(busMerWalletInfo.getUpdateTimeRange()[0] + " 00:00:00") );
            busMerWalletInfo.setUpdateTimeEnd(DateUtils.parseDate(busMerWalletInfo.getUpdateTimeRange()[1] + " 24:00:00"));
        }
        startPage();
        List<BusMerWalletInfo> list = busMerWalletInfoService.selectBusMerWalletInfoListVo(busMerWalletInfo);
        return getDataTable(list);
    }




    /**
     * 方法描述：导出对公钱包开立报表  EXCEL表格
     *
     **/
    @ApiOperation("导出申请报表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "busMerWalletInfo", value = "搜索参数", required = true, dataType = "String", paramType = "query"),
    })
    //@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:busmerwalletinfo:busmerwalletreportlist')")
    @ResponseBody
    @PostMapping("/exportReportExcel")
    public void exportExcel(HttpServletResponse response,@ModelAttribute BusMerWalletInfoVo busMerWalletInfo){

        if(busMerWalletInfo.getUpdateTimeRange() != null){
            //System.out.println("time:" + busMerWalletInfo.getUpdateTimeRange()[0] + busMerWalletInfo.getUpdateTimeRange()[1]);
            busMerWalletInfo.setUpdateTimeStart(DateUtils.parseDate(busMerWalletInfo.getUpdateTimeRange()[0] + " 00:00:00") );
            busMerWalletInfo.setUpdateTimeEnd(DateUtils.parseDate(busMerWalletInfo.getUpdateTimeRange()[1] + " 24:00:00"));
        }
        //导出的表格名称
        String title = "商户开立报表";
        //表中第一行表头字段
        String [] headers ={"客户号","企业名称","行业类型","企业证件类型","企业证件号码","企业联系人姓名","企业联系人手机号","企业联系人证件类型",
                "开户账号","机构号","开户行名称",

                "外部商户号","钱包ID","商家号","交易类型"};
        //从数据库查询出来的结果集
        List<BusMerWalletInfo> pProjectSalaryList = busMerWalletInfoService.selectBusMerWalletInfoListVo(busMerWalletInfo);

        //具体需要写入excel需要的那些字段，这些字段从PprojectSalary类中拿，也就是上面的实际数据结果集的泛型
        List<String> list = Arrays.asList("custNo","entName","industryType","entCertType","entCertNo","entCustContName","entCustContPhone","entCustContCertType",

                "account","bankBranchNo","bankBranchName",
                "externalMerchantNo","walletId","merchantId","transactionCode"
                );
        try {
            FilePortUtil.exportExcel(response,title,headers,pProjectSalaryList,list);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /*********
     * 获取当前用户所属部门下 所有用户
     * @return
     */
    private List<String> getCurrentDeptUserNames(){

        Long deptId = getDeptId();
        String userName = getUsername();
        //获取该deptId的用户(应该包括本用户)
        List<String> dataPermissionUsers = new ArrayList<>();
        if(deptId == null){
            if(SysUser.DEFAULT_ADMIN_USERNAME.equals(userName)){
                //管理员
                dataPermissionUsers.add(SysUser.DEFAULT_ADMIN_USERNAME);
            }else{
                //其他未分配部门的用户
            }
        }else{
            List<SysUser> users = sysUserService.selectUserByDeptId(deptId);
            for(SysUser user : users){
                dataPermissionUsers.add(user.getUserName());
            }
        }
        return dataPermissionUsers;

    }


    /*****
     * 能看所有信息吗
     * 如果用户没有所属机构  不包含机构号  就显示全部
     * 如果用户包含机构，如果是 网点经办角色 根据机构显示  否则(其他角色) 显示全部
     * 支持这里 需要添加四个角色对应参数和开关
     * @return
     */
    private boolean isCanSeeAllInfo() {
        String isOpenDataSplit =  configService.selectConfigByKey("bus.isopen.data.split");
        if(org.apache.commons.lang3.StringUtils.isBlank(isOpenDataSplit)){
            //没配置数据隔离
            return true;
        }else{
            String closeDataSplit = "0";

            if(closeDataSplit.equals(isOpenDataSplit)){
                //配置关闭数据隔离
                return true;
            }
        }



        String loginUserName = getUsername();
        SysUser sysUser = sysUserService.selectUserByUserName(loginUserName);
        if (sysUser.getDeptId() != null) {
            SysDept dept = sysDeptService.selectDeptById(sysUser.getDeptId());
            if (dept != null) {
                //如果 只是 网点经办角色 根据机构显示  否则(其他角色) 显示全部
                //两个参数配置有一个没加 数据隔离不起作用
                String handleRoleIdStr =  configService.selectConfigByKey("role.network.bank.handle.code");
                String recheckRoleIdStr =  configService.selectConfigByKey("role.network.bank.recheck.code");

                if(StringUtils.isBlank(handleRoleIdStr) || StringUtils.isBlank(recheckRoleIdStr)){
                    return true;
                }else{
                    List<SysRole> roles = sysRoleService.selectOnlyRolesByUserId(sysUser.getUserId());
                    if(CollectionUtils.isNotEmpty(roles)){
                        if(roles.size() == 1 &&
                                (handleRoleIdStr.equals(String.valueOf(roles.get(0).getRoleId())) ||
                                recheckRoleIdStr.equals(String.valueOf(roles.get(0).getRoleId())))
                                ){
                            //用户只包含经办角色
                            return false;
                        }else {
                            return true;
                        }
                    }else{
                        //如果用户 有部门 无角色(尚未分配)全显示
                        return true;

                    }

                }

            } else {
                //如果当前用户不存在机构  则 只能看所有没有机构的数据
                return true;
            }
        }
        return true;
    }



}
