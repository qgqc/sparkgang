package com.gcl.corporatewalletmanagement.controller;

import java.util.List;
import java.util.UUID;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.corporatewalletmanagement.domain.BusMerFileUploadRecord;
import com.gcl.corporatewalletmanagement.service.IBusMerFileUploadRecordService;

/**
 * 资料上传日志Controller
 *
 * @author zhaihb
 * @date 2022-01-14
 */
@RestController
@RequestMapping("/corporatewalletmanagement/enterprisedatalog")
public class BusMerFileUploadRecordController extends BaseController
{
    @Autowired
    private IBusMerFileUploadRecordService busMerFileUploadRecordService;

    /**
     * 查询资料上传日志列表
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:enterprisedatalog:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusMerFileUploadRecord busMerFileUploadRecord)
    {
        startPage();
        String id = UUID.randomUUID().toString().replace("-", "");
        busMerFileUploadRecord.setId(id);
        List<BusMerFileUploadRecord> list = busMerFileUploadRecordService.selectBusMerFileUploadRecordList(busMerFileUploadRecord);
        return getDataTable(list);
    }

    /**
     * 导出资料上传日志列表
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:enterprisedatalog:export')")
    @Log(title = "资料上传日志", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BusMerFileUploadRecord busMerFileUploadRecord)
    {
        List<BusMerFileUploadRecord> list = busMerFileUploadRecordService.selectBusMerFileUploadRecordList(busMerFileUploadRecord);
        ExcelUtil<BusMerFileUploadRecord> util = new ExcelUtil<BusMerFileUploadRecord>(BusMerFileUploadRecord.class);
        return util.exportExcel(list, "资料上传日志数据");
    }

    /**
     * 获取资料上传日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:enterprisedatalog:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(busMerFileUploadRecordService.selectBusMerFileUploadRecordById(id));
    }

    /**
     * 新增资料上传日志
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:enterprisedatalog:add')")
    @Log(title = "资料上传日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusMerFileUploadRecord busMerFileUploadRecord)
    {

        return toAjax(busMerFileUploadRecordService.insertBusMerFileUploadRecord(busMerFileUploadRecord));
    }

    /**
     * 修改资料上传日志
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:enterprisedatalog:edit')")
    @Log(title = "资料上传日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusMerFileUploadRecord busMerFileUploadRecord)
    {
        return toAjax(busMerFileUploadRecordService.updateBusMerFileUploadRecord(busMerFileUploadRecord));
    }

    /**
     * 删除资料上传日志
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:enterprisedatalog:remove')")
    @Log(title = "资料上传日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(busMerFileUploadRecordService.deleteBusMerFileUploadRecordByIds(ids));
    }
}
