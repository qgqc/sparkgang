package com.gcl.corporatewalletmanagement.controller;

import java.util.List;
import java.util.UUID;

import com.gcl.common.core.domain.entity.SysDept;
import com.gcl.common.core.domain.entity.SysUser;
import com.gcl.system.service.ISysDeptService;
import com.gcl.system.service.ISysUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.corporatewalletmanagement.domain.BusFeedback;
import com.gcl.corporatewalletmanagement.service.IBusFeedbackService;

/**
 * 意见反馈Controller
 * 
 * @author zhaihb
 * @date 2022-01-14
 */
@RestController
@RequestMapping("/corporatewalletmanagement/feedback")
public class BusFeedbackController extends BaseController
{
    @Autowired
    private IBusFeedbackService busFeedbackService;
    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private ISysDeptService sysDeptService;
    /**
     * 查询意见反馈列表
     */
    //@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:feedback:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusFeedback busFeedback)
    {
        startPage();
        List<BusFeedback> list = busFeedbackService.selectBusFeedbackList(busFeedback);
        return getDataTable(list);
    }

    /**
     * 导出意见反馈列表
     */
    //@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:feedback:export')")
    @Log(title = "意见反馈", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BusFeedback busFeedback)
    {
        List<BusFeedback> list = busFeedbackService.selectBusFeedbackList(busFeedback);
        ExcelUtil<BusFeedback> util = new ExcelUtil<BusFeedback>(BusFeedback.class);
        return util.exportExcel(list, "意见反馈数据");
    }

    /**
     * 获取意见反馈详细信息
     */
    //@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:feedback:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(busFeedbackService.selectBusFeedbackById(id));
    }

    /**
     * 新增意见反馈
     */
    //@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:feedback:add')")
    @Log(title = "意见反馈", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusFeedback busFeedback)
    {
        String id = UUID.randomUUID().toString().replace("-", "");
        busFeedback.setId(id);
        busFeedback.setLoginName(getUsername());
        SysUser logUser = getLoginUser().getUser();
        if(logUser != null){
            busFeedback.setUserId(logUser.getUserId());
            busFeedback.setUserName(logUser.getNickName());
        }


        String orgId = getDeptNo();
        busFeedback.setOrgId(StringUtils.isNotBlank(orgId)?orgId:"0");



        return toAjax(busFeedbackService.insertBusFeedback(busFeedback));
    }

    /**
     * 修改意见反馈
     */
    //@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:feedback:edit')")
    @Log(title = "意见反馈", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusFeedback busFeedback)
    {
        return toAjax(busFeedbackService.updateBusFeedback(busFeedback));
    }

    /**
     * 删除意见反馈
     */
    //@PreAuthorize("@ss.hasPermi('corporatewalletmanagement:feedback:remove')")
    @Log(title = "意见反馈", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(busFeedbackService.deleteBusFeedbackByIds(ids));
    }


    @GetMapping("/getLoginUserInfo")
    public AjaxResult getLoginUserInfo()
    {
        SysUser loginUser = getLoginUser().getUser();
        return AjaxResult.success(loginUser);

    }

    public String getDeptNo()
    {
        String loginUserName = getUsername();
        SysUser sysUser = sysUserService.selectUserByUserName(loginUserName);
        if (sysUser.getDeptId() != null) {
            SysDept dept = sysDeptService.selectDeptById(sysUser.getDeptId());
            if (dept != null) {
                return dept.getDeptNo();
            }
        }

        return null;
    }
}
