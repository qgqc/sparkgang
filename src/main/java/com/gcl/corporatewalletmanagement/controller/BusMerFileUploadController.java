package com.gcl.corporatewalletmanagement.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.corporatewalletmanagement.domain.BusMerFileUpload;
import com.gcl.corporatewalletmanagement.service.IBusMerFileUploadService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 企业资料信息Controller
 * 
 * @author yada
 * @date 2022-01-12
 */
@RestController
@RequestMapping("/corporatewalletmanagement/enterprisefile")
public class BusMerFileUploadController extends BaseController
{
    @Autowired
    private IBusMerFileUploadService busMerFileUploadService;

    /**
     * 查询企业资料信息列表
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:enterprisefile:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusMerFileUpload busMerFileUpload)
    {
        startPage();
        List<BusMerFileUpload> list = busMerFileUploadService.selectBusMerFileUploadList(busMerFileUpload);
        return getDataTable(list);
    }

    /**
     * 导出企业资料信息列表
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:enterprisefile:export')")
    @Log(title = "企业资料信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, BusMerFileUpload busMerFileUpload)
    {
        List<BusMerFileUpload> list = busMerFileUploadService.selectBusMerFileUploadList(busMerFileUpload);
        ExcelUtil<BusMerFileUpload> util = new ExcelUtil<BusMerFileUpload>(BusMerFileUpload.class);
        util.exportExcel(response, list, "企业资料信息数据");
    }

    /**
     * 获取企业资料信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:enterprisefile:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(busMerFileUploadService.selectBusMerFileUploadById(id));
    }

    /**
     * 新增企业资料信息
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:enterprisefile:add')")
    @Log(title = "企业资料信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusMerFileUpload busMerFileUpload)
    {
        return toAjax(busMerFileUploadService.insertBusMerFileUpload(busMerFileUpload));
    }

    /**
     * 修改企业资料信息
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:enterprisefile:edit')")
    @Log(title = "企业资料信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusMerFileUpload busMerFileUpload)
    {
        return toAjax(busMerFileUploadService.updateBusMerFileUpload(busMerFileUpload));
    }

    /**
     * 删除企业资料信息
     */
    @PreAuthorize("@ss.hasPermi('corporatewalletmanagement:enterprisefile:remove')")
    @Log(title = "企业资料信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(busMerFileUploadService.deleteBusMerFileUploadByIds(ids));
    }
}
