package com.gcl.corporatewalletmanagement.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.OutputStream;

@Api(value = "文件下载",description = "文件下载")
@RestController
@RequestMapping("/templates/download")
public class TemplatesDownloadController {

    private static final String TEMPLATES_PATH = "templates/";

    /*******
     * fileType =1 中国银行股份有限公司试点阶段对公钱包服务协议
     * fileType =2 法定代表人授权书
     * @param fileType
     * @param response
     */
    @ApiOperation("下载协议或授权书文件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fileType", value = "", required = true, dataType = "Integer", paramType = "query"),
    })
    @PostMapping(value = "/downLoadProtocolOrAuthorizationLetterFile")
    public void downLoadProtocolFile(Integer fileType, HttpServletResponse response)  {
        try {
            Integer protocolFileType = 1;
            Integer authorizationLetterFileType = 2;
            if(fileType == null || (!fileType.equals(protocolFileType) && !fileType.equals(authorizationLetterFileType))){
                throw new Exception("fileType参数异常");
            }
            ClassPathResource classPathResource = null;

            if(protocolFileType.equals(fileType)){
                classPathResource = new ClassPathResource(TEMPLATES_PATH + "中国银行股份有限公司试点阶段对公钱包服务协议.docx");

            }else if(authorizationLetterFileType.equals(fileType)){
                classPathResource = new ClassPathResource(TEMPLATES_PATH + "法定代表人授权书.docx");
            }
            InputStream inputStream =classPathResource.getInputStream();
            OutputStream outputStream = response.getOutputStream();
            byte[] b = new byte[4096];
            int len;
            while ((len = inputStream.read(b)) > 0) {
                outputStream.write(b, 0, len);
            }
            inputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    /*******
     * fileType =1 存量客户数据批量导入模板.xlsx
     * fileType =2 新增客户数据批量导入模板.xlsx
     * @param fileType
     * @param response
     */
    @ApiOperation("下载Excel文件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fileType", value = "", required = true, dataType = "Integer", paramType = "query"),
    })
    @PostMapping(value = "/downLoadExcelFile")
    public void downLoadExcelFile(Integer fileType, HttpServletResponse response)  {
        try {
            Integer stockExcelModelFileType = 1;
            Integer newExcelModelFileType = 2;
            if(fileType == null || (!fileType.equals(stockExcelModelFileType) && !fileType.equals(newExcelModelFileType))){
                throw new Exception("fileType参数异常");
            }
            ClassPathResource classPathResource = null;

            if(stockExcelModelFileType.equals(fileType)){
                classPathResource = new ClassPathResource(TEMPLATES_PATH + "存量客户数据批量导入模板.xlsx");

            }else if(newExcelModelFileType.equals(fileType)){
                classPathResource = new ClassPathResource(TEMPLATES_PATH + "新增客户数据批量导入模板.xlsx");
            }
            InputStream inputStream =classPathResource.getInputStream();
            OutputStream outputStream = response.getOutputStream();
            byte[] b = new byte[4096];
            int len;
            while ((len = inputStream.read(b)) > 0) {
                outputStream.write(b, 0, len);
            }
            inputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /******
     * 根据文件名适用templates下文件
     * @param fileName
     * @param response
     */
    @ApiOperation("下载templates下文件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "fileName", value = "", required = true, dataType = "Integer", paramType = "query"),
    })
    @PostMapping(value = "/downLoadFileByFileName")
    public void downLoadFileByFileName(Integer fileName, HttpServletResponse response)  {
        try {
            ClassPathResource classPathResource = new ClassPathResource(TEMPLATES_PATH  + fileName);
            InputStream inputStream =classPathResource.getInputStream();
            OutputStream outputStream = response.getOutputStream();
            byte[] b = new byte[4096];
            int len;
            while ((len = inputStream.read(b)) > 0) {
                outputStream.write(b, 0, len);
            }
            inputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
