package com.gcl.corporatewalletmanagement.mapper;

import java.util.List;
import java.util.Map;

import com.gcl.corporatewalletmanagement.domain.BusMerFileUpload;

/**
 * 企业资料信息Mapper接口
 * 
 * @author yada
 * @date 2022-01-12
 */
public interface BusMerFileUploadMapper 
{
    /**
     * 查询企业资料信息
     * 
     * @param id 企业资料信息主键
     * @return 企业资料信息
     */
    public BusMerFileUpload selectBusMerFileUploadById(String id);

    /**
     * 查询企业资料信息列表
     * 
     * @param busMerFileUpload 企业资料信息
     * @return 企业资料信息集合
     */
    public List<BusMerFileUpload> selectBusMerFileUploadList(BusMerFileUpload busMerFileUpload);

    /**
     * 新增企业资料信息
     * 
     * @param busMerFileUpload 企业资料信息
     * @return 结果
     */
    public int insertBusMerFileUpload(BusMerFileUpload busMerFileUpload);

    /**
     * 修改企业资料信息
     * 
     * @param busMerFileUpload 企业资料信息
     * @return 结果
     */
    public int updateBusMerFileUpload(BusMerFileUpload busMerFileUpload);

    /**
     * 删除企业资料信息
     * 
     * @param id 企业资料信息主键
     * @return 结果
     */
    public int deleteBusMerFileUploadById(String id);

    /**
     * 批量删除企业资料信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusMerFileUploadByIds(String[] ids);

    BusMerFileUpload findByOriginalFileNameAndMerchantId(Map param);

    List<BusMerFileUpload> findFileLoadByMerchantId(String merchantId);
}
