package com.gcl.corporatewalletmanagement.mapper;

import java.util.List;
import com.gcl.corporatewalletmanagement.domain.BusBpsLog;

/**
 * BPS日志Mapper接口
 * 
 * @author zhaihb
 * @date 2022-01-13
 */
public interface BusBpsLogMapper 
{
    /**
     * 查询BPS日志
     * 
     * @param logId BPS日志主键
     * @return BPS日志
     */
    public BusBpsLog selectBusBpsLogByLogId(String logId);

    /**
     * 查询BPS日志列表
     * 
     * @param busBpsLog BPS日志
     * @return BPS日志集合
     */
    public List<BusBpsLog> selectBusBpsLogList(BusBpsLog busBpsLog);

    /**
     * 新增BPS日志
     * 
     * @param busBpsLog BPS日志
     * @return 结果
     */
    public int insertBusBpsLog(BusBpsLog busBpsLog);

    /**
     * 修改BPS日志
     * 
     * @param busBpsLog BPS日志
     * @return 结果
     */
    public int updateBusBpsLog(BusBpsLog busBpsLog);

    /**
     * 删除BPS日志
     * 
     * @param logId BPS日志主键
     * @return 结果
     */
    public int deleteBusBpsLogByLogId(String logId);

    /**
     * 批量删除BPS日志
     * 
     * @param logIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusBpsLogByLogIds(String[] logIds);



    void deleteByDate(String date);

    void saveAll(List<BusBpsLog> item);
}
