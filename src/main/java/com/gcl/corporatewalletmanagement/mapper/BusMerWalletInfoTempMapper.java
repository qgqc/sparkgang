package com.gcl.corporatewalletmanagement.mapper;

import com.gcl.corporatewalletmanagement.domain.BusMerWalletInfoTemp;

import java.util.List;


/**
 * 商户钱包临时Mapper接口
 * 
 * @author wjw
 * @date 2022-02-13
 */
public interface BusMerWalletInfoTempMapper 
{
    /**
     * 查询商户钱包临时
     * 
     * @param id 商户钱包临时主键
     * @return 商户钱包临时
     */
    public BusMerWalletInfoTemp selectBusMerWalletInfoTempById(String id);

    /**
     * 查询商户钱包临时列表
     * 
     * @param busMerWalletInfoTemp 商户钱包临时
     * @return 商户钱包临时集合
     */
    public List<BusMerWalletInfoTemp> selectBusMerWalletInfoTempList(BusMerWalletInfoTemp busMerWalletInfoTemp);

    /**
     * 新增商户钱包临时
     * 
     * @param busMerWalletInfoTemp 商户钱包临时
     * @return 结果
     */
    public int insertBusMerWalletInfoTemp(BusMerWalletInfoTemp busMerWalletInfoTemp);

    /**
     * 修改商户钱包临时
     * 
     * @param busMerWalletInfoTemp 商户钱包临时
     * @return 结果
     */
    public int updateBusMerWalletInfoTemp(BusMerWalletInfoTemp busMerWalletInfoTemp);

    /**
     * 删除商户钱包临时
     * 
     * @param id 商户钱包临时主键
     * @return 结果
     */
    public int deleteBusMerWalletInfoTempById(String id);

    /**
     * 批量删除商户钱包临时
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusMerWalletInfoTempByIds(String[] ids);
}
