package com.gcl.corporatewalletmanagement.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;

/**
 * BPS文件处理管理Mapper接口
 * 
 * @author yada
 * @date 2022-01-13
 */
public interface BusBpsFilehandlMapper 
{
    /**
     * 查询BPS文件处理管理
     * 
     * @param id BPS文件处理管理主键
     * @return BPS文件处理管理
     */
    public BusBpsFilehandl selectBusBpsFilehandlById(String id);

    /**
     * 查询BPS文件处理管理列表
     * 
     * @param busBpsFilehandl BPS文件处理管理
     * @return BPS文件处理管理集合
     */
    public List<BusBpsFilehandl> selectBusBpsFilehandlList(BusBpsFilehandl busBpsFilehandl);

    /**
     * 新增BPS文件处理管理
     * 
     * @param busBpsFilehandl BPS文件处理管理
     * @return 结果
     */
    public int insertBusBpsFilehandl(BusBpsFilehandl busBpsFilehandl);

    /**
     * 修改BPS文件处理管理
     * 
     * @param busBpsFilehandl BPS文件处理管理
     * @return 结果
     */
    public int updateBusBpsFilehandl(BusBpsFilehandl busBpsFilehandl);

    /**
     * 删除BPS文件处理管理
     * 
     * @param id BPS文件处理管理主键
     * @return 结果
     */
    public int deleteBusBpsFilehandlById(String id);

    /**
     * 批量删除BPS文件处理管理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusBpsFilehandlByIds(String[] ids);


    BusBpsFilehandl findByOrgAndAccountDateAndTellerNo(Map map);

    void updateById(Map map);

    ArrayList<BusBpsFilehandl> findByStatuDownFile(Map map);

    ArrayList<BusBpsFilehandl> findByStatuUpFile(Map map);

    List<BusBpsFilehandl> selectByType(String type);


    ArrayList<BusBpsFilehandl> selectReadyClearDATBusBpsFilehandlByDate(String clearDate);

    ArrayList<BusBpsFilehandl> selectReadyClearERTBusBpsFilehandlByDate(String clearDate);
}
