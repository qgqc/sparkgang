package com.gcl.corporatewalletmanagement.mapper;

import java.util.List;

import com.gcl.corporatewalletmanagement.domain.BusMerWalletReturnInfo;

/**
 * 商户开钱包退回信息Mapper接口
 * 
 * @author wjw
 * @date 2022-02-16
 */
public interface BusMerWalletReturnInfoMapper 
{
    /**
     * 查询商户开钱包退回信息
     * 
     * @param id 商户开钱包退回信息主键
     * @return 商户开钱包退回信息
     */
    public BusMerWalletReturnInfo selectBusMerWalletReturnInfoById(String id);

    /**
     * 查询商户开钱包退回信息列表
     * 
     * @param busMerWalletReturnInfo 商户开钱包退回信息
     * @return 商户开钱包退回信息集合
     */
    public List<BusMerWalletReturnInfo> selectBusMerWalletReturnInfoList(BusMerWalletReturnInfo busMerWalletReturnInfo);

    /**
     * 新增商户开钱包退回信息
     * 
     * @param busMerWalletReturnInfo 商户开钱包退回信息
     * @return 结果
     */
    public int insertBusMerWalletReturnInfo(BusMerWalletReturnInfo busMerWalletReturnInfo);

    /**
     * 修改商户开钱包退回信息
     * 
     * @param busMerWalletReturnInfo 商户开钱包退回信息
     * @return 结果
     */
    public int updateBusMerWalletReturnInfo(BusMerWalletReturnInfo busMerWalletReturnInfo);

    /**
     * 删除商户开钱包退回信息
     * 
     * @param id 商户开钱包退回信息主键
     * @return 结果
     */
    public int deleteBusMerWalletReturnInfoById(String id);

    /**
     * 批量删除商户开钱包退回信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusMerWalletReturnInfoByIds(String[] ids);

    List<BusMerWalletReturnInfo> findByCustomerInfoId(String customerInfoId);

    void deleteByCustomerInfoId(String customerInfoId);
}
