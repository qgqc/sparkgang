package com.gcl.corporatewalletmanagement.mapper;

import java.util.List;
import java.util.Map;

import com.gcl.corporatewalletmanagement.domain.BusMerWalletInfo;
import com.gcl.corporatewalletmanagement.domain.vo.BusMerWalletInfoVo;

/**
 * 对公钱包开立申请Mapper接口
 *
 * @author yada
 * @date 2022-01-18
 */
public interface BusMerWalletInfoMapper
{
    /**
     * 查询对公钱包开立申请
     *
     * @param id 对公钱包开立申请主键
     * @return 对公钱包开立申请
     */
    public BusMerWalletInfo selectBusMerWalletInfoById(String id);

    /**
     * 查询对公钱包开立申请列表
     *
     * @param busMerWalletInfo 对公钱包开立申请
     * @return 对公钱包开立申请集合
     */
    public List<BusMerWalletInfo> selectBusMerWalletInfoList(BusMerWalletInfo busMerWalletInfo);

    /**
     * 新增对公钱包开立申请
     *
     * @param busMerWalletInfo 对公钱包开立申请
     * @return 结果
     */
    public int insertBusMerWalletInfo(BusMerWalletInfo busMerWalletInfo);

    /**
     * 修改对公钱包开立申请
     *
     * @param busMerWalletInfo 对公钱包开立申请
     * @return 结果
     */
    public int updateBusMerWalletInfo(BusMerWalletInfo busMerWalletInfo);

    /**
     * 删除对公钱包开立申请
     *
     * @param id 对公钱包开立申请主键
     * @return 结果
     */
    public int deleteBusMerWalletInfoById(String id);

    /**
     * 批量删除对公钱包开立申请
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusMerWalletInfoByIds(String[] ids);

    /**
     * 根据统一社会信用代码和营业执照号码查询企业信息
     * @param busMerWalletInfo 统一社会信用代码和营业执照号码
     * @return 企业信息
     */
    List<BusMerWalletInfo> busMerWalletInfoCheck(BusMerWalletInfo busMerWalletInfo);


    void updatefileBatchNoByStatus(Map map);


    void updateBatchById(List<BusMerWalletInfo> list);

    void updateStatusByHeadId(Map map);

    void updateBatchBysequenceNumberAndheadId(List<BusMerWalletInfo> list);

    List<BusMerWalletInfo> selectExamineBusMerWalletInfoList(BusMerWalletInfo busMerWalletInfo);

    List<BusMerWalletInfo> selectInitStatusBusMerWalletInfoList(BusMerWalletInfo busMerWalletInfo);

    List<BusMerWalletInfo> selectApplyPageBusMerWalletInfoList(BusMerWalletInfo busMerWalletInfo);

    int countAllByLoginName(String account);

    List<BusMerWalletInfo> busMerWalletInfoCustNoAlreadyExists(String custNo);

    Long getSeqPaperContractNo();


    List<BusMerWalletInfo> selectBusMerWalletInfoListVo(BusMerWalletInfoVo busMerWalletInfo);
}
