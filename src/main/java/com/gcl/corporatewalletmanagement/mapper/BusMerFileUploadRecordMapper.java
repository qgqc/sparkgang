package com.gcl.corporatewalletmanagement.mapper;

import java.util.List;
import com.gcl.corporatewalletmanagement.domain.BusMerFileUploadRecord;

/**
 * 资料上传日志Mapper接口
 *
 * @author zhaihb
 * @date 2022-01-14
 */
public interface BusMerFileUploadRecordMapper
{
    /**
     * 查询资料上传日志
     *
     * @param id 资料上传日志主键
     * @return 资料上传日志
     */
    public BusMerFileUploadRecord selectBusMerFileUploadRecordById(String id);

    /**
     * 查询资料上传日志列表
     *
     * @param busMerFileUploadRecord 资料上传日志
     * @return 资料上传日志集合
     */
    public List<BusMerFileUploadRecord> selectBusMerFileUploadRecordList(BusMerFileUploadRecord busMerFileUploadRecord);

    /**
     * 新增资料上传日志
     *
     * @param busMerFileUploadRecord 资料上传日志
     * @return 结果
     */
    public int insertBusMerFileUploadRecord(BusMerFileUploadRecord busMerFileUploadRecord);

    /**
     * 修改资料上传日志
     *
     * @param busMerFileUploadRecord 资料上传日志
     * @return 结果
     */
    public int updateBusMerFileUploadRecord(BusMerFileUploadRecord busMerFileUploadRecord);

    /**
     * 删除资料上传日志
     *
     * @param id 资料上传日志主键
     * @return 结果
     */
    public int deleteBusMerFileUploadRecordById(String id);

    /**
     * 批量删除资料上传日志
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusMerFileUploadRecordByIds(String[] ids);
}
