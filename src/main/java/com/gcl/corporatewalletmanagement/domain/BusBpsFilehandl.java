package com.gcl.corporatewalletmanagement.domain;

import com.gcl.bps.domain.Base;
import com.gcl.bps.util.DateUtil;
import com.gcl.common.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.beans.Transient;
import java.io.UnsupportedEncodingException;
import java.util.Objects;
import java.util.UUID;


/**
 * BPS文件处理管理对象 bus_bps_filehandl
 * 原TBFileHead,切换到该系统后改了当前名字
 *
 * @author yada
 * @date 2022-01-13
 */
public class BusBpsFilehandl extends Base {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;

    /**
     * 上传日期
     */
    @Excel(name = "上传日期")
    private String accountDate;

    /**
     * 批次号
     */
    @Excel(name = "批次号")
    private int batchNo;

    /**
     * 文件创建日期
     */
    @Excel(name = "文件创建日期")
    private String createDate;

    /**
     * 文件明细条数
     */
    @Excel(name = "文件明细条数")
    private int detialCount;

    /**
     * 回盘明细
     */
    @Excel(name = "回盘明细")
    private String msg;

    /**
     * 执行步骤  文件 0待生成，  1已生成，2已上传，3已回盘更新
     */
    @Excel(name = "执行步骤  文件 0待生成，  1已生成，2已上传，3已回盘更新")
    private String proNo;

    /**
     * 文件回盘日期
     */
    @Excel(name = "文件回盘日期")
    private String returnDate;

    /**
     * 源文件名称
     */
    @Excel(name = "源文件名称")
    private String sourceFile;

    /**
     * 执行步骤的执行结果 1成功，2失败
     */
    @Excel(name = "执行步骤的执行结果 1成功，2失败")
    private String statu;

    /**
     * 柜员号
     */
    @Excel(name = "柜员号")
    private String tellerNo;

    /**
     * 终端号
     */
    @Excel(name = "终端号")
    private String terminalNo;

    /**
     * 交易机构号
     */
    @Excel(name = "交易机构号")
    private String tranOrgId;

    /**
     * 业务类型
     */
    @Excel(name = "业务类型")
    private String type;


    public BusBpsFilehandl() {
        super();
    }


    public BusBpsFilehandl(String proNo, String sourceFile) {
        this.proNo = proNo;
        this.sourceFile = sourceFile;
    }


    public BusBpsFilehandl(String accountDate, int batchNo, int detialCount, String type, String tranOrgId,
                           String tellerNo, String sourceFile) {
        this.id = UUID.randomUUID().toString().replace("-", "");
        this.accountDate = accountDate;
        this.batchNo = batchNo;
        this.sourceFile = String.format(sourceFile, type, tranOrgId, getNumberFormat(3, Integer.toString(batchNo)), accountDate.substring(4, 8));
        this.detialCount = detialCount;
        this.type = type;
        this.tranOrgId = tranOrgId;
        this.tellerNo = tellerNo;
        this.terminalNo = "001";
        this.statu = "1";
        this.proNo = "0";
        this.createDate = DateUtil.getCurDateTime();

    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setAccountDate(String accountDate) {
        this.accountDate = accountDate;
    }

    public String getAccountDate() {
        return accountDate;
    }

    public void setBatchNo(int batchNo) {
        this.batchNo = batchNo;
    }

    public int getBatchNo() {
        return batchNo;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setDetialCount(int detialCount) {
        this.detialCount = detialCount;
    }

    public int getDetialCount() {
        return detialCount;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setProNo(String proNo) {
        this.proNo = proNo;
    }

    public String getProNo() {
        return proNo;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setSourceFile(String sourceFile) {
        this.sourceFile = sourceFile;
    }

    public String getSourceFile() {
        return sourceFile;
    }

    public void setStatu(String statu) {
        this.statu = statu;
    }

    public String getStatu() {
        return statu;
    }

    public void setTellerNo(String tellerNo) {
        this.tellerNo = tellerNo;
    }

    public String getTellerNo() {
        return tellerNo;
    }

    public void setTerminalNo(String terminalNo) {
        this.terminalNo = terminalNo;
    }

    public String getTerminalNo() {
        return terminalNo;
    }

    public void setTranOrgId(String tranOrgId) {
        this.tranOrgId = tranOrgId;
    }

    public String getTranOrgId() {
        return tranOrgId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }


    /**
     * @Description: 获取文件的头, cwfs, ppfs的文件头格式一致可以复用
     * @Param: type（cwfs,ppfs）
     * @return:
     * @Author: shaowu.ni
     * @Date:
     */

    @Transient
    public String getHeadString(String type) throws UnsupportedEncodingException {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("H000000000001");
        stringBuffer.append(getOneVsSpace(50, this.sourceFile));
        stringBuffer.append(tranOrgId);
        stringBuffer.append("CNY");
        stringBuffer.append(getNumberFormat(12, Integer.toString(detialCount)));
        stringBuffer.append(getNumberFormat(17, "0"));
        stringBuffer.append("T");
        stringBuffer.append(getNumberFormat(7, tellerNo));
        stringBuffer.append("001");
        stringBuffer.append(sourceFile.substring(14, 17));
        stringBuffer.append(accountDate);
        stringBuffer.append(type);
        stringBuffer.append(getNumberFormat(24, "0"));
        stringBuffer.append(getOneVsSpace(217, " "));
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }

    /**
     * @Description: 获取文件的头
     * @Param: type（bwls）
     * @return:
     * @Author: shaowu.ni
     * @Date:
     */

    @Transient
    public String pwlsHeadMsg(String type) throws UnsupportedEncodingException {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("H000000000001");
        stringBuffer.append(getOneVsSpace(50, this.sourceFile));
        stringBuffer.append(tranOrgId);
        stringBuffer.append(getNumberFormat(12, Integer.toString(detialCount)));
        stringBuffer.append("T");
        stringBuffer.append(getNumberFormat(7, tellerNo));
        stringBuffer.append("001");
        stringBuffer.append(sourceFile.substring(14, 17));
        stringBuffer.append(type);
        stringBuffer.append(getNumberFormat(24, "0"));
        stringBuffer.append(getOneVsSpace(198, " "));
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }

    /**
     * @Description: 获取文件的头
     * @Param: type（wlfs和pwfs）
     * @return:
     * @Author: shaowu.ni
     * @Date:
     */
    @Transient
    public String smartHeadMsg(String type) throws UnsupportedEncodingException {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("H000000000001");
        stringBuffer.append(getOneVsSpace(50, this.sourceFile));
        stringBuffer.append(tranOrgId);
        stringBuffer.append("CNY");
        stringBuffer.append(getNumberFormat(12, Integer.toString(detialCount)));
        stringBuffer.append(getNumberFormat(17, "0"));
        stringBuffer.append("T");
        stringBuffer.append(getNumberFormat(7, tellerNo));
        stringBuffer.append("001");
        stringBuffer.append(sourceFile.substring(14, 17));
        stringBuffer.append(accountDate);
        stringBuffer.append(type);
        stringBuffer.append(getNumberFormat(87, "0"));
        stringBuffer.append(getOneVsSpace(217, " "));
        stringBuffer.append("\n");
        return stringBuffer.toString();
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("accountDate", getAccountDate())
                .append("batchNo", getBatchNo())
                .append("createDate", getCreateDate())
                .append("detialCount", getDetialCount())
                .append("msg", getMsg())
                .append("proNo", getProNo())
                .append("returnDate", getReturnDate())
                .append("sourceFile", getSourceFile())
                .append("statu", getStatu())
                .append("tellerNo", getTellerNo())
                .append("terminalNo", getTerminalNo())
                .append("tranOrgId", getTranOrgId())
                .append("type", getType())
                .toString();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BusBpsFilehandl that = (BusBpsFilehandl) o;
        return batchNo == that.getBatchNo() && detialCount == that.getDetialCount() && Objects.equals(id, that.getId()) && Objects.equals(accountDate, that.getAccountDate()) && Objects.equals(sourceFile, that.getSourceFile()) && Objects.equals(createDate, that.getCreateDate()) && Objects.equals(returnDate, that.getReturnDate()) && Objects.equals(statu, that.getStatu()) && Objects.equals(msg, that.getMsg()) && Objects.equals(proNo, that.getProNo()) && Objects.equals(type, that.getType()) && Objects.equals(tranOrgId, that.getTranOrgId()) && Objects.equals(terminalNo, that.getTerminalNo()) && Objects.equals(tellerNo, that.getTellerNo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountDate, batchNo, sourceFile, detialCount, createDate, returnDate, statu, msg, proNo, type, tranOrgId, terminalNo, tellerNo);
    }
}
