package com.gcl.corporatewalletmanagement.domain;

import com.gcl.bps.util.DateUtil;
import com.gcl.common.annotation.Excel;
import com.gcl.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.UUID;


/**
 * BPS日志对象 bus_bps_log
 *
 * @author zhaihb
 * @date 2022-01-13
 */
public class BusBpsLog extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /** 日志ID */
    private String logId;

    /** 上传日期 */
    @Excel(name = "上传日期")
    private String accountDate;

    /** 批次号 */
    @Excel(name = "批次号")
    private Long batchNo;

    /** BPSID */
    private String bpsId;

    /** 文件创建日期 */
    @Excel(name = "文件创建日期")
    private String createDate;

    /** 日期 */
    @Excel(name = "日期")
    private String operDate;

    /** 日志 */
    @Excel(name = "日志")
    private String operInfo;

    /** 操作结果0成功（info）1（失败error） */
    @Excel(name = "操作结果0成功", readConverterExp = "i=nfo")
    private String operResult;

    /** 时间 */
    @Excel(name = "时间")
    private String operTime;

    /** 操作类型 */
    @Excel(name = "操作类型")
    private String operType;

    /** 文件回盘日期 */
    @Excel(name = "文件回盘日期")
    private String returnDate;

    /** 源文件名称 */
    @Excel(name = "源文件名称")
    private String sourceFile;

    /** 执行步骤的执行结果 1成功，2失败 */
    @Excel(name = "执行步骤的执行结果 1成功，2失败")
    private String statu;

    public BusBpsLog(String operType, String operInfo, String operResult) {
        this.logId= UUID.randomUUID().toString().replace("-","");
        this.operType = operType;
        this.operInfo = operInfo;
        this.operResult =operResult;
        this.operDate = DateUtil.getCurDate();
        this.operTime = DateUtil.getCurTime();
    }

    public void setLogId(String logId)
    {
        this.logId = logId;
    }

    public String getLogId()
    {
        return logId;
    }
    public void setAccountDate(String accountDate)
    {
        this.accountDate = accountDate;
    }

    public String getAccountDate()
    {
        return accountDate;
    }
    public void setBatchNo(Long batchNo)
    {
        this.batchNo = batchNo;
    }

    public Long getBatchNo()
    {
        return batchNo;
    }
    public void setBpsId(String bpsId)
    {
        this.bpsId = bpsId;
    }

    public String getBpsId()
    {
        return bpsId;
    }
    public void setCreateDate(String createDate)
    {
        this.createDate = createDate;
    }

    public String getCreateDate()
    {
        return createDate;
    }
    public void setOperDate(String operDate)
    {
        this.operDate = operDate;
    }

    public String getOperDate()
    {
        return operDate;
    }
    public void setOperInfo(String operInfo)
    {
        this.operInfo = operInfo;
    }

    public String getOperInfo()
    {
        return operInfo;
    }
    public void setOperResult(String operResult)
    {
        this.operResult = operResult;
    }

    public String getOperResult()
    {
        return operResult;
    }
    public void setOperTime(String operTime)
    {
        this.operTime = operTime;
    }

    public String getOperTime()
    {
        return operTime;
    }
    public void setOperType(String operType)
    {
        this.operType = operType;
    }

    public String getOperType()
    {
        return operType;
    }
    public void setReturnDate(String returnDate)
    {
        this.returnDate = returnDate;
    }

    public String getReturnDate()
    {
        return returnDate;
    }
    public void setSourceFile(String sourceFile)
    {
        this.sourceFile = sourceFile;
    }

    public String getSourceFile()
    {
        return sourceFile;
    }
    public void setStatu(String statu)
    {
        this.statu = statu;
    }

    public String getStatu()
    {
        return statu;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("logId", getLogId())
                .append("accountDate", getAccountDate())
                .append("batchNo", getBatchNo())
                .append("bpsId", getBpsId())
                .append("createDate", getCreateDate())
                .append("operDate", getOperDate())
                .append("operInfo", getOperInfo())
                .append("operResult", getOperResult())
                .append("operTime", getOperTime())
                .append("operType", getOperType())
                .append("returnDate", getReturnDate())
                .append("sourceFile", getSourceFile())
                .append("statu", getStatu())
                .toString();
    }
}
