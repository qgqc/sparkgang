package com.gcl.corporatewalletmanagement.domain;

import com.gcl.common.annotation.Excel;
import com.gcl.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 企业资料信息对象 bus_mer_file_upload
 * 
 * @author yada
 * @date 2022-01-12
 */
public class BusMerFileUpload extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** 客户经理Id */
    @Excel(name = "客户经理Id")
    private String accountManagerId;

    /** 客户经理姓名 */
    @Excel(name = "客户经理姓名")
    private String accountManagerName;

    /** 创建日期 */
    @Excel(name = "创建日期")
    private String createdDate;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private String createdTime;

    /** 资源文件的下载URL */
    @Excel(name = "资源文件的下载URL")
    private String downloadUrl;

    /** 企业名称 */
    @Excel(name = "企业名称")
    private String entName;

    /** 商户id */
    @Excel(name = "商户id")
    private String merchantId;

    /** 资源文件MIME类型 */
    @Excel(name = "资源文件MIME类型")
    private String mimeType;

    /** 原始文件名 */
    @Excel(name = "原始文件名")
    private String originalFielName;

    /** 资源文件的相对路径 */
    @Excel(name = "资源文件的相对路径")
    private String relativePath;

    /** 资源文件类型 */
    @Excel(name = "资源文件类型")
    private String type;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updatedDate;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updatedTime;

    /** 资源文件的大小 */
    @Excel(name = "资源文件的大小")
    private String fileSize;

    /** 资源文件的删除URL */
    @Excel(name = "资源文件的删除URL")
    private String deleteUrl;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setAccountManagerId(String accountManagerId) 
    {
        this.accountManagerId = accountManagerId;
    }

    public String getAccountManagerId() 
    {
        return accountManagerId;
    }
    public void setAccountManagerName(String accountManagerName) 
    {
        this.accountManagerName = accountManagerName;
    }

    public String getAccountManagerName() 
    {
        return accountManagerName;
    }
    public void setCreatedDate(String createdDate) 
    {
        this.createdDate = createdDate;
    }

    public String getCreatedDate() 
    {
        return createdDate;
    }
    public void setCreatedTime(String createdTime) 
    {
        this.createdTime = createdTime;
    }

    public String getCreatedTime() 
    {
        return createdTime;
    }
    public void setDownloadUrl(String downloadUrl) 
    {
        this.downloadUrl = downloadUrl;
    }

    public String getDownloadUrl() 
    {
        return downloadUrl;
    }
    public void setEntName(String entName) 
    {
        this.entName = entName;
    }

    public String getEntName() 
    {
        return entName;
    }
    public void setMerchantId(String merchantId) 
    {
        this.merchantId = merchantId;
    }

    public String getMerchantId() 
    {
        return merchantId;
    }
    public void setMimeType(String mimeType) 
    {
        this.mimeType = mimeType;
    }

    public String getMimeType() 
    {
        return mimeType;
    }
    public void setOriginalFielName(String originalFielName) 
    {
        this.originalFielName = originalFielName;
    }

    public String getOriginalFielName() 
    {
        return originalFielName;
    }
    public void setRelativePath(String relativePath) 
    {
        this.relativePath = relativePath;
    }

    public String getRelativePath() 
    {
        return relativePath;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setUpdatedDate(String updatedDate) 
    {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedDate() 
    {
        return updatedDate;
    }
    public void setUpdatedTime(String updatedTime) 
    {
        this.updatedTime = updatedTime;
    }

    public String getUpdatedTime() 
    {
        return updatedTime;
    }
    public void setFileSize(String fileSize) 
    {
        this.fileSize = fileSize;
    }

    public String getFileSize() 
    {
        return fileSize;
    }
    public void setDeleteUrl(String deleteUrl) 
    {
        this.deleteUrl = deleteUrl;
    }

    public String getDeleteUrl() 
    {
        return deleteUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("accountManagerId", getAccountManagerId())
            .append("accountManagerName", getAccountManagerName())
            .append("createdDate", getCreatedDate())
            .append("createdTime", getCreatedTime())
            .append("downloadUrl", getDownloadUrl())
            .append("entName", getEntName())
            .append("merchantId", getMerchantId())
            .append("mimeType", getMimeType())
            .append("originalFielName", getOriginalFielName())
            .append("relativePath", getRelativePath())
            .append("type", getType())
            .append("updatedDate", getUpdatedDate())
            .append("updatedTime", getUpdatedTime())
            .append("fileSize", getFileSize())
            .append("deleteUrl", getDeleteUrl())
            .toString();
    }
}
