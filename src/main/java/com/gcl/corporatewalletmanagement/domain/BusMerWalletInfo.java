package com.gcl.corporatewalletmanagement.domain;

import java.util.Date;
import java.util.List;

import com.gcl.common.core.domain.entity.SysDept;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.gcl.common.annotation.Excel;
import com.gcl.common.core.domain.BaseEntity;

import javax.validation.constraints.Size;

/**
 * 对公钱包开立申请对象 bus_mer_wallet_info
 *
 * @author yada
 * @date 2022-01-18
 */
public class BusMerWalletInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    public static final String DEFAULT_TRADING_ORGANIZATION_NO = "27005";

    public static final String PAPER_CONTRACT_NO_PREFIX = "DCEP";



    /** 主键ID */
    private String id;

    /** 客户号 1*/
    @Excel(name = "企业客户号")
    @Size(max = 4, message = "企业客户号长度不能超过40个字符")
    private String custNo;

    /** 企业名称 2*/
    @Excel(name = "企业名称")
    private String entName;

    /** 客户类型，01：企业客户，02：个体工商户  3*/
    @Excel(name = "客户类型"/*,dictType = "bus_mer_walletinfo_custType"*/)
    private String custType;

    /** 企业地址  4*/
    @Excel(name = "企业地址")
    @Size(max = 125, message = "企业地址长度不能超过125个字符")
    private String entAddr;

    /** 行业类型  5*/
    @Excel(name = "行业类型"/*,dictType = "bus_mer_walletinfo_industryType"*/)
    private String industryType;

    /** 企业证件类型 6*/
    @Excel(name = "企业证件类型"/*,dictType = "bus_mer_walletinfo_EntCertType"*/)
    private String entCertType;

    /** 企业证件号码,企业证件类型相同时，企业证件号码不能重复 7*/
    @Excel(name = "企业证件号码")
    @Size(max = 32, message = "企业证件号码长度不能超过32个字符")
    private String entCertNo;

    /** 企业证件到期日,YYYYMMDD,例：20200522 8*/
    @Excel(name = "企业证件到期日")
    private String entCertTypeExpireDate;


    /** 企业客户联系人姓名,最多只能输入40个汉字  12*/
    @Excel(name = "联系人姓名")
    private String entCustContName;


    /** 企业客户联系人手机号  13*/
    @Excel(name = "联系人手机号")
    private String entCustContPhone;

    /** 企业客户联系人证件类型  11*/
    @Excel(name = "联系人证件类型"/*,dictType = "bus_mer_walletinfo_EntCustContCertType"*/)
    private String entCustContCertType;


    /** 企业客户联系人证件号码 10*/
    @Excel(name = "联系人证件号码")
    private String entCustContCertNo;

    /** 企业客户联系人证件号码过期日,YYYYMMDD,例：20200522  9*/
    @Excel(name = "联系人证件到期日")
    private String entCustConCertNoExDate;



    /** 企业法人姓名 */
    @Excel(name = "企业法人姓名")
    private String entLegalPsName;


    /** 企业法人身份证件类型*/
    @Excel(name = "企业法人证件类型"/*,dictType = "bus_mer_wallinfo_EntLegalPsCertType"*/)
    private String entLegalPsCertType;


    /** 企业法人证件号码 */
    @Excel(name = "企业法人证件号码")
    private String entLegalPsCertNo;

    /** 企业法人证件到期日,YYYYMMDD,例：20200522 */
    @Excel(name = "企业法人证件到期日")
    private String entLegalPsCertExpireDate;







    /** 企业成立日期 */
    @Excel(name = "成立日期")
    private String entCreationDate;

    /** 营业执照期限,格式：yyyyMMdd */
    @Excel(name = "营业执照期限")
    private String durationBusinessLicense;

    /** 经营范围 */
    @Excel(name = "经营范围")
    private String businessScope;

    /** 营业执照号码 */
    @Excel(name = "营业执照号码")
    private String businessLicenseNo;

    /** 统一社会信用代码 */
    @Excel(name = "统一社会信用代码")
    private String uniformSocialCreditCode;

    /** 组织机构代码 */
    @Excel(name = "组织机构代码")
    private String organizationCode;

    /** 税务登记号 */
    @Excel(name = "税务登记号")
    private String taxRegistrationNo;

    /** 企业所属省,6位数字 参见 国家行政区划编码 */
    @Excel(name = "企业所属省编码"/*,dictType = "pub_province_code"*/)
    private String enterpriseOwnedProvince;

    /** 企业所属市 */
    @Excel(name = "企业所属市编码"/*,dictType = "pub_city_code"*/)
    private String enterpriseOwnedCity;

    /** 账户名 */
    @Excel(name = "账户名称")
    private String accountName;

    /** 开户账号 */
    @Excel(name = "开户账号")
    private String account;

    /** 账户类型 */
    //@Excel(name = "账户类型"/*,dictType = "bus_mer_walletinfo_accountType"*/)
    private String accountType;

    /** 绑定银行账户类型yin */
    @Excel(name = "开户类型")
    private String bindBankAccountType;


    /** 分支行机构号 */
    @Excel(name = "开户行机构号")
    private String bankBranchNo;

    /** 分支行名称 */
    @Excel(name = "开户行名称")
    private String bankBranchName;



    /** MCC编码，参见现有银联MCC规则 */
    @Excel(name = "MCC编码")
    private String mccCode;

    /** 商户责任人 */
    @Excel(name = "商户责任人")
    private String merchantResponsiblePerson;

    /** 商户开户联系人姓名 */
    @Excel(name = "商户开户联系人姓名")
    private String merchantAcctOpenContName;

    /** 商户开户联系人电话 */
    @Excel(name = "商户开户联系人电话")
    private String merchantAcctOpenContPhone;

    /** 客户经理所属机构号 */
    @Excel(name = "客户经理所属机构号")
    private String custMngOrgNo;



    /** 客户经理员工号 */
    @Excel(name = "客户经理员工号")
    private String custMngEmpNum;

    /** 客户经理姓名 */
    @Excel(name = "客户经理姓名")
    private String accountManager;

    /** 客户归属客户经理 */
    private String custMngId;

    /** 营业地址 */
    private String businessAddress;

    /** 接入标识 */
    private String accessId;

    /** 开户行 */
    private String accountBank;

    /** 开户行市编码 */
    private String accountOpeningCityCode;

    /** 开户行省编码 */
    private String accountOpeningProvinceCode;

    /** 所属代理机构编码 */
    private String agencyCode;

    /** 批次号 */
    private String batchNo;



    /** CNAPS编号(商户结算标志：0-关闭。此栏位，置空;当 */
    private String cnapsno;

    /** 市编码,不能为空，长度6位数字 */
    private String cityCode;

    /** 银行所属城市,6位数字 参见 国家行政区划编码 */
    private String cityOfBank;

    /** 合同生效日期YYYYMMDD */
    private String contractEffectiveDate;

    /** 合同失效日期YYYYMMDD */
    private String contractExpireDate;

    /** 数据状态，1：存量信息导入"2：新增信息导入"3：申请表已打印"4：资料已上传"5：提交复核"6：复核通过"7：复核退回"8：生成BPS文件"9：上传BPS文件成功"10：下载BPS回盘成功"11：更新ABC处理结果 */
    private String dataStatus;

    /** 设备信息 */
    private String deviceMsgEntity;

    /** 企业创建人 */
    private String entCreator;

    /** 企业类型,企业类型01:合资企业 ,02:独资企业 ,03:国有企业 ,04:私营企业 ,05:股份制企业 ,06:有限责任制企业 ,07:其他 */
    private String entType;

    /** 审核成功日期 */
    private String examineSuccessDate;

    /** 外部商户名称，代收币一级商户必输，二级商户选输，收币商户不输 */
    private String externalMerchantName;

    /** 外部商户号，代收币一级商户必输，二级商户选输，收币商户不输 */
    @Excel(name = "外部商户号")
    private String externalMerchantNo;

    /** 文件批次号 */
    private String fileBatchNo;

    /** 头部id */
    private String headId;

    /** 系统间过渡账户,01：信用卡系统间过渡账户，02：核心系统过渡账户 */
    private String interSystemTranAccount;

    /** 登录名称,注册邮箱,建议使用商户公司公共邮箱，而不是个人邮箱，避免因人员流动造成无法使用 */
    private String loginName;

    /** 登录类型，02：邮箱 */
    private String loginType;

    /** 邮箱,同登录名称 */
    private String mail;

    /** 商户开户联系人邮箱 */
    private String merchantAcctOpenContEmail;

    /** 商户属性/标识，商户属性/标识不能为空。0-中行内部 1-电信内部 2-联通内部 3-系统内部（如：党组织） 4-外部，固定填写0 */
    private String merchantAttributes;

    /** 商户回调地址 */
    private String merchantCallbackurl;

    /** 商户连锁类型(01-非连锁,02-全国连锁, 03-大型连锁, 04-单省连锁, 05-连锁商户) */
    private String merchantChainType;

    /** 商户全称 */
    private String merchantFullName;

    /** 商户号 */
    private String merchantId;

    /** 商户等级，必输项,一级：ML01,二级：ML02 */
    private String merchantLevel;

    /** 商户开立交易返回信息码(00000000－交易成功, 00000002－交易失败) */
    private String merchantOpenTranReturnCode;

    /** 商户开立交易返回信息，返回成功或错误信息 */
    private String merchantOpenTranReturnMsg;

    /** 商户注册渠道, 默认04：批量标准商户 */
    private String merchantRegisterChannels;

    /** 商户结算标志,0-关闭 1-开通 */
    private String merchantSettlement;

    /** 商户简称 */
    private String merchantShortlName;

    /** 商户类型,0000(收币)0002(聚合收币) */
    private String merchantType;

    /** 幂等流水（扩位）。选输项， */
    private String miDengLiuShui;

    /** 结算最小金额（单位：分）,商户结算标志：0-关闭。此栏位置空. */
    private String minimumSettlementAmount;

    /** 开户名称 */
    private String openAccountName;

    /** 运营机构 */
    private String operatingOrganization;

    /** 客户归属网点机构号 */
    private String orgNo;

    /** 人行商户号 */
    private String pbocmerchantNo;

    /** 纸质合同号，合同号(系统自动创建，不是与商户签订的纸面合同）,实际签约的合同号为ACTCONTRACTNO字段 */
    private String paperContractNo;

    /** 打款时间点，10代表10:00, 11代表11:00, 12代表12:00 */
    private String paymentTime;

    /** 省编码，省编码不能为空，长度6位数字 */
    private String provinceCode;

    /** 银行所属省份,6位数字 参见 国家行政区划编码 */
    private String provinceOfBank;

    /** 收款银行代码,商户结算标志：0-关闭。此栏位置空.当结算处理方式选择余额归集时，选填 */
    private String receivePaymentBankCode;

    /** 收款银行账号:对公填写收款银行账号，对私填写银行卡号 */
    private String receivingBankAccount;

    /** 收款银行名称 */
    private String receivingBankName;

    /** 记录标识,H为头纪录，D为明细,第一行头记录写H，其他行写D */
    private String recordId;

    /** 冗余域 */
    private String redundantInfo;

    /** 备注信息 */
    private String remarkInfo;

    /** 对公钱包开立交易返回信息码,00000000－交易成功;00000002－交易失败 */
    private String returnCode;

    /** 对公钱包开立交易返回信息 */
    private String returnMsg;

    /** 顺序号，从2开始；十二位数字 */
    private Long sequenceNumber;

    /** 结算周期,填写结算周期的N值 */
    private String settleCycle;

    /** 结算周期类型，01:T+N， 02：D+N，03：周结，04：月结：05-季度结算.商户结算标志：0-关闭。此栏位置空 */
    private String settleCycleType;

    /** 结算处理方式,一级商户支持如下选项：结算处理方式：05-自动提现 。二级商户支持：结算处理方式：05-自动提现 、 07- 余额归集 */
    private String settleTreatmentMethod;

    /** 结算类型:01-定期结算02-手工结算03-银行代扣 */
    private String settlementType;

    /** 股东证件到期日期,格式：yyyyMMdd */
    private String shareholderCertExpireDate;

    /** 股东证件号 */
    private String shareholderCertNo;

    /** 股东证件类型,股东证件类型
     * IT01:居民身份证
     * IT02:军官证
     * IT03:护照
     * IT04:户口薄
     * IT05:士兵证
     * IT06:港澳往来内地通行证
     * IT07:台湾同胞来往内地通行证
     * IT08:临时身份证
     * IT09:外国人居留证
     * IT10:警官证
     * IT11:港澳身份证
     * IT12:台湾身份证
     * IT31:驾驶证
     * IT32: 教师证
     * IT33: 学生证
     * IT99:其他 ,  */
    private String shareholderCertType;

    /** 签约人 */
    private String signatory;

    /** 特种行业许可证号 */
    private String specialIndustryIicenseNo;

    /** 是否支持协议代扣（01-支持，02-不支持） */
    private String supAgreeWithholding;

    /** 柜员号 */
    private String tellerNo;

    /** 交易机构号，固定:27005 */
    private String tradingOrganizationNo;

    /** 交易码,ABC交易码,TF0006：开对公钱包&商户TF0007：根据钱包开商户TF0008：开对公钱包不开商户 */
    private String transactionCode;

    /** 上级商户号，当选择一级商户时，上级商户号不必填写，当选择二级商户时，上级商户号必填 */
    private String upperLevelMerchantNo;

    /** 网址 */
    private String url;

    /** 钱包id,ABC交易码TF0006不输,ABC交易码TF0007必输,ABC交易码TF0008不输 */
    private String walletId;

    /** 钱包名称,最多60个汉字 */
    private String walletName;

    /** 钱包类型，WT01：对私，WT02：子对私，WT09：对公，WT10：子对公 */
    private String walletType;

    /** 数据来源，1：存量信息导入"2：新增信息导入 */
    private String dataSources;

    /** BPS处理表ID */
    private String dealId;

    /** 回盘更新时间 */
    private Date returnTime;

    //商户开立机构号
    @Excel(name = "商户开立机构号")
    private String merchantOpenOrgNo;
    //商户开立机构名
    @Excel(name = "商户开立机构名")
    private String merchantOpenOrgName;


    //查询条件 0不能看所有信息  1 能看所有信息  控住数据分离
    private String canSeeAllInfo;
    private List<SysDept> depts;

    public String getCanSeeAllInfo() {
        return canSeeAllInfo;
    }

    public void setCanSeeAllInfo(String canSeeAllInfo) {
        this.canSeeAllInfo = canSeeAllInfo;
    }

    public List<SysDept> getDepts() {
        return depts;
    }

    public void setDepts(List<SysDept> depts) {
        this.depts = depts;
    }

    public String getMerchantOpenOrgNo() {
        return merchantOpenOrgNo;
    }

    public void setMerchantOpenOrgNo(String merchantOpenOrgNo) {
        this.merchantOpenOrgNo = merchantOpenOrgNo;
    }

    public String getMerchantOpenOrgName() {
        return merchantOpenOrgName;
    }

    public void setMerchantOpenOrgName(String merchantOpenOrgName) {
        this.merchantOpenOrgName = merchantOpenOrgName;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setEnterpriseOwnedCity(String enterpriseOwnedCity)
    {
        this.enterpriseOwnedCity = enterpriseOwnedCity;
    }

    public String getEnterpriseOwnedCity()
    {
        return enterpriseOwnedCity;
    }
    public void setAccessId(String accessId)
    {
        this.accessId = accessId;
    }

    public String getAccessId()
    {
        return accessId;
    }
    public void setAccount(String account)
    {
        this.account = account;
    }

    public String getAccount()
    {
        return account;
    }
    public void setAccountBank(String accountBank)
    {
        this.accountBank = accountBank;
    }

    public String getAccountBank()
    {
        return accountBank;
    }
    public void setAccountManager(String accountManager)
    {
        this.accountManager = accountManager;
    }

    public String getAccountManager()
    {
        return accountManager;
    }
    public void setAccountName(String accountName)
    {
        this.accountName = accountName;
    }

    public String getAccountName()
    {
        return accountName;
    }
    public void setAccountOpeningCityCode(String accountOpeningCityCode)
    {
        this.accountOpeningCityCode = accountOpeningCityCode;
    }

    public String getAccountOpeningCityCode()
    {
        return accountOpeningCityCode;
    }
    public void setAccountOpeningProvinceCode(String accountOpeningProvinceCode)
    {
        this.accountOpeningProvinceCode = accountOpeningProvinceCode;
    }

    public String getAccountOpeningProvinceCode()
    {
        return accountOpeningProvinceCode;
    }
    public void setAccountType(String accountType)
    {
        this.accountType = accountType;
    }

    public String getAccountType()
    {
        return accountType;
    }
    public void setAgencyCode(String agencyCode)
    {
        this.agencyCode = agencyCode;
    }

    public String getAgencyCode()
    {
        return agencyCode;
    }
    public void setBankBranchName(String bankBranchName)
    {
        this.bankBranchName = bankBranchName;
    }

    public String getBankBranchName()
    {
        return bankBranchName;
    }
    public void setBankBranchNo(String bankBranchNo)
    {
        this.bankBranchNo = bankBranchNo;
    }

    public String getBankBranchNo()
    {
        return bankBranchNo;
    }
    public void setBatchNo(String batchNo)
    {
        this.batchNo = batchNo;
    }

    public String getBatchNo()
    {
        return batchNo;
    }
    public void setBindBankAccountType(String bindBankAccountType)
    {
        this.bindBankAccountType = bindBankAccountType;
    }

    public String getBindBankAccountType()
    {
        return bindBankAccountType;
    }
    public void setBusinessAddress(String businessAddress)
    {
        this.businessAddress = businessAddress;
    }

    public String getBusinessAddress()
    {
        return businessAddress;
    }
    public void setBusinessLicenseNo(String businessLicenseNo)
    {
        this.businessLicenseNo = businessLicenseNo;
    }

    public String getBusinessLicenseNo()
    {
        return businessLicenseNo;
    }
    public void setBusinessScope(String businessScope)
    {
        this.businessScope = businessScope;
    }

    public String getBusinessScope()
    {
        return businessScope;
    }
    public void setCnapsno(String cnapsno)
    {
        this.cnapsno = cnapsno;
    }

    public String getCnapsno()
    {
        return cnapsno;
    }
    public void setCityCode(String cityCode)
    {
        this.cityCode = cityCode;
    }

    public String getCityCode()
    {
        return cityCode;
    }
    public void setCityOfBank(String cityOfBank)
    {
        this.cityOfBank = cityOfBank;
    }

    public String getCityOfBank()
    {
        return cityOfBank;
    }
    public void setContractEffectiveDate(String contractEffectiveDate)
    {
        this.contractEffectiveDate = contractEffectiveDate;
    }

    public String getContractEffectiveDate()
    {
        return contractEffectiveDate;
    }
    public void setContractExpireDate(String contractExpireDate)
    {
        this.contractExpireDate = contractExpireDate;
    }

    public String getContractExpireDate()
    {
        return contractExpireDate;
    }
    public void setCustMngId(String custMngId)
    {
        this.custMngId = custMngId;
    }

    public String getCustMngId()
    {
        return custMngId;
    }
    public void setCustNo(String custNo)
    {
        this.custNo = custNo;
    }

    public String getCustNo()
    {
        return custNo;
    }
    public void setCustType(String custType)
    {
        this.custType = custType;
    }

    public String getCustType()
    {
        return custType;
    }
    public void setDataStatus(String dataStatus)
    {
        this.dataStatus = dataStatus;
    }

    public String getDataStatus()
    {
        return dataStatus;
    }
    public void setDeviceMsgEntity(String deviceMsgEntity)
    {
        this.deviceMsgEntity = deviceMsgEntity;
    }

    public String getDeviceMsgEntity()
    {
        return deviceMsgEntity;
    }
    public void setDurationBusinessLicense(String durationBusinessLicense)
    {
        this.durationBusinessLicense = durationBusinessLicense;
    }

    public String getDurationBusinessLicense()
    {
        return durationBusinessLicense;
    }
    public void setEntAddr(String entAddr)
    {
        this.entAddr = entAddr;
    }

    public String getEntAddr()
    {
        return entAddr;
    }
    public void setEntCertNo(String entCertNo)
    {
        this.entCertNo = entCertNo;
    }

    public String getEntCertNo()
    {
        return entCertNo;
    }
    public void setEntCertType(String entCertType)
    {
        this.entCertType = entCertType;
    }

    public String getEntCertType()
    {
        return entCertType;
    }
    public void setEntCertTypeExpireDate(String entCertTypeExpireDate)
    {
        this.entCertTypeExpireDate = entCertTypeExpireDate;
    }

    public String getEntCertTypeExpireDate()
    {
        return entCertTypeExpireDate;
    }
    public void setEntCreationDate(String entCreationDate)
    {
        this.entCreationDate = entCreationDate;
    }

    public String getEntCreationDate()
    {
        return entCreationDate;
    }
    public void setEntCreator(String entCreator)
    {
        this.entCreator = entCreator;
    }

    public String getEntCreator()
    {
        return entCreator;
    }
    public void setEntCustConCertNoExDate(String entCustConCertNoExDate)
    {
        this.entCustConCertNoExDate = entCustConCertNoExDate;
    }

    public String getEntCustConCertNoExDate()
    {
        return entCustConCertNoExDate;
    }
    public void setEntCustContCertNo(String entCustContCertNo)
    {
        this.entCustContCertNo = entCustContCertNo;
    }

    public String getEntCustContCertNo()
    {
        return entCustContCertNo;
    }
    public void setEntCustContCertType(String entCustContCertType)
    {
        this.entCustContCertType = entCustContCertType;
    }

    public String getEntCustContCertType()
    {
        return entCustContCertType;
    }
    public void setEntCustContName(String entCustContName)
    {
        this.entCustContName = entCustContName;
    }

    public String getEntCustContName()
    {
        return entCustContName;
    }
    public void setEntCustContPhone(String entCustContPhone)
    {
        this.entCustContPhone = entCustContPhone;
    }

    public String getEntCustContPhone()
    {
        return entCustContPhone;
    }
    public void setEntLegalPsCertExpireDate(String entLegalPsCertExpireDate)
    {
        this.entLegalPsCertExpireDate = entLegalPsCertExpireDate;
    }

    public String getEntLegalPsCertExpireDate()
    {
        return entLegalPsCertExpireDate;
    }
    public void setEntLegalPsCertNo(String entLegalPsCertNo)
    {
        this.entLegalPsCertNo = entLegalPsCertNo;
    }

    public String getEntLegalPsCertNo()
    {
        return entLegalPsCertNo;
    }
    public void setEntLegalPsCertType(String entLegalPsCertType)
    {
        this.entLegalPsCertType = entLegalPsCertType;
    }

    public String getEntLegalPsCertType()
    {
        return entLegalPsCertType;
    }
    public void setEntLegalPsName(String entLegalPsName)
    {
        this.entLegalPsName = entLegalPsName;
    }

    public String getEntLegalPsName()
    {
        return entLegalPsName;
    }
    public void setEntName(String entName)
    {
        this.entName = entName;
    }

    public String getEntName()
    {
        return entName;
    }
    public void setEntType(String entType)
    {
        this.entType = entType;
    }

    public String getEntType()
    {
        return entType;
    }
    public void setEnterpriseOwnedProvince(String enterpriseOwnedProvince)
    {
        this.enterpriseOwnedProvince = enterpriseOwnedProvince;
    }

    public String getEnterpriseOwnedProvince()
    {
        return enterpriseOwnedProvince;
    }
    public void setExamineSuccessDate(String examineSuccessDate)
    {
        this.examineSuccessDate = examineSuccessDate;
    }

    public String getExamineSuccessDate()
    {
        return examineSuccessDate;
    }
    public void setExternalMerchantName(String externalMerchantName)
    {
        this.externalMerchantName = externalMerchantName;
    }

    public String getExternalMerchantName()
    {
        return externalMerchantName;
    }
    public void setExternalMerchantNo(String externalMerchantNo)
    {
        this.externalMerchantNo = externalMerchantNo;
    }

    public String getExternalMerchantNo()
    {
        return externalMerchantNo;
    }
    public void setFileBatchNo(String fileBatchNo)
    {
        this.fileBatchNo = fileBatchNo;
    }

    public String getFileBatchNo()
    {
        return fileBatchNo;
    }
    public void setHeadId(String headId)
    {
        this.headId = headId;
    }

    public String getHeadId()
    {
        return headId;
    }
    public void setIndustryType(String industryType)
    {
        this.industryType = industryType;
    }

    public String getIndustryType()
    {
        return industryType;
    }
    public void setInterSystemTranAccount(String interSystemTranAccount)
    {
        this.interSystemTranAccount = interSystemTranAccount;
    }

    public String getInterSystemTranAccount()
    {
        return interSystemTranAccount;
    }
    public void setLoginName(String loginName)
    {
        this.loginName = loginName;
    }

    public String getLoginName()
    {
        return loginName;
    }
    public void setLoginType(String loginType)
    {
        this.loginType = loginType;
    }

    public String getLoginType()
    {
        return loginType;
    }
    public void setMail(String mail)
    {
        this.mail = mail;
    }

    public String getMail()
    {
        return mail;
    }
    public void setMccCode(String mccCode)
    {
        this.mccCode = mccCode;
    }

    public String getMccCode()
    {
        return mccCode;
    }
    public void setMerchantAcctOpenContEmail(String merchantAcctOpenContEmail)
    {
        this.merchantAcctOpenContEmail = merchantAcctOpenContEmail;
    }

    public String getMerchantAcctOpenContEmail()
    {
        return merchantAcctOpenContEmail;
    }
    public void setMerchantAcctOpenContName(String merchantAcctOpenContName)
    {
        this.merchantAcctOpenContName = merchantAcctOpenContName;
    }

    public String getMerchantAcctOpenContName()
    {
        return merchantAcctOpenContName;
    }
    public void setMerchantAcctOpenContPhone(String merchantAcctOpenContPhone)
    {
        this.merchantAcctOpenContPhone = merchantAcctOpenContPhone;
    }

    public String getMerchantAcctOpenContPhone()
    {
        return merchantAcctOpenContPhone;
    }
    public void setMerchantAttributes(String merchantAttributes)
    {
        this.merchantAttributes = merchantAttributes;
    }

    public String getMerchantAttributes()
    {
        return merchantAttributes;
    }
    public void setMerchantCallbackurl(String merchantCallbackurl)
    {
        this.merchantCallbackurl = merchantCallbackurl;
    }

    public String getMerchantCallbackurl()
    {
        return merchantCallbackurl;
    }
    public void setMerchantChainType(String merchantChainType)
    {
        this.merchantChainType = merchantChainType;
    }

    public String getMerchantChainType()
    {
        return merchantChainType;
    }
    public void setMerchantFullName(String merchantFullName)
    {
        this.merchantFullName = merchantFullName;
    }

    public String getMerchantFullName()
    {
        return merchantFullName;
    }
    public void setMerchantId(String merchantId)
    {
        this.merchantId = merchantId;
    }

    public String getMerchantId()
    {
        return merchantId;
    }
    public void setMerchantLevel(String merchantLevel)
    {
        this.merchantLevel = merchantLevel;
    }

    public String getMerchantLevel()
    {
        return merchantLevel;
    }
    public void setMerchantOpenTranReturnCode(String merchantOpenTranReturnCode)
    {
        this.merchantOpenTranReturnCode = merchantOpenTranReturnCode;
    }

    public String getMerchantOpenTranReturnCode()
    {
        return merchantOpenTranReturnCode;
    }
    public void setMerchantOpenTranReturnMsg(String merchantOpenTranReturnMsg)
    {
        this.merchantOpenTranReturnMsg = merchantOpenTranReturnMsg;
    }

    public String getMerchantOpenTranReturnMsg()
    {
        return merchantOpenTranReturnMsg;
    }
    public void setMerchantRegisterChannels(String merchantRegisterChannels)
    {
        this.merchantRegisterChannels = merchantRegisterChannels;
    }

    public String getMerchantRegisterChannels()
    {
        return merchantRegisterChannels;
    }
    public void setMerchantResponsiblePerson(String merchantResponsiblePerson)
    {
        this.merchantResponsiblePerson = merchantResponsiblePerson;
    }

    public String getMerchantResponsiblePerson()
    {
        return merchantResponsiblePerson;
    }
    public void setMerchantSettlement(String merchantSettlement)
    {
        this.merchantSettlement = merchantSettlement;
    }

    public String getMerchantSettlement()
    {
        return merchantSettlement;
    }
    public void setMerchantShortlName(String merchantShortlName)
    {
        this.merchantShortlName = merchantShortlName;
    }

    public String getMerchantShortlName()
    {
        return merchantShortlName;
    }
    public void setMerchantType(String merchantType)
    {
        this.merchantType = merchantType;
    }

    public String getMerchantType()
    {
        return merchantType;
    }
    public void setMiDengLiuShui(String miDengLiuShui)
    {
        this.miDengLiuShui = miDengLiuShui;
    }

    public String getMiDengLiuShui()
    {
        return miDengLiuShui;
    }
    public void setMinimumSettlementAmount(String minimumSettlementAmount)
    {
        this.minimumSettlementAmount = minimumSettlementAmount;
    }

    public String getMinimumSettlementAmount()
    {
        return minimumSettlementAmount;
    }
    public void setOpenAccountName(String openAccountName)
    {
        this.openAccountName = openAccountName;
    }

    public String getOpenAccountName()
    {
        return openAccountName;
    }
    public void setOperatingOrganization(String operatingOrganization)
    {
        this.operatingOrganization = operatingOrganization;
    }

    public String getOperatingOrganization()
    {
        return operatingOrganization;
    }
    public void setOrgNo(String orgNo)
    {
        this.orgNo = orgNo;
    }

    public String getOrgNo()
    {
        return orgNo;
    }
    public void setOrganizationCode(String organizationCode)
    {
        this.organizationCode = organizationCode;
    }

    public String getOrganizationCode()
    {
        return organizationCode;
    }
    public void setPbocmerchantNo(String pbocmerchantNo)
    {
        this.pbocmerchantNo = pbocmerchantNo;
    }

    public String getPbocmerchantNo()
    {
        return pbocmerchantNo;
    }
    public void setPaperContractNo(String paperContractNo)
    {
        this.paperContractNo = paperContractNo;
    }

    public String getPaperContractNo()
    {
        return paperContractNo;
    }
    public void setPaymentTime(String paymentTime)
    {
        this.paymentTime = paymentTime;
    }

    public String getPaymentTime()
    {
        return paymentTime;
    }
    public void setProvinceCode(String provinceCode)
    {
        this.provinceCode = provinceCode;
    }

    public String getProvinceCode()
    {
        return provinceCode;
    }
    public void setProvinceOfBank(String provinceOfBank)
    {
        this.provinceOfBank = provinceOfBank;
    }

    public String getProvinceOfBank()
    {
        return provinceOfBank;
    }
    public void setReceivePaymentBankCode(String receivePaymentBankCode)
    {
        this.receivePaymentBankCode = receivePaymentBankCode;
    }

    public String getReceivePaymentBankCode()
    {
        return receivePaymentBankCode;
    }
    public void setReceivingBankAccount(String receivingBankAccount)
    {
        this.receivingBankAccount = receivingBankAccount;
    }

    public String getReceivingBankAccount()
    {
        return receivingBankAccount;
    }
    public void setReceivingBankName(String receivingBankName)
    {
        this.receivingBankName = receivingBankName;
    }

    public String getReceivingBankName()
    {
        return receivingBankName;
    }
    public void setRecordId(String recordId)
    {
        this.recordId = recordId;
    }

    public String getRecordId()
    {
        return recordId;
    }
    public void setRedundantInfo(String redundantInfo)
    {
        this.redundantInfo = redundantInfo;
    }

    public String getRedundantInfo()
    {
        return redundantInfo;
    }
    public void setRemarkInfo(String remarkInfo)
    {
        this.remarkInfo = remarkInfo;
    }

    public String getRemarkInfo()
    {
        return remarkInfo;
    }
    public void setReturnCode(String returnCode)
    {
        this.returnCode = returnCode;
    }

    public String getReturnCode()
    {
        return returnCode;
    }
    public void setReturnMsg(String returnMsg)
    {
        this.returnMsg = returnMsg;
    }

    public String getReturnMsg()
    {
        return returnMsg;
    }
    public void setSequenceNumber(Long sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    public Long getSequenceNumber()
    {
        return sequenceNumber;
    }
    public void setSettleCycle(String settleCycle)
    {
        this.settleCycle = settleCycle;
    }

    public String getSettleCycle()
    {
        return settleCycle;
    }
    public void setSettleCycleType(String settleCycleType)
    {
        this.settleCycleType = settleCycleType;
    }

    public String getSettleCycleType()
    {
        return settleCycleType;
    }
    public void setSettleTreatmentMethod(String settleTreatmentMethod)
    {
        this.settleTreatmentMethod = settleTreatmentMethod;
    }

    public String getSettleTreatmentMethod()
    {
        return settleTreatmentMethod;
    }
    public void setSettlementType(String settlementType)
    {
        this.settlementType = settlementType;
    }

    public String getSettlementType()
    {
        return settlementType;
    }
    public void setShareholderCertExpireDate(String shareholderCertExpireDate)
    {
        this.shareholderCertExpireDate = shareholderCertExpireDate;
    }

    public String getShareholderCertExpireDate()
    {
        return shareholderCertExpireDate;
    }
    public void setShareholderCertNo(String shareholderCertNo)
    {
        this.shareholderCertNo = shareholderCertNo;
    }

    public String getShareholderCertNo()
    {
        return shareholderCertNo;
    }
    public void setShareholderCertType(String shareholderCertType)
    {
        this.shareholderCertType = shareholderCertType;
    }

    public String getShareholderCertType()
    {
        return shareholderCertType;
    }
    public void setSignatory(String signatory)
    {
        this.signatory = signatory;
    }

    public String getSignatory()
    {
        return signatory;
    }
    public void setSpecialIndustryIicenseNo(String specialIndustryIicenseNo)
    {
        this.specialIndustryIicenseNo = specialIndustryIicenseNo;
    }

    public String getSpecialIndustryIicenseNo()
    {
        return specialIndustryIicenseNo;
    }
    public void setSupAgreeWithholding(String supAgreeWithholding)
    {
        this.supAgreeWithholding = supAgreeWithholding;
    }

    public String getSupAgreeWithholding()
    {
        return supAgreeWithholding;
    }
    public void setTaxRegistrationNo(String taxRegistrationNo)
    {
        this.taxRegistrationNo = taxRegistrationNo;
    }

    public String getTaxRegistrationNo()
    {
        return taxRegistrationNo;
    }
    public void setTellerNo(String tellerNo)
    {
        this.tellerNo = tellerNo;
    }

    public String getTellerNo()
    {
        return tellerNo;
    }
    public void setTradingOrganizationNo(String tradingOrganizationNo)
    {
        this.tradingOrganizationNo = tradingOrganizationNo;
    }

    public String getTradingOrganizationNo()
    {
        return tradingOrganizationNo;
    }
    public void setTransactionCode(String transactionCode)
    {
        this.transactionCode = transactionCode;
    }

    public String getTransactionCode()
    {
        return transactionCode;
    }
    public void setUniformSocialCreditCode(String uniformSocialCreditCode)
    {
        this.uniformSocialCreditCode = uniformSocialCreditCode;
    }

    public String getUniformSocialCreditCode()
    {
        return uniformSocialCreditCode;
    }
    public void setUpperLevelMerchantNo(String upperLevelMerchantNo)
    {
        this.upperLevelMerchantNo = upperLevelMerchantNo;
    }

    public String getUpperLevelMerchantNo()
    {
        return upperLevelMerchantNo;
    }
    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getUrl()
    {
        return url;
    }
    public void setWalletId(String walletId)
    {
        this.walletId = walletId;
    }

    public String getWalletId()
    {
        return walletId;
    }
    public void setWalletName(String walletName)
    {
        this.walletName = walletName;
    }

    public String getWalletName()
    {
        return walletName;
    }
    public void setWalletType(String walletType)
    {
        this.walletType = walletType;
    }

    public String getWalletType()
    {
        return walletType;
    }
    public void setDataSources(String dataSources)
    {
        this.dataSources = dataSources;
    }

    public String getDataSources()
    {
        return dataSources;
    }
    public void setCustMngOrgNo(String custMngOrgNo)
    {
        this.custMngOrgNo = custMngOrgNo;
    }

    public String getCustMngOrgNo()
    {
        return custMngOrgNo;
    }

    public void setDealId(String dealId)
    {
        this.dealId = dealId;
    }

    public String getDealId()
    {
        return dealId;
    }
    public void setReturnTime(Date returnTime)
    {
        this.returnTime = returnTime;
    }

    public Date getReturnTime()
    {
        return returnTime;
    }

    public String getCustMngEmpNum() {
        return custMngEmpNum;
    }

    public void setCustMngEmpNum(String custMngEmpNum) {
        this.custMngEmpNum = custMngEmpNum;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("enterpriseOwnedCity", getEnterpriseOwnedCity())
                .append("accessId", getAccessId())
                .append("account", getAccount())
                .append("accountBank", getAccountBank())
                .append("accountManager", getAccountManager())
                .append("accountName", getAccountName())
                .append("accountOpeningCityCode", getAccountOpeningCityCode())
                .append("accountOpeningProvinceCode", getAccountOpeningProvinceCode())
                .append("accountType", getAccountType())
                .append("agencyCode", getAgencyCode())
                .append("bankBranchName", getBankBranchName())
                .append("bankBranchNo", getBankBranchNo())
                .append("batchNo", getBatchNo())
                .append("bindBankAccountType", getBindBankAccountType())
                .append("businessAddress", getBusinessAddress())
                .append("businessLicenseNo", getBusinessLicenseNo())
                .append("businessScope", getBusinessScope())
                .append("cnapsno", getCnapsno())
                .append("cityCode", getCityCode())
                .append("cityOfBank", getCityOfBank())
                .append("contractEffectiveDate", getContractEffectiveDate())
                .append("contractExpireDate", getContractExpireDate())
                .append("custMngId", getCustMngId())
                .append("custNo", getCustNo())
                .append("custType", getCustType())
                .append("dataStatus", getDataStatus())
                .append("deviceMsgEntity", getDeviceMsgEntity())
                .append("durationBusinessLicense", getDurationBusinessLicense())
                .append("entAddr", getEntAddr())
                .append("entCertNo", getEntCertNo())
                .append("entCertType", getEntCertType())
                .append("entCertTypeExpireDate", getEntCertTypeExpireDate())
                .append("entCreationDate", getEntCreationDate())
                .append("entCreator", getEntCreator())
                .append("entCustConCertNoExDate", getEntCustConCertNoExDate())
                .append("entCustContCertNo", getEntCustContCertNo())
                .append("entCustContCertType", getEntCustContCertType())
                .append("entCustContName", getEntCustContName())
                .append("entCustContPhone", getEntCustContPhone())
                .append("entLegalPsCertExpireDate", getEntLegalPsCertExpireDate())
                .append("entLegalPsCertNo", getEntLegalPsCertNo())
                .append("entLegalPsCertType", getEntLegalPsCertType())
                .append("entLegalPsName", getEntLegalPsName())
                .append("entName", getEntName())
                .append("entType", getEntType())
                .append("enterpriseOwnedProvince", getEnterpriseOwnedProvince())
                .append("examineSuccessDate", getExamineSuccessDate())
                .append("externalMerchantName", getExternalMerchantName())
                .append("externalMerchantNo", getExternalMerchantNo())
                .append("fileBatchNo", getFileBatchNo())
                .append("headId", getHeadId())
                .append("industryType", getIndustryType())
                .append("interSystemTranAccount", getInterSystemTranAccount())
                .append("loginName", getLoginName())
                .append("loginType", getLoginType())
                .append("mail", getMail())
                .append("mccCode", getMccCode())
                .append("merchantAcctOpenContEmail", getMerchantAcctOpenContEmail())
                .append("merchantAcctOpenContName", getMerchantAcctOpenContName())
                .append("merchantAcctOpenContPhone", getMerchantAcctOpenContPhone())
                .append("merchantAttributes", getMerchantAttributes())
                .append("merchantCallbackurl", getMerchantCallbackurl())
                .append("merchantChainType", getMerchantChainType())
                .append("merchantFullName", getMerchantFullName())
                .append("merchantId", getMerchantId())
                .append("merchantLevel", getMerchantLevel())
                .append("merchantOpenTranReturnCode", getMerchantOpenTranReturnCode())
                .append("merchantOpenTranReturnMsg", getMerchantOpenTranReturnMsg())
                .append("merchantRegisterChannels", getMerchantRegisterChannels())
                .append("merchantResponsiblePerson", getMerchantResponsiblePerson())
                .append("merchantSettlement", getMerchantSettlement())
                .append("merchantShortlName", getMerchantShortlName())
                .append("merchantType", getMerchantType())
                .append("miDengLiuShui", getMiDengLiuShui())
                .append("minimumSettlementAmount", getMinimumSettlementAmount())
                .append("openAccountName", getOpenAccountName())
                .append("operatingOrganization", getOperatingOrganization())
                .append("orgNo", getOrgNo())
                .append("organizationCode", getOrganizationCode())
                .append("pbocmerchantNo", getPbocmerchantNo())
                .append("paperContractNo", getPaperContractNo())
                .append("paymentTime", getPaymentTime())
                .append("provinceCode", getProvinceCode())
                .append("provinceOfBank", getProvinceOfBank())
                .append("receivePaymentBankCode", getReceivePaymentBankCode())
                .append("receivingBankAccount", getReceivingBankAccount())
                .append("receivingBankName", getReceivingBankName())
                .append("recordId", getRecordId())
                .append("redundantInfo", getRedundantInfo())
                .append("remarkInfo", getRemarkInfo())
                .append("returnCode", getReturnCode())
                .append("returnMsg", getReturnMsg())
                .append("sequenceNumber", getSequenceNumber())
                .append("settleCycle", getSettleCycle())
                .append("settleCycleType", getSettleCycleType())
                .append("settleTreatmentMethod", getSettleTreatmentMethod())
                .append("settlementType", getSettlementType())
                .append("shareholderCertExpireDate", getShareholderCertExpireDate())
                .append("shareholderCertNo", getShareholderCertNo())
                .append("shareholderCertType", getShareholderCertType())
                .append("signatory", getSignatory())
                .append("specialIndustryIicenseNo", getSpecialIndustryIicenseNo())
                .append("supAgreeWithholding", getSupAgreeWithholding())
                .append("taxRegistrationNo", getTaxRegistrationNo())
                .append("tellerNo", getTellerNo())
                .append("tradingOrganizationNo", getTradingOrganizationNo())
                .append("transactionCode", getTransactionCode())
                .append("uniformSocialCreditCode", getUniformSocialCreditCode())
                .append("updateTime", getUpdateTime())
                .append("upperLevelMerchantNo", getUpperLevelMerchantNo())
                .append("url", getUrl())
                .append("walletId", getWalletId())
                .append("walletName", getWalletName())
                .append("walletType", getWalletType())
                .append("dataSources", getDataSources())
                .append("custMngOrgNo", getCustMngOrgNo())
                .append("dealId", getDealId())
                .append("returnTime", getReturnTime())
                .append("custMngEmpNum",getCustMngEmpNum())
                .toString();
    }
}
