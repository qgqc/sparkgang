package com.gcl.corporatewalletmanagement.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcl.common.annotation.Excel;
import com.gcl.common.core.domain.BaseEntity;

/**
 * 资料上传日志对象 bus_mer_file_upload_record
 *
 * @author zhaihb
 * @date 2022-01-14
 */
public class BusMerFileUploadRecord extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String id;

    /** 客户经理Id */
    @Excel(name = "客户经理Id")
    private String accountManagerId;

    /** 客户经理姓名 */
    @Excel(name = "客户经理姓名")
    private String accountManagerName;

    /** 创建日期 */
    @Excel(name = "创建日期")
    private String createdDate;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private String createdTime;

    /** 删除时间 */
    @Excel(name = "删除时间")
    private String deletedDate;

    /** 删除时间 */
    @Excel(name = "删除时间")
    private String deletedTime;

    /** 资源文件的下载URL */
    @Excel(name = "资源文件的下载URL")
    private String downloadUrl;

    /** 企业名称 */
    @Excel(name = "企业名称")
    private String entName;

    /** 商户id */
    @Excel(name = "商户id")
    private String merchantId;

    /** 资源文件MIME类型 */
    @Excel(name = "资源文件MIME类型")
    private String mimeType;

    /** 原始文件名 */
    @Excel(name = "原始文件名")
    private String originalFielName;

    /** 资源文件的相对路径 */
    @Excel(name = "资源文件的相对路径")
    private String relativePath;

    /** 资源文件类型 */
    @Excel(name = "资源文件类型")
    private String type;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updatedDate;

    /** 更新时间 */
    @Excel(name = "更新时间")
    private String updatedTime;

	/** 文件创建时间 */
	@Excel(name = "文件创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date fileCreateTime;

	/** 文件更新时间 */
	@Excel(name = "文件更新时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date fileUpdateTime;

	/** 文件删除时间 */
	@Excel(name = "文件删除时间")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date fileDeleteTime;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setAccountManagerId(String accountManagerId)
    {
        this.accountManagerId = accountManagerId;
    }

    public String getAccountManagerId()
    {
        return accountManagerId;
    }
    public void setAccountManagerName(String accountManagerName)
    {
        this.accountManagerName = accountManagerName;
    }

    public String getAccountManagerName()
    {
        return accountManagerName;
    }
    public void setCreatedDate(String createdDate)
    {
        this.createdDate = createdDate;
    }

    public String getCreatedDate()
    {
        return createdDate;
    }
    public void setCreatedTime(String createdTime)
    {
        this.createdTime = createdTime;
    }

    public String getCreatedTime()
    {
        return createdTime;
    }
    public void setDeletedDate(String deletedDate)
    {
        this.deletedDate = deletedDate;
    }

    public String getDeletedDate()
    {
        return deletedDate;
    }
    public void setDeletedTime(String deletedTime)
    {
        this.deletedTime = deletedTime;
    }

    public String getDeletedTime()
    {
        return deletedTime;
    }
    public void setDownloadUrl(String downloadUrl)
    {
        this.downloadUrl = downloadUrl;
    }

    public String getDownloadUrl()
    {
        return downloadUrl;
    }
    public void setEntName(String entName)
    {
        this.entName = entName;
    }

    public String getEntName()
    {
        return entName;
    }
    public void setMerchantId(String merchantId)
    {
        this.merchantId = merchantId;
    }

    public String getMerchantId()
    {
        return merchantId;
    }
    public void setMimeType(String mimeType)
    {
        this.mimeType = mimeType;
    }

    public String getMimeType()
    {
        return mimeType;
    }
    public void setOriginalFielName(String originalFielName)
    {
        this.originalFielName = originalFielName;
    }

    public String getOriginalFielName()
    {
        return originalFielName;
    }
    public void setRelativePath(String relativePath)
    {
        this.relativePath = relativePath;
    }

    public String getRelativePath()
    {
        return relativePath;
    }
    public void setType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return type;
    }
    public void setUpdatedDate(String updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public String getUpdatedDate()
    {
        return updatedDate;
    }
    public void setUpdatedTime(String updatedTime)
    {
        this.updatedTime = updatedTime;
    }

    public String getUpdatedTime()
    {
        return updatedTime;
    }

	public Date getFileCreateTime() {
		return fileCreateTime;
	}

	public void setFileCreateTime(Date fileCreateTime) {
		this.fileCreateTime = fileCreateTime;
	}

	public Date getFileUpdateTime() {
		return fileUpdateTime;
	}

	public void setFileUpdateTime(Date fileUpdateTime) {
		this.fileUpdateTime = fileUpdateTime;
	}

	public Date getFileDeleteTime() {
		return fileDeleteTime;
	}

	public void setFileDeleteTime(Date fileDeleteTime) {
		this.fileDeleteTime = fileDeleteTime;
	}

	@Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("accountManagerId", getAccountManagerId())
                .append("accountManagerName", getAccountManagerName())
                .append("createdDate", getCreatedDate())
                .append("createdTime", getCreatedTime())
                .append("deletedDate", getDeletedDate())
                .append("deletedTime", getDeletedTime())
                .append("downloadUrl", getDownloadUrl())
                .append("entName", getEntName())
                .append("merchantId", getMerchantId())
                .append("mimeType", getMimeType())
                .append("originalFielName", getOriginalFielName())
                .append("relativePath", getRelativePath())
                .append("type", getType())
                .append("updatedDate", getUpdatedDate())
                .append("updatedTime", getUpdatedTime())
                .toString();
    }
}
