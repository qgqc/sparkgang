package com.gcl.corporatewalletmanagement.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.gcl.common.annotation.Excel;
import com.gcl.common.core.domain.BaseEntity;

/**
 * 商户开钱包退回信息对象 bus_mer_wallet_return_info
 * 
 * @author wjw
 * @date 2022-02-16
 */
public class BusMerWalletReturnInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 客户信息 */
    @Excel(name = "客户信息")
    private String customerInfoId;

    /** 审核反馈信息 */
    @Excel(name = "审核反馈信息")
    private String feedbackInfor;

    /** 审核时间 */
    @Excel(name = "审核时间")
    private String reviewDate;

    /** 审核人 */
    @Excel(name = "审核人")
    private String reviewer;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setCustomerInfoId(String customerInfoId) 
    {
        this.customerInfoId = customerInfoId;
    }

    public String getCustomerInfoId() 
    {
        return customerInfoId;
    }
    public void setFeedbackInfor(String feedbackInfor) 
    {
        this.feedbackInfor = feedbackInfor;
    }

    public String getFeedbackInfor() 
    {
        return feedbackInfor;
    }
    public void setReviewDate(String reviewDate) 
    {
        this.reviewDate = reviewDate;
    }

    public String getReviewDate() 
    {
        return reviewDate;
    }
    public void setReviewer(String reviewer) 
    {
        this.reviewer = reviewer;
    }

    public String getReviewer() 
    {
        return reviewer;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("customerInfoId", getCustomerInfoId())
            .append("feedbackInfor", getFeedbackInfor())
            .append("reviewDate", getReviewDate())
            .append("reviewer", getReviewer())
            .toString();
    }
}
