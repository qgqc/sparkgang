package com.gcl.corporatewalletmanagement.domain.vo;

import com.gcl.corporatewalletmanagement.domain.BusMerWalletInfo;

import java.util.Date;

/**
 * 对公钱包开立申请对象包装类 bus_mer_wallet_info
 *
 * @author wjw
 * @date 2022-02-19
 */
public class BusMerWalletInfoVo extends BusMerWalletInfo
{
    //审核退回反馈信息
   private String feedbackInfo;


   private String[] updateTimeRange;


   private Date updateTimeStart;
    private Date updateTimeEnd;


    public String[] getUpdateTimeRange() {
        return updateTimeRange;
    }


    public Date getUpdateTimeStart() {
        return updateTimeStart;
    }

    public void setUpdateTimeStart(Date updateTimeStart) {
        this.updateTimeStart = updateTimeStart;
    }

    public Date getUpdateTimeEnd() {
        return updateTimeEnd;
    }

    public void setUpdateTimeEnd(Date updateTimeEnd) {
        this.updateTimeEnd = updateTimeEnd;
    }

    public void setUpdateTimeRange(String[] updateTimeRange) {
        this.updateTimeRange = updateTimeRange;
    }

    public String getFeedbackInfo() {
        return feedbackInfo;
    }

    public void setFeedbackInfo(String feedbackInfo) {
        this.feedbackInfo = feedbackInfo;
    }
}
