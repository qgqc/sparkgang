package com.gcl.corporatewalletmanagement.domain.vo;

import com.gcl.corporatewalletmanagement.domain.BusMerFileUpload;

public class BusMerFileUploadVo extends BusMerFileUpload {
    private String fileId;

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }
}
