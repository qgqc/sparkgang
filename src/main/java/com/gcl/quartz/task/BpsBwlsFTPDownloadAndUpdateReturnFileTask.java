package com.gcl.quartz.task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.gcl.bps.constant.BpsTypeConstant;
import com.gcl.bps.constant.BusPersonInfoStatus;
import com.gcl.bps.constant.ExecuteStepsAndResult;
import com.gcl.bps.domain.Base;
import com.gcl.bps.util.FileTools;
import com.gcl.bps.util.FtpUtil;
import com.gcl.personalwalletmanagement.domain.BpsBwlsDetail;
import com.gcl.personalwalletmanagement.domain.BpsBwlsHead;
import com.gcl.personalwalletmanagement.domain.BusPersonInfo;
import com.gcl.personalwalletmanagement.mapper.BpsBwlsHeadMapper;
import com.gcl.personalwalletmanagement.service.IBpsBwlsDetailService;
import com.gcl.personalwalletmanagement.service.IBpsBwlsHeadService;
import com.gcl.personalwalletmanagement.service.IBusPersonInfoService;

/******
 * @author gcl
 * @date 2022-02-11
 * 
 *
 * 
 */
@Component("bpsBwlsFTPDownloadAndUpdateReturnFileTask")
public class BpsBwlsFTPDownloadAndUpdateReturnFileTask extends Base {

	private static Logger logger = LoggerFactory.getLogger(BpsBwlsFTPDownloadAndUpdateReturnFileTask.class);

	/**
	 * 个人基本信息服务
	 */
	@Autowired
	private IBusPersonInfoService busPersonInfoService;

	@Autowired
	private IBpsBwlsHeadService bpsBwlsHeadService;

	@Autowired
	private BpsBwlsHeadMapper bpsBwlsHeadMapper;

	@Autowired
	private IBpsBwlsDetailService bpsBwlsDetailService;

	/**
	 * 查询当前时间往前几天的数据，此天数可配置，通常为1天
	 */
	@Value("${daySize}")
	private int daySize;

	/**
	 * 每个文件最大条数
	 */
	@Value("${fileSize}")
	private int fileSize;

	/**
	 * #文件名正则 sourceFile: 51.%s.%s.%s.T%s.DAT
	 */
	@Value("${sourceFile}")
	private String sourceFileName;
	@Value("${bpsDownFileLocalPath}")
	private String downloadLocalPath;
	@Value("${bpsCreatLocalPath}")
	private String bpsCreatLocalPath;

	@Value("${bpsDownFilePath}")
	private String downloadPath;
	@Value("${bpsCreatPath}")
	private String bpsCreatPath;

	@Value("${bpsFtpIp}")
	private String bpsFtpIp;
	@Value("${ftpPort}")
	private int ftpPort;
	@Value("${ftpUserName}")
	private String ftpUserName;
	@Value("${ftpPassword}")
	private String ftpPassword;

	/******
	 * 
	 */
	public void bwlsDoWork() {
		_printProcessTimeLog(BpsTypeConstant.BWLS_TYPE);
		try {

			// 查询需要下载的回盘文件 执行步骤 执行结果 2已上传，0成功 或者 3已回盘更新 1失败
			BpsBwlsHead bpsBwlsHeadForReturnFileQuery = new BpsBwlsHead();
			HashMap<String, Object> bpsBwlsHeadForReturnFileParams = new HashMap<String, Object>();
			// 结束日期
			Date endDate = new Date();
			// 开始日期
			Date beginDate = DateUtils.addDays(endDate, -daySize + 1);
			bpsBwlsHeadForReturnFileParams.put("endDate",
					com.gcl.common.utils.DateUtils.dateTime(endDate) + " 23:59:59");
			bpsBwlsHeadForReturnFileParams.put("beginDate",
					com.gcl.common.utils.DateUtils.dateTime(beginDate) + " 00:00:00");
			bpsBwlsHeadForReturnFileQuery.setParams(bpsBwlsHeadForReturnFileParams);
			bpsBwlsHeadForReturnFileQuery.setExecuteSteps(ExecuteStepsAndResult.UPLOADED);
			bpsBwlsHeadForReturnFileQuery.setExecuteResult(ExecuteStepsAndResult.SUCCESS);
			List<BpsBwlsHead> bpsBwlsHeadForReturnFileList = bpsBwlsHeadService.selectBpsBwlsHeadList(bpsBwlsHeadForReturnFileQuery);
			if (bpsBwlsHeadForReturnFileList.size() == 0) {
				_printProcessTimeLog("BpsBwlsFTPDownloadAndUpdateReturnFileTask:私人钱包注册白名单下载回盘文件条数为0");
				return;
			}
			// 创建本地download文件夹
			String downloadLocalPathBwls = downloadLocalPath + BpsTypeConstant.BWLS_TYPE;
			FileTools.CreateDir(downloadLocalPathBwls);
			// 初始化FTP，准备下载
			FtpUtil ftpUtil = new FtpUtil(bpsFtpIp, ftpPort, ftpUserName, ftpPassword, downloadPath);
			for (BpsBwlsHead b : bpsBwlsHeadForReturnFileList) {
				String fileName = ftpUtil.downloadToPath(b.getSourceFileName().replace(".DAT", ".RET"),
						downloadLocalPathBwls);
				if (!fileName.contains(b.getSourceFileName().replace("DAT", "RET"))) {
					continue;
				}
				b.setExecuteSteps(ExecuteStepsAndResult.DOWNLOADED);
				b.setExecuteResult(ExecuteStepsAndResult.SUCCESS);
				b.setFileDownloadFtpTime(new Date());
				bpsBwlsHeadService.updateBpsBwlsHead(b);
				logger.info("BpsBwlsFTPDownloadAndUpdateReturnFileTask：已下载回盘文件" + b.getSourceFileName());
				// 读取回盘文件并更新数据库
				logger.info("BpsBwlsFTPDownloadAndUpdateReturnFileTask：读取回盘文件" + b.getSourceFileName());
				File returnFile = new File(downloadLocalPath + BpsTypeConstant.BWLS_TYPE + File.separator + fileName);
				InputStream in = null;
				BufferedReader bufferedReader = null;
				Reader reader = null;
				String line = null;
				boolean isOneLine = true;
				try {
					in = new FileInputStream(returnFile);
					reader = new InputStreamReader(in, "utf-8");
					bufferedReader = new BufferedReader(reader);
					while ((line = bufferedReader.readLine()) != null) {
						if (isOneLine) {
							b.setDetailSuccessCount(Integer.valueOf(line.substring(98, 110).trim()));
							b.setDetailFailedCount(Integer.valueOf(line.substring(110, 122).trim()));
							b.setDetailUnknownCount(Integer.valueOf(line.substring(122, 134).trim()));
							b.setFileReturnCode(line.substring(134, 138).trim());
							b.setFileReturnMsg(line.substring(138, 168).trim());
							b.setFileUpdateReturnTime(new Date());
							b.setUpdateTime(new Date());
							b.setExecuteSteps(ExecuteStepsAndResult.REVERTED_TO_UPDATE);
							b.setExecuteResult(ExecuteStepsAndResult.SUCCESS);
							bpsBwlsHeadService.updateBpsBwlsHead(b);
							isOneLine = false;
							continue;
						}
						// 读取正文
						BpsBwlsDetail bpsBwlsDetail = new BpsBwlsDetail();
						bpsBwlsDetail.setSequenceNumber(Long.parseLong(line.substring(1, 13)));
						bpsBwlsDetail = bpsBwlsDetailService.selectBpsBwlsDetailList(bpsBwlsDetail).get(0);
						bpsBwlsDetail.setReturnCode(line.substring(466, 474).trim());
						bpsBwlsDetail.setReturnMsg(line.substring(466, 534).trim());
						bpsBwlsDetail.setMiDengLiuShui(line.substring(534, 581).trim());
						BusPersonInfo busPersonInfo = new BusPersonInfo();
						busPersonInfo.setId(bpsBwlsDetail.getBusPersonInfoId());
						busPersonInfo.setUpdateTime(new Date());
						busPersonInfo.setDataStatus(bpsBwlsDetail.getReturnCode().equals("00000000")
								?BusPersonInfoStatus.BWLS_SUCCESSFUL_OPENING.getStatus()
										:BusPersonInfoStatus.BWLS_OPENING_FAILED.getStatus());
						bpsBwlsDetailService.updateBpsBwlsDetail(bpsBwlsDetail);
						// busPersonInfoService.updateBusPersonInfo(busPersonInfo);
					}
					logger.info("--Success:{} ", b.getSourceFileName());
				} catch (Exception e) {
					logger.error("download file and update db file Exception:{} error:{}" + b.getSourceFileName(), e);
				} finally {
					try {
						bufferedReader.close();
						reader.close();
						in.close();
					} catch (IOException e) {
						logger.error("download file and update db file:{} error:{}" + b.getSourceFileName(), e);
					}
				}

			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("BWLSdoWork Exception:" + ex);
		}

	}

	/*****
	 * 打印处理时间日志
	 * 
	 * @param typeStr
	 */
	private void _printProcessTimeLog(String typeStr) {
		String nowTimeStr = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
		logger.info("执行" + typeStr + "定时任务,执行时间：" + nowTimeStr);

	}

}
