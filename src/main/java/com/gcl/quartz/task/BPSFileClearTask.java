package com.gcl.quartz.task;


import com.gcl.common.utils.StringUtils;
import com.gcl.corporatewalletmanagement.service.IBusBpsFilehandlService;
import com.gcl.system.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/*******
 * BPS 文件清理 定时任务
 */
@Component("bpsFileClearTask")
public class BPSFileClearTask {

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private IBusBpsFilehandlService busBpsFilehandlService;

    /******
     * 清理CWFS定时任务 生成的DAT文件 和 回盘的RET文件
     * 清理X天前的文件，X参数为若依系统 参数设置 配置的
     */
    public void clearCwfsDATAndRETFile(){
        String cwfsSaveDays =  configService.selectConfigByKey("bus.mer.wallet.bpsfile.cwfs.save.days");
        if(StringUtils.isNotBlank(cwfsSaveDays)){
            System.out.println("BPS CWFS 生成的DAT文件 和 回盘的RET文件 将被保留" + cwfsSaveDays + "天，" + cwfsSaveDays + "天前的将会被删除！");
            busBpsFilehandlService.clearDATAndRETFileBefoerDays(cwfsSaveDays);
        }
    }



}
