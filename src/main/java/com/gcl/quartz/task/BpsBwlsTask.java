package com.gcl.quartz.task;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.gcl.bps.constant.BpsTypeConstant;
import com.gcl.bps.constant.BusPersonInfoStatus;
import com.gcl.bps.constant.ExecuteStepsAndResult;
import com.gcl.bps.domain.Base;
import com.gcl.bps.domain.OrgNoAndTellerNo;
import com.gcl.bps.util.FileTools;
import com.gcl.bps.util.FtpUtil;
import com.gcl.common.utils.uuid.IdUtils;
import com.gcl.personalwalletmanagement.domain.BpsBwlsDetail;
import com.gcl.personalwalletmanagement.domain.BpsBwlsHead;
import com.gcl.personalwalletmanagement.domain.BusPersonInfo;
import com.gcl.personalwalletmanagement.mapper.BpsBwlsHeadMapper;
import com.gcl.personalwalletmanagement.service.IBpsBwlsDetailService;
import com.gcl.personalwalletmanagement.service.IBpsBwlsHeadService;
import com.gcl.personalwalletmanagement.service.IBusPersonInfoService;

/******
 * @author gcl
 * @date 2022-02-11
 * 
 * 
 */
@Component("bpsBwlsTask")
public class BpsBwlsTask extends Base {

	private static Logger logger = LoggerFactory.getLogger(BpsBwlsTask.class);

	/**
	 * 个人基本信息服务
	 */
	@Autowired
	private IBusPersonInfoService busPersonInfoService;

	@Autowired
	private IBpsBwlsHeadService bpsBwlsHeadService;

	@Autowired
	private BpsBwlsHeadMapper bpsBwlsHeadMapper;

	@Autowired
	private IBpsBwlsDetailService bpsBwlsDetailService;

	/**
	 * 查询当前时间往前几天的数据，此天数可配置，通常为1天
	 */
	@Value("${daySize}")
	private int daySize;

	/**
	 * 每个文件最大条数
	 */
	@Value("${fileSize}")
	private int fileSize;

	/**
	 * #文件名正则 sourceFile: 51.%s.%s.%s.T%s.DAT
	 */
	@Value("${sourceFile}")
	private String sourceFileName;
	@Value("${bpsDownFileLocalPath}")
	private String downloadLocalPath;
	@Value("${bpsCreatLocalPath}")
	private String bpsCreatLocalPath;

	@Value("${bpsDownFilePath}")
	private String downloadPath;
	@Value("${bpsCreatPath}")
	private String bpsCreatPath;

	@Value("${bpsFtpIp}")
	private String bpsFtpIp;
	@Value("${ftpPort}")
	private int ftpPort;
	@Value("${ftpUserName}")
	private String ftpUserName;
	@Value("${ftpPassword}")
	private String ftpPassword;

	/******
	 * 
	 */
	public void bwlsDoWork() {
		_printProcessTimeLog(BpsTypeConstant.BWLS_TYPE);
		try {
			// 从个人基本信息中查出daySize天内，并且状态是审核通过的数据
			BusPersonInfo busPersonInfoReviewPassed = new BusPersonInfo();
			// 审核通过 REVIEW_PASSED("2","审核通过"),
			busPersonInfoReviewPassed.setDataStatus(BusPersonInfoStatus.REVIEW_PASSED.getStatus());
			HashMap<String, Object> params = new HashMap<String, Object>();
			// 结束时间
			Date endTime = new Date();
			// 开始时间
			Date beginTime = DateUtils.addDays(endTime, -daySize + 1);
			params.put("endTime", com.gcl.common.utils.DateUtils.dateTime(endTime) + " 23:59:59");
			params.put("beginTime", com.gcl.common.utils.DateUtils.dateTime(beginTime) + " 00:00:00");
			busPersonInfoReviewPassed.setParams(params);
			// 查出已审核通过的数据
			List<BusPersonInfo> busPersonInfoList = busPersonInfoService
					.selectBusPersonInfoList(busPersonInfoReviewPassed);
			int busPersonInfoListCount = busPersonInfoList.size();
			if (busPersonInfoListCount == 0) {
				logger.info("私人钱包注册白名单执行任务条数为0");
				_printProcessTimeLog(BpsTypeConstant.BWLS_TYPE);
				return;
			}
			for (BusPersonInfo b : busPersonInfoList) {
				// BWLS_SYSTEM_PROCESSING("7","钱包注册白名单开立系统处理中"),
				BusPersonInfo busPersonInfoBwlsSystemProcessing = new BusPersonInfo();
				busPersonInfoBwlsSystemProcessing.setId(b.getId());
				busPersonInfoBwlsSystemProcessing.setDataStatus(BusPersonInfoStatus.BWLS_SYSTEM_PROCESSING.getStatus());
				busPersonInfoBwlsSystemProcessing.setUpdateTime(new Date());
				busPersonInfoService.updateBusPersonInfo(busPersonInfoBwlsSystemProcessing);
			}

			// 生成文件批次号,20220128144630 FILE_BATCH_NO
			String fileBatchNo = com.gcl.common.utils.DateUtils.dateTimeNow();
			for (BusPersonInfo b : busPersonInfoList) {
				BpsBwlsDetail bpsBwlsDetail = new BpsBwlsDetail();
				// ID
				bpsBwlsDetail.setId(IdUtils.fastSimpleUUID());
				// 记录标识
				bpsBwlsDetail.setRecordId("D");
				// 顺序号
				bpsBwlsDetail.setSequenceNumber(busPersonInfoService.getSeqBpsSequenceNumbeValue());
				// 交易机构号
				bpsBwlsDetail.setTradingOrganizationNo(b.getOrgId());// 27005L
				// 交易码
				bpsBwlsDetail.setTransactionCode("TF0013");// TF0013:批量导入白名单
				// 邮箱地址 为空
				// 电话号码
				bpsBwlsDetail.setPhone(b.getMobileNumber());
				// 国际电话区号
				bpsBwlsDetail.setPhoneAreaCode("86");
				// 电话城市区号为空
				// 姓名
				bpsBwlsDetail.setCustName(b.getCustName());
				// 证件类型
				bpsBwlsDetail.setIdCardType(b.getIdCardType());
				// 证件号
				bpsBwlsDetail.setIdCard(b.getIdCard());
				// 预留信息
				bpsBwlsDetail.setReservedInfo(b.getReservedInfo());
				// 员工姓名(创建人)
				bpsBwlsDetail.setStaffName(b.getStaffName());
				// 客户类型，00中国银行
				bpsBwlsDetail.setCustomerType("00");
				// 机构号
				bpsBwlsDetail.setCoreOrgNo(b.getOrgId());
				// 用户所属省编码
				bpsBwlsDetail.setCustProvinceCode(b.getProvinceCode());
				// 用户所属市编码
				bpsBwlsDetail.setCustCityType(b.getProvinceOfUser());
				// 变更类型
				bpsBwlsDetail.setChangeType("CC00");
				// 备注
				bpsBwlsDetail.setRemarkInfo(b.getReservedInfo());
				// 创建人
				// 创建时间
				// 更新人
				// 最后更新时间
				bpsBwlsDetail.setCreateBy("BpsBwlsTask");
				bpsBwlsDetail.setCreateTime(new Date());
				bpsBwlsDetail.setUpdateBy("BpsBwlsTask");
				bpsBwlsDetail.setUpdateTime(new Date());
				// 文件头主键
				// bpsBwlsDetail.setBpsPwlsHeadId(bpsBwlsHeadID);
				// 文件批次号
				bpsBwlsDetail.setFileBatchNo(fileBatchNo);
				// 交易柜员号
				// 交易终端号
				bpsBwlsDetail.setTransactionTellerNumber(b.getTransactionTellerNumber());
				bpsBwlsDetail.setTradingTerminalNumber(b.getTradingTerminalNumber());
				bpsBwlsDetail.setBusPersonInfoId(b.getId());
				bpsBwlsDetailService.insertBpsBwlsDetail(bpsBwlsDetail);
			}
			// 通过交易机构号和柜员号进行分组，查出共有几组，遍历生成文件，不同的交易机构号和柜员号不能生成在同一文件中
			List<OrgNoAndTellerNo> orgNoAndTellerNoList = bpsBwlsDetailService
					.selectOrgNoAndTellerNoListByFileBatchNo(fileBatchNo);
			logger.error("新建文件前准备,文件批次号：" + fileBatchNo);
			for (OrgNoAndTellerNo orgNoAndTellerNo : orgNoAndTellerNoList) {
				// 查询出当前条件下的所有明细
				BpsBwlsDetail bpsBwlsDetailOrgNoAndTellerNo = new BpsBwlsDetail();
				bpsBwlsDetailOrgNoAndTellerNo.setFileBatchNo(fileBatchNo);
				bpsBwlsDetailOrgNoAndTellerNo.setTradingOrganizationNo(orgNoAndTellerNo.getTradingOrganizationNo());
				bpsBwlsDetailOrgNoAndTellerNo.setTransactionTellerNumber(orgNoAndTellerNo.getTransactionTellerNumber());
				bpsBwlsDetailOrgNoAndTellerNo.setTradingTerminalNumber(orgNoAndTellerNo.getTradingTerminalNumber());
				// 查询出对应的明细
				List<BpsBwlsDetail> bpsBwlsDetailOrgNoAndTellerNoList = bpsBwlsDetailService
						.selectBpsBwlsDetailList(bpsBwlsDetailOrgNoAndTellerNo);
				try {
					// 获取当前分组后，此组的条数
					// long orgNoAndTellerNoCount = orgNoAndTellerNo.getCount(); 7
					int orgNoAndTellerNoCount = bpsBwlsDetailOrgNoAndTellerNoList.size();
					// 获取当前条数余数 1
					int remainder = orgNoAndTellerNoCount % fileSize;
					// 计算出几个文件 3
					int fileNum = (new Double(Math.ceil((double) orgNoAndTellerNoCount / fileSize))).intValue();
					for (int i = 0; i < fileNum; i++) {
						// 默认当前批次号时当天的第一批，则001
						int batchNo = 1;
						// 查询当天日期已生成批次号的最大值,文件创建时间转为YYYYMMDD
						// FILE_CREATION_TIME等于当前日期,TRADING_ORGANIZATION_NO 交易机构号为当前orgNoAndTellerNo中的
						// 机构号，EXECUTE_STEPS 执行步骤,0待生成, 1已生成，2已上传，3已回盘更新 是1已生成，EXECUTE_RESULT
						// 执行结果,执行步骤的执行结果 0:成功，1:失败 是0:成功，通过文件批次号倒排
						// 取第一个就是最大值
						BpsBwlsHead bpsBwlsHeadMaxBatchQuery = new BpsBwlsHead();
						HashMap<String, Object> fileCreationTimeParams = new HashMap<String, Object>();
						// 结束日期
						Date endDate = new Date();
						// 开始日期
						Date beginDate = DateUtils.addDays(endDate, -daySize + 1);
						fileCreationTimeParams.put("endDate",
								com.gcl.common.utils.DateUtils.dateTime(endDate) + " 23:59:59");
						fileCreationTimeParams.put("beginDate",
								com.gcl.common.utils.DateUtils.dateTime(beginDate) + " 00:00:00");
						bpsBwlsHeadMaxBatchQuery.setParams(fileCreationTimeParams);
						BpsBwlsHead bpsBwlsHeadMaxBatch = bpsBwlsHeadMapper
								.findMaxBatchNoByCurrentDateAndOrgNo(bpsBwlsHeadMaxBatchQuery);
						// 如果不为空，则最大值赋给当前批次号
						if (bpsBwlsHeadMaxBatch != null) {
							batchNo = bpsBwlsHeadMaxBatch.getDataBatchNumber() + 1;
							if (batchNo > 999) {
								logger.error("BpsBwls当日批次号超出999！！！");
							}
						}
						// detailTotal 明细合计笔数
						int detailTotal = 0;
						if ((i == fileNum - 1) && remainder != 0) {
							detailTotal = remainder;
						} else {
							detailTotal = fileSize;
						}
						// 生成BPS_BWLS_HEAD文件头数据
						String bpsBwlsHeadID = IdUtils.fastSimpleUUID();
						BpsBwlsHead bpsBwlsHead = new BpsBwlsHead();
						bpsBwlsHead.setId(bpsBwlsHeadID);
						// 记录标识,H为头文件
						bpsBwlsHead.setRecordId("H");
						// SEQUENCE_NUMBER 顺序号,从1开始,数据库中获取序列SEQ_BPS_SEQUENCE_NUMBER.NEXTVAL
						bpsBwlsHead.setSequenceNumber(busPersonInfoService.getSeqBpsSequenceNumbeValue());
						// 交易机构号 TRADING_ORGANIZATION_NO 交易机构号,数据提交行机构号
						bpsBwlsHead.setTradingOrganizationNo(orgNoAndTellerNo.getTradingOrganizationNo());
						// 明细合计比数
						bpsBwlsHead.setDetailTotal(detailTotal);
						bpsBwlsHead.setChannelNo("T");
						bpsBwlsHead.setTransactionTellerNumber(orgNoAndTellerNo.getTransactionTellerNumber());
						bpsBwlsHead.setTradingTerminalNumber(orgNoAndTellerNo.getTradingTerminalNumber()); // 需要同机构号和柜员号一样？
						// 当天处理的第几批数据001
						bpsBwlsHead.setDataBatchNumber(batchNo);
						bpsBwlsHead.setFileHandlingType(BpsTypeConstant.BWLS_TYPE);
						// 待生成
						bpsBwlsHead.setExecuteSteps("0");
						// 成功
						bpsBwlsHead.setExecuteResult("0");
						bpsBwlsHead.setUpdateTime(new Date());
						bpsBwlsHead.setFileBatchNo(fileBatchNo);
						// file_creation_time date y 文件创建时间
						Date fileCreationTimeDate = new Date();
						bpsBwlsHead.setFileCreationTime(fileCreationTimeDate);
						// 文件名 正则是#文件名正则sourceFile: 51.%s.%s.%s.T%s.DAT
						// 上送文件命名：来源系统标识+.+文件类型+.+机构号+.+顺序号+.+”T“MMDD.DAT （机构号栏位可填写字母或数字）
						// 上送文件举例：51.CWFS.02078.001.T0716.DAT
						String sourceFileNameForCreate = String.format(sourceFileName, BpsTypeConstant.BWLS_TYPE,
								bpsBwlsHead.getTradingOrganizationNo(),
								getNumberFormat(3, bpsBwlsHead.getDataBatchNumber().toString()),
								com.gcl.common.utils.DateUtils.dateTimeYYYYMMDDHHMMSS(fileCreationTimeDate)
										.substring(4, 8));
						bpsBwlsHead.setSourceFileName(sourceFileNameForCreate);
						// 插入头文件表
						bpsBwlsHeadService.insertBpsBwlsHead(bpsBwlsHead);
						logger.info("对私钱包批量注册白名单BpsBwlsTask生成文件:" + bpsBwlsHead.getSourceFileName());
						File file = new File(bpsCreatLocalPath + File.separator + BpsTypeConstant.BWLS_TYPE
								+ File.separator
								+ bpsBwlsHead.getSourceFileName());
						// 生成文件中的第一行，文件头信息
						StringBuffer stringBufferFile = new StringBuffer();
						// 1 ID ID VARCHAR2(32) M
						// 2 记录标识 RECORD_ID VARCHAR2(1) M
						stringBufferFile.append(bpsBwlsHead.getRecordId());
						// 3 顺序号 SEQUENCE_NUMBER H NUMBER(12) M
						stringBufferFile.append(getNumberFormat(12, bpsBwlsHead.getSequenceNumber().toString()));
						// 4 源文件名称 SOURCE_FILE_NAME VARCHAR2(50) M
						stringBufferFile.append(getOneVsSpace(50, bpsBwlsHead.getSourceFileName()));
						// 5 交易机构号 TRADING_ORGANIZATION_NO NUMBER(5) M
						stringBufferFile.append(getOneVsSpace(5, bpsBwlsHead.getTradingOrganizationNo().toString()));
						// 6 明细合计笔数 DETAIL_TOTAL NUMBER(12) M
						stringBufferFile.append(getNumberFormat(12, bpsBwlsHead.getDetailTotal().toString()));
						// 7 渠道号 CHANNEL_NO VARCHAR2(1) M
						stringBufferFile.append(getOneVsSpace(1, bpsBwlsHead.getChannelNo()));
						// 8 交易柜员号 TRANSACTION_TELLER_NUMBER NUMBER(7) M
						stringBufferFile
								.append(getNumberFormat(7, bpsBwlsHead.getTransactionTellerNumber().toString()));
						// 9 交易终端号 TRADING_TERMINAL_NUMBER NUMBER(3) M
						stringBufferFile.append(getNumberFormat(3, bpsBwlsHead.getTradingTerminalNumber().toString()));
						// 10 数据批次号 DATA_BATCH_NUMBER NUMBER(3) M
						stringBufferFile.append(getNumberFormat(3, bpsBwlsHead.getDataBatchNumber().toString()));
						// 11 文件处理类型 FILE_HANDLING_TYPE VARCHAR2(4) M
						stringBufferFile.append(bpsBwlsHead.getFileHandlingType());
						// 12 明细成功笔数 DETAIL_SUCCESS_COUNT NUMBER(12) ABC返回填写
						stringBufferFile.append(getNumberFormat(12, "0"));
						// 13 明细失败笔数 DETAIL_FAILED_COUNT NUMBER(12) ABC返回填写
						stringBufferFile.append(getNumberFormat(12, "0"));
						// 14 明细未明笔数 DETAIL_UNKNOWN_COUNT NUMBER(12) ABC返回填写
						stringBufferFile.append(getNumberFormat(12, "0"));
						// 15 文件返回信息码 FILE_RETURN_CODE VARCHAR2(4) ABC返回填写
						stringBufferFile.append(getOneVsSpace(4, ""));
						// 16 文件返回信息 FILE_RETURN_MSG VARCHAR2(30) ABC返回填写
						stringBufferFile.append(getOneVsSpace(30, ""));
						// 17 备注 REMARK VARCHAR2(64) O
						stringBufferFile.append(getOneVsSpace(64, bpsBwlsHead.getRemark()));
						// 18 冗余域 REDUNDANT_INFO VARCHAR2(100) O
						stringBufferFile.append(getOneVsSpace(100, bpsBwlsHead.getRedundantInfo()));
						// 换行
						stringBufferFile.append("\n");
						// logger.info(stringBufferFile.toString());
						// 遍历符合条件的明细，写入到文件中，将文件头ID插入到明细中
						int chushizhi = i * fileSize;
						for (int j = chushizhi; (i + 1 != fileNum) ? (j < (i + 1) * fileSize)
								: (j < orgNoAndTellerNoCount); j++) {
							// logger.info(bpsBwlsDetailOrgNoAndTellerNoList.get(j).toString());
							BpsBwlsDetail bpsBwlsDetailForCreate = bpsBwlsDetailOrgNoAndTellerNoList.get(j);
							// 写入到文件中
							// 记录标识 1 String 1 记录标识 M H 为头纪录，D为明细
							stringBufferFile.append(bpsBwlsDetailForCreate.getRecordId());
							// 顺序号 2 Number 12 顺序号 M 顺序号，从2开始；
							stringBufferFile
									.append(getNumberFormat(12, bpsBwlsDetailForCreate.getSequenceNumber().toString()));
							// 交易机构号 14 Number 5 交易机构号 M 交易机构号
							stringBufferFile.append(
									getOneVsSpace(5, bpsBwlsDetailForCreate.getTradingOrganizationNo().toString()));
							// 交易码 19 String 6 交易码 M TF0013:批量导入白名单
							stringBufferFile.append(bpsBwlsDetailForCreate.getTransactionCode());
							// 邮箱地址 25 String 64 邮箱地址 C 当上送邮箱信息时，电话号码、国家电话区号可不上送 邮箱地址
							stringBufferFile.append(getOneVsSpace(64, ""));
							// 电话号码 89 String 64 电话号码 C 当上送电话号码时，国际电话区号必须上送 电话号码，仅可上送数字
							stringBufferFile.append(getOneVsSpace(64, bpsBwlsDetailForCreate.getPhone()));
							// 国际电话区号 153 String 5 国际电话区号 C 当上送电话号码时，国际电话区号必须上送 国际电话区号，仅可上送数字，如果不输入，默认86
							stringBufferFile.append(getOneVsSpace(5, bpsBwlsDetailForCreate.getPhoneAreaCode()));
							// 电话城市区号 158 String 5 电话城市区号 O 电话城市区号
							stringBufferFile.append(getOneVsSpace(5, bpsBwlsDetailForCreate.getPhoneCityCode()));
							// 姓名 163 String 64 姓名 O 姓名 允许中文，最多64个汉字
							stringBufferFile.append(getOneVsSpace(64, bpsBwlsDetailForCreate.getCustName()));
							// 证件类型 227 String 10
							stringBufferFile.append(getOneVsSpace(10, bpsBwlsDetailForCreate.getIdCardType()));
							// 证件号 237 String 50 证件号 O 证件号
							stringBufferFile.append(getOneVsSpace(50, bpsBwlsDetailForCreate.getIdCard()));
							// 预留信息 287 String 75 预留信息 O 预留信息 允许中文，最多75个汉字
							stringBufferFile.append(getOneVsSpace(75, bpsBwlsDetailForCreate.getReservedInfo()));
							// 创建人 362 String 75 创建人 O 创建人 允许中文，最多75个汉字
							stringBufferFile.append(getOneVsSpace(75, bpsBwlsDetailForCreate.getStaffName()));
							// 客户类型 437 String 2 客户类型 C 00:中国银行,01:中国电信，02:中国联通
							stringBufferFile.append(getOneVsSpace(2, bpsBwlsDetailForCreate.getCustomerType()));
							// 机构号 439 String 12 机构号 O 核心机构号，所属银行5位机构号，仅中国银行有数据，中国电信与中国联通该栏位可为空
							stringBufferFile.append(getOneVsSpace(12, bpsBwlsDetailForCreate.getCoreOrgNo()));
							// 用户所属省编码 451 String 6 用户所属省编码 C 6位数的省编码，参见 国家行政区划编码
							stringBufferFile.append(getOneVsSpace(6, bpsBwlsDetailForCreate.getCustProvinceCode()));
							// 用户所属市编码 457 String 6 用户所属市编码 C 6位数的城市编码，参见 国家行政区划编码
							stringBufferFile.append(getOneVsSpace(6, bpsBwlsDetailForCreate.getCustCityType()));
							// 变更类型 463 String 4 变更类型 M CC00：新增 CC03：删除
							stringBufferFile.append(getOneVsSpace(4, bpsBwlsDetailForCreate.getChangeType()));
							// 返回信息码 467 String 8 过账交易返回信息码 ABC返回填写 "00000000－交易成功，
							// 00000001－交易未明，
							// 00000002－交易失败"
							stringBufferFile.append(getOneVsSpace(8, bpsBwlsDetailForCreate.getReturnCode()));
							// 返回信息 475 String 60 过账交易返回信息 ABC返回填写 返回成功或错误信息；上送时填空格
							stringBufferFile.append(getOneVsSpace(60, bpsBwlsDetailForCreate.getReturnMsg()));
							// 幂等流水 535 String 47 幂等流水 O
							// "选输项，上送时填写外围的幂等流水，若填写值不为空（包括全0），则ABC交易处理时使用该幂等流水，ABC不对此值进行合法性检查；若填写为空，则ABC交易时使用ABC按现有机制生成的幂等流水。
							stringBufferFile.append(getOneVsSpace(47, bpsBwlsDetailForCreate.getMiDengLiuShui()));
							// 备注 582 String 64 备注 O 备注信息
							stringBufferFile.append(getOneVsSpace(64, bpsBwlsDetailForCreate.getRemarkInfo()));
							// 冗余域 646 String 100 冗余域 O 暂不使用，上送时填空格。
							stringBufferFile.append(getOneVsSpace(64, bpsBwlsDetailForCreate.getRedundantInfo()));
							stringBufferFile.append("\n");
							FileTools.writeToTxtEncoding(stringBufferFile.toString(), file, ENCODING);

							// 更新bps_pwls_head_id varchar2(32) y 文件头主键,人行共建app白名单批量文件头表主键
							bpsBwlsDetailForCreate.setBpsPwlsHeadId(bpsBwlsHeadID);
							bpsBwlsDetailForCreate.setUpdateTime(new Date());
							bpsBwlsDetailService.updateBpsBwlsDetail(bpsBwlsDetailForCreate);
						}
						// 生成完成后更新头文件 1已生成 0:成功
						bpsBwlsHead.setExecuteSteps(ExecuteStepsAndResult.GENERATED);
						bpsBwlsHead.setExecuteResult(ExecuteStepsAndResult.SUCCESS);
						bpsBwlsHead.setUpdateTime(new Date());
						bpsBwlsHeadService.updateBpsBwlsHead(bpsBwlsHead);
						// todo 将生成的文件上传到FTP服务器上
						// #上传(BPS本地生成DAT文件的存储目录)
						// bpsCreatLocalPath: 'D:/data/bps/upfile/'
						// #下载(BPS本地下载RET文件的存储目录)
						// bpsDownFileLocalPath: 'D:/data/bps/download/'
						// #BPS系统 远程FTP服务器 上传目录
						// bpsCreatPath: '/upload'
						// #BPS系统 远程FTP服务器 下载回盘RET文件目录
						// bpsDownFilePath: '/download'
						// #本地环境 ftp
						// bpsFtpIp: 180.76.237.67
						// ftpPort: 21
						// ftpUserName: szrmb
						// ftpPassword: szrmb@2022!
						// 初始化FTP服务器
						FtpUtil ftpUtil = new FtpUtil(bpsFtpIp, ftpPort, ftpUserName, ftpPassword, bpsCreatPath);
						String fileString = ftpUtil.uploadFile(file);
						logger.info("BpsBwlsTask423:" + fileString + "已上传FTP");
						// 上传完成后更新头文件 2已生成 0:成功
						bpsBwlsHead.setExecuteSteps(ExecuteStepsAndResult.UPLOADED);
						bpsBwlsHead.setExecuteResult(ExecuteStepsAndResult.SUCCESS);
						bpsBwlsHead.setFileUploadFtpTime(new Date());
						bpsBwlsHead.setUpdateTime(new Date());
						bpsBwlsHeadService.updateBpsBwlsHead(bpsBwlsHead);
						continue;
					}
				} catch (Exception e) {
					logger.error("BWLS生成文件和上传出错：{}", e);
				}
				continue;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("BWLSdoWork Exception:" + ex);
		}

	}

	/*****
	 * 打印处理时间日志
	 * 
	 * @param typeStr
	 */
	private void _printProcessTimeLog(String typeStr) {
		String nowTimeStr = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
		logger.info("执行" + typeStr + "定时任务,执行时间：" + nowTimeStr);

	}

}
