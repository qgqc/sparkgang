package com.gcl.quartz.task;

import com.gcl.bps.constant.BpsTypeConstant;
import com.gcl.bps.service.IBpsDoWorkService;
import com.gcl.bps.service.IPBpsLogService;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

/******
 * @author wjw
 * @date 2022-01-17
 *
 *
 * BPS定时任务
 * 写入DAT文件
 * 读取RET回盘文件
 *
 * 执行任务，需在Ruoyi控制台添加该任务，在配置文件设置标志位true后，执行
 */
@Component("bpsProcessTask")
public class BPSProcessTask {
    private static Logger logger = LoggerFactory.getLogger(BPSProcessTask.class);

    @Autowired
    private IBpsDoWorkService bpsDoWorkService;

    @Autowired
    private IPBpsLogService pBpsLogService;

    @Value("${cwfs.doWork}")
    private Boolean cwfs;

    @Value("${pwls.doWork}")
    private Boolean pwls;

    @Value("${bwls.doWork}")
    private Boolean bwls;


    @Value("${ppfs.doWork}")
    private Boolean ppfs;
    /******
     * cwfs回盘文件生成、上传、读取 定时任务
     *
     */
    public void cwfsDoWork(){
        if(cwfs){
            _printProcessTimeLog(BpsTypeConstant.CWFS_TYPE);
            try {
                bpsDoWorkService.doWorkCWFS();
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.error("CWFS doWork Exception:" + ex);
            }
        }

    }


    /******
     * 私人钱包开通  pwls--》bwls--》ppfs
     */
    /**
     * 共建APP白名单PWLS  回盘文件生成、上传、读取 定时任务
     */
    public void pwlsDoWork(){
        if (pwls) {
            _printProcessTimeLog(BpsTypeConstant.PWLS_TYPE);
            try {
                bpsDoWorkService.doWorkPWLS();
            } catch (Exception ex) {
                logger.error("configureTasks-pwls-creatFile error:" + ex);
            }
        }
    }


    /*******
     * 钱包注册白名单 BWLS  回盘文件生成、上传、读取 定时任务
     */
    public void bwlsDoWork(){
        if (bwls) {
            _printProcessTimeLog(BpsTypeConstant.BWLS_TYPE);
            try {
                bpsDoWorkService.doWorkBWLS();
            } catch (Exception ex) {
                logger.error("configureTasks-bwls-creatFile error:" + ex);
            }
        }
    }


    /******
     * 私人钱包开立
     */
    public void ppfsDoWork(){
        if (ppfs) {
            _printProcessTimeLog(BpsTypeConstant.PPFS_TYPE);
            try {
                bpsDoWorkService.doWorkPPFS();
            } catch (Exception ex) {
                logger.error("configureTasks-ppfs-creatFile error:" + ex);
            }
        }
    }





    /******
     * 用户商户白名单
     */
    public void wlfsDoWork(){
        if (ppfs) {
            _printProcessTimeLog(BpsTypeConstant.WLFS_TYPE);
            try {
                bpsDoWorkService.doWorkWLFS();
            } catch (Exception ex) {
                logger.error("configureTasks-wlfs-creatFile error:" + ex);
            }
        }
    }

    /******
     * BPS模块日志处理   数据库写日志
     * 日志清理执行时间 原bps系统里配置为：0 2 12 * * ?
     * 该系统中可在若依管理后台配置
     */
    public void logDealWork(){
        try {
            logger.info("BPS 定时日志处理任务执行...");
            pBpsLogService.logDeal();
        } catch (Exception ex) {
            logger.error("logDealWork error:" + ex);
        }

    }


    /*****
     * 打印处理时间日志
     * @param typeStr
     */
    private void _printProcessTimeLog(String typeStr){
        String nowTimeStr = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        logger.info("执行"+ typeStr +"定时任务,执行时间：" + nowTimeStr);

    }

}
