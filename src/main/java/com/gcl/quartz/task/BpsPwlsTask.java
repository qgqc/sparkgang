package com.gcl.quartz.task;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.gcl.bps.constant.BpsTypeConstant;
import com.gcl.bps.constant.BusPersonInfoStatus;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.utils.uuid.IdUtils;
import com.gcl.personalwalletmanagement.domain.BpsPwlsDetail;
import com.gcl.personalwalletmanagement.domain.BpsPwlsHead;
import com.gcl.personalwalletmanagement.domain.BusPersonInfo;
import com.gcl.personalwalletmanagement.service.IBpsPwlsDetailService;
import com.gcl.personalwalletmanagement.service.IBpsPwlsHeadService;
import com.gcl.personalwalletmanagement.service.IBusPersonInfoService;

/******
 * @author gcl
 * @date 2022-02-11
 * 
 *       PWLS 对私批量中的人行共建APP白名单BPS调用流程 将已审核的个人基本信息查出来
 *       打上批量标记插入到BPS_PWLS_DETAIL明细表中 将明细表中的同一批次数据通过柜员号和终端号排序 BPS定时任务 写入DAT文件
 *       读取RET回盘文件
 *
 * 
 */
@Component("bpsPwlsTask")
public class BpsPwlsTask extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(BpsPwlsTask.class);

	/**
	 * 个人基本信息服务
	 */
    @Autowired
	private IBusPersonInfoService busPersonInfoService;

    @Autowired
	private IBpsPwlsHeadService bpsPwlsHeadService;

	@Autowired
	private IBpsPwlsDetailService bpsPwlsDetailService;

	/**
	 * 查询当前时间往前几天的数据，此天数可配置，通常为1天
	 */
	@Value("${daySize}")
	private int daySize;

    /******
     * cwfs回盘文件生成、上传、读取 定时任务
     */
	public void pwlsDoWork() {
			_printProcessTimeLog(BpsTypeConstant.PWLS_TYPE);
            try {
			// 从个人基本信息中查出daySize天内，并且状态是审核通过的数据
			BusPersonInfo busPersonInfoReviewPassed = new BusPersonInfo();
			// 审核通过 REVIEW_PASSED("2","审核通过"),
			busPersonInfoReviewPassed.setDataStatus(BusPersonInfoStatus.REVIEW_PASSED.getStatus());
			HashMap<String, Object> params = new HashMap<String, Object>();
			// 结束时间
			Date endTime = new Date();
			// 开始时间
			Date beginTime = DateUtils.addDays(endTime, -daySize);
			params.put("endTime", com.gcl.common.utils.DateUtils.dateTimeYYYY_MM_DD_HH_MM_SS(endTime));
			params.put("beginTime", com.gcl.common.utils.DateUtils.dateTimeYYYY_MM_DD_HH_MM_SS(beginTime));
			busPersonInfoReviewPassed.setParams(params);
			// 查出已审核通过的数据
			List<BusPersonInfo> busPersonInfoList = busPersonInfoService
					.selectBusPersonInfoList(busPersonInfoReviewPassed);
			int busPersonInfoListCount = busPersonInfoList.size();
			if (busPersonInfoListCount == 0) {
				logger.info("人行共建APP白名单可执行任务条数为0");
				_printProcessTimeLog(BpsTypeConstant.PWLS_TYPE);
				return;
			}
			for (BusPersonInfo b : busPersonInfoList) {
				// 将已审核通过的数据状态更新为PWLS_SYSTEM_PROCESSING("4","人行共建APP白名单系统处理中")
				BusPersonInfo busPersonInfoPwlsSystemProcessing = new BusPersonInfo();
				busPersonInfoPwlsSystemProcessing.setId(b.getId());
				busPersonInfoPwlsSystemProcessing.setDataStatus(BusPersonInfoStatus.PWLS_SYSTEM_PROCESSING.getStatus());
				busPersonInfoPwlsSystemProcessing.setUpdateTime(new Date());
				busPersonInfoService.updateBusPersonInfo(busPersonInfoPwlsSystemProcessing);
			}

			// 生成文件批次号,20220128144630 FILE_BATCH_NO
			String fileBatchNo = com.gcl.common.utils.DateUtils.dateTimeNow();
			// 生成BPS_PWLS_HEAD文件头数据
			String bpsPwlsHeadID = IdUtils.fastSimpleUUID();
			BpsPwlsHead bpsPwlsHead = new BpsPwlsHead();
			bpsPwlsHead.setId(bpsPwlsHeadID);
			// 记录标识,H为头文件
			bpsPwlsHead.setRecordId("H");
			// SEQUENCE_NUMBER 顺序号,从1开始,数据库中获取序列SEQ_BPS_SEQUENCE_NUMBER.NEXTVAL
			//交易机构号 TRADING_ORGANIZATION_NO  交易机构号,数据提交行机构号
			bpsPwlsHead.setTradingOrganizationNo(27005L);
			// 明细合计比数
			bpsPwlsHead.setDetailTotal((long) busPersonInfoListCount);
			// 遍历已通过审核的个人信息，插入到BPS_PWLS_DETAIL明细表中
			bpsPwlsHead.setChannelNo("T");
			bpsPwlsHead.setTransactionTellerNumber((long) 7777777);
			bpsPwlsHead.setTradingTerminalNumber((long) 777);
			// 当天处理的第几批数据001
			bpsPwlsHead.setDataBatchNumber((long) 2);
			bpsPwlsHead.setFileHandlingType(BpsTypeConstant.PWLS_TYPE);
			// 待生成
			bpsPwlsHead.setExecuteSteps("0");
			bpsPwlsHead.setExecuteResult("0");
			bpsPwlsHead.setUpdateTime(new Date());
			bpsPwlsHead.setFileBatchNo(fileBatchNo);
			// 插入头文件表
			bpsPwlsHeadService.insertBpsPwlsHead(bpsPwlsHead);
			for (BusPersonInfo b : busPersonInfoList) {
				BpsPwlsDetail bpsPwlsDetail = new BpsPwlsDetail();
				bpsPwlsDetail.setId(IdUtils.fastSimpleUUID());
				bpsPwlsDetail.setRecordId("D");
				bpsPwlsDetail.setTradingOrganizationNo(27005L);
				bpsPwlsDetail.setTransactionCode("TF0017");
				bpsPwlsDetail.setContactType("00");
				bpsPwlsDetail.setPhoneAreaCode("86");
				bpsPwlsDetail.setPhone(b.getMobileNumber());
				bpsPwlsDetail.setChangeType("CC00");
				bpsPwlsDetail.setUserName(b.getCustName());
				bpsPwlsDetail.setUserLabel(b.getUserLabel());
				bpsPwlsDetail.setSystemType(b.getSystemType());
				bpsPwlsDetail.setCustomerType("00");
				bpsPwlsDetail.setCoreOrgNo("27005");
				bpsPwlsDetail.setCustProvinceCode(b.getProvinceCode());
				bpsPwlsDetail.setCustCityType(b.getProvinceOfUser());

				bpsPwlsDetail.setCreateBy("BpsTask");
				bpsPwlsDetail.setCreateTime(new Date());
				bpsPwlsDetail.setUpdateBy("BpsTask");
				bpsPwlsDetail.setUpdateTime(new Date());

				bpsPwlsDetail.setBpsPwlsHeadId(bpsPwlsHeadID);
				bpsPwlsDetail.setFileBatchNo(fileBatchNo);
				// bpsPwlsDetail.setTransactionTellerNumber((long) 7777777);
				// bpsPwlsDetail.setTradingTerminalNumber((long) 777);
				bpsPwlsDetailService.insertBpsPwlsDetail(bpsPwlsDetail);
			}
			logger.info(busPersonInfoList.toString());
			// bpsDoWorkService.doWorkCWFS();
            } catch (Exception ex) {
                ex.printStackTrace();
                logger.error("CWFS doWork Exception:" + ex);
            }

    }

    /*****
     * 打印处理时间日志
     * @param typeStr
     */
    private void _printProcessTimeLog(String typeStr){
        String nowTimeStr = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
        logger.info("执行"+ typeStr +"定时任务,执行时间：" + nowTimeStr);

    }

}
