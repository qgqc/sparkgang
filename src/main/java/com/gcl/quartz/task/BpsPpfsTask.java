package com.gcl.quartz.task;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.gcl.bps.constant.BpsTypeConstant;
import com.gcl.bps.constant.BusPersonInfoStatus;
import com.gcl.bps.constant.ExecuteStepsAndResult;
import com.gcl.bps.domain.Base;
import com.gcl.bps.domain.OrgNoAndTellerNo;
import com.gcl.bps.util.FileTools;
import com.gcl.bps.util.FtpUtil;
import com.gcl.common.utils.uuid.IdUtils;
import com.gcl.personalwalletmanagement.domain.BpsPpfsDetail;
import com.gcl.personalwalletmanagement.domain.BpsPpfsHead;
import com.gcl.personalwalletmanagement.domain.BusPersonInfo;
import com.gcl.personalwalletmanagement.mapper.BpsPpfsDetailMapper;
import com.gcl.personalwalletmanagement.mapper.BpsPpfsHeadMapper;
import com.gcl.personalwalletmanagement.service.IBpsPpfsDetailService;
import com.gcl.personalwalletmanagement.service.IBpsPpfsHeadService;
import com.gcl.personalwalletmanagement.service.IBusPersonInfoService;

/******
 * @author gcl
 * @date 2022-02-11
 * 
 * 
 */
@Component("bpsPpfsTask")
public class BpsPpfsTask extends Base {

	private static Logger logger = LoggerFactory.getLogger(BpsPpfsTask.class);

	/**
	 * 个人基本信息服务
	 */
	@Autowired
	private IBusPersonInfoService busPersonInfoService;

	@Autowired
	private IBpsPpfsHeadService bpsPpfsHeadService;

	@Autowired
	private BpsPpfsHeadMapper bpsPpfsHeadMapper;

	@Autowired
	private IBpsPpfsDetailService bpsPpfsDetailService;

	@Autowired
	private BpsPpfsDetailMapper bpsPpfsDetailMapper;

	/**
	 * 查询当前时间往前几天的数据，此天数可配置，通常为1天
	 */
	@Value("${daySize}")
	private int daySize;

	/**
	 * 每个文件最大条数
	 */
	@Value("${fileSize}")
	private int fileSize;

	/**
	 * #文件名正则 sourceFile: 51.%s.%s.%s.T%s.DAT
	 */
	@Value("${sourceFile}")
	private String sourceFileName;
	@Value("${bpsDownFileLocalPath}")
	private String downloadLocalPath;
	@Value("${bpsCreatLocalPath}")
	private String bpsCreatLocalPath;

	@Value("${bpsDownFilePath}")
	private String downloadPath;
	@Value("${bpsCreatPath}")
	private String bpsCreatPath;

	@Value("${bpsFtpIp}")
	private String bpsFtpIp;
	@Value("${ftpPort}")
	private int ftpPort;
	@Value("${ftpUserName}")
	private String ftpUserName;
	@Value("${ftpPassword}")
	private String ftpPassword;

	/******
	 * 
	 */
	public void ppfsDoWork() {
		_printProcessTimeLog(BpsTypeConstant.PPFS_TYPE);
		try {
			// 从个人基本信息中查出daySize天内，并且状态是BWLS_SUCCESSFUL_OPENING("8","钱包注册白名单开立成功"),
			BusPersonInfo busPersonInfoReviewPassed = new BusPersonInfo();
			// BWLS_SUCCESSFUL_OPENING("8","钱包注册白名单开立成功"),
			busPersonInfoReviewPassed.setDataStatus(BusPersonInfoStatus.BWLS_SUCCESSFUL_OPENING.getStatus());
			HashMap<String, Object> params = new HashMap<String, Object>();
			// 结束时间
			Date endTime = new Date();
			// 开始时间
			Date beginTime = DateUtils.addDays(endTime, -daySize + 1);
			params.put("endTime", com.gcl.common.utils.DateUtils.dateTime(endTime) + " 23:59:59");
			params.put("beginTime", com.gcl.common.utils.DateUtils.dateTime(beginTime) + " 00:00:00");
			busPersonInfoReviewPassed.setParams(params);
			// 查出已审核通过的数据
			List<BusPersonInfo> busPersonInfoList = busPersonInfoService
					.selectBusPersonInfoList(busPersonInfoReviewPassed);
			int busPersonInfoListCount = busPersonInfoList.size();
			if (busPersonInfoListCount == 0) {
				logger.info("私人钱包开立执行任务条数为0");
				_printProcessTimeLog(BpsTypeConstant.PPFS_TYPE);
				return;
			}
			for (BusPersonInfo b : busPersonInfoList) {
				// PPFS_SYSTEM_PROCESSING("10","私人钱包批量开立系统处理中"),
				BusPersonInfo busPersonInfoPpfsSystemProcessing = new BusPersonInfo();
				busPersonInfoPpfsSystemProcessing.setId(b.getId());
				busPersonInfoPpfsSystemProcessing.setDataStatus(BusPersonInfoStatus.PPFS_SYSTEM_PROCESSING.getStatus());
				busPersonInfoPpfsSystemProcessing.setUpdateTime(new Date());
				busPersonInfoService.updateBusPersonInfo(busPersonInfoPpfsSystemProcessing);
			}

			// 生成文件批次号,20220128144630 FILE_BATCH_NO
			String fileBatchNo = com.gcl.common.utils.DateUtils.dateTimeNow();
			for (BusPersonInfo b : busPersonInfoList) {
				BpsPpfsDetail bpsPpfsDetail = new BpsPpfsDetail();
				// ID
				bpsPpfsDetail.setId(IdUtils.fastSimpleUUID());
				// 记录标识
				bpsPpfsDetail.setRecordId("D");
				// 顺序号
				bpsPpfsDetail.setSequenceNumber(busPersonInfoService.getSeqBpsSequenceNumbeValue());
				// 交易机构号
				bpsPpfsDetail.setTradingOrganizationNo(b.getOrgId());// 27005L
				// 交易码TF0014：开个人收款钱包
				bpsPpfsDetail.setTransactionCode("TF0014");
				// 国际电话区号
				bpsPpfsDetail.setPhoneAreaCode("86");
				// 电话城市区号为空
				// 电话号码
				bpsPpfsDetail.setPhone(b.getMobileNumber());
				// 用户所属国家编码
				bpsPpfsDetail.setCustCountryCode("CN");
				// 用户所属省编码
				bpsPpfsDetail.setCustProvinceCode(Integer.parseInt(b.getProvinceCode()));
				// 用户所属市编码
				bpsPpfsDetail.setCustCityCode(Integer.parseInt(b.getProvinceOfUser()));
				// 钱包昵称
				bpsPpfsDetail.setWalletNickname(b.getNickName());
				// 姓名
				bpsPpfsDetail.setCustName(b.getCustName());
				// 证件类型
				bpsPpfsDetail.setIdCardType(b.getIdCardType());
				// 证件号
				bpsPpfsDetail.setIdCard(b.getIdCard());
				// 备注
				bpsPpfsDetail.setRemarkInfo(b.getRemark());
				// 创建人
				// 创建时间
				// 更新人
				// 最后更新时间
				bpsPpfsDetail.setCreateBy("BpsPpfsTask");
				bpsPpfsDetail.setCreateTime(new Date());
				bpsPpfsDetail.setUpdateBy("BpsPpfsTask");
				bpsPpfsDetail.setUpdateTime(new Date());
				// 文件批次号
				bpsPpfsDetail.setFileBatchNo(fileBatchNo);
				// 交易柜员号
				// 交易终端号
				bpsPpfsDetail.setTransactionTellerNumber(b.getTransactionTellerNumber());
				bpsPpfsDetail.setTradingTerminalNumber(b.getTradingTerminalNumber());
				bpsPpfsDetail.setBusPersonInfoId(b.getId());
				bpsPpfsDetailService.insertBpsPpfsDetail(bpsPpfsDetail);
			}
			// 通过交易机构号和柜员号进行分组，查出共有几组，遍历生成文件，不同的交易机构号和柜员号不能生成在同一文件中
			List<OrgNoAndTellerNo> orgNoAndTellerNoList = bpsPpfsDetailMapper
					.selectOrgNoAndTellerNoListByFileBatchNo(fileBatchNo);
			logger.info("新建文件前准备,文件批次号：" + fileBatchNo);
			for (OrgNoAndTellerNo orgNoAndTellerNo : orgNoAndTellerNoList) {
				// 查询出当前条件下的所有明细
				BpsPpfsDetail bpsPpfsDetailOrgNoAndTellerNo = new BpsPpfsDetail();
				bpsPpfsDetailOrgNoAndTellerNo.setFileBatchNo(fileBatchNo);
				bpsPpfsDetailOrgNoAndTellerNo.setTradingOrganizationNo(orgNoAndTellerNo.getTradingOrganizationNo());
				bpsPpfsDetailOrgNoAndTellerNo.setTransactionTellerNumber(orgNoAndTellerNo.getTransactionTellerNumber());
				bpsPpfsDetailOrgNoAndTellerNo.setTradingTerminalNumber(orgNoAndTellerNo.getTradingTerminalNumber());
				// 查询出对应的明细
				List<BpsPpfsDetail> bpsPpfsDetailOrgNoAndTellerNoList = bpsPpfsDetailService
						.selectBpsPpfsDetailList(bpsPpfsDetailOrgNoAndTellerNo);

				try {
					// 获取当前分组后，此组的条数
					int orgNoAndTellerNoCount = bpsPpfsDetailOrgNoAndTellerNoList.size();
					// 获取当前条数余数
					int remainder = orgNoAndTellerNoCount % fileSize;
					// 计算出几个文件
					int fileNum = (new Double(Math.ceil((double) orgNoAndTellerNoCount / fileSize))).intValue();
					for (int i = 0; i < fileNum; i++) {
						// 默认当前批次号时当天的第一批，则001
						int batchNo = 1;
						// 查询当天日期已生成批次号的最大值,文件创建时间转为YYYYMMDD
						// FILE_CREATION_TIME等于当前日期,TRADING_ORGANIZATION_NO 交易机构号为当前orgNoAndTellerNo中的
						// 机构号，EXECUTE_STEPS 执行步骤,0待生成, 1已生成，2已上传，3已回盘更新 是1已生成，EXECUTE_RESULT
						// 执行结果,执行步骤的执行结果 0:成功，1:失败 是0:成功，通过文件批次号倒排
						// 取第一个就是最大值
						BpsPpfsHead bpsPpfsHeadMaxBatchQuery = new BpsPpfsHead();
						HashMap<String, Object> fileCreationTimeParams = new HashMap<String, Object>();
						// 结束日期
						Date endDate = new Date();
						// 开始日期
						Date beginDate = DateUtils.addDays(endDate, -daySize + 1);
						fileCreationTimeParams.put("endDate",
								com.gcl.common.utils.DateUtils.dateTime(endDate) + " 23:59:59");
						fileCreationTimeParams.put("beginDate",
								com.gcl.common.utils.DateUtils.dateTime(beginDate) + " 00:00:00");
						bpsPpfsHeadMaxBatchQuery.setParams(fileCreationTimeParams);
						BpsPpfsHead bpsPpfsHeadMaxBatch = bpsPpfsHeadMapper
								.findMaxBatchNoByCurrentDateAndOrgNo(bpsPpfsHeadMaxBatchQuery);
						// 如果不为空，则最大值赋给当前批次号
						if (bpsPpfsHeadMaxBatch != null) {
							batchNo = bpsPpfsHeadMaxBatch.getDataBatchNumber() + 1;
							if (batchNo > 999) {
								logger.error("BpsPpfs当日批次号超出999！！！");
							}
						}
						// detailTotal 明细合计笔数
						int detailTotal = 0;
						if ((i == fileNum - 1) && remainder != 0) {
							detailTotal = remainder;
						} else {
							detailTotal = fileSize;
						}
						// 生成BPS_PPFS_HEAD文件头数据
						String bpsPpfsHeadID = IdUtils.fastSimpleUUID();
						BpsPpfsHead bpsPpfsHead = new BpsPpfsHead();
						bpsPpfsHead.setId(bpsPpfsHeadID);
						// 记录标识,H为头文件
						bpsPpfsHead.setRecordId("H");
						// SEQUENCE_NUMBER 顺序号,从1开始,数据库中获取序列SEQ_BPS_SEQUENCE_NUMBER.NEXTVAL
						bpsPpfsHead.setSequenceNumber(busPersonInfoService.getSeqBpsSequenceNumbeValue());
						// 交易机构号 TRADING_ORGANIZATION_NO 交易机构号,数据提交行机构号
						bpsPpfsHead.setTradingOrganizationNo(orgNoAndTellerNo.getTradingOrganizationNo());
						// 币种
						bpsPpfsHead.setCurrency("CNY");
						// 明细合计比数
						bpsPpfsHead.setDetailTotalCount(detailTotal);
						// 明细合计金额
						bpsPpfsHead.setDetailTotalAmount(0);
						// 渠道号
						bpsPpfsHead.setChannelNo("T");
						// 交易柜员号
						bpsPpfsHead.setTransactionTellerNumber(orgNoAndTellerNo.getTransactionTellerNumber());
						// 终端号
						bpsPpfsHead.setTradingTerminalNumber(orgNoAndTellerNo.getTradingTerminalNumber());
						// 当天处理的第几批数据001
						bpsPpfsHead.setDataBatchNumber(batchNo);
						// 记账日期
						bpsPpfsHead.setAccountingDate(com.gcl.common.utils.DateUtils.dateNow());
						// 文件处理类型
						bpsPpfsHead.setFileHandlingType(BpsTypeConstant.PPFS_TYPE);
						// 待生成
						bpsPpfsHead.setExecuteSteps(ExecuteStepsAndResult.TO_BE_GENERATED);
						// 成功
						bpsPpfsHead.setExecuteResult(ExecuteStepsAndResult.SUCCESS);
						bpsPpfsHead.setUpdateTime(new Date());
						bpsPpfsHead.setFileBatchNo(fileBatchNo);
						// file_creation_time date y 文件创建时间
						Date fileCreationTimeDate = new Date();
						bpsPpfsHead.setFileCreationTime(fileCreationTimeDate);
						bpsPpfsHead.setUpdateTime(new Date());
						// 文件名 正则是#文件名正则sourceFile: 51.%s.%s.%s.T%s.DAT
						// 上送文件命名：来源系统标识+.+文件类型+.+机构号+.+顺序号+.+”T“MMDD.DAT （机构号栏位可填写字母或数字）
						// 上送文件举例：51.CWFS.02078.001.T0716.DAT
						String sourceFileNameForCreate = String.format(sourceFileName, BpsTypeConstant.PPFS_TYPE,
								bpsPpfsHead.getTradingOrganizationNo(),
								getNumberFormat(3, bpsPpfsHead.getDataBatchNumber().toString()),
								com.gcl.common.utils.DateUtils.dateTimeYYYYMMDDHHMMSS(fileCreationTimeDate)
										.substring(4, 8));
						bpsPpfsHead.setSourceFileName(sourceFileNameForCreate);
						// 插入头文件表
						bpsPpfsHeadService.insertBpsPpfsHead(bpsPpfsHead);
						logger.info("对私钱包批量开立BpsPpfsTask生成文件:" + bpsPpfsHead.getSourceFileName());
						File file = new File(bpsCreatLocalPath + File.separator + BpsTypeConstant.PPFS_TYPE
								+ File.separator + bpsPpfsHead.getSourceFileName());
						// 生成文件中的第一行，文件头信息
						StringBuffer stringBufferFile = new StringBuffer();
						// 1 ID ID VARCHAR2(32) M
						// 2 记录标识 RECORD_ID VARCHAR2(1) M
						stringBufferFile.append(bpsPpfsHead.getRecordId());
						// 3 顺序号 SEQUENCE_NUMBER H NUMBER(12) M
						stringBufferFile.append(getNumberFormat(12, bpsPpfsHead.getSequenceNumber().toString()));
						// 4 源文件名称 SOURCE_FILE_NAME VARCHAR2(50) M
						stringBufferFile.append(getOneVsSpace(50, bpsPpfsHead.getSourceFileName()));
						// 5 交易机构号 TRADING_ORGANIZATION_NO NUMBER(5) M
						stringBufferFile.append(getOneVsSpace(5, bpsPpfsHead.getTradingOrganizationNo().toString()));
						// 币种 3
						stringBufferFile.append(bpsPpfsHead.getCurrency());
						// 6 明细合计笔数 DETAIL_TOTAL NUMBER(12) M
						stringBufferFile.append(getNumberFormat(12, bpsPpfsHead.getDetailTotalCount().toString()));
						// 明细合计金额
						stringBufferFile.append(getNumberFormat(17, "0"));
						// 7 渠道号 CHANNEL_NO VARCHAR2(1) M
						stringBufferFile.append(getOneVsSpace(1, bpsPpfsHead.getChannelNo()));
						// 8 交易柜员号 TRANSACTION_TELLER_NUMBER NUMBER(7) M
						stringBufferFile
								.append(getNumberFormat(7, bpsPpfsHead.getTransactionTellerNumber().toString()));
						// 9 交易终端号 TRADING_TERMINAL_NUMBER NUMBER(3) M
						stringBufferFile.append(getNumberFormat(3, bpsPpfsHead.getTradingTerminalNumber().toString()));
						// 10 数据批次号 DATA_BATCH_NUMBER NUMBER(3) M
						stringBufferFile.append(getNumberFormat(3, bpsPpfsHead.getDataBatchNumber().toString()));
						// 记账日期 8
						stringBufferFile.append(bpsPpfsHead.getAccountingDate());
						// 11 文件处理类型 FILE_HANDLING_TYPE VARCHAR2(4) M
						stringBufferFile.append(bpsPpfsHead.getFileHandlingType());
						// 12 明细成功笔数 DETAIL_SUCCESS_COUNT NUMBER(12) ABC返回填写
						stringBufferFile.append(getNumberFormat(12, "0"));
						// 13 明细失败笔数 DETAIL_FAILED_COUNT NUMBER(12) ABC返回填写
						stringBufferFile.append(getNumberFormat(12, "0"));
						// 15 文件返回信息码 FILE_RETURN_CODE VARCHAR2(4) ABC返回填写
						stringBufferFile.append(getOneVsSpace(4, ""));
						// 16 文件返回信息 FILE_RETURN_MSG VARCHAR2(30) ABC返回填写
						stringBufferFile.append(getOneVsSpace(30, ""));
						// 17 备注 REMARK VARCHAR2(64) O
						stringBufferFile.append(getOneVsSpace(64, bpsPpfsHead.getRemark()));
						// 18 冗余域 REDUNDANT_INFO VARCHAR2(100) O
						stringBufferFile.append(getOneVsSpace(119, bpsPpfsHead.getRedundantInfo()));
						// 换行
						stringBufferFile.append("\n");
						// logger.info(stringBufferFile.toString());
						// 遍历符合条件的明细，写入到文件中，将文件头ID插入到明细中
						int chushizhi = i * fileSize;
						for (int j = chushizhi; (i + 1 != fileNum) ? (j < (i + 1) * fileSize)
								: (j < orgNoAndTellerNoCount); j++) {
							// logger.info(bpsPpfsDetailOrgNoAndTellerNoList.get(j).toString());
							BpsPpfsDetail bpsPpfsDetailForCreate = bpsPpfsDetailOrgNoAndTellerNoList.get(j);
							// 写入到文件中
							// 记录标识 1 String 1
							stringBufferFile.append(bpsPpfsDetailForCreate.getRecordId());
							// 顺序号 2 Number 12
							stringBufferFile
									.append(getNumberFormat(12, bpsPpfsDetailForCreate.getSequenceNumber().toString()));
							// 交易机构号 14 Number 5
							stringBufferFile.append(
									getOneVsSpace(5, bpsPpfsDetailForCreate.getTradingOrganizationNo().toString()));
							// 交易码 19 String 6
							stringBufferFile.append(bpsPpfsDetailForCreate.getTransactionCode());
							// 手机国际区号 25 String 10
							stringBufferFile.append(getOneVsSpace(10, bpsPpfsDetailForCreate.getPhoneAreaCode()));
							// 手机号城市区号 35 String 10
							stringBufferFile.append(getOneVsSpace(10, bpsPpfsDetailForCreate.getPhoneCityCode()));
							// 手机号 45 String 16
							stringBufferFile.append(getOneVsSpace(16, bpsPpfsDetailForCreate.getPhone()));
							// 用户所属国家编码 61 String 2
							stringBufferFile.append(getOneVsSpace(2, bpsPpfsDetailForCreate.getCustCountryCode()));
							// 用户所属省编码 63 Number 6
							stringBufferFile
									.append(getOneVsSpace(6, bpsPpfsDetailForCreate.getCustProvinceCode().toString()));
							// 用户所属地区编码 69 Number 6
							stringBufferFile
									.append(getOneVsSpace(6, bpsPpfsDetailForCreate.getCustCityCode().toString()));
							// 钱包昵称 75 String 60
							stringBufferFile
									.append(getOneVsSpace(60, bpsPpfsDetailForCreate.getWalletNickname()));
							// 用户姓名 135 String 40
							stringBufferFile.append(getOneVsSpace(40, bpsPpfsDetailForCreate.getCustName()));
							// 身份证件类型 175 String 4
							stringBufferFile.append(getOneVsSpace(4, bpsPpfsDetailForCreate.getIdCardType()));
							// 身份证件号码 179 String 32
							stringBufferFile.append(getOneVsSpace(32, bpsPpfsDetailForCreate.getIdCard()));
							// 对私钱包开立交易返回信息码 211 String 8
							// 对私钱包开立交易返回信息 219 String 60
							// 商户号 279 String 32
							// 商户全称 311 String 60
							// 商户类型 371 String 4
							// 商户简称 375 String 30
							// 所属代理机构编码 405 String 4
							// 钱包id 409 String 16
							// 外部商户号" 425 String 32
							// 外部商户名称" 457 String 60
							// 商户等级 517 String 4
							// 上级商户号 521 String 23
							// 人行商户号 544 String 8
							// 商户属性/标识 552 Number 1
							// MCC编码 553 String 4
							// 商户结算标志 557 Number 1
							// 客户经理 558 String 40
							// 省编码 598 Number 6
							// 市编码 604 Number 6
							// 营业地址 610 String 80
							// 接入标识 690 Number 2
							// 商户责任人 692 String 40
							// 商户开户联系人姓名 732 String 40
							// 商户开户联系人邮箱 772 String 50
							// 商户开户联系人电话 822 String 18
							// 商户连锁类型 840 Number 2
							// 商户回调地址 842 String 100
							// 是否支持协议代扣 942 Number 2
							// 纸质合同号 944 String 40
							// 合同生效日期 984 String 8
							// 合同失效日期 992 String 8
							// 结算周期类型 1000 String 2
							// 账户类型 1002 Number 2
							// 账户名 1004 String 240
							// 收款银行代码 1244 String 16
							// 分支行机构号 1260 String 5
							// 分支行名称 1265 String 64
							// 开户行省编码 1329 Number 6
							// 开户行市编码 1335 Number 6
							// 结算最小金额（单位：分） 1341 String 19
							// 结算类型 1360 String 2
							// 打款时间点 1362 String 2
							// 收款银行名称 1364 String 99
							// 收款银行账号 1463 String 68
							// 系统间过渡账户 1531 String 2
							// 结算周期 1533 String 2
							// 结算处理方式 1535 String 2
							// 商户注册渠道 1537 String 2
							// 签约人 1539 String 40
							// CNAPS编号 1579 String 14
							// 商户开立交易返回信息码 1593 String 8
							// 商户开立交易返回信息 1601 String 60
							// 幂等流水（扩位） 1661 String 47
							// 备注 1708 String 64
							// 冗余域 1772 String 265
							stringBufferFile.append("\n");
							FileTools.writeToTxtEncoding(stringBufferFile.toString(), file, ENCODING);

							// 更新bps_pwls_head_id varchar2(32) y 文件头主键,人行共建app白名单批量文件头表主键
							bpsPpfsDetailForCreate.setBpsPpfsHeadId(bpsPpfsHeadID);
							bpsPpfsDetailForCreate.setUpdateTime(new Date());
							bpsPpfsDetailService.updateBpsPpfsDetail(bpsPpfsDetailForCreate);
						}
						// 生成完成后更新头文件 1已生成 0:成功
						bpsPpfsHead.setExecuteSteps(ExecuteStepsAndResult.GENERATED);
						bpsPpfsHead.setExecuteResult(ExecuteStepsAndResult.SUCCESS);
						bpsPpfsHead.setUpdateTime(new Date());
						bpsPpfsHeadService.updateBpsPpfsHead(bpsPpfsHead);
						// 将生成的文件上传到FTP服务器上
						// #上传(BPS本地生成DAT文件的存储目录)
						// bpsCreatLocalPath: 'D:/data/bps/upfile/'
						// #下载(BPS本地下载RET文件的存储目录)
						// bpsDownFileLocalPath: 'D:/data/bps/download/'
						// #BPS系统 远程FTP服务器 上传目录
						// bpsCreatPath: '/upload'
						// #BPS系统 远程FTP服务器 下载回盘RET文件目录
						// bpsDownFilePath: '/download'
						// #本地环境 ftp
						// bpsFtpIp: 180.76.237.67
						// ftpPort: 21
						// ftpUserName: szrmb
						// ftpPassword: szrmb@2022!
						// 初始化FTP服务器
						FtpUtil ftpUtil = new FtpUtil(bpsFtpIp, ftpPort, ftpUserName, ftpPassword, bpsCreatPath);
						String fileString = ftpUtil.uploadFile(file);
						logger.info("BpsPpfsTask:" + fileString + "已上传FTP");
						// 上传完成后更新头文件 2已生成 0:成功
						bpsPpfsHead.setExecuteSteps(ExecuteStepsAndResult.UPLOADED);
						bpsPpfsHead.setExecuteResult(ExecuteStepsAndResult.SUCCESS);
						bpsPpfsHead.setFileUploadFtpTime(new Date());
						bpsPpfsHead.setUpdateTime(new Date());
						bpsPpfsHeadService.updateBpsPpfsHead(bpsPpfsHead);
						continue;
					}
				} catch (Exception e) {
					logger.error("PPFS生成文件和上传出错：{}", e);
				}
				continue;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("PPFSdoWork Exception:" + ex);
		}

	}

	/*****
	 * 打印处理时间日志
	 * 
	 * @param typeStr
	 */
	private void _printProcessTimeLog(String typeStr) {
		String nowTimeStr = DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss");
		logger.info("执行" + typeStr + "定时任务,执行时间：" + nowTimeStr);

	}

}
