package com.gcl.scancodemanagement.service;

import java.util.List;
import com.gcl.scancodemanagement.domain.BusPbocccoBuildappwhitelist;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author yada
 * @date 2022-01-28
 */
public interface IBusPbocccoBuildappwhitelistService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public BusPbocccoBuildappwhitelist selectBusPbocccoBuildappwhitelistById(String id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param busPbocccoBuildappwhitelist 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<BusPbocccoBuildappwhitelist> selectBusPbocccoBuildappwhitelistList(BusPbocccoBuildappwhitelist busPbocccoBuildappwhitelist);

    /**
     * 新增【请填写功能名称】
     * 
     * @param busPbocccoBuildappwhitelist 【请填写功能名称】
     * @return 结果
     */
    public int insertBusPbocccoBuildappwhitelist(BusPbocccoBuildappwhitelist busPbocccoBuildappwhitelist);

    /**
     * 修改【请填写功能名称】
     * 
     * @param busPbocccoBuildappwhitelist 【请填写功能名称】
     * @return 结果
     */
    public int updateBusPbocccoBuildappwhitelist(BusPbocccoBuildappwhitelist busPbocccoBuildappwhitelist);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteBusPbocccoBuildappwhitelistByIds(String[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteBusPbocccoBuildappwhitelistById(String id);
}
