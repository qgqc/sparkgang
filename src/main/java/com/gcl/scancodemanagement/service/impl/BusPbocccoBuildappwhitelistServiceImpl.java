package com.gcl.scancodemanagement.service.impl;

import java.util.List;

import com.gcl.scancodemanagement.domain.BusPbocccoBuildappwhitelist;
import com.gcl.scancodemanagement.mapper.BusPbocccoBuildappwhitelistMapper;
import com.gcl.scancodemanagement.service.IBusPbocccoBuildappwhitelistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author yada
 * @date 2022-01-28
 */
@Service
public class BusPbocccoBuildappwhitelistServiceImpl implements IBusPbocccoBuildappwhitelistService
{
    @Autowired
    private BusPbocccoBuildappwhitelistMapper busPbocccoBuildappwhitelistMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public BusPbocccoBuildappwhitelist selectBusPbocccoBuildappwhitelistById(String id)
    {
        return busPbocccoBuildappwhitelistMapper.selectBusPbocccoBuildappwhitelistById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param busPbocccoBuildappwhitelist 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<BusPbocccoBuildappwhitelist> selectBusPbocccoBuildappwhitelistList(BusPbocccoBuildappwhitelist busPbocccoBuildappwhitelist)
    {
        return busPbocccoBuildappwhitelistMapper.selectBusPbocccoBuildappwhitelistList(busPbocccoBuildappwhitelist);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param busPbocccoBuildappwhitelist 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertBusPbocccoBuildappwhitelist(BusPbocccoBuildappwhitelist busPbocccoBuildappwhitelist)
    {
        return busPbocccoBuildappwhitelistMapper.insertBusPbocccoBuildappwhitelist(busPbocccoBuildappwhitelist);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param busPbocccoBuildappwhitelist 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateBusPbocccoBuildappwhitelist(BusPbocccoBuildappwhitelist busPbocccoBuildappwhitelist)
    {
        return busPbocccoBuildappwhitelistMapper.updateBusPbocccoBuildappwhitelist(busPbocccoBuildappwhitelist);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteBusPbocccoBuildappwhitelistByIds(String[] ids)
    {
        return busPbocccoBuildappwhitelistMapper.deleteBusPbocccoBuildappwhitelistByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteBusPbocccoBuildappwhitelistById(String id)
    {
        return busPbocccoBuildappwhitelistMapper.deleteBusPbocccoBuildappwhitelistById(id);
    }
}
