package com.gcl.scancodemanagement.controller;

import java.util.List;
import java.util.UUID;

import com.gcl.scancodemanagement.domain.BusPbocccoBuildappwhitelist;
import com.gcl.scancodemanagement.service.IBusPbocccoBuildappwhitelistService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;

/**
 * 【请填写功能名称】Controller
 * 
 * @author yada
 * @date 2022-01-28
 */
@RestController
@RequestMapping("/redenvelopemanagement/buildappwhitelist")
public class BusPbocccoBuildappwhitelistController extends BaseController
{
    @Autowired
    private IBusPbocccoBuildappwhitelistService busPbocccoBuildappwhitelistService;

    /**
     * 查询【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:buildappwhitelist:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusPbocccoBuildappwhitelist busPbocccoBuildappwhitelist)
    {
        startPage();
        List<BusPbocccoBuildappwhitelist> list = busPbocccoBuildappwhitelistService.selectBusPbocccoBuildappwhitelistList(busPbocccoBuildappwhitelist);
        return getDataTable(list);
    }

    /**
     * 导出【请填写功能名称】列表
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:buildappwhitelist:export')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BusPbocccoBuildappwhitelist busPbocccoBuildappwhitelist)
    {
        List<BusPbocccoBuildappwhitelist> list = busPbocccoBuildappwhitelistService.selectBusPbocccoBuildappwhitelistList(busPbocccoBuildappwhitelist);
        ExcelUtil<BusPbocccoBuildappwhitelist> util = new ExcelUtil<BusPbocccoBuildappwhitelist>(BusPbocccoBuildappwhitelist.class);
        return util.exportExcel(list, "【请填写功能名称】数据");
    }

    /**
     * 获取【请填写功能名称】详细信息
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:buildappwhitelist:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(busPbocccoBuildappwhitelistService.selectBusPbocccoBuildappwhitelistById(id));
    }

    /**
     * 新增【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:buildappwhitelist:add')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusPbocccoBuildappwhitelist busPbocccoBuildappwhitelist)
    {
        String id = UUID.randomUUID().toString().replace("-", "");
        busPbocccoBuildappwhitelist.setId(id);
        return toAjax(busPbocccoBuildappwhitelistService.insertBusPbocccoBuildappwhitelist(busPbocccoBuildappwhitelist));
    }

    /**
     * 修改【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:buildappwhitelist:edit')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusPbocccoBuildappwhitelist busPbocccoBuildappwhitelist)
    {
        return toAjax(busPbocccoBuildappwhitelistService.updateBusPbocccoBuildappwhitelist(busPbocccoBuildappwhitelist));
    }

    /**
     * 删除【请填写功能名称】
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:buildappwhitelist:remove')")
    @Log(title = "【请填写功能名称】", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(busPbocccoBuildappwhitelistService.deleteBusPbocccoBuildappwhitelistByIds(ids));
    }
}
