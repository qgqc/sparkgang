package com.gcl.scancodemanagement.domain;

import java.beans.Transient;
import java.io.UnsupportedEncodingException;
import java.util.Date;

import com.gcl.bps.domain.Base;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.gcl.common.annotation.Excel;

/**
 * 【请填写功能名称】对象 bus_pboccco_buildappwhitelist
 * 
 * @author yada
 * @date 2022-01-28
 */
public class BusPbocccoBuildappwhitelist extends Base
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sequenceNumber;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String tradingOrganizationNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String transactionCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String contactType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String phoneAreaCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String phoneCityCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String phone;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String changeType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String userName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String userLabel;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String systemType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String customerType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String coreOrgNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String custProvinceCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String custCityType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dataStatus;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String returnCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String returnMsg;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String miDengLiuShui;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remarkInfo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String redundantInfo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date ts;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String examineDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String sourceFileName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String tellerNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String headId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String fileBatchNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String v2Id;


    public BusPbocccoBuildappwhitelist(){}

    public BusPbocccoBuildappwhitelist(String returnCode, String returnMsg, String dataStatus,String miDengLiuShui,String sequenceNumber, String headId){
        this.returnCode = returnCode;
        this.returnMsg = returnMsg;
        this.dataStatus = dataStatus;
        this.miDengLiuShui = miDengLiuShui;
        this.sequenceNumber = sequenceNumber;
        this.headId = headId;


    }



    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setSequenceNumber(String sequenceNumber) 
    {
        this.sequenceNumber = sequenceNumber;
    }

    public String getSequenceNumber() 
    {
        return sequenceNumber;
    }
    public void setTradingOrganizationNo(String tradingOrganizationNo) 
    {
        this.tradingOrganizationNo = tradingOrganizationNo;
    }

    public String getTradingOrganizationNo() 
    {
        return tradingOrganizationNo;
    }
    public void setTransactionCode(String transactionCode) 
    {
        this.transactionCode = transactionCode;
    }

    public String getTransactionCode() 
    {
        return transactionCode;
    }
    public void setContactType(String contactType) 
    {
        this.contactType = contactType;
    }

    public String getContactType() 
    {
        return contactType;
    }
    public void setPhoneAreaCode(String phoneAreaCode) 
    {
        this.phoneAreaCode = phoneAreaCode;
    }

    public String getPhoneAreaCode() 
    {
        return phoneAreaCode;
    }
    public void setPhoneCityCode(String phoneCityCode) 
    {
        this.phoneCityCode = phoneCityCode;
    }

    public String getPhoneCityCode() 
    {
        return phoneCityCode;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setChangeType(String changeType) 
    {
        this.changeType = changeType;
    }

    public String getChangeType() 
    {
        return changeType;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setUserLabel(String userLabel) 
    {
        this.userLabel = userLabel;
    }

    public String getUserLabel() 
    {
        return userLabel;
    }
    public void setSystemType(String systemType) 
    {
        this.systemType = systemType;
    }

    public String getSystemType() 
    {
        return systemType;
    }
    public void setCustomerType(String customerType) 
    {
        this.customerType = customerType;
    }

    public String getCustomerType() 
    {
        return customerType;
    }
    public void setCoreOrgNo(String coreOrgNo) 
    {
        this.coreOrgNo = coreOrgNo;
    }

    public String getCoreOrgNo() 
    {
        return coreOrgNo;
    }
    public void setCustProvinceCode(String custProvinceCode) 
    {
        this.custProvinceCode = custProvinceCode;
    }

    public String getCustProvinceCode() 
    {
        return custProvinceCode;
    }
    public void setCustCityType(String custCityType) 
    {
        this.custCityType = custCityType;
    }

    public String getCustCityType() 
    {
        return custCityType;
    }
    public void setDataStatus(String dataStatus) 
    {
        this.dataStatus = dataStatus;
    }

    public String getDataStatus() 
    {
        return dataStatus;
    }
    public void setReturnCode(String returnCode) 
    {
        this.returnCode = returnCode;
    }

    public String getReturnCode() 
    {
        return returnCode;
    }
    public void setReturnMsg(String returnMsg) 
    {
        this.returnMsg = returnMsg;
    }

    public String getReturnMsg() 
    {
        return returnMsg;
    }
    public void setMiDengLiuShui(String miDengLiuShui) 
    {
        this.miDengLiuShui = miDengLiuShui;
    }

    public String getMiDengLiuShui() 
    {
        return miDengLiuShui;
    }
    public void setRemarkInfo(String remarkInfo) 
    {
        this.remarkInfo = remarkInfo;
    }

    public String getRemarkInfo() 
    {
        return remarkInfo;
    }
    public void setRedundantInfo(String redundantInfo) 
    {
        this.redundantInfo = redundantInfo;
    }

    public String getRedundantInfo() 
    {
        return redundantInfo;
    }
    public void setTs(Date ts) 
    {
        this.ts = ts;
    }

    public Date getTs() 
    {
        return ts;
    }
    public void setExamineDate(String examineDate) 
    {
        this.examineDate = examineDate;
    }

    public String getExamineDate() 
    {
        return examineDate;
    }
    public void setSourceFileName(String sourceFileName) 
    {
        this.sourceFileName = sourceFileName;
    }

    public String getSourceFileName() 
    {
        return sourceFileName;
    }
    public void setTellerNo(String tellerNo) 
    {
        this.tellerNo = tellerNo;
    }

    public String getTellerNo() 
    {
        return tellerNo;
    }
    public void setHeadId(String headId) 
    {
        this.headId = headId;
    }

    public String getHeadId() 
    {
        return headId;
    }
    public void setFileBatchNo(String fileBatchNo) 
    {
        this.fileBatchNo = fileBatchNo;
    }

    public String getFileBatchNo() 
    {
        return fileBatchNo;
    }
    public void setV2Id(String v2Id) 
    {
        this.v2Id = v2Id;
    }

    public String getV2Id() 
    {
        return v2Id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sequenceNumber", getSequenceNumber())
            .append("tradingOrganizationNo", getTradingOrganizationNo())
            .append("transactionCode", getTransactionCode())
            .append("contactType", getContactType())
            .append("phoneAreaCode", getPhoneAreaCode())
            .append("phoneCityCode", getPhoneCityCode())
            .append("phone", getPhone())
            .append("changeType", getChangeType())
            .append("userName", getUserName())
            .append("userLabel", getUserLabel())
            .append("systemType", getSystemType())
            .append("customerType", getCustomerType())
            .append("coreOrgNo", getCoreOrgNo())
            .append("custProvinceCode", getCustProvinceCode())
            .append("custCityType", getCustCityType())
            .append("dataStatus", getDataStatus())
            .append("returnCode", getReturnCode())
            .append("returnMsg", getReturnMsg())
            .append("miDengLiuShui", getMiDengLiuShui())
            .append("remarkInfo", getRemarkInfo())
            .append("redundantInfo", getRedundantInfo())
            .append("ts", getTs())
            .append("examineDate", getExamineDate())
            .append("sourceFileName", getSourceFileName())
            .append("tellerNo", getTellerNo())
            .append("headId", getHeadId())
            .append("fileBatchNo", getFileBatchNo())
            .append("v2Id", getV2Id())
            .toString();
    }



    @Transient
    public String getDetailFileRow() throws UnsupportedEncodingException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("D");
        stringBuilder.append(getNumberFormat(12,this.sequenceNumber));
        stringBuilder.append(getOneVsSpace(5,this.tradingOrganizationNo));
        stringBuilder.append(getOneVsSpace(6,this.transactionCode));
        stringBuilder.append(getOneVsSpace(2,this.contactType));
        stringBuilder.append(getOneVsSpace(10,phoneAreaCode));
        stringBuilder.append(getOneVsSpace(10,phoneCityCode));
        stringBuilder.append(getOneVsSpace(35,phone));
        stringBuilder.append(getOneVsSpace(4,changeType));
        stringBuilder.append(getOneVsSpace(60,userName));
        stringBuilder.append(getOneVsSpace(20,userLabel));
        stringBuilder.append(getOneVsSpace(2,systemType));
        stringBuilder.append(getOneVsSpace(2,customerType));
        stringBuilder.append(getOneVsSpace(12,coreOrgNo));
        stringBuilder.append(getOneVsSpace(6,custProvinceCode));
        stringBuilder.append(getOneVsSpace(6,custCityType));
        stringBuilder.append(getOneVsSpace(8,returnCode));
        stringBuilder.append(getOneVsSpace(60,returnMsg));
        stringBuilder.append(getOneVsSpace(47,miDengLiuShui));
        stringBuilder.append(getOneVsSpace(64,remarkInfo));
        stringBuilder.append(getOneVsSpace(100,redundantInfo));
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }
}
