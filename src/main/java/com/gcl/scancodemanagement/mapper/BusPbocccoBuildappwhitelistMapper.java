package com.gcl.scancodemanagement.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.gcl.bps.domain.OrgAndNum;
import com.gcl.scancodemanagement.domain.BusPbocccoBuildappwhitelist;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author yada
 * @date 2022-01-28
 */
public interface BusPbocccoBuildappwhitelistMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public BusPbocccoBuildappwhitelist selectBusPbocccoBuildappwhitelistById(String id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param busPbocccoBuildappwhitelist 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<BusPbocccoBuildappwhitelist> selectBusPbocccoBuildappwhitelistList(BusPbocccoBuildappwhitelist busPbocccoBuildappwhitelist);

    /**
     * 新增【请填写功能名称】
     * 
     * @param busPbocccoBuildappwhitelist 【请填写功能名称】
     * @return 结果
     */
    public int insertBusPbocccoBuildappwhitelist(BusPbocccoBuildappwhitelist busPbocccoBuildappwhitelist);

    /**
     * 修改【请填写功能名称】
     * 
     * @param busPbocccoBuildappwhitelist 【请填写功能名称】
     * @return 结果
     */
    public int updateBusPbocccoBuildappwhitelist(BusPbocccoBuildappwhitelist busPbocccoBuildappwhitelist);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteBusPbocccoBuildappwhitelistById(String id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusPbocccoBuildappwhitelistByIds(String[] ids);



    int countByDataStatusAndExamineDateGreaterThanEqual(Map queryParam);

    void updatefileBatchNoByStatus(Map queryParam);

    List<OrgAndNum> selectCount(String fileBatchNo);

    ArrayList<BusPbocccoBuildappwhitelist> selectByFileBatchNo(Map queryParam);

    void saveAll(List<BusPbocccoBuildappwhitelist> item);


    void updateAll(List<BusPbocccoBuildappwhitelist> item);

    void insertByStatus(BusPbocccoBuildappwhitelist insertObj);

    void updateBatchBySequenceNumberAndHeadId(ArrayList<BusPbocccoBuildappwhitelist> list);
}
