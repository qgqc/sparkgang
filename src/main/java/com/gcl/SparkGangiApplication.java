package com.gcl;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.web.server.ConfigurableWebServerFactory;
import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@MapperScan("com.yada.**.mapper")
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class SparkGangiApplication {
	private static Logger logger = LoggerFactory.getLogger(SparkGangiApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(SparkGangiApplication.class, args);
		logger.info("-----------------------------数币管理平台启动成功--------------------------------------");
    }

	@Bean
	public WebServerFactoryCustomizer<ConfigurableWebServerFactory> webServerFactoryCustomizer(){
    	return factory -> {
			ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, "/static/index.html");
    		factory.addErrorPages(error404Page);
		};
    }
}
