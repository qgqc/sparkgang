package com.gcl.redenvelopemanagement.mapper;

import java.util.List;
import com.gcl.redenvelopemanagement.domain.TBTestInfo;

/**
 * 生成测试Mapper接口
 * 
 * @author yada
 * @date 2022-01-17
 */
public interface TBTestInfoMapper 
{
    /**
     * 查询生成测试
     * 
     * @param id 生成测试主键
     * @return 生成测试
     */
    public TBTestInfo selectTBTestInfoById(String id);

    /**
     * 查询生成测试列表
     * 
     * @param tBTestInfo 生成测试
     * @return 生成测试集合
     */
    public List<TBTestInfo> selectTBTestInfoList(TBTestInfo tBTestInfo);

    /**
     * 新增生成测试
     * 
     * @param tBTestInfo 生成测试
     * @return 结果
     */
    public int insertTBTestInfo(TBTestInfo tBTestInfo);

    /**
     * 修改生成测试
     * 
     * @param tBTestInfo 生成测试
     * @return 结果
     */
    public int updateTBTestInfo(TBTestInfo tBTestInfo);

    /**
     * 删除生成测试
     * 
     * @param id 生成测试主键
     * @return 结果
     */
    public int deleteTBTestInfoById(String id);

    /**
     * 批量删除生成测试
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTBTestInfoByIds(String[] ids);
}
