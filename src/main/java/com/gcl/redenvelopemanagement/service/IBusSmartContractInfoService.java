package com.gcl.redenvelopemanagement.service;

import java.util.List;
import com.gcl.redenvelopemanagement.domain.BusSmartContractInfo;

/**
 * 智能合约管理Service接口
 * 
 * @author gcl
 * @date 2022-01-13
 */
public interface IBusSmartContractInfoService 
{
    /**
     * 查询智能合约管理
     * 
     * @param id 智能合约管理主键
     * @return 智能合约管理
     */
    public BusSmartContractInfo selectBusSmartContractInfoById(String id);

    /**
     * 查询智能合约管理列表
     * 
     * @param busSmartContractInfo 智能合约管理
     * @return 智能合约管理集合
     */
    public List<BusSmartContractInfo> selectBusSmartContractInfoList(BusSmartContractInfo busSmartContractInfo);

    /**
     * 新增智能合约管理
     * 
     * @param busSmartContractInfo 智能合约管理
     * @return 结果
     */
    public int insertBusSmartContractInfo(BusSmartContractInfo busSmartContractInfo);

    /**
     * 修改智能合约管理
     * 
     * @param busSmartContractInfo 智能合约管理
     * @return 结果
     */
    public int updateBusSmartContractInfo(BusSmartContractInfo busSmartContractInfo);

    /**
     * 批量删除智能合约管理
     * 
     * @param ids 需要删除的智能合约管理主键集合
     * @return 结果
     */
    public int deleteBusSmartContractInfoByIds(String[] ids);

    /**
     * 删除智能合约管理信息
     * 
     * @param id 智能合约管理主键
     * @return 结果
     */
    public int deleteBusSmartContractInfoById(String id);
}
