package com.gcl.redenvelopemanagement.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gcl.redenvelopemanagement.mapper.TBTestInfoMapper;
import com.gcl.redenvelopemanagement.domain.TBTestInfo;
import com.gcl.redenvelopemanagement.service.ITBTestInfoService;

/**
 * 生成测试Service业务层处理
 * 
 * @author yada
 * @date 2022-01-17
 */
@Service
public class TBTestInfoServiceImpl implements ITBTestInfoService 
{
    @Autowired
    private TBTestInfoMapper tBTestInfoMapper;

    /**
     * 查询生成测试
     * 
     * @param id 生成测试主键
     * @return 生成测试
     */
    @Override
    public TBTestInfo selectTBTestInfoById(String id)
    {
        return tBTestInfoMapper.selectTBTestInfoById(id);
    }

    /**
     * 查询生成测试列表
     * 
     * @param tBTestInfo 生成测试
     * @return 生成测试
     */
    @Override
    public List<TBTestInfo> selectTBTestInfoList(TBTestInfo tBTestInfo)
    {
        return tBTestInfoMapper.selectTBTestInfoList(tBTestInfo);
    }

    /**
     * 新增生成测试
     * 
     * @param tBTestInfo 生成测试
     * @return 结果
     */
    @Override
    public int insertTBTestInfo(TBTestInfo tBTestInfo)
    {
        return tBTestInfoMapper.insertTBTestInfo(tBTestInfo);
    }

    /**
     * 修改生成测试
     * 
     * @param tBTestInfo 生成测试
     * @return 结果
     */
    @Override
    public int updateTBTestInfo(TBTestInfo tBTestInfo)
    {
        return tBTestInfoMapper.updateTBTestInfo(tBTestInfo);
    }

    /**
     * 批量删除生成测试
     * 
     * @param ids 需要删除的生成测试主键
     * @return 结果
     */
    @Override
    public int deleteTBTestInfoByIds(String[] ids)
    {
        return tBTestInfoMapper.deleteTBTestInfoByIds(ids);
    }

    /**
     * 删除生成测试信息
     * 
     * @param id 生成测试主键
     * @return 结果
     */
    @Override
    public int deleteTBTestInfoById(String id)
    {
        return tBTestInfoMapper.deleteTBTestInfoById(id);
    }
}
