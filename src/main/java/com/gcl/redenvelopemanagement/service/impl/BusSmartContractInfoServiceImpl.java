package com.gcl.redenvelopemanagement.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gcl.redenvelopemanagement.mapper.BusSmartContractInfoMapper;
import com.gcl.redenvelopemanagement.domain.BusSmartContractInfo;
import com.gcl.redenvelopemanagement.service.IBusSmartContractInfoService;

/**
 * 智能合约管理Service业务层处理
 * 
 * @author gcl
 * @date 2022-01-13
 */
@Service
public class BusSmartContractInfoServiceImpl implements IBusSmartContractInfoService 
{
    @Autowired
    private BusSmartContractInfoMapper busSmartContractInfoMapper;

    /**
     * 查询智能合约管理
     * 
     * @param id 智能合约管理主键
     * @return 智能合约管理
     */
    @Override
    public BusSmartContractInfo selectBusSmartContractInfoById(String id)
    {
        return busSmartContractInfoMapper.selectBusSmartContractInfoById(id);
    }

    /**
     * 查询智能合约管理列表
     * 
     * @param busSmartContractInfo 智能合约管理
     * @return 智能合约管理
     */
    @Override
    public List<BusSmartContractInfo> selectBusSmartContractInfoList(BusSmartContractInfo busSmartContractInfo)
    {
        return busSmartContractInfoMapper.selectBusSmartContractInfoList(busSmartContractInfo);
    }

    /**
     * 新增智能合约管理
     * 
     * @param busSmartContractInfo 智能合约管理
     * @return 结果
     */
    @Override
    public int insertBusSmartContractInfo(BusSmartContractInfo busSmartContractInfo)
    {
        return busSmartContractInfoMapper.insertBusSmartContractInfo(busSmartContractInfo);
    }

    /**
     * 修改智能合约管理
     * 
     * @param busSmartContractInfo 智能合约管理
     * @return 结果
     */
    @Override
    public int updateBusSmartContractInfo(BusSmartContractInfo busSmartContractInfo)
    {
        return busSmartContractInfoMapper.updateBusSmartContractInfo(busSmartContractInfo);
    }

    /**
     * 批量删除智能合约管理
     * 
     * @param ids 需要删除的智能合约管理主键
     * @return 结果
     */
    @Override
    public int deleteBusSmartContractInfoByIds(String[] ids)
    {
        return busSmartContractInfoMapper.deleteBusSmartContractInfoByIds(ids);
    }

    /**
     * 删除智能合约管理信息
     * 
     * @param id 智能合约管理主键
     * @return 结果
     */
    @Override
    public int deleteBusSmartContractInfoById(String id)
    {
        return busSmartContractInfoMapper.deleteBusSmartContractInfoById(id);
    }
}
