package com.gcl.redenvelopemanagement.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.gcl.common.annotation.Excel;
import com.gcl.common.core.domain.BaseEntity;

/**
 * 生成测试对象 t_b_test_info
 * 
 * @author yada
 * @date 2022-01-17
 */
public class TBTestInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String id;

    /** 文本框 */
    @Excel(name = "文本框")
    private String wenbenkuang;

    /** 文本域 */
    @Excel(name = "文本域")
    private String wenbenyu;

    /** 下拉选 */
    @Excel(name = "下拉选")
    private String xialaxuan;

    /** 单选框 */
    @Excel(name = "单选框")
    private String danxuankuang;

    /** 复选框 */
    @Excel(name = "复选框")
    private String fuxuankuang;

    /** 日期控件 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "日期控件", width = 30, dateFormat = "yyyy-MM-dd")
    private Date riqikongjian;

    /** 图片上传 */
    @Excel(name = "图片上传")
    private String tupianshangchuan;

    /** 文件上传 */
    @Excel(name = "文件上传")
    private String wenjianshangchuan;

    /** 富文本控件 */
    @Excel(name = "富文本控件")
    private String fuwenbenkuangjian;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setWenbenkuang(String wenbenkuang) 
    {
        this.wenbenkuang = wenbenkuang;
    }

    public String getWenbenkuang() 
    {
        return wenbenkuang;
    }
    public void setWenbenyu(String wenbenyu) 
    {
        this.wenbenyu = wenbenyu;
    }

    public String getWenbenyu() 
    {
        return wenbenyu;
    }
    public void setXialaxuan(String xialaxuan) 
    {
        this.xialaxuan = xialaxuan;
    }

    public String getXialaxuan() 
    {
        return xialaxuan;
    }
    public void setDanxuankuang(String danxuankuang) 
    {
        this.danxuankuang = danxuankuang;
    }

    public String getDanxuankuang() 
    {
        return danxuankuang;
    }
    public void setFuxuankuang(String fuxuankuang) 
    {
        this.fuxuankuang = fuxuankuang;
    }

    public String getFuxuankuang() 
    {
        return fuxuankuang;
    }
    public void setRiqikongjian(Date riqikongjian) 
    {
        this.riqikongjian = riqikongjian;
    }

    public Date getRiqikongjian() 
    {
        return riqikongjian;
    }
    public void setTupianshangchuan(String tupianshangchuan) 
    {
        this.tupianshangchuan = tupianshangchuan;
    }

    public String getTupianshangchuan() 
    {
        return tupianshangchuan;
    }
    public void setWenjianshangchuan(String wenjianshangchuan) 
    {
        this.wenjianshangchuan = wenjianshangchuan;
    }

    public String getWenjianshangchuan() 
    {
        return wenjianshangchuan;
    }
    public void setFuwenbenkuangjian(String fuwenbenkuangjian) 
    {
        this.fuwenbenkuangjian = fuwenbenkuangjian;
    }

    public String getFuwenbenkuangjian() 
    {
        return fuwenbenkuangjian;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("wenbenkuang", getWenbenkuang())
            .append("wenbenyu", getWenbenyu())
            .append("xialaxuan", getXialaxuan())
            .append("danxuankuang", getDanxuankuang())
            .append("fuxuankuang", getFuxuankuang())
            .append("riqikongjian", getRiqikongjian())
            .append("tupianshangchuan", getTupianshangchuan())
            .append("wenjianshangchuan", getWenjianshangchuan())
            .append("fuwenbenkuangjian", getFuwenbenkuangjian())
            .toString();
    }
}
