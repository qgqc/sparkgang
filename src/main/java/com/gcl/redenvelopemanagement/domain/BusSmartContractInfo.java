package com.gcl.redenvelopemanagement.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcl.common.annotation.Excel;
import com.gcl.common.core.domain.BaseEntity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 智能合约管理对象 bus_smart_contract_info
 * 
 * @author gcl
 * @date 2022-01-13
 */
public class BusSmartContractInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID主键 */
    private String id;

    /** 智能合约索引 */
    @Excel(name = "智能合约索引")
    private String smartContractIndex;

    /** 活动编号 */
    @Excel(name = "活动编号")
    private String activityCode;

    /** 活动名称 */
    @Excel(name = "活动名称")
    private String activityName;

    /** 活动开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "活动开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date activityStartTime;

    /** 活动结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "活动结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date activityEndTime;

    /** 合约发起方 */
    private String contractInitiator;

    /** 出资方类型 */
    private String investorType;

    /** 合约规则 */
    private String contractRules;

    /** 核心机构号 */
    private Long orgId;

    /** 员工号 */
    private Long staffId;

    /** 版本号 */
    private Long objectVersion;

    /** 创建人 */
    private String createdBy;

    /** 创建时间 */
    private Date createdDate;

    /** 更新人 */
    private String lastUpdatedBy;

    /** 最后更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lastUpdatedDate;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setSmartContractIndex(String smartContractIndex) 
    {
        this.smartContractIndex = smartContractIndex;
    }

    public String getSmartContractIndex() 
    {
        return smartContractIndex;
    }
    public void setActivityCode(String activityCode) 
    {
        this.activityCode = activityCode;
    }

    public String getActivityCode() 
    {
        return activityCode;
    }
    public void setActivityName(String activityName) 
    {
        this.activityName = activityName;
    }

    public String getActivityName() 
    {
        return activityName;
    }
    public void setActivityStartTime(Date activityStartTime) 
    {
        this.activityStartTime = activityStartTime;
    }

    public Date getActivityStartTime() 
    {
        return activityStartTime;
    }
    public void setActivityEndTime(Date activityEndTime) 
    {
        this.activityEndTime = activityEndTime;
    }

    public Date getActivityEndTime() 
    {
        return activityEndTime;
    }
    public void setContractInitiator(String contractInitiator) 
    {
        this.contractInitiator = contractInitiator;
    }

    public String getContractInitiator() 
    {
        return contractInitiator;
    }
    public void setInvestorType(String investorType) 
    {
        this.investorType = investorType;
    }

    public String getInvestorType() 
    {
        return investorType;
    }
    public void setContractRules(String contractRules) 
    {
        this.contractRules = contractRules;
    }

    public String getContractRules() 
    {
        return contractRules;
    }
    public void setOrgId(Long orgId) 
    {
        this.orgId = orgId;
    }

    public Long getOrgId() 
    {
        return orgId;
    }
    public void setStaffId(Long staffId) 
    {
        this.staffId = staffId;
    }

    public Long getStaffId() 
    {
        return staffId;
    }
    public void setObjectVersion(Long objectVersion) 
    {
        this.objectVersion = objectVersion;
    }

    public Long getObjectVersion() 
    {
        return objectVersion;
    }
    public void setCreatedBy(String createdBy) 
    {
        this.createdBy = createdBy;
    }

    public String getCreatedBy() 
    {
        return createdBy;
    }
    public void setCreatedDate(Date createdDate) 
    {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate() 
    {
        return createdDate;
    }
    public void setLastUpdatedBy(String lastUpdatedBy) 
    {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public String getLastUpdatedBy() 
    {
        return lastUpdatedBy;
    }
    public void setLastUpdatedDate(Date lastUpdatedDate) 
    {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public Date getLastUpdatedDate() 
    {
        return lastUpdatedDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("smartContractIndex", getSmartContractIndex())
            .append("activityCode", getActivityCode())
            .append("activityName", getActivityName())
            .append("activityStartTime", getActivityStartTime())
            .append("activityEndTime", getActivityEndTime())
            .append("contractInitiator", getContractInitiator())
            .append("investorType", getInvestorType())
            .append("contractRules", getContractRules())
            .append("orgId", getOrgId())
            .append("staffId", getStaffId())
            .append("objectVersion", getObjectVersion())
            .append("createdBy", getCreatedBy())
            .append("createdDate", getCreatedDate())
            .append("lastUpdatedBy", getLastUpdatedBy())
            .append("lastUpdatedDate", getLastUpdatedDate())
            .toString();
    }
}
