package com.gcl.redenvelopemanagement.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.redenvelopemanagement.domain.BusSmartContractInfo;
import com.gcl.redenvelopemanagement.service.IBusSmartContractInfoService;
/**
 * 智能合约管理Controller
 * 
 * @author gcl
 * @date 2022-01-13
 */
@RestController
@RequestMapping("/redenvelopemanagement/bussmartcontractinfo")
public class BusSmartContractInfoController extends BaseController
{
    @Autowired
    private IBusSmartContractInfoService busSmartContractInfoService;

    /**
     * 查询智能合约管理列表
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:bussmartcontractinfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusSmartContractInfo busSmartContractInfo)
    {
        startPage();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        List<BusSmartContractInfo> list = busSmartContractInfoService.selectBusSmartContractInfoList(busSmartContractInfo);
        return getDataTable(list);
    }

    /**
     * 导出智能合约管理列表
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:bussmartcontractinfo:export')")
    @Log(title = "智能合约管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BusSmartContractInfo busSmartContractInfo)
    {
        List<BusSmartContractInfo> list = busSmartContractInfoService.selectBusSmartContractInfoList(busSmartContractInfo);
        ExcelUtil<BusSmartContractInfo> util = new ExcelUtil<BusSmartContractInfo>(BusSmartContractInfo.class);
        return util.exportExcel(list, "智能合约管理数据");
    }

    /**
     * 获取智能合约管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:bussmartcontractinfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(busSmartContractInfoService.selectBusSmartContractInfoById(id));
    }

    /**
     * 新增智能合约管理
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:bussmartcontractinfo:add')")
    @Log(title = "智能合约管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusSmartContractInfo busSmartContractInfo)
    {
        return toAjax(busSmartContractInfoService.insertBusSmartContractInfo(busSmartContractInfo));
    }

    /**
     * 修改智能合约管理
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:bussmartcontractinfo:edit')")
    @Log(title = "智能合约管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusSmartContractInfo busSmartContractInfo)
    {
        return toAjax(busSmartContractInfoService.updateBusSmartContractInfo(busSmartContractInfo));
    }

    /**
     * 删除智能合约管理
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:bussmartcontractinfo:remove')")
    @Log(title = "智能合约管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(busSmartContractInfoService.deleteBusSmartContractInfoByIds(ids));
    }
}
