package com.gcl.redenvelopemanagement.controller;

import java.util.List;
import java.util.UUID;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.redenvelopemanagement.domain.TBTestInfo;
import com.gcl.redenvelopemanagement.service.ITBTestInfoService;

/**
 * 生成测试Controller
 * 
 * @author yada
 * @date 2022-01-17
 */
@RestController
@RequestMapping("/redenvelopemanagement/test")
public class TBTestInfoController extends BaseController
{
    @Autowired
    private ITBTestInfoService tBTestInfoService;

    /**
     * 查询生成测试列表
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:test:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBTestInfo tBTestInfo)
    {
        startPage();
        List<TBTestInfo> list = tBTestInfoService.selectTBTestInfoList(tBTestInfo);
        return getDataTable(list);
    }

    /**
     * 导出生成测试列表
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:test:export')")
    @Log(title = "生成测试", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TBTestInfo tBTestInfo)
    {
        List<TBTestInfo> list = tBTestInfoService.selectTBTestInfoList(tBTestInfo);
        ExcelUtil<TBTestInfo> util = new ExcelUtil<TBTestInfo>(TBTestInfo.class);
        return util.exportExcel(list, "生成测试数据");
    }

    /**
     * 获取生成测试详细信息
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:test:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(tBTestInfoService.selectTBTestInfoById(id));
    }

    /**
     * 新增生成测试
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:test:add')")
    @Log(title = "生成测试", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBTestInfo tBTestInfo)
    {
        String id = UUID.randomUUID().toString().replace("-", "");
        tBTestInfo.setId(id);
        return toAjax(tBTestInfoService.insertTBTestInfo(tBTestInfo));
    }

    /**
     * 修改生成测试
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:test:edit')")
    @Log(title = "生成测试", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBTestInfo tBTestInfo)
    {
        return toAjax(tBTestInfoService.updateTBTestInfo(tBTestInfo));
    }

    /**
     * 删除生成测试
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:test:remove')")
    @Log(title = "生成测试", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(tBTestInfoService.deleteTBTestInfoByIds(ids));
    }
}
