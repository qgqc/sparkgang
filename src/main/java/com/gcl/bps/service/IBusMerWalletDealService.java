package com.gcl.bps.service;

import com.gcl.bps.domain.BusMerWalletDeal;

import java.util.List;

public interface IBusMerWalletDealService {

    int insertBusMerWalletDeal(BusMerWalletDeal busMerWalletDeal);

    BusMerWalletDeal selectBusMerWalletDealById(String id);

    int updateBusMerWalletDeal(BusMerWalletDeal busMerWalletDeal);


    void updateList(List<BusMerWalletDeal> list);
}
