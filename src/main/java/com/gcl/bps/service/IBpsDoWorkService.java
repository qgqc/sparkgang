package com.gcl.bps.service;


/**
 *
 * @author wjw
 * @date 2022-01-18
 */
public interface IBpsDoWorkService {

    void doWorkCWFS();
    void doWorkPWLS();
    void doWorkBWLS();

    void doWorkPPFS();

    void doWorkWLFS();
}
