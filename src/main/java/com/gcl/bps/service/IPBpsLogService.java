package com.gcl.bps.service;


import com.gcl.bps.domain.DBLogger;

/**
 *
 * @author wjw
 * @date 2022-01-18
 */
public interface IPBpsLogService {

    void logDeal();

    DBLogger getLogger(Class<?> clazz, String operType);
}
