package com.gcl.bps.service.impl;

import com.gcl.bps.constant.DataStatusEnum;
import com.gcl.bps.constant.FileStatus;
import com.gcl.bps.constant.Status;
import com.gcl.bps.domain.BpsOperType;
import com.gcl.bps.domain.BusMerWalletDeal;
import com.gcl.bps.domain.DBLogger;
import com.gcl.bps.service.IPBpsLogService;
import com.gcl.bps.util.FileTools;
import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;
import com.gcl.corporatewalletmanagement.mapper.BusBpsFilehandlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/*****
 * 创建文件服务
 */
@Service
public class CreateFileService {
    @Autowired
    private IPBpsLogService ipBpsLogService;

    @Value("${bpsCreatLocalPath}")
    private String bpsCreatPath;
    @Autowired
    private BusBpsFilehandlMapper busBpsFilehandlMapper;

    @Autowired
    private BpsOperType bpsOperType;

    //初始化BPS日志
    private DBLogger bpsLogger = null;

    @PostConstruct
    public void initLogger(){
        bpsLogger =  ipBpsLogService.getLogger(MerAndWalletService.class, bpsOperType.cwfsCreateFile);
    }

    /**
     * @Description: 生成文件(生成文件成功后需要进行更改数据库的状态 ( 包括文件信息的状态和对应每条明细的状态))
     * @Author: shaowu.ni
     * @Date:
     */
    public Boolean create(BusBpsFilehandl head, ArrayList<BusMerWalletDeal> list, String type)  {
        bpsLogger.info("do start cerate file ,filename:" + head.getSourceFile());
        File file = new File(bpsCreatPath +type+ File.separator + head.getSourceFile());
        FileOutputStream out = null;
        BufferedOutputStream buff = null;
        try {
            //写入文件头 H开头部分
            Boolean status = FileTools.writeToTxtEncoding(head.getHeadString(type), file, "utf-8");
            if (!status) {
                return false;
            }

            //追加文件正文  详细数据条目 D开头部分
            out=new FileOutputStream(file,true);
            buff=new BufferedOutputStream(out);
            //获得明细文件 count代表序列号
            long count = 2 ;
            for (BusMerWalletDeal tbMerWalletDeal : list) {
                tbMerWalletDeal.setSequenceNumber(count);
                buff.write(tbMerWalletDeal.getFileRow().getBytes("utf-8"));
                buff.flush();
                tbMerWalletDeal.setHeadId(head.getId());
                tbMerWalletDeal.setDataStatus(DataStatusEnum.CREATE_SUCCESS.getStatus());
                count++;
            }
            //更新文件头信息表   更新明细文件的状态，更新头文件信息的状态
            Map queryParam = new HashMap();
            queryParam.put("proNo",FileStatus.CREATE_FILE.getStatus());
            queryParam.put("status", Status.SUCCESS);
            queryParam.put("id", head.getId());
            busBpsFilehandlMapper.updateById(queryParam);
            return true;
        } catch (Exception e) {
            bpsLogger.info(" create file fail:,{}",e);
            return false;
        } finally {
            try {
                if (buff != null){
                    buff.close();
                }
                if (buff != null){
                    out.close();
                }
            } catch (Exception e) {
                bpsLogger.info("关闭句柄 fail:,{}",e);
                return false;
            }
        }
    }
}