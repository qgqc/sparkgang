package com.gcl.bps.service.impl;

import com.gcl.bps.constant.BpsTypeConstant;
import com.gcl.bps.domain.BpsOperType;
import com.gcl.bps.domain.DBLogger;
import com.gcl.bps.service.IBpsDoWorkService;
import com.gcl.bps.service.IPBpsLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 *
 * @author wjw
 * @date 2022-01-18
 */
@Service
public class BpsDoWorkServiceImpl implements IBpsDoWorkService {


    @Autowired
    private IPBpsLogService ipBpsLogService;


    /**公有钱包BPS处理service**/
    @Autowired
    private MerAndWalletService merAndWalletService;

    /**白名单APP BPS处理service**/
    @Autowired
    private WhiteAppService whiteAppService;

    @Autowired
    private WhiteAppDealService whiteAppDealService;


    @Autowired
    private ProcessBPSFileService processBPSFileService;

    @Autowired
    private RegisterWhiteListService registerWhiteListService;

    @Autowired
    private  RegisterWhiteListRetService registerWhiteListRetService;

    @Autowired
    private PrivateWalletService privateWalletService;


    @Autowired
    private PriDealDataService priDealDataService;

    /*@Autowired
    private UserAndMerWhiteService userAndMerWhiteService;*/

    @Autowired
    private BpsOperType bpsOperType;
    /**
     * @Description:按照业务类型生成需要上传的bps文件
     * @Param: type 业务类型（人行共建白名单，个人/商户钱包开立等）
     * @Date:
     */
    @Override
    public void doWorkCWFS() {
        //初始化日志  该日志用于将一组操作记录日志并存储到数据库中
        //原系统一个controller一个logger，新系统中做了修改，一个doWorkCWFS一个logger对象
        DBLogger bpsLogger = ipBpsLogService.getLogger(BpsDoWorkServiceImpl.class, bpsOperType.cwfsService);
        try {
            bpsLogger.info("user/merchant  "+ BpsTypeConstant.CWFS_TYPE +" do work start");
            //1.生成DAT文件
            merAndWalletService.carryOut(BpsTypeConstant.CWFS_TYPE );

            //2.从远程FTP服务器读取回盘RET 上传生成的DAT到远程FTP服务器
            processBPSFileService.downloadRETFileAndupDATFile(BpsTypeConstant.CWFS_TYPE );

        } finally {
            bpsLogger.Save();
        }
    }


    /******
     * PWLS 具体处理任务
     */
    @Override
    public void doWorkPWLS() {
        //初始化日志  该日志用于将一组操作记录日志并存储到数据库中
        //原系统一个controller一个logger，新系统中做了修改，一个doWorkCWFS一个logger对象
        DBLogger bpsLogger = ipBpsLogService.getLogger(BpsDoWorkServiceImpl.class, bpsOperType.pwlsService);
        try {
            bpsLogger.info("pwls create file start" + BpsTypeConstant.PWLS_TYPE);

            whiteAppService.carryOut(BpsTypeConstant.PWLS_TYPE);
            whiteAppDealService.updateData(BpsTypeConstant.PWLS_TYPE);
        } finally {
            bpsLogger.Save();
        }
    }


    /*****
     * BWLS 具体处理任务
     */
    @Override
    public void doWorkBWLS() {
        DBLogger bpsLogger = ipBpsLogService.getLogger(BpsDoWorkServiceImpl.class, bpsOperType.bwlsService);
        try {
            bpsLogger.info("create file start" + BpsTypeConstant.BWLS_TYPE);

            registerWhiteListService.carryOut(BpsTypeConstant.BWLS_TYPE);

            registerWhiteListRetService.updateData(BpsTypeConstant.BWLS_TYPE);
        }finally {
            bpsLogger.Save();
        }
    }

    @Override
    public void doWorkPPFS() {
        DBLogger bpsLogger = ipBpsLogService.getLogger(BpsDoWorkServiceImpl.class, bpsOperType.ppfsService);

        try {
            bpsLogger.info("create file start" + BpsTypeConstant.PPFS_TYPE);

            privateWalletService.carryOut(BpsTypeConstant.PPFS_TYPE);

            priDealDataService.updateData(BpsTypeConstant.PPFS_TYPE);
        }finally {
            bpsLogger.Save();
        }

    }


    /******
     * 这个先放放，等以后再迁移
     */
    @Override
    public void doWorkWLFS() {
        DBLogger bpsLogger = ipBpsLogService.getLogger(BpsDoWorkServiceImpl.class, bpsOperType.wlfsService);
        try {
            bpsLogger.info("create file start" + BpsTypeConstant.WLFS_TYPE);

            //userAndMerWhiteService.carryOut(BpsTypeConstant.WLFS_TYPE);

            //userAndMerDealDataService.updateData(BpsTypeConstant.WLFS_TYPE);

        }finally {
            bpsLogger.Save();
        }

    }
}
