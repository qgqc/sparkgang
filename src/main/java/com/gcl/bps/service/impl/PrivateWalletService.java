package com.gcl.bps.service.impl;


import com.gcl.bps.constant.FileStatus;
import com.gcl.bps.constant.PrivateState;
import com.gcl.bps.constant.Status;
import com.gcl.bps.domain.BpsOperType;
import com.gcl.bps.domain.DBLogger;
import com.gcl.bps.domain.OrgAndNum;
import com.gcl.bps.service.IPBpsLogService;
import com.gcl.bps.util.DateUtil;
import com.gcl.bps.util.FileTools;
import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;
import com.gcl.corporatewalletmanagement.mapper.BusBpsFilehandlMapper;
import com.gcl.personalwalletmanagement.domain.BusPrivateInfoBps;
import com.gcl.personalwalletmanagement.mapper.BusPrivateInfoBpsMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program:bps
 * @description:
 * @author: shaowu.ni
 * @create:2021-11-15 17:30
 **/
@Service
public class PrivateWalletService {
    @Autowired
    private BusPrivateInfoBpsMapper busPrivateInfoBpsMapper;

    @Autowired
    private BusBpsFilehandlMapper busBpsFilehandlMapper;

    @Autowired
    private PrepareService prepareService;

    @Value("${daySize}")
    private int daySize;

    @Autowired
    private PrivateCreateFile privateCreateFile;
    @Value("${bpsCreatLocalPath}")
    private String bpsCreatPath;

    @Autowired
    private IPBpsLogService ipBpsLogService;

    @Autowired
    private BpsOperType bpsOperType;

    //初始化BPS日志
    private DBLogger bpsLogger = null;


    @PostConstruct
    public void initLogger(){
        bpsLogger =  ipBpsLogService.getLogger(PrivateWalletService.class, bpsOperType.ppfsCreateFile);
    }

    public void carryOut(String type) {
        FileTools.CreateDir(bpsCreatPath + type);
        String oldDate = DateUtil.getOldDay(daySize);
        String fileBatchNo = DateUtil.getCurDateTime();
//        privateCreateFile.proDeal();

        Map queryParam = new HashMap();
        queryParam.put("status", PrivateState.REVIEW_PASSED.getStatus());
        queryParam.put("oldDate", oldDate);
        int i = busPrivateInfoBpsMapper.countByDataStatusAndExamineSuccessDateGreaterThanEqual(queryParam);
        if (i <= 0) {
            bpsLogger.info("ppfs not having data creat file");
            return;
        }
        // todo 更新数据库需要上传数据的批次，并获取批次号
        queryParam = new HashMap();
        queryParam.put("fileBatchNo",fileBatchNo);
        queryParam.put("status",PrivateState.SYSTEM_PROCESSING.getStatus());
        queryParam.put("oldDate", oldDate);
        busPrivateInfoBpsMapper.updatefileBatchNoByStatus(queryParam);

        // todo 按交易机构号和柜员号进行分组
        List<OrgAndNum> orgAndNums = busPrivateInfoBpsMapper.selectCount(fileBatchNo);
        //        todo 计算文件信息和文件的头信息
        try {
            for (OrgAndNum orgAndNum : orgAndNums) {
                bpsLogger.info("do start create file prepare , fileBatchNo : " + fileBatchNo);
                ArrayList<BusBpsFilehandl> data = prepareService.prepare(orgAndNum, type);
                // todo 执行 准备方法 失败
                if (data == null) {
                    return;
                }
                //         todo 按照计算出的文件信息循环生成文件
                for (BusBpsFilehandl datum : data) {
                    busBpsFilehandlMapper.insertBusBpsFilehandl(datum);
                    //       todo 查询明细数据
                    queryParam = new HashMap();
                    queryParam.put("fileBatchNo", fileBatchNo);
                    queryParam.put("status", PrivateState.SYSTEM_PROCESSING.getStatus());
                    queryParam.put("detialCount", datum.getDetialCount());
                    ArrayList<BusPrivateInfoBps> TBPrivateWalletInfos = busPrivateInfoBpsMapper.selectByFileBatchNo(queryParam);
                    //        todo  生成文件
                    Boolean status = privateCreateFile.create(datum, TBPrivateWalletInfos, type);
                    datum.setProNo(FileStatus.CREATE_FILE.getStatus());
                    if (!status) {
                        datum.setStatu(Status.FAIL);
                        for (BusPrivateInfoBps tbMerWalletInfo : TBPrivateWalletInfos) {
                            tbMerWalletInfo.setDataStatus(PrivateState.PPFS_OPENING_FAILED.getStatus());
                        }
                        privateCreateFile.dataUpdate(datum, TBPrivateWalletInfos, FileStatus.CREATE_FILE.getStatus(), Status.FAIL);
                        privateCreateFile.postDeal(PrivateState.PPFS_OPENING_FAILED.getStatus(),datum.getId());
                        bpsLogger.error("create file fail :" + datum.getSourceFile());
                        continue;
                    } else {
                        bpsLogger.info("create file success :" + datum.getSourceFile());
                    }
                }
            }
        } finally {
            bpsLogger.Save();
        }
    }
}
