package com.gcl.bps.service.impl;


import com.gcl.bps.constant.FileStatus;
import com.gcl.bps.constant.PrivateState;
import com.gcl.bps.constant.Status;
import com.gcl.bps.domain.BpsOperType;
import com.gcl.bps.domain.DBLogger;
import com.gcl.bps.domain.OrgAndNum;
import com.gcl.bps.service.IPBpsLogService;
import com.gcl.bps.util.DateUtil;
import com.gcl.bps.util.FileTools;
import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;
import com.gcl.corporatewalletmanagement.mapper.BusBpsFilehandlMapper;
import com.gcl.personalwalletmanagement.domain.BusPrivateWhitelistRegister;
import com.gcl.personalwalletmanagement.mapper.BusPrivateWhitelistRegisterMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @program:bps
 * @description:
 * @author: shaowu.ni
 * @create:2021-11-15 17:30
 **/
@Service
public class RegisterWhiteListService {

    @Autowired
    private IPBpsLogService ipBpsLogService;

    @Autowired
    private BusPrivateWhitelistRegisterMapper busPrivateWhitelistRegisterMapper;


    @Autowired
    private BusBpsFilehandlMapper busBpsFilehandlMapper;
    @Autowired
    private PrepareService prepareService;
    @Value("${daySize}")
    private int daySize;

    @Autowired
    private RegisterWhiteListCreateFile registerWhiteListCreateFile;
    @Value("${bpsCreatLocalPath}")
    private String bpsCreatPath;
    private  DBLogger bpsLogger;

    @Autowired
    private BpsOperType bpsOperType;

    @PostConstruct
    public void initLogger(){
        bpsLogger =  ipBpsLogService.getLogger(RegisterWhiteListService.class, bpsOperType.bwlsCreateFile);
    }



    public void carryOut(String type) {
        FileTools.CreateDir(bpsCreatPath + type);
        String oldDate = DateUtil.getOldDay(daySize);
        registerWhiteListCreateFile.proDeal();
        String fileBatchNo = DateUtil.getCurDateTime();

        Map queryParam = new HashMap();
        queryParam.put("status", PrivateState.REVIEW_PASSED.getStatus());
        queryParam.put("oldDate", oldDate);
        int i = busPrivateWhitelistRegisterMapper.countByDataStatusAndExamineDateGreaterThanEqual(queryParam);
        if (i <= 0) {
            bpsLogger.info("bwls not having data creat file");
            return;
        }


        // 更新数据库需要上传数据的批次，并获取批次号
        queryParam = new HashMap();
        queryParam.put("fileBatchNo",fileBatchNo);
        queryParam.put("status",PrivateState.SYSTEM_PROCESSING.getStatus());
        queryParam.put("oldDate", oldDate);
        busPrivateWhitelistRegisterMapper.updatefileBatchNoByStatus(queryParam);

        // todo 按交易机构号和柜员号进行分组
        List<OrgAndNum> orgAndNums = busPrivateWhitelistRegisterMapper.selectCount(fileBatchNo);
        //        todo 计算文件信息和文件的头信息
        try {
            for (OrgAndNum orgAndNum : orgAndNums) {
                bpsLogger.info("do start create file prepare , fileBatchNo : " + fileBatchNo);
                ArrayList<BusBpsFilehandl> data = prepareService.prepare(orgAndNum, type);
                // todo 执行 准备方法 失败
                if (data == null) {
                    return;
                }
                //         todo 按照计算出的文件信息循环生成文件
                for (BusBpsFilehandl datum : data) {
                    busBpsFilehandlMapper.insertBusBpsFilehandl(datum);
                    //查询明细数据

                    queryParam = new HashMap();
                    queryParam.put("fileBatchNo", fileBatchNo);
                    queryParam.put("status", PrivateState.SYSTEM_PROCESSING.getStatus());
                    queryParam.put("detialCount", datum.getDetialCount());
                    ArrayList<BusPrivateWhitelistRegister>  tbWalletWhiteListRegisters= busPrivateWhitelistRegisterMapper.selectByFileBatchNo(queryParam);
                    //        todo  生成文件
                    Boolean status = registerWhiteListCreateFile.create(datum, tbWalletWhiteListRegisters, type);
                    datum.setProNo(FileStatus.CREATE_FILE.getStatus());
                    if (!status) {
                        datum.setStatu(Status.FAIL);
                        for (BusPrivateWhitelistRegister tbWalletWhiteListRegister : tbWalletWhiteListRegisters) {
                            tbWalletWhiteListRegister.setDataStatus(PrivateState.BWLS_OPENING_FAILED.getStatus());
                        }
                        registerWhiteListCreateFile.dataUpdate(datum, tbWalletWhiteListRegisters, FileStatus.CREATE_FILE.getStatus(), Status.FAIL);
                        registerWhiteListCreateFile.postDeal(PrivateState.BWLS_OPENING_FAILED.getStatus(),datum.getId());
                        bpsLogger.error("create file fail :" + datum.getSourceFile());
                        continue;
                    }else {
                        bpsLogger.info("create file success :" + datum.getSourceFile());
                    }
                }
            }
        } finally {
            bpsLogger.Save();
        }
    }
}
