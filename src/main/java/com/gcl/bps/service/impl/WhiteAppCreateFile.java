package com.gcl.bps.service.impl;


import com.gcl.bps.constant.FileStatus;
import com.gcl.bps.constant.PrivateState;
import com.gcl.bps.constant.StaticDataEnum;
import com.gcl.bps.constant.Status;
import com.gcl.bps.domain.BpsOperType;
import com.gcl.bps.domain.BusPrivateStaticData;
import com.gcl.bps.domain.DBLogger;
import com.gcl.bps.mapper.BusPrivateStaticDataMapper;
import com.gcl.bps.service.IPBpsLogService;
import com.gcl.bps.util.FileTools;
import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;
import com.gcl.corporatewalletmanagement.mapper.BusBpsFilehandlMapper;
import com.gcl.personalwalletmanagement.mapper.BusPrivateInfoMapper;
import com.gcl.scancodemanagement.domain.BusPbocccoBuildappwhitelist;
import com.gcl.scancodemanagement.mapper.BusPbocccoBuildappwhitelistMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @program:bps
 * @description: 生成bps（）文件
 * @author: shaowu.ni
 * @create:2021-11-05 09:35
 **/
@Service
public class WhiteAppCreateFile {
    private DBLogger logger;

    @Autowired
    private BpsOperType bpsOperType;

    @Autowired
    private IPBpsLogService ipBpsLogService;

    @Value("${bpsCreatLocalPath}")
    private String bpsCreatPath;

    @Autowired
    private BusPrivateInfoMapper busPrivateInfoMapper;

    private  HashMap<String, String> map;
    @Autowired
    private BusBpsFilehandlMapper busBpsFilehandlMapper;

    @Autowired
    private BusPbocccoBuildappwhitelistMapper busPbocccoBuildappwhitelistMapper;

    @Autowired
    private BusPrivateStaticDataMapper busPrivateStaticDataMapper;


    @PostConstruct
    public void initData(){
        this.map = new HashMap<>();
        ArrayList<BusPrivateStaticData> byType = busPrivateStaticDataMapper.findByType(StaticDataEnum.APP_LIST_WALLET.getStatus());
        for (BusPrivateStaticData privateWalletStaticData : byType) {
            map.put(privateWalletStaticData.getKey(), privateWalletStaticData.getValue());
        }
        logger =  ipBpsLogService.getLogger(MerAndWalletService.class, bpsOperType.pwlsCreateFile);
    }



    /**
     * @Description: 生成文件(生成文件成功后需要进行更改数据库的状态 ( 包括文件信息的状态和对应每条明细的状态))
     * @Author: shaowu.ni
     * @Date:
     */
    public Boolean create(BusBpsFilehandl head, ArrayList<BusPbocccoBuildappwhitelist> list, String type)  {
        logger.info("do start cerate file ,filename:" + head.getSourceFile());
        File file = new File(bpsCreatPath +type+ File.separator + head.getSourceFile());
        FileOutputStream out = null;
        BufferedOutputStream buff = null;
        try {
            //        todo 获得文件头
            Boolean status = FileTools.writeToTxtEncoding(head.pwlsHeadMsg(type), file, "utf-8");
            if (!status) {
                return false;
            }
            out = new FileOutputStream(file,true);
            buff = new BufferedOutputStream(out);
            //        todo 获得明细文件 count代表序列号
            long count = 2 ;
            for (BusPbocccoBuildappwhitelist pBOCCCoBuildAPPWhitelist : list) {
                pBOCCCoBuildAPPWhitelist.setSequenceNumber(count+"");
                buff.write(pBOCCCoBuildAPPWhitelist.getDetailFileRow().getBytes("utf-8"));
                buff.flush();
                pBOCCCoBuildAPPWhitelist.setHeadId(head.getId());
                pBOCCCoBuildAPPWhitelist.setDataStatus(PrivateState.PWLS_SYSTEM_PROCESSING.getStatus());
                count++;
            }
            //        todo 更新明细文件的状态，更新头文件信息的状态
            dataUpdate(head,list, FileStatus.CREATE_FILE.getStatus(),Status.SUCCESS);
            return true;
        } catch (Exception e) {
            logger.info(" create file fail:,{}",e);
            return false;
        } finally {
            try {
                if (buff != null){
                    buff.close();
                }
                if (buff != null){
                    out.close();
                }
            } catch (Exception e) {
               logger.info("关闭句柄 fail:,{}",e);
               return false;
            }
        }
    }
    @Transactional
    public void dataUpdate(BusBpsFilehandl head, ArrayList<BusPbocccoBuildappwhitelist> list, String proNo, String status){

        Map queryParam = new HashMap();
        queryParam.put("proNo",proNo);
        queryParam.put("status", status);
        queryParam.put("id", head.getId());
        busBpsFilehandlMapper.updateById(queryParam);

        busPbocccoBuildappwhitelistMapper.updateAll(list);
    }

    /**
     * 将数据插入bps表中
     */
    public void proDeal() {
        BusPbocccoBuildappwhitelist insertObj = new BusPbocccoBuildappwhitelist();
        insertObj.setTradingOrganizationNo(map.get("tradingOrganizationNo"));
        insertObj.setContactType(map.get("contactType"));
        insertObj.setPhoneAreaCode(map.get("phoneAreaCode"));
        insertObj.setPhoneCityCode(map.get("phoneCityCode"));
        insertObj.setChangeType(map.get("changeType"));
        insertObj.setCustomerType(map.get("customerType"));
        insertObj.setDataStatus(PrivateState.REVIEW_PASSED.getStatus());

        busPbocccoBuildappwhitelistMapper.insertByStatus(insertObj);

        Map queryParam = new HashMap();
        queryParam.put("dataStatus1",PrivateState.PWLS_SYSTEM_PROCESSING.getStatus());
        queryParam.put("dataStatus2", PrivateState.REVIEW_PASSED.getStatus());
        busPrivateInfoMapper.updatePwlsUpdateById(queryParam);
    }

    /**
     * 更新业务表中的数据状态
     *
     * @param status
     * @param headId
     */
    public void postDeal(String status, String headId) {

        Map queryParam = new HashMap();
        queryParam.put("status",status);
        queryParam.put("headId", headId);

        busPrivateInfoMapper.updatePwlsStatusByHeadId(queryParam);
    }
}
