package com.gcl.bps.service.impl;


import com.gcl.bps.domain.DBLogger;
import com.gcl.bps.service.IPBpsLogService;
import com.gcl.bps.util.FileTools;
import com.gcl.bps.util.FtpUtil;
import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;
import com.gcl.corporatewalletmanagement.mapper.BusBpsFilehandlMapper;
import com.gcl.corporatewalletmanagement.mapper.BusMerWalletInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @program:bps
 * @description:
 * @author: shaowu.ni
 * @create:2021-11-02 18:05
 **/
@Service
public class DownloadAndUpService {

    @Autowired
    private BusBpsFilehandlMapper busBpsFilehandlMapper;

    @Autowired
    private BusMerWalletInfoMapper busMerWalletInfoMapper;

    @Autowired
    private IPBpsLogService ipBpsLogService;

    @Value("${bpsDownFileLocalPath}")
    private String downloadLocalPath;
    @Value("${bpsCreatLocalPath}")
    private String bpsCreatLocalPath;

    @Value("${bpsDownFilePath}")
    private String downloadPath;
    @Value("${bpsCreatPath}")
    private String bpsCreatPath;

    @Value("${bpsFtpIp}")
    private String ip;
    @Value("${ftpPort}")
    private int ftpPort;
    @Value("${ftpUserName}")
    private String ftpUserName;
    @Value("${ftpPassword}")
    private String ftpPassword;

    //初始化BPS日志
    private DBLogger bpsLogger = null;

    @PostConstruct
    public void initLogger(){
        bpsLogger =  ipBpsLogService.getLogger(MerAndWalletService.class, "03");
    }

    /**
     * @Description:下载bps回盘文件
     * @return: 返回下载成功的 文件头信息 列表
     * @Author: shaowu.ni
     * @Date:
     */

    public ArrayList<BusBpsFilehandl> download(String type, String date) {
        //创建本地download文件夹
        FileTools.CreateDir(downloadLocalPath + type);
        ArrayList<BusBpsFilehandl> tbFileHeads = new ArrayList<>();


        //查询头文件信息表，获取在处理范围内的头文件信息列表
        Map queryParam = new HashMap();
        queryParam.put("date", date);
        queryParam.put("type", type);
        ArrayList<BusBpsFilehandl> list = busBpsFilehandlMapper.findByStatuDownFile(queryParam);
        //初始化FTP，准备下载
        FtpUtil ftpUtil = new FtpUtil(ip, ftpPort, ftpUserName, ftpPassword, downloadPath);
        if (list != null) {
            for (BusBpsFilehandl tbFileHead : list) {

                //下载文件，下载完成后更新头文件信息表中 信息状态
                bpsLogger.info("do start down RET file :" + tbFileHead.getSourceFile().replace(".DAT", ".RET"));
                String fileName = ftpUtil.downloadToPath(tbFileHead.getSourceFile().replace(".DAT", ".RET"), downloadLocalPath+type);
                //设置ProNo状态为 3 已回盘更新
                tbFileHead.setProNo("3");
                //设置Statu状态为 1 成功
                tbFileHead.setStatu("1");
                //如果 文件头信息中 文件名 DAT 替换为 RET 失败，则设置状态为 Statu状态为2失败
                if (!fileName.contains(tbFileHead.getSourceFile().replace("DAT","RET"))){
                    tbFileHead.setStatu("2");
                }else {
                    //将成功的文件头信息加入列表中，最后返回
                    tbFileHeads.add(tbFileHead);
                }
                //更新文件头信息
                busBpsFilehandlMapper.updateBusBpsFilehandl(tbFileHead);
                bpsLogger.info("RET file download SUCCESS or ERT FILE NOT BACK!");
                continue;
            }
        }
        return tbFileHeads;
    }

    /**
     * @Description:
     * @return  返回上传成功的BusBpsFilehandl信息列表
     * @Param:上传bps文件
     * @Author: shaowu.ni
     * @Date:
     */
    public  ArrayList<BusBpsFilehandl> upFile(String type, String date) {
        ArrayList<BusBpsFilehandl> tbFileHeads = new ArrayList<>();

        //查询处于 生成 |成功 或 上传 |失败 的头文件信息，进行上传
        Map queryParam = new HashMap();
        queryParam.put("date", date);
        queryParam.put("type", type);
        ArrayList<BusBpsFilehandl> list = busBpsFilehandlMapper.findByStatuUpFile(queryParam);
        //初始化FTP服务器
        FtpUtil ftpUtil = new FtpUtil(ip, ftpPort, ftpUserName, ftpPassword, bpsCreatPath);
        if (list != null) {
            for (BusBpsFilehandl tbFileHead : list) {

                //上传后 更新 头文件信息表中的数据 为 上传|成功 状态
                File file = new File(bpsCreatLocalPath +type+"/"+ tbFileHead.getSourceFile().trim());
                if (!file.exists()){
                    bpsLogger.error(file.getName()+" not exists");
                }
                String fileName = ftpUtil.uploadFile(file);
                tbFileHead.setProNo("2");
                tbFileHead.setStatu("1");
                if (!fileName.contains(tbFileHead.getSourceFile().trim())){
                    tbFileHead.setStatu("2");
                }else {
                    tbFileHeads.add(tbFileHead);
                }
                busBpsFilehandlMapper.updateBusBpsFilehandl(tbFileHead);
                bpsLogger.info("up "+tbFileHead.getSourceFile()+" or error resaon: ---" + fileName);
                continue;
            }
        }
        return tbFileHeads;
    }
}
