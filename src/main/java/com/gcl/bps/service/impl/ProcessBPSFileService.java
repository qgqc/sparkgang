package com.gcl.bps.service.impl;


import com.gcl.bps.constant.DataStatusEnum;
import com.gcl.bps.constant.FileStatus;
import com.gcl.bps.constant.Status;
import com.gcl.bps.domain.BpsOperType;
import com.gcl.bps.domain.BusMerWalletDeal;
import com.gcl.bps.domain.DBLogger;
import com.gcl.bps.mapper.BusMerWalletDealMapper;
import com.gcl.bps.service.IPBpsLogService;
import com.gcl.bps.util.DateUtil;
import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;
import com.gcl.corporatewalletmanagement.domain.BusMerWalletInfo;
import com.gcl.corporatewalletmanagement.mapper.BusBpsFilehandlMapper;
import com.gcl.corporatewalletmanagement.mapper.BusMerWalletInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.*;

/**
 * @program:bps
 * @description:
 * @author: shaowu.ni
 * @create:2021-11-02 17:59
 **/
@Service
public class ProcessBPSFileService {

    @Value("${bpsDownFileLocalPath}")
    private String path;
    @Value("${daySize}")
    private int daySize;
    @Autowired
    private BusBpsFilehandlMapper busBpsFilehandlMapper;
    @Autowired
    private BusMerWalletInfoMapper busMerWalletInfoMapper;
    @Autowired
    private BusMerWalletDealMapper busMerWalletDealMapper;
    @Autowired
    private DownloadAndUpService downloadAndUpService;



    @Autowired
    private IPBpsLogService ipBpsLogService;
    @Autowired
    private BpsOperType bpsOperType;

    //初始化BPS日志
    private DBLogger bpsLogger = null;

    @PostConstruct
    public void initLogger(){
        bpsLogger =  ipBpsLogService.getLogger(ProcessBPSFileService.class, bpsOperType.cwfsUpdateData);
    }

    //按照回盘文件进行数据的更新,不同的业务需要更新的字段也不同
    public void downloadRETFileAndupDATFile(String type) {
        bpsLogger.info("UpdateService start do type:" + type);
        File file = null;
        try {
            //调用文件上传和下载接口，先下载再上传

            /*****
             * 从远程FTP服务器下载RET文件
             */
            //获取下载文件日期范围（下载什么时间范围内的文件）
            String oldDate = DateUtil.getOldDay(daySize);
            Map queryParam = null;

            //1 从FTP服务器下载文件到本地download文件夹
            //2更新下载文件关联的DEAL信息 的 状态 为 DOWNLOAD_FILE("12","下载文件"),
            ////为什么只更新 deal 不更新 info? 因为 info表 和 deal表中数据状态并不完全相同，deal负责处理BPS系统
            // 要比info多出几个BPS文件处理的状态  详见 DataStatusEnum
            ArrayList<BusBpsFilehandl> download = downloadAndUpService.download(type, oldDate);
            for (BusBpsFilehandl tbFileHead : download) {
                queryParam = new HashMap();
                queryParam.put("id", tbFileHead.getId());
                queryParam.put("status", DataStatusEnum.DOWNLOAD_FILE.getStatus());
                busMerWalletDealMapper.updateStatusByHeadId(queryParam);
            }


            //查询处于 已回盘更新 状态的CWFS头文件信息  根据回盘文件信息更新 数据库相关表信息
            List<BusBpsFilehandl> updateDataFile = busBpsFilehandlMapper.selectByType(type);

            for (BusBpsFilehandl tbFileHead : updateDataFile) {
                file = new File(path + type + "/" + tbFileHead.getSourceFile().replace("DAT", "RET"));
                if (file.exists()) {
                    //读取已下载到本地的 回盘RET文件 更新info表 和 deal表 信息
                    Boolean aBoolean = readFilesVsUpdateDb(tbFileHead, file);
                    //如果上一步执行失败，更新文件头信息表  和info 和deal 表为失败状态
                    if (!aBoolean) {
                        jdbcBatch(tbFileHead, Status.FAIL, DataStatusEnum.OPENING_FAILED.getStatus());
                    }
                } else {
                    bpsLogger.error(file.getName() + "  该文件不存在");
                }
            }

            /******
             * 上传生成的DAT文件 到远程FTP服务器
             */
            //上传文件到FTP服务器
            ArrayList<BusBpsFilehandl> tbFileHeads = downloadAndUpService.upFile(type, oldDate);

            //更新deal表信息为 UP_FILE_FILE("11","上传文件")状态
            //为什么只更新 deal 不更新 info? 因为 info表 和 deal表中数据状态并不完全相同，deal负责处理BPS系统
            // 要比info多出几个BPS文件处理的状态  详见 DataStatusEnum
            for (BusBpsFilehandl tbFileHead : tbFileHeads) {
                queryParam = new HashMap();
                queryParam.put("id", tbFileHead.getId());
                queryParam.put("status", DataStatusEnum.UP_FILE_FILE.getStatus());

                busMerWalletDealMapper.updateStatusByHeadId(queryParam);
            }

        } catch (Exception e) {
            bpsLogger.error((file != null? file.getName():"") + "ret fail ,reseaon,{}", e);
        } finally {
            bpsLogger.Save();
        }
    }


    /*******
     * 读取回盘文件 并且 根据回盘文件更新数据库相关表的信息
     * 更新info表 和 deal表 信息
     * @param head
     * @param file
     * @return
     */
    private Boolean readFilesVsUpdateDb(BusBpsFilehandl head, File file) {
        bpsLogger.info("start readFilesVsUpdateDb");
        InputStream in = null;
        BufferedReader bufferedReader = null;
        Reader reader = null;
        String line = null;
        boolean isOneLine = true;
        try {
            in = new FileInputStream(file);
            reader = new InputStreamReader(in, "utf-8");
            bufferedReader = new BufferedReader(reader);
            ArrayList<Object[]> list = new ArrayList<>();
            Date date = new Date();
            while ((line = bufferedReader.readLine()) != null) {
                String dataStatus = "0";
                if (isOneLine) {
                    String returnHeadMsg = line.substring(126, 126 + 58).trim();
                    head.setMsg(returnHeadMsg);
                    isOneLine = false;
                    continue;
                }
                //TODO:读取正文
                String sequenceNumber = Integer.parseInt(line.substring(1, 13)) + "";
                String returnCode = line.substring(1378, 1386).trim();
                String returnMsg = line.substring(1386, 1446).trim();
                String merchantID = line.substring(1446, 1478).trim();
                String walletID = line.substring(1576, 1592).trim();
                String merchantOpenTranReturnCode = line.substring(2760, 2768).trim();
                String merchantOpenTranReturnMsg = line.substring(2768, 2828).trim();
                String miDengLiuShui = line.substring(2828, 2875).trim();
                String remarkInfo = line.substring(2875, 2939).trim();
                String redundantInfo = line.substring(2939, 3204).trim();
                String tradingCode = line.substring(18, 24).trim();
                if ("TF0008".equals(tradingCode)) {
                    dataStatus = returnCode.equals("00000000") ? DataStatusEnum.SUCCESSFUL_OPENING.getStatus() : DataStatusEnum.OPENING_FAILED.getStatus();
                } else if ("TF0006".equals(tradingCode)) {
                    dataStatus = returnCode.equals("00000000") && merchantOpenTranReturnCode.equals("00000000") ? DataStatusEnum.SUCCESSFUL_OPENING.getStatus() : DataStatusEnum.OPENING_FAILED.getStatus();
                } else if ("TF0007".equals(tradingCode)) {
                    dataStatus = merchantOpenTranReturnCode.equals("00000000") ? DataStatusEnum.SUCCESSFUL_OPENING.getStatus() : DataStatusEnum.OPENING_FAILED.getStatus();
                } else {
                    bpsLogger.info("ret data error");
                    continue;
                }

                Object[] oneData = {returnCode, returnMsg, merchantID, walletID, merchantOpenTranReturnCode, merchantOpenTranReturnMsg,
                        miDengLiuShui, remarkInfo, redundantInfo, dataStatus, date, sequenceNumber, head.getId()};
                list.add(oneData);
            }
            //更新info表 和 deal表 信息
            Boolean aBoolean = JdbcBatch(list, head.getSourceFile());
            if (!aBoolean) {
                return false;
            }
            //TODO: 更新下载情况
            bpsLogger.info("--Success:{} ", head.getSourceFile());
            head.setProNo(FileStatus.RETURN_FILE.getStatus());
            head.setReturnDate(DateUtil.getCurDate());
            busBpsFilehandlMapper.updateBusBpsFilehandl(head);
            return true;
        } catch (UnsupportedEncodingException e) {
            bpsLogger.error("download file and update db file:{} error:{}" + head.getSourceFile(), e);
            return false;
        } catch (FileNotFoundException e) {
            bpsLogger.error("download file and update db file:{} error:{}" + head.getSourceFile(), e);
            return false;
        } catch (IOException e) {
            bpsLogger.error("download file and update db file:{} error:{}" + head.getSourceFile(), e);
            return false;
        } catch (Exception e) {
            bpsLogger.error("download file and update db file Exception:{} error:{}" + head.getSourceFile(), e);
            return false;
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
                if (reader != null) {
                    reader.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                bpsLogger.error("download file and update db file:{} error:{}" + head.getSourceFile(), e);
            }
        }
    }

    /**
     * @Description:数据库批量更新
     * @Author: shaowu.ni
     * @Date:
     */
    @Transactional
    public Boolean JdbcBatch(List<Object[]> list, String fileName) {
        try {

            List<BusMerWalletInfo> infoList = new ArrayList<>(list.size());
            List<BusMerWalletDeal> dealList = new ArrayList<>(list.size());


             for (Object[] objArray: list) {
                BusMerWalletInfo busMerWalletInfo = new BusMerWalletInfo();
                BusMerWalletDeal busMerWalletDeal = new BusMerWalletDeal();
                busMerWalletInfo.setReturnCode((String)objArray[0]);
                busMerWalletDeal.setReturnCode((String)objArray[0]);
                busMerWalletInfo.setReturnMsg((String)objArray[1]);
                busMerWalletDeal.setReturnMsg((String)objArray[1]);
                busMerWalletInfo.setMerchantId((String)objArray[2]);
                busMerWalletDeal.setMerchantId((String)objArray[2]);
                busMerWalletInfo.setWalletId((String)objArray[3]);
                busMerWalletDeal.setWalletId((String)objArray[3]);
                busMerWalletInfo.setMerchantOpenTranReturnCode((String)objArray[4]);
                busMerWalletDeal.setMerchantOpenTranReturnCode((String)objArray[4]);
                busMerWalletInfo.setMerchantOpenTranReturnMsg((String)objArray[5]);
                busMerWalletDeal.setMerchantOpenTranReturnMsg((String)objArray[5]);
                busMerWalletInfo.setMiDengLiuShui((String)objArray[6]);
                busMerWalletDeal.setMiDengLiuShui((String)objArray[6]);
                busMerWalletInfo.setRemarkInfo((String)objArray[7]);
                busMerWalletDeal.setRemarkInfo((String)objArray[7]);
                busMerWalletInfo.setRedundantInfo((String)objArray[8]);
                busMerWalletDeal.setRedundantInfo((String)objArray[8]);
                busMerWalletInfo.setDataStatus((String)objArray[9]);
                busMerWalletDeal.setDataStatus((String)objArray[9]);
                busMerWalletInfo.setReturnTime((Date)objArray[10]);
                busMerWalletDeal.setReturnTime((Date)objArray[10]);
                 //System.out.println("String------:objArray[11]:" + objArray[11]);
                busMerWalletInfo.setSequenceNumber(Long.parseLong((String)objArray[11]));
                busMerWalletDeal.setSequenceNumber(Long.parseLong((String)objArray[11]));
                busMerWalletInfo.setHeadId((String)objArray[12]);
                busMerWalletDeal.setHeadId((String)objArray[12]);


                infoList.add(busMerWalletInfo);
                dealList.add(busMerWalletDeal);
            }

            busMerWalletInfoMapper.updateBatchBysequenceNumberAndheadId(infoList);
            busMerWalletDealMapper.updateBatchBysequenceNumberAndheadId(dealList);
        } catch (DataAccessException e) {
            bpsLogger.info("cwfs save downfile data fail,source fileName:" + fileName);
            return false;
        }
        return true;
    }

    /**
     * status1:bps文件执行步骤的状态
     * status2:bps单条数据的执行的状态
     */
    @Transactional
    public void jdbcBatch(BusBpsFilehandl tbFileHead, String status1, String status2) {
        tbFileHead.setProNo(FileStatus.RETURN_FILE.getStatus());
        tbFileHead.setProNo(status1);
        busBpsFilehandlMapper.updateBusBpsFilehandl(tbFileHead);

        Map updateParam = new HashMap();
        updateParam.put("id", tbFileHead.getId());
        updateParam.put("status", status2);
        busMerWalletDealMapper.updateStatusByHeadId(updateParam);
        busMerWalletInfoMapper.updateStatusByHeadId(updateParam);
    }
}


