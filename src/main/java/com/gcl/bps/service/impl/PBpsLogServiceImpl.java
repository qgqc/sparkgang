package com.gcl.bps.service.impl;

import com.gcl.bps.domain.DBLogger;
import com.gcl.bps.service.IPBpsLogService;
import com.gcl.bps.util.DateUtil;
import com.gcl.corporatewalletmanagement.mapper.BusBpsLogMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


/**
 *
 * @author wjw
 * @date 2022-01-18
 */
@Service
public class PBpsLogServiceImpl implements IPBpsLogService {
    private static Logger logger = LoggerFactory.getLogger(PBpsLogServiceImpl.class);



    @Autowired
    private BusBpsLogMapper bpsLogMapper;

    //清理从今天往前多少天以前的数日志
    @Value("${saveLogDay}")
    private int saveLogDay;

    @Override
    public void logDeal() {
      String date = DateUtil.getOldDay(saveLogDay);
      bpsLogMapper.deleteByDate(date);
    }

    @Override
    public DBLogger getLogger(Class<?> clazz, String operType) {
        return new DBLogger(clazz,operType,bpsLogMapper);
    }
}
