package com.gcl.bps.service.impl;

import com.gcl.bps.constant.DataStatusEnum;
import com.gcl.bps.constant.FileStatus;
import com.gcl.bps.constant.Status;
import com.gcl.bps.domain.BpsOperType;
import com.gcl.bps.domain.BusMerWalletDeal;
import com.gcl.bps.domain.DBLogger;
import com.gcl.bps.domain.OrgAndNum;
import com.gcl.bps.mapper.BusMerWalletDealMapper;
import com.gcl.bps.service.IBusMerWalletDealService;
import com.gcl.bps.service.IPBpsLogService;
import com.gcl.bps.util.DateUtil;
import com.gcl.bps.util.FileTools;
import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;
import com.gcl.corporatewalletmanagement.domain.BusMerWalletInfo;
import com.gcl.corporatewalletmanagement.mapper.BusBpsFilehandlMapper;
import com.gcl.corporatewalletmanagement.mapper.BusMerWalletInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*****
 * @date 2022-01-18
 */
@Service
public class MerAndWalletService {
    @Autowired
    private IPBpsLogService ipBpsLogService;



    @Autowired
    private IBusMerWalletDealService busMerWalletDealService;

    @Autowired
    private BusMerWalletDealMapper busMerWalletDealMapper;


    @Autowired
    private BusMerWalletInfoMapper busMerWalletInfoMapper;

    @Autowired
    private PrepareService prepareService;

    @Autowired
    private CreateFileService createFileService;

    @Autowired
    private BusBpsFilehandlMapper busBpsFilehandlMapper;

    @Autowired
    private BpsOperType bpsOperType;
    @Value("${daySize}")
    private int daySize;

    @Value("${bpsCreatLocalPath}")
    private String bpsCreatLocalPath;

    //初始化BPS日志
    private DBLogger bpsLogger = null;

    @PostConstruct
    public void initLogger(){
        bpsLogger =  ipBpsLogService.getLogger(MerAndWalletService.class, bpsOperType.cwfsCreateFile);
    }




    public void carryOut(String type) {
        //本地DAT文件上传目录（若不存在，则创建）
        FileTools.CreateDir(bpsCreatLocalPath + type);
        //计算需要处理从过去哪个时间开始到现在的数据
        String oldDate = DateUtil.getOldDay(daySize);
        //文件批次
        String fileBatchNo = DateUtil.getCurDateTime();


        //查询某个时间段内处于审核通过状态(DataStatusEnum.REVIEW_PASSED)的数据，生成DAT文件
        Map queryParam = new HashMap();
        queryParam.put("status",DataStatusEnum.REVIEW_PASSED.getStatus());
        queryParam.put("oldDate", oldDate);
        int i = busMerWalletDealMapper.countByStatusAndExamineSuccessDate(queryParam);
        if (i <= 0) {
            bpsLogger.info("there is no data to process");
            return;
        }

        //更新数据库需要上传数据的批次(更新日期范围内审核通过的数据)(deal表和info表都更新)
        updateStatus(fileBatchNo, oldDate);
        //根据待处理批次号，按交易机构号和柜员号进行分组(由于生成的DAT文件需要按照交易号存放，所以进行分组)
        List<OrgAndNum> orgAndNums = busMerWalletDealMapper.selectCount(fileBatchNo);

        try{
            for (OrgAndNum orgAndNum : orgAndNums) {
                bpsLogger.info("do start create file prepare , fileBatchNo : " + fileBatchNo);
                ArrayList<BusBpsFilehandl> data = prepareService.prepare(orgAndNum, type);
                //执行 准备方法失败
                if (data == null) {
                    return;
                }
                //按照计算出的文件信息  循环生成文件 （文件信息可能有多个，所以循环遍历）
                for (BusBpsFilehandl busBpsFilehandl : data) {
                    //将该文件头信息插入到 文件头信息表中
                    busBpsFilehandlMapper.insertBusBpsFilehandl(busBpsFilehandl);
                    //查询待处理的明细数据 detialCount为条数
                    queryParam = new HashMap();
                    //queryParam.put("headId", busBpsFilehandl.getId());

                    queryParam.put("tradingOrganizationNo", busBpsFilehandl.getTranOrgId());
                    queryParam.put("tellerNo", busBpsFilehandl.getTellerNo());

                    queryParam.put("fileBatchNo", fileBatchNo);
                    queryParam.put("status", DataStatusEnum.REVIEW_PASSED.getStatus());
                    queryParam.put("detialCount", busBpsFilehandl.getDetialCount());
                    ArrayList<BusMerWalletDeal> tbMerWalletDeals = busMerWalletDealMapper.selectByFileBatchNo(queryParam);

                    //根据查询出来的数据详细条目 正式生成DAT文件（H信息和D信息部分都在这里生成）
                    Boolean status = createFileService.create(busBpsFilehandl, tbMerWalletDeals, type);

                    //进行deal数据的批量更新
                    busMerWalletDealService.updateList(tbMerWalletDeals);
                    //设置文件执行状态
                    busBpsFilehandl.setProNo(FileStatus.CREATE_FILE.getStatus());

                    queryParam = new HashMap();
                    queryParam.put("id", busBpsFilehandl.getId());
                    if (!status) {
                        busBpsFilehandl.setStatu(Status.FAIL);
                        busBpsFilehandlMapper.updateBusBpsFilehandl(busBpsFilehandl);
                        queryParam.put("status", DataStatusEnum.CREATE_FILE_FAIL.getStatus());
                        busMerWalletDealMapper.updateStatusByHeadId(queryParam);
                        /**
                         * 生成文件（开立失败）失败,将deal的数据同步到info中
                         */
                        synchroData(tbMerWalletDeals,DataStatusEnum.OPENING_FAILED.getStatus());
                        bpsLogger.error("create file fail :" + busBpsFilehandl.getSourceFile());
                        continue;
                    } else {
                        busBpsFilehandlMapper.updateBusBpsFilehandl(busBpsFilehandl);
                        //生成文件
                        queryParam.put("status", DataStatusEnum.CREATE_SUCCESS.getStatus());
                        busMerWalletDealMapper.updateStatusByHeadId(queryParam);

                        /**
                         * 将deal的数据同步到info中
                         */
                        synchroData(tbMerWalletDeals,DataStatusEnum.SYSTEM_PROCESSING.getStatus());
                        bpsLogger.info("create file success :" + busBpsFilehandl.getSourceFile());
                    }



                }

            }
        }finally {
            bpsLogger.Save();
        }

    }



    @Transactional
    public void updateStatus(String fileBatchNo,String oldDate){
        //更新数据库需要上传数据的批次，并获取批次号
        Map queryParam = new HashMap();
        queryParam.put("fileBatchNo",fileBatchNo);
        queryParam.put("oldDate", oldDate);
        busMerWalletDealMapper.updatefileBatchNoByStatus(queryParam);
        busMerWalletInfoMapper.updatefileBatchNoByStatus(queryParam);
    }



    @Transactional
    public void synchroData(ArrayList<BusMerWalletDeal> tbMerWalletDeals,String dataStatus) {
        ArrayList<BusMerWalletInfo> objects = new ArrayList<>();
        for (BusMerWalletDeal tbMerWalletDeal : tbMerWalletDeals) {
            BusMerWalletInfo busMerWalletInfo = new BusMerWalletInfo();
            busMerWalletInfo.setSequenceNumber(tbMerWalletDeal.getSequenceNumber());
            busMerWalletInfo.setHeadId(tbMerWalletDeal.getHeadId());
            busMerWalletInfo.setDataStatus(dataStatus);
            busMerWalletInfo.setDealId(tbMerWalletDeal.getId());
            objects.add(busMerWalletInfo);
        }
        try {
            busMerWalletInfoMapper.updateBatchById(objects);
        } catch (DataAccessException e) {
            bpsLogger.error("cwfs synchroData fail ");
        }
    }
}
