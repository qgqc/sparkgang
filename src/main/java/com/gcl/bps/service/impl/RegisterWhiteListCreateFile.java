package com.gcl.bps.service.impl;


import com.gcl.bps.constant.FileStatus;
import com.gcl.bps.constant.PrivateState;
import com.gcl.bps.constant.StaticDataEnum;
import com.gcl.bps.constant.Status;
import com.gcl.bps.domain.BpsOperType;
import com.gcl.bps.domain.BusPrivateStaticData;
import com.gcl.bps.domain.DBLogger;
import com.gcl.bps.mapper.BusPrivateStaticDataMapper;
import com.gcl.bps.service.IPBpsLogService;
import com.gcl.bps.util.FileTools;
import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;
import com.gcl.corporatewalletmanagement.mapper.BusBpsFilehandlMapper;
import com.gcl.personalwalletmanagement.domain.BusPrivateWhitelistRegister;
import com.gcl.personalwalletmanagement.mapper.BusPrivateInfoMapper;
import com.gcl.personalwalletmanagement.mapper.BusPrivateWhitelistRegisterMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @program:bps
 * @description: 生成bps（bwls）文件
 * @author: shaowu.ni
 * @create:2021-11-05 09:35
 **/
@Service
public class RegisterWhiteListCreateFile {
    private DBLogger bpsLogger;

    @Autowired
    private IPBpsLogService ipBpsLogService;

    @Autowired
    private BpsOperType bpsOperType;

    @Value("${bpsCreatLocalPath}")
    private String bpsCreatPath;
    @Autowired
    private BusPrivateWhitelistRegisterMapper busPrivateWhitelistRegisterMapper;
    @Autowired
    private BusBpsFilehandlMapper busBpsFilehandlMapper;



    @Autowired
    private BusPrivateInfoMapper busPrivateInfoMapper;

    private  HashMap<String, String> map;

    @Autowired
    private BusPrivateStaticDataMapper busPrivateStaticDataMapper;

    @PostConstruct
    public void init(){
        bpsLogger =  ipBpsLogService.getLogger(RegisterWhiteListCreateFile.class, bpsOperType.bwlsCreateFile);
        this.map = new HashMap<>();
        ArrayList<BusPrivateStaticData> byType = busPrivateStaticDataMapper.findByType(StaticDataEnum.WALLET_REGISTER_LIST_WALLET.getStatus());
        for (BusPrivateStaticData privateWalletStaticData : byType) {
            map.put(privateWalletStaticData.getKey(), privateWalletStaticData.getValue());
        }
    }


    /**
     * @Description: 生成文件(生成文件成功后需要进行更改数据库的状态 ( 包括文件信息的状态和对应每条明细的状态))
     * @Author: shaowu.ni
     * @Date:
     */
    public Boolean create(BusBpsFilehandl head, ArrayList<BusPrivateWhitelistRegister> list, String type)  {
        bpsLogger.info("do start cerate file ,filename:" + head.getSourceFile());
        File file = new File(bpsCreatPath +type+ File.separator + head.getSourceFile());
        FileOutputStream out = null;
        BufferedOutputStream buff = null;
        try {
            //        todo 获得文件头
            Boolean status = FileTools.writeToTxtEncoding(head.pwlsHeadMsg(type), file, "utf-8");
            if (!status) {
                return false;
            }
            out=new FileOutputStream(file,true);
            buff=new BufferedOutputStream(out);
            //        todo 获得明细文件 count代表序列号
            long count = 2 ;
            for (BusPrivateWhitelistRegister tbWalletWhiteListRegister : list) {
                tbWalletWhiteListRegister.setSequenceNumber(Long.toString(count));
                buff.write(tbWalletWhiteListRegister.getDetailFileRow().getBytes("utf-8"));
                buff.flush();
                tbWalletWhiteListRegister.setHeadId(head.getId());
                tbWalletWhiteListRegister.setDataStatus(PrivateState.BWLS_SYSTEM_PROCESSING.getStatus());
                count++;
            }
            //        todo 更新明细文件的状态，更新头文件信息的状态
            dataUpdate(head,list, FileStatus.CREATE_FILE.getStatus(),Status.SUCCESS);
            return true;
        } catch (Exception e) {
            bpsLogger.info(" create file fail:,{}",e);
            return false;
        } finally {
            try {
                if (buff != null){
                    buff.close();
                }
                if (buff != null){
                    out.close();
                }
            } catch (Exception e) {
                bpsLogger.info("关闭句柄 fail:,{}",e);
                return false;
            }
        }
    }

    @Transactional
    public void dataUpdate(BusBpsFilehandl head, ArrayList<BusPrivateWhitelistRegister> list, String proNo, String status){


        Map queryParam = new HashMap();
        queryParam.put("proNo",proNo);
        queryParam.put("status", status);
        queryParam.put("id", head.getId());
        busBpsFilehandlMapper.updateById(queryParam);
        busPrivateWhitelistRegisterMapper.updateAll(list);
    }

    /**
     * 将数据插入bps表中
     */
    public void proDeal() {

        Map insertMap = new HashMap();
        insertMap.put("a",map.get("tradingOrganizationNo"));
        insertMap.put("b",map.get("phoneAreaCode"));
        insertMap.put("c",map.get("phoneCityCode"));
        insertMap.put("d",map.get("customerType"));
        insertMap.put("e",map.get("changeType"));
        insertMap.put("f",PrivateState.REVIEW_PASSED.getStatus());
        insertMap.put("g",PrivateState.PWLS_SUCCESSFUL_OPENING.getStatus());
        busPrivateWhitelistRegisterMapper.insertByStatus(insertMap);

        Map queryParam = new HashMap();
        queryParam.put("dataStatus1",PrivateState.BWLS_SYSTEM_PROCESSING.getStatus());
        queryParam.put("dataStatus2", PrivateState.REVIEW_PASSED.getStatus());
        busPrivateInfoMapper.updatePwlsUpdateById(queryParam);
    }
    /**
     * 更新业务表中的数据状态
     *
     * @param status
     * @param headId
     */
    public void postDeal(String status, String headId) {

        Map queryParam = new HashMap();
        queryParam.put("status",status);
        queryParam.put("headId", headId);
        busPrivateInfoMapper.updatePwlsStatusByHeadId(queryParam);
    }
}
