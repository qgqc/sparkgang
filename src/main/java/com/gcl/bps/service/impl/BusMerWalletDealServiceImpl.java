package com.gcl.bps.service.impl;

import com.gcl.bps.domain.BusMerWalletDeal;
import com.gcl.bps.mapper.BusMerWalletDealMapper;
import com.gcl.bps.service.IBusMerWalletDealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusMerWalletDealServiceImpl implements IBusMerWalletDealService {
    @Autowired
    private BusMerWalletDealMapper busMerWalletDealMapper;

    @Override
    public int insertBusMerWalletDeal(BusMerWalletDeal busMerWalletDeal) {
        return busMerWalletDealMapper.insertBusMerWalletDeal(busMerWalletDeal);
    }

    @Override
    public BusMerWalletDeal selectBusMerWalletDealById(String id) {
        return busMerWalletDealMapper.selectBusMerWalletDealById(id);
    }

    @Override
    public int updateBusMerWalletDeal(BusMerWalletDeal busMerWalletDeal) {
        return busMerWalletDealMapper.updateBusMerWalletDeal(busMerWalletDeal);
    }

    @Override
    public void updateList(List<BusMerWalletDeal> list) {
        for (BusMerWalletDeal deal :  list) {
            busMerWalletDealMapper.updateBusMerWalletDeal(deal);
        }
    }
}
