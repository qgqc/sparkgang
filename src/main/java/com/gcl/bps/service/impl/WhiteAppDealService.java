package com.gcl.bps.service.impl;


import com.gcl.bps.constant.PrivateState;
import com.gcl.bps.domain.BpsOperType;
import com.gcl.bps.domain.DBLogger;
import com.gcl.bps.service.IPBpsLogService;
import com.gcl.bps.util.DateUtil;
import com.gcl.bps.util.FileTools;
import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;
import com.gcl.corporatewalletmanagement.mapper.BusBpsFilehandlMapper;
import com.gcl.personalwalletmanagement.mapper.BusPrivateInfoMapper;
import com.gcl.scancodemanagement.domain.BusPbocccoBuildappwhitelist;
import com.gcl.scancodemanagement.mapper.BusPbocccoBuildappwhitelistMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program:bps
 * @description:
 * @author: shaowu.ni
 * @create:2021-11-02 17:59
 **/
@Service
public class WhiteAppDealService {

    @Value("${bpsDownFileLocalPath}")
    private String path;
    @Value("${daySize}")
    private int daySize;

    @Autowired
    private BusBpsFilehandlMapper busBpsFilehandlMapper;
    @Autowired
    private BusPrivateInfoMapper busPrivateInfoMapper;
    @Autowired
    private DownloadAndUpService downloadAndUpService;


    @Autowired
    private BusPbocccoBuildappwhitelistMapper busPbocccoBuildappwhitelistMapper;




    @Autowired
    private IPBpsLogService ipBpsLogService;

    @Autowired
    private BpsOperType bpsOperType;

    private DBLogger bpsLogger = null;


    @PostConstruct
    public void init(){
        bpsLogger =  ipBpsLogService.getLogger(MerAndWalletService.class, bpsOperType.bwlsUpdateData);
        FileTools.CreateDir(this.path+"PWLS");
    }

    //todo 按照回盘文件进行数据的更新,不同的业务需要更新的字段也不同
    public void updateData(String type) {
        bpsLogger.info("UpdateService start do type:" + type);

        // todo 调用文件上传和下载接口，先下载再上传
        String oldDate = DateUtil.getOldDay(daySize);
        downloadAndUpService.download(type, oldDate);

        List<BusBpsFilehandl> updateDataFile = busBpsFilehandlMapper.selectByType(type);

        for (BusBpsFilehandl tbFileHead : updateDataFile) {
            File file = new File(path + type + "/" + tbFileHead.getSourceFile().replace("DAT", "RET"));
            readFilesVsUpdateDb(tbFileHead, file);
        }
       downloadAndUpService.upFile(type, oldDate);
    }


    private void readFilesVsUpdateDb(BusBpsFilehandl head, File file) {
        bpsLogger.info("ppfs start readFilesVsUpdateDb:" + file.getName());
        InputStream in = null;
        BufferedReader bufferedReader = null;
        Reader reader = null;
        String line = null;
        boolean isOneLine = true;
        try {
            in = new FileInputStream(file);
            reader = new InputStreamReader(in, "utf-8");
            bufferedReader = new BufferedReader(reader);
            ArrayList<BusPbocccoBuildappwhitelist> list = new ArrayList<>();

            while ((line = bufferedReader.readLine()) != null) {
                if (isOneLine) {
                    String returnHeadMsg = line.substring(98, 98 + 58).trim();
                    head.setReturnDate(DateUtil.getCurDate());
                    head.setMsg(returnHeadMsg);
                    isOneLine = false;
                    continue;
                }
                //TODO:读取正文 TF0014
                String sequenceNumber = Integer.parseInt(line.substring(1, 13)) + "";
                String returnCode = line.substring(193, 201).trim();
                String returnMsg = line.substring(201, 261).trim();
                String miDengLiuShui = line.substring(261, 308).trim();
                String dataStatus="00000000".equals(returnCode)? PrivateState.PWLS_SUCCESSFUL_OPENING.getStatus():PrivateState.PWLS_OPENING_FAILED.getStatus();
                list.add(new BusPbocccoBuildappwhitelist(returnCode, returnMsg, dataStatus,miDengLiuShui,sequenceNumber, head.getId()));
            }
            dealRetData(list);
            postDeal(head.getId());
            //TODO: 更新下载情况
            bpsLogger.info("--Success:{} ", head.getSourceFile());
            head.setProNo("4");
            busBpsFilehandlMapper.updateBusBpsFilehandl(head);
        } catch (UnsupportedEncodingException e) {
            bpsLogger.error("download file and update db file:{} error:{}" + head.getSourceFile(), e);
        } catch (FileNotFoundException e) {
            bpsLogger.error("download file and update db file:{} error:{}" + head.getSourceFile(), e);
        } catch (IOException e) {
            bpsLogger.error("download file and update db file:{} error:{}" + head.getSourceFile(), e);
        } catch (Exception e) {
            bpsLogger.error("download file and update db file Exception:{} error:{}" + head.getSourceFile(), e);
        } finally {
            try {
                bufferedReader.close();
                reader.close();
                in.close();
            } catch (IOException e) {
                bpsLogger.error("download file and update db file:{} error:{}" + head.getSourceFile(), e);
            }
            bpsLogger.Save();
        }
    }

    @Transactional
    public void dealRetData(ArrayList<BusPbocccoBuildappwhitelist> list) {
        try {
            busPbocccoBuildappwhitelistMapper.updateBatchBySequenceNumberAndHeadId(list);
        } catch (DataAccessException e) {
            bpsLogger.error(" update ret file fail ,{}", e);
        }
    }



    private void postDeal(String headId) {
        Map param = new HashMap();
        param.put("statusOpenFail",PrivateState.PWLS_OPENING_FAILED.getStatus());
        param.put("statusOpenSuccess", PrivateState.PWLS_SUCCESSFUL_OPENING.getStatus());
        param.put("headId", headId);
        busPrivateInfoMapper.updatePwlsBusinessTable(param);

    }
}


