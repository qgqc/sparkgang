package com.gcl.bps.service.impl;

import com.gcl.bps.constant.FileStatus;
import com.gcl.bps.constant.PrivateState;
import com.gcl.bps.constant.StaticDataEnum;
import com.gcl.bps.constant.Status;
import com.gcl.bps.domain.BpsOperType;
import com.gcl.bps.domain.BusPrivateStaticData;
import com.gcl.bps.domain.DBLogger;
import com.gcl.bps.mapper.BusPrivateStaticDataMapper;
import com.gcl.bps.service.IPBpsLogService;
import com.gcl.bps.util.FileTools;
import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;
import com.gcl.corporatewalletmanagement.mapper.BusBpsFilehandlMapper;
import com.gcl.personalwalletmanagement.domain.BusPrivateInfoBps;
import com.gcl.personalwalletmanagement.mapper.BusPrivateInfoBpsMapper;
import com.gcl.personalwalletmanagement.mapper.BusPrivateInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @program:bps
 * @description: 生成bps（cwfs）文件
 * @author: shaowu.ni
 * @create:2021-11-05 09:35
 **/
@Service
public class PrivateCreateFile {
    private DBLogger logger;

    @Autowired
    private IPBpsLogService ipBpsLogService;

    @Autowired
    private BusPrivateStaticDataMapper busPrivateStaticDataMapper;

    @Autowired
    private BpsOperType bpsOperType;

    @Value("${bpsCreatLocalPath}")
    private String bpsCreatPath;


    @Autowired
    private BusPrivateInfoMapper busPrivateInfoMapper;

    @Autowired
    private BusPrivateInfoBpsMapper busPrivateInfoBpsMapper;

    private  HashMap<String, String> map;

    @Autowired
    private BusBpsFilehandlMapper busBpsFilehandlMapper;


    @PostConstruct
    public void initData(){
        this.map = new HashMap<>();
        ArrayList<BusPrivateStaticData> byType = busPrivateStaticDataMapper.findByType(StaticDataEnum.PRIVATE_WALLET.getStatus());
        for (BusPrivateStaticData privateWalletStaticData : byType) {
            map.put(privateWalletStaticData.getKey(), privateWalletStaticData.getValue());
        }
        logger =  ipBpsLogService.getLogger(MerAndWalletService.class, bpsOperType.pwlsCreateFile);
    }

    /**
     * @Description: 生成文件(生成文件成功后需要进行更改数据库的状态 ( 包括文件信息的状态和对应每条明细的状态))
     * @Author: shaowu.ni
     * @Date:
     */
    public Boolean create(BusBpsFilehandl head, ArrayList<BusPrivateInfoBps> list, String type) {
        logger.info("do start cerate file ,filename:" + head.getSourceFile());
        File file = new File(bpsCreatPath + type + File.separator + head.getSourceFile());
        FileOutputStream out = null;
        BufferedOutputStream buff = null;
        try {
            //        todo 获得文件头
            Boolean status = FileTools.writeToTxtEncoding(head.getHeadString(type), file, "utf-8");
            if (!status) {
                return false;
            }
            out = new FileOutputStream(file, true);
            buff = new BufferedOutputStream(out);
            //        todo 获得明细文件 count代表序列号
            long count = 2;
            for (BusPrivateInfoBps busPrivateInfoBps : list) {
                busPrivateInfoBps.setSequenceNumber(count);
                buff.write(busPrivateInfoBps.getDetailFileRow().getBytes("utf-8"));
                buff.flush();
                busPrivateInfoBps.setHeadId(head.getId());
                busPrivateInfoBps.setDataStatus(PrivateState.PPFS_SYSTEM_PROCESSING.getStatus());
                count++;
            }
            //        todo 更新明细文件的状态，更新头文件信息的状态
            dataUpdate(head, list, FileStatus.CREATE_FILE.getStatus(), Status.SUCCESS);
            return true;
        } catch (Exception e) {
            logger.info(" create file fail:,{}", e);
            return false;
        } finally {
            try {
                if (buff != null) {
                    buff.close();
                }
                if (buff != null) {
                    out.close();
                }
            } catch (Exception e) {
                logger.info("关闭句柄 fail:,{}", e);
                return false;
            }
        }
    }

    @Transactional
    public void dataUpdate(BusBpsFilehandl head, ArrayList<BusPrivateInfoBps> list, String proNo, String status) {

        Map queryParam = new HashMap();
        queryParam.put("proNo",proNo);
        queryParam.put("status", status);
        queryParam.put("id", head.getId());
        busBpsFilehandlMapper.updateById(queryParam);
        busPrivateInfoBpsMapper.updateAll(list);
    }

    public void proDeal() {

        Map insertParam = new HashMap();
        insertParam.put("a",map.get("tradingOrganizationNo"));
        insertParam.put("b",map.get("mobileInternationalAreaCode"));
        insertParam.put("c",map.get("mobileNumberCityAreaCode"));
        insertParam.put("d",map.get("countryOfUser"));
        insertParam.put("e",PrivateState.REVIEW_PASSED.getStatus());
        insertParam.put("f",PrivateState.BWLS_SUCCESSFUL_OPENING.getStatus());

        busPrivateInfoBpsMapper.insertByStatus(insertParam);


        Map updateParam = new HashMap();
        updateParam.put("dataStatus1",PrivateState.PPFS_SYSTEM_PROCESSING.getStatus());
        updateParam.put("dataStatus2", PrivateState.REVIEW_PASSED.getStatus());
        busPrivateInfoMapper.updatePPFSUpdateById(updateParam);
    }

    /**
     * 更新数据状态
     *
     * @param status
     * @param
     */
    public void postDeal(String status, String headId) {
        Map param = new HashMap();
        param.put("status",status);
        param.put("headId",headId);
        busPrivateInfoMapper.updatePPFSStatus(param);
    }


}
