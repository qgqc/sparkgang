package com.gcl.bps.service.impl;


import com.gcl.bps.constant.FileStatus;
import com.gcl.bps.constant.PrivateState;
import com.gcl.bps.constant.Status;
import com.gcl.bps.domain.BpsOperType;
import com.gcl.bps.domain.DBLogger;
import com.gcl.bps.domain.OrgAndNum;
import com.gcl.bps.service.IPBpsLogService;
import com.gcl.bps.util.DateUtil;
import com.gcl.bps.util.FileTools;
import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;
import com.gcl.corporatewalletmanagement.mapper.BusBpsFilehandlMapper;
import com.gcl.scancodemanagement.domain.BusPbocccoBuildappwhitelist;
import com.gcl.scancodemanagement.mapper.BusPbocccoBuildappwhitelistMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program:bps
 * @description:
 * @author: shaowu.ni
 * @create:2021-11-15 17:30
 **/
@Service
public class WhiteAppService {

    @Autowired
    private IPBpsLogService ipBpsLogService;

    @Autowired
    private BusBpsFilehandlMapper busBpsFilehandlMapper;

    @Autowired
    private PrepareService prepareService;

    @Autowired
    private WhiteAppCreateFile whiteAppCreateFile;

    @Value("${daySize}")
    private int daySize;
   /* @Autowired
    private WhiteAppCreateFile whiteAppCreateFile;


*/
   @Autowired
   private BusPbocccoBuildappwhitelistMapper busPbocccoBuildappwhitelistMapper;

    @Autowired
    private BpsOperType bpsOperType;


    @Value("${bpsCreatLocalPath}")
    private String bpsCreatPath;


    //初始化BPS日志
    private DBLogger bpsLogger = null;

    @PostConstruct
    public void initLogger(){
        bpsLogger =  ipBpsLogService.getLogger(MerAndWalletService.class, bpsOperType.pwlsCreateFile);
    }


    /******
     * TODO 年前待做，放开注释，添加whiteAppCreateFile
     * @param type
     */
    public void carryOut(String type) {
        FileTools.CreateDir(bpsCreatPath + type);
        String oldDate = DateUtil.getOldDay(daySize);
        String fileBatchNo = DateUtil.getCurDateTime();
        whiteAppCreateFile.proDeal();

        Map queryParam = new HashMap();
        queryParam.put("status", PrivateState.REVIEW_PASSED.getStatus());
        queryParam.put("oldDate", oldDate);
        int i = busPbocccoBuildappwhitelistMapper.countByDataStatusAndExamineDateGreaterThanEqual(queryParam);
        if (i <= 0) {
            bpsLogger.info("ppfs not having data creat file");
            return;
        }


        queryParam = new HashMap();
        queryParam.put("fileBatchNo",fileBatchNo);
        queryParam.put("status",PrivateState.SYSTEM_PROCESSING.getStatus());
        queryParam.put("oldDate", oldDate);
        // todo 更新数据库需要上传数据的批次，并获取批次号
        busPbocccoBuildappwhitelistMapper.updatefileBatchNoByStatus(queryParam);

        // todo 按交易机构号和柜员号进行分组
        List<OrgAndNum> orgAndNums = busPbocccoBuildappwhitelistMapper.selectCount(fileBatchNo);
        //        todo 计算文件信息和文件的头信息
        try {
            for (OrgAndNum orgAndNum : orgAndNums) {
                bpsLogger.info("do start create file prepare , fileBatchNo : " + fileBatchNo);
                ArrayList<BusBpsFilehandl> data = prepareService.prepare(orgAndNum, type);
                // todo 执行 准备方法 失败
                if (data == null) {
                    return;
                }
                //         todo 按照计算出的文件信息循环生成文件
                for (BusBpsFilehandl datum : data) {
                    busBpsFilehandlMapper.insertBusBpsFilehandl(datum);
                    //       todo 查询明细数据

                    queryParam = new HashMap();
                    queryParam.put("fileBatchNo", fileBatchNo);
                    queryParam.put("status", PrivateState.SYSTEM_PROCESSING.getStatus());
                    queryParam.put("detialCount", datum.getDetialCount());
                    ArrayList<BusPbocccoBuildappwhitelist> pboccCoBuildAPPWhitelists = busPbocccoBuildappwhitelistMapper.selectByFileBatchNo(queryParam);
                    //        todo  生成文件
                    Boolean status = whiteAppCreateFile.create(datum, pboccCoBuildAPPWhitelists, type);
                    datum.setProNo(FileStatus.CREATE_FILE.getStatus());
                    try {
                        if (!status) {
                            datum.setStatu(Status.FAIL);
                            for (BusPbocccoBuildappwhitelist pboccCoBuildAPPWhitelist : pboccCoBuildAPPWhitelists) {
                                pboccCoBuildAPPWhitelist.setDataStatus(PrivateState.PWLS_OPENING_FAILED.getStatus());
                            }
                            whiteAppCreateFile.dataUpdate(datum, pboccCoBuildAPPWhitelists, FileStatus.CREATE_FILE.getStatus(), Status.FAIL);
                            whiteAppCreateFile.postDeal(PrivateState.PWLS_OPENING_FAILED.getStatus(),datum.getId());
                            bpsLogger.error("create file fail :" + datum.getSourceFile());
                            continue;
                        } else {
                            bpsLogger.info("create file success :" + datum.getSourceFile());
                        }
                    }catch (Exception e){
                        bpsLogger.error("pwls create file fail"+datum.getSourceFile()+",{}",e);
                    }finally {
                        busBpsFilehandlMapper.insertBusBpsFilehandl(datum);
                    }

                }
            }
        } finally {
            bpsLogger.Save();
        }
    }

}
