package com.gcl.bps.service.impl;


import com.gcl.bps.domain.BpsOperType;
import com.gcl.bps.domain.DBLogger;
import com.gcl.bps.domain.OrgAndNum;
import com.gcl.bps.service.IPBpsLogService;
import com.gcl.bps.util.DateUtil;
import com.gcl.corporatewalletmanagement.domain.BusBpsFilehandl;
import com.gcl.corporatewalletmanagement.mapper.BusBpsFilehandlMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @program:bps
 * @description:
 * @author: shaowu.ni
 * @create:2021-11-05 09:40
 **/
@Service
public class PrepareService {
    @Autowired
    private IPBpsLogService ipBpsLogService;



    @Value("${fileSize}")
    private int fileSize;

    @Autowired
    private BpsOperType bpsOperType;

    @Autowired
    private BusBpsFilehandlMapper busBpsFilehandlMapper;
    @Value("${sourceFile}")
    private String sourceFileName;



    //初始化BPS日志
    private DBLogger bpsLogger = null;

    @PostConstruct
    public void initLogger(){
        bpsLogger =  ipBpsLogService.getLogger(MerAndWalletService.class, bpsOperType.cwfsCreateFile);
    }

    /**
     * @Description:计算相同交易机构号和柜员号所需要生成的文件信息
     * @Param:
     * @return:
     * @Author: shaowu.ni
     * @Date:
     */
    public ArrayList<BusBpsFilehandl> prepare(OrgAndNum orgAndNum, String type) {
        long num = orgAndNum.getNum();
        //获取当前最新的文件批次号（注：后面创建新文件的时候会+1使用）
        Map queryParamOfBusBpsFilehandl = new HashMap();
        queryParamOfBusBpsFilehandl.put("curDate", DateUtil.getCurDate());
        queryParamOfBusBpsFilehandl.put("tradingOrganizationNo", orgAndNum.getTradingOrganizationNo());
        //queryParamOfBusBpsFilehandl.put("tellerNo", orgAndNum.getTellerNo());


        queryParamOfBusBpsFilehandl.put("type", type);
        BusBpsFilehandl tBFileHead = busBpsFilehandlMapper.findByOrgAndAccountDateAndTellerNo(queryParamOfBusBpsFilehandl);
        int batchNo = 0;
        if (tBFileHead != null) {
            batchNo = tBFileHead.getBatchNo();
        }
        ArrayList<BusBpsFilehandl> tbFileHeads = null;
        try {
            //根据每个文件最大存放数据条数（来自己于配置文件），判断需要将数据分别存放到多少个文件中
            int remainder = (int) num % fileSize;  //取余数
            int fileNum = (new Double(Math.ceil((double) num / fileSize))).intValue(); //取整数
            tbFileHeads = new ArrayList<>();

            for (int i = 0; i < fileNum; i++) {
                //TODO:写入head;
                int detailCount = 0;
                if ((i == fileNum - 1)&&remainder!=0) {
                    detailCount = remainder;
                } else {
                    detailCount = fileSize;
                }
                //文件头表信息，包含文件存放数量，批次等一些信息
                BusBpsFilehandl tbFileHead1 = new BusBpsFilehandl(DateUtil.getCurDate(),  ++batchNo, detailCount, type,
                        orgAndNum.getTradingOrganizationNo(), orgAndNum.getTellerNo(),sourceFileName);

                tbFileHeads.add(tbFileHead1);
            }
        } catch (Exception exception) {
            bpsLogger.error("prepare fail : ,{}",exception);
           return null;
        }finally {
            bpsLogger.Save();
        }
        return tbFileHeads;
    }
}
