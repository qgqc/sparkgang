package com.gcl.bps.mapper;

import java.util.ArrayList;
import java.util.List;

import com.gcl.bps.domain.BusPrivateStaticData;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author yada
 * @date 2022-01-28
 */
public interface BusPrivateStaticDataMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public BusPrivateStaticData selectBusPrivateStaticDataById(String id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param busPrivateStaticData 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<BusPrivateStaticData> selectBusPrivateStaticDataList(BusPrivateStaticData busPrivateStaticData);

    /**
     * 新增【请填写功能名称】
     * 
     * @param busPrivateStaticData 【请填写功能名称】
     * @return 结果
     */
    public int insertBusPrivateStaticData(BusPrivateStaticData busPrivateStaticData);

    /**
     * 修改【请填写功能名称】
     * 
     * @param busPrivateStaticData 【请填写功能名称】
     * @return 结果
     */
    public int updateBusPrivateStaticData(BusPrivateStaticData busPrivateStaticData);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteBusPrivateStaticDataById(String id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusPrivateStaticDataByIds(String[] ids);

    ArrayList<BusPrivateStaticData> findByType(String status);
}
