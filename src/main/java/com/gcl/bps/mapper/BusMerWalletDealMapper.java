package com.gcl.bps.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.gcl.bps.domain.BusMerWalletDeal;
import com.gcl.bps.domain.OrgAndNum;

/**
 * BPS处理Mapper接口
 * 
 * @author yada
 * @date 2022-01-18
 */
public interface BusMerWalletDealMapper 
{
    /**
     * 查询BPS处理
     * 
     * @param id BPS处理主键
     * @return BPS处理
     */
    public BusMerWalletDeal selectBusMerWalletDealById(String id);

    /**
     * 查询BPS处理列表
     * 
     * @param busMerWalletDeal BPS处理
     * @return BPS处理集合
     */
    public List<BusMerWalletDeal> selectBusMerWalletDealList(BusMerWalletDeal busMerWalletDeal);

    /**
     * 新增BPS处理
     * 
     * @param busMerWalletDeal BPS处理
     * @return 结果
     */
    public int insertBusMerWalletDeal(BusMerWalletDeal busMerWalletDeal);

    /**
     * 修改BPS处理
     * 
     * @param busMerWalletDeal BPS处理
     * @return 结果
     */
    public int updateBusMerWalletDeal(BusMerWalletDeal busMerWalletDeal);

    /**
     * 删除BPS处理
     * 
     * @param id BPS处理主键
     * @return 结果
     */
    public int deleteBusMerWalletDealById(String id);

    /**
     * 批量删除BPS处理
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusMerWalletDealByIds(String[] ids);

    int countByStatusAndExamineSuccessDate(Map map);

    void updatefileBatchNoByStatus(Map map);

    List<OrgAndNum> selectCount(String fileBatchNo);

    ArrayList<BusMerWalletDeal> selectByFileBatchNo(Map map);

    void saveAll(List<BusMerWalletDeal> item);

    void updateStatusByHeadId(Map map);

    void updateBatchBysequenceNumberAndheadId(List<BusMerWalletDeal> list);

}
