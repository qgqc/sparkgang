package com.gcl.bps.domain;


import com.gcl.corporatewalletmanagement.domain.BusBpsLog;
import com.gcl.corporatewalletmanagement.mapper.BusBpsLogMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @ClassName Logger
 * @Deseription TODO 兼容日志处理
 * @Author xiaofeng.ye
 * @Date 2020/3/29 2:15
 * @Version 1.0
 */


public final class DBLogger implements Logger {
    private static Logger logger = LoggerFactory.getLogger(DBLogger.class);
    private Logger tlogger;
    private String className;
    private List<BusBpsLog> list;
    private String info;
    private String operType;

    private BusBpsLogMapper bpsLogMapper;

    @Autowired
    public DBLogger(Class<?> clazz, String operType, BusBpsLogMapper bpsLogMapper) {
        this.bpsLogMapper = bpsLogMapper;
        this.className = clazz.getName();
        this.tlogger = LoggerFactory.getLogger(clazz);
        this.operType = operType;
        if (list == null) {
            list = Collections.synchronizedList(new ArrayList<BusBpsLog>());
        }
    }

    private String getMessage() {
        String message = String.format("%s:%s", className, info);
        return message.substring(0, Math.min(message.length(), 1000)).replace("'", "");
    }

    private void Clear() {
        if (list.size() > 500) {
            list.clear();
        }
    }

    private void infoMe(String var1) {
        try {
            this.info = var1;
            BusBpsLog batchLog = new BusBpsLog(operType, getMessage(), "0");
            list.add(batchLog);
        } catch (Exception ex) {
            logger.error("---log info error:{}", ex);
        } finally {
            Clear();
        }
    }


    private void errorMe(String var1) {
        try {
            this.info = var1;
            BusBpsLog batchLog = new BusBpsLog(operType, getMessage(), "1");
            list.add(batchLog);
        } catch (Exception ex) {
            logger.error("---log error:{}", ex);
        } finally {
            Clear();
        }
    }


    public synchronized void Save() {
        try {
            if (this.list.size() > 0) {
                bpsLogMapper.saveAll(list);
                logger.info("---log save ");
            } else {
                logger.info("---log save count=0");
            }
        } catch (Exception ex) {
            logger.error("----log save error:{}", ex);
        } finally {
            this.list.clear();
        }
    }

    @Override
    public void info(String s, Throwable throwable) {
        tlogger.info(s, throwable);
    }

    @Override
    public boolean isInfoEnabled(Marker marker) {
        return tlogger.isInfoEnabled(marker);
    }

    @Override
    public void info(Marker marker, String s) {
        tlogger.info(marker, s);
    }

    @Override
    public void info(Marker marker, String s, Object o) {
        tlogger.info(marker, s, o);
    }

    @Override
    public void info(Marker marker, String s, Object o, Object o1) {
        tlogger.info(marker, s, o, o1);
    }

    @Override
    public void info(Marker marker, String s, Object... objects) {
        tlogger.info(marker, s, objects);
    }

    @Override
    public void info(Marker marker, String s, Throwable throwable) {
        tlogger.info(marker, s, throwable);
    }

    @Override
    public boolean isWarnEnabled() {
        return tlogger.isWarnEnabled();
    }

    @Override
    public void warn(String s) {
        tlogger.warn(s);
    }

    @Override
    public void warn(String s, Object o) {
        tlogger.warn(s, o);
    }

    @Override
    public void warn(String s, Object... objects) {
        tlogger.error(s, objects);
    }

    @Override
    public void warn(String s, Object o, Object o1) {
        tlogger.warn(s, o, o1);
    }

    @Override
    public void warn(String s, Throwable throwable) {
        tlogger.warn(s, throwable);
    }

    @Override
    public boolean isWarnEnabled(Marker marker) {

        return tlogger.isWarnEnabled(marker);
    }

    @Override
    public void warn(Marker marker, String s) {
        tlogger.warn(marker, s);
    }

    @Override
    public void warn(Marker marker, String s, Object o) {
        tlogger.warn(marker, s, o);
    }

    @Override
    public void warn(Marker marker, String s, Object o, Object o1) {
        tlogger.warn(marker, s, o, o1);
    }

    @Override
    public void warn(Marker marker, String s, Object... objects) {
        tlogger.warn(marker, s, objects);
    }

    @Override
    public void warn(Marker marker, String s, Throwable throwable) {
        tlogger.warn(marker, s, throwable);
    }

    @Override
    public boolean isErrorEnabled() {
        return tlogger.isErrorEnabled();
    }

    @Override
    public void error(String s) {
        tlogger.error(s);
        errorMe(s);
    }

    @Override
    public String getName() {
        return tlogger.getName();
    }

    @Override
    public boolean isTraceEnabled() {
        return tlogger.isTraceEnabled();
    }

    @Override
    public void trace(String s) {
        tlogger.trace(s);
    }

    @Override
    public void trace(String s, Object o) {
        tlogger.trace(s, o);
    }

    @Override
    public void trace(String s, Object o, Object o1) {
        tlogger.trace(s, o, o1);
    }

    @Override
    public void trace(String s, Object... objects) {
        tlogger.trace(s, objects);
    }

    @Override
    public void trace(String s, Throwable throwable) {
        tlogger.trace(s, throwable);
    }

    @Override
    public boolean isTraceEnabled(Marker marker) {
        return tlogger.isTraceEnabled(marker);
    }

    @Override
    public void trace(Marker marker, String s) {
        tlogger.trace(marker, s);
    }

    @Override
    public void trace(Marker marker, String s, Object o) {
        tlogger.trace(marker, s, o);
    }

    @Override
    public void trace(Marker marker, String s, Object o, Object o1) {
        tlogger.trace(marker, s, o, o1);
    }

    @Override
    public void trace(Marker marker, String s, Object... objects) {
        tlogger.trace(marker, s, objects);
    }

    @Override
    public void trace(Marker marker, String s, Throwable throwable) {
        tlogger.trace(marker, s, throwable);
    }

    @Override
    public boolean isDebugEnabled() {
        return tlogger.isDebugEnabled();
    }

    @Override
    public void debug(String s) {
        tlogger.debug(s);
    }

    @Override
    public void debug(String s, Object o) {
        tlogger.debug(s, o);
    }

    @Override
    public void debug(String s, Object o, Object o1) {
        tlogger.debug(s, o, o1);
    }

    @Override
    public void debug(String s, Object... objects) {
        tlogger.debug(s, objects);
    }

    @Override
    public void debug(String s, Throwable throwable) {
        tlogger.debug(s, throwable);
    }

    @Override
    public boolean isDebugEnabled(Marker marker) {
        return tlogger.isDebugEnabled(marker);
    }

    @Override
    public void debug(Marker marker, String s) {
        tlogger.debug(marker, s);
    }

    @Override
    public void debug(Marker marker, String s, Object o) {
        tlogger.debug(marker, s, o);
    }

    @Override
    public void debug(Marker marker, String s, Object o, Object o1) {
        tlogger.debug(marker, s, o, o1);
    }

    @Override
    public void debug(Marker marker, String s, Object... objects) {
        tlogger.debug(marker, s, objects);
    }

    @Override
    public void debug(Marker marker, String s, Throwable throwable) {
        tlogger.debug(marker, s, throwable);
    }

    @Override
    public boolean isInfoEnabled() {
        return tlogger.isInfoEnabled();
    }

    @Override
    public void info(String var1) {
        tlogger.info(var1);
        infoMe(var1);
    }

    @Override
    public void info(String s, Object o) {
        tlogger.info(s, o);
        infoMe(s + ":" + o);
    }

    @Override
    public void info(String s, Object o, Object o1) {
        tlogger.info(s, o, o1);
        infoMe(s + ":" + o + "," + o1);
    }

    @Override
    public void info(String s, Object... objects) {
        tlogger.info(s, objects);
        infoMe(String.format(s.replace("{}", "%s"), objects));
    }


    @Override
    public void error(String s, Object o) {
        tlogger.error(s, o);
    }

    @Override
    public void error(String s, Object o, Object o1) {
        tlogger.error(s, o, o1);
    }

    @Override
    public void error(String s, Object... objects) {
        tlogger.error(s, objects);
    }

    @Override
    public void error(String s, Throwable throwable) {
        tlogger.error(s, throwable);
        errorMe(s + ":" + throwable.getMessage());
    }

    @Override
    public boolean isErrorEnabled(Marker marker) {
        return tlogger.isErrorEnabled(marker);
    }

    @Override
    public void error(Marker marker, String s) {
        tlogger.error(marker, s);
    }

    @Override
    public void error(Marker marker, String s, Object o) {
        tlogger.error(marker, s, o);
    }

    @Override
    public void error(Marker marker, String s, Object o, Object o1) {
        tlogger.error(marker, s, o, o1);
    }

    @Override
    public void error(Marker marker, String s, Object... objects) {
        tlogger.error(marker, s, objects);
    }

    @Override
    public void error(Marker marker, String s, Throwable throwable) {
        tlogger.error(marker, s, throwable);
    }
}

