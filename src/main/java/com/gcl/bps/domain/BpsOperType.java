package com.gcl.bps.domain;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class BpsOperType {




    //钱包商户开立
    @Value("${cwfs.operType.service}")
    public String cwfsService;

    @Value("${cwfs.operType.createFile}")
    public String cwfsCreateFile;

    @Value("${cwfs.operType.upAndDown}")
    public String cwfsUpAndDown;


    @Value("${cwfs.operType.updateData}")
    public String cwfsUpdateData;





    //共建APP白名单
    @Value("${pwls.operType.service}")
    public String pwlsService;

    @Value("${pwls.operType.createFile}")
    public String pwlsCreateFile;

    @Value("${pwls.operType.upAndDown}")
    public String pwlsUpAndDown;


    @Value("${pwls.operType.updateData}")
    public String pwlsUpdateData;



    //钱包注册白名单
    @Value("${bwls.operType.service}")
    public String bwlsService;

    @Value("${bwls.operType.createFile}")
    public String bwlsCreateFile;

    @Value("${bwls.operType.upAndDown}")
    public String bwlsUpAndDown;


    @Value("${bwls.operType.updateData}")
    public String bwlsUpdateData;



    //私人钱包开立
    @Value("${ppfs.operType.service}")
    public String ppfsService;

    @Value("${ppfs.operType.createFile}")
    public String ppfsCreateFile;

    @Value("${ppfs.operType.upAndDown}")
    public String ppfsUpAndDown;


    @Value("${ppfs.operType.updateData}")
    public String ppfsUpdateData;



    //商户白名单
    @Value("${wlfs.operType.service}")
    public String wlfsService;

    @Value("${wlfs.operType.createFile}")
    public String wlfsCreateFile;

    @Value("${wlfs.operType.upAndDown}")
    public String wlfsUpAndDown;


    @Value("${wlfs.operType.updateData}")
    public String wlfsUpdateData;



}
