package com.gcl.bps.domain;/**
 * 作者: xiaofeng.ye
 * 日期: 2020/12/15-18:45
 * 描述：
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;

/**
 *@ClassName Base
 *@Deseription TODO
 *@Author xiaofeng.ye
 *@Date 2020/12/15 18:45
 *@Version 1.0
 */
public class Base {
    private static Logger logger = LoggerFactory.getLogger(Base.class);
    protected  static    String  ENCODING="utf-8";
//    protected String getOneVsSpace(int len,String arg) throws UnsupportedEncodingException {
//        return   String.format("%-"+(len-arg.getBytes(ENCODING).length+arg.length())+"s",arg );
//    }

    /**
    * @Description:右补空格
    * @Author: shaowu.ni
    * @Date:
    */
    protected String getOneVsSpace(int len,String arg) throws UnsupportedEncodingException {
        if (arg==null){
            arg=" ";
        }
        String argM= new String(arg.getBytes(ENCODING),ENCODING);
        return   String.format("%-"+len +"s",argM);
    }


    /**
     * 将数字类型指定长度左补0
     * @param len  指定长度
     * @param number  原数字
     * @return 转换后的字符串
     */
    protected String getNumberFormat(int len, String number) {
        BigDecimal num = new BigDecimal(number);
        return String.format("%0"+(len)+"d", num.longValue());
    }
}
