package com.gcl.bps.domain;

/**
 * @program:bps
 * @description: 不同机构 不同柜员，进行分组
 * @author: gcl
 * @create:2022-02-15
 **/
public class OrgNoAndTellerNo {

	private String tradingOrganizationNo;
	private String transactionTellerNumber;
	private String tradingTerminalNumber;
	private Integer count;

	@Override
	public String toString() {
		return "OrgNoAndTellerNo [tradingOrganizationNo=" + tradingOrganizationNo + ", transactionTellerNumber="
				+ transactionTellerNumber + ", tradingTerminalNumber=" + tradingTerminalNumber + ", count=" + count
				+ "]";
	}

	public OrgNoAndTellerNo() {
	}

	public String getTradingOrganizationNo() {
		return tradingOrganizationNo;
	}

	public void setTradingOrganizationNo(String tradingOrganizationNo) {
		this.tradingOrganizationNo = tradingOrganizationNo;
	}

	public String getTransactionTellerNumber() {
		return transactionTellerNumber;
	}

	public void setTransactionTellerNumber(String transactionTellerNumber) {
		this.transactionTellerNumber = transactionTellerNumber;
	}

	public String getTradingTerminalNumber() {
		return tradingTerminalNumber;
	}

	public void setTradingTerminalNumber(String tradingTerminalNumber) {
		this.tradingTerminalNumber = tradingTerminalNumber;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}


}
