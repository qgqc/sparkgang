package com.gcl.bps.domain;

import org.springframework.stereotype.Component;


/**
 * @program:bps
 * @description: 不同机构 不同柜员，进行分组
 * @author: shaowu.ni
 * @create:2021-11-03 16:50
 **/
@Component
public class OrgAndNum {
    //wjw注释 @GeneratedValue(generator="jpa-uuid")
    private String tradingOrganizationNo;
    private String tellerNo;
    private Long num;

    public OrgAndNum() {
    }

    public OrgAndNum(String tradingOrganizationNo, String tellerNo, long num) {
        this.tradingOrganizationNo = tradingOrganizationNo;
        this.tellerNo = tellerNo;
        this.num = num;
    }

    public String getTradingOrganizationNo() {
        return tradingOrganizationNo;
    }

    public void setTradingOrganizationNo(String tradingOrganizationNo) {
        this.tradingOrganizationNo = tradingOrganizationNo;
    }

    public String getTellerNo() {
        return tellerNo;
    }

    public void setTellerNo(String tellerNo) {
        this.tellerNo = tellerNo;
    }

    public long getNum() {
        return num;
    }

    public void setNum(long num) {
        this.num = num;
    }
}
