package com.gcl.bps.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileFilter;
import org.apache.commons.net.ftp.FTPReply;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FtpUtil {
    private static Logger logger = LoggerFactory.getLogger(FtpUtil.class);
    /**
     * 该目录不存在
     */
    public String DIR_NOT_EXIST = "该目录不存在";
    /**
     * "该目录下没有文件
     */
    public String DIR_CONTAINS_NO_FILE = "该目录下没有文件";
    /**
     * 设置缓冲区大小4M
     **/
    private int BUFFER_SIZE = 1024 * 1024 * 4;
    /**
     * 本地字符编码
     **/
    private String localCharset = "GBK";

    /**
     * FTP协议里面，规定文件名编码为iso-8859-1
     **/
    private String serverCharset = "ISO-8859-1";

    /**
     * UTF-8字符编码
     **/
    private final String CHARSET_UTF8 = "UTF-8";

    /**
     * OPTS UTF8字符串常量
     **/
    private final String OPTS_UTF8 = "OPTS UTF8";
    /**
     * FTP地址
     **/
    private String ftpAddress;
    /**
     * FTP端口
     **/
    private int ftpPort;
    /**
     * FTP用户名
     **/
    private String ftpUsername;
    /**
     * FTP密码
     **/
    private String ftpPassword;

    private String ftpPath;

    private FTPClient ftpClient;

    public FtpUtil() {
    }


    public FtpUtil(String ftpAddress, int ftpPort, String ftpUsername, String ftpPassword, String ftpPath) {
        this.ftpAddress = ftpAddress;
        this.ftpPort = ftpPort;
        this.ftpUsername = ftpUsername;
        this.ftpPassword = ftpPassword;
        this.ftpPath = ftpPath;
        ftpClient = new FTPClient();
    }

    /**
     * 连接FTP服务器
     */
    public Boolean login() {
        try {
//            logger.info("----"+ftpAddress+"----"+ftpPort+"----"+ftpUsername+"------"+ftpPassword);
            ftpClient.connect(ftpAddress, ftpPort);
            ftpClient.login(ftpUsername, ftpPassword);
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            //限制缓冲区大小
            ftpClient.setBufferSize(BUFFER_SIZE);
            int reply = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                closeConnect();
                logger.error("FTP服务器连接失败");
                return false;
            }
        } catch (Exception e) {
            logger.error("FTP登录失败", e);
        }
        return true;
    }

    /**
     * 关闭FTP连接
     */
    public void closeConnect() {
        if (ftpClient != null && ftpClient.isConnected()) {
            try {
                ftpClient.logout();
                ftpClient.disconnect();
            } catch (IOException e) {
                logger.error("关闭FTP连接失败", e);
            }
        }
    }

    /**
     * 上传文件到ftp , 临时文件后缀 now
     *
     * @param file
     * @return
     */
    public String uploadFile(File file) {
        boolean returnStatus = false;
        try {
            int reply;
            this.login();
            String loginPath = ftpClient.printWorkingDirectory();
            ftpClient.changeWorkingDirectory(loginPath + ftpPath);
            reply = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(reply)) {
                ftpClient.disconnect();
                return "ftp conn unusual";
            }
            ftpClient.setFileType(ftpClient.BINARY_FILE_TYPE);

            ftpClient.setControlEncoding("GBK");
            InputStream inputStream = null;

            try {
                String path = file.getAbsolutePath();
                inputStream = new FileInputStream(path);
                ftpClient.enterLocalPassiveMode();
                boolean a = ftpClient.storeFile(file.getName() + ".now", inputStream);
                ftpClient.rename(file.getName() + ".now", file.getName());
            } catch (IOException e) {
                logger.error("-uploadFile-ftp error ", e);
                return "uploadFile-ftp error" + e.getMessage();
            } finally {
                inputStream.close();
            }

            this.closeConnect();
            returnStatus = true;
        } catch (IOException e) {
            logger.error("-ftp error ", e);
            return "-ftp error" + e.getMessage();
        } finally {
            if (ftpClient.isConnected()) {
                try {
                    ftpClient.disconnect();
                } catch (IOException ioe) {
                    logger.error("-ftp error ", ioe);
                    return "-ftp error ioe";
                }
            }
        }
        return file.getName();
    }

    private List<String> listRemoteAllFiles(String path, String pattern) throws Exception {
        ftpClient.enterLocalPassiveMode();  // 设置被动模式，开通一个端口来传输数据
        FTPFile[] files;
        if (pattern != null && !pattern.equals("")) {
            files = ftpClient.listFiles(path, new FTPFileFilter() {
                @Override
                public boolean accept(FTPFile file) {
                    // System.out.println(file.getName());
                    if (file.isFile() && Pattern.matches(pattern, file.getName())) {
                        return true;
                    } else {
                        return false;
                    }
                }
            });
        } else {
            files = ftpClient.listFiles(path);
        }

        List<String> list = new ArrayList();
        for (FTPFile file : files) {
            list.add(file.getName());
        }
        return list;
    }

    /**
     * 查看本地从服务器是否下载文件
     *
     * @return
     */


    public String downloadToPath(String fileName, String savePath) {
        // 登录
        this.login();
        File file = null;
        if (ftpClient != null) {
            try {
                String loginPath = ftpClient.printWorkingDirectory();
                String path = changeEncoding(ftpPath);
                // 判断是否存在该目录
                if (!ftpClient.changeWorkingDirectory(loginPath + path)) {
                    logger.error(ftpPath + DIR_NOT_EXIST);
                    return DIR_NOT_EXIST;
                }
                ftpClient.changeWorkingDirectory(loginPath + path);
                ftpClient.setFileType(ftpClient.BINARY_FILE_TYPE);
                ftpClient.enterLocalPassiveMode();
                String[] fs = ftpClient.listNames(fileName);
                // 判断该目录下是否有文件
                if (fs == null || fs.length == 0) {
					// logger.error(ftpPath + DIR_CONTAINS_NO_FILE);
                    return DIR_CONTAINS_NO_FILE;
                }
                String ftpName = new String(fs[0].getBytes(serverCharset), localCharset);

                file = new File(savePath + File.separator + ftpName);
                if (file.exists()) {
                    file.delete();
                    file.createNewFile();
                } else {
                    file.createNewFile();
                }
                OutputStream os = null;
                try {
                    os = new FileOutputStream(file);
                    ftpClient.retrieveFile(ftpName, os);
                    os.flush();
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                    return "fail Exception" + e.getMessage();
                } finally {
                    os.close();
                }
                return file.getName();
            } catch (Exception e) {
                logger.error("下载文件失败", e);
                return "下载文件失败" + e.getMessage();
            } finally {
                if (ftpClient.isConnected()) {
                    try {
                        ftpClient.disconnect();
                    } catch (IOException ioe) {
                        logger.error("-ftp error ", ioe);
                        return "ftp error";
                    }
                }
            }
        } else {
            logger.error("ftpClient is un constrator");
            return "ftpClient is un constrator";
        }

    }


    /**
     * 从ftp服务器下载文件
     *
     * @return
     */
    public List<File> downloadFile(String ftpPath, String pattern, String savePath) {

        List<File> downLoadFiles = new ArrayList<>();
        Pattern p = Pattern.compile(pattern);
        // 登录
        this.login();
        if (ftpClient != null) {
            try {
                String path = changeEncoding(ftpPath);
                // 判断是否存在该目录
                if (!ftpClient.changeWorkingDirectory(path)) {
                    logger.error(ftpPath + DIR_NOT_EXIST);
                    return null;
                }
                ftpClient.setFileType(ftpClient.BINARY_FILE_TYPE);
                List<String> fs = this.listRemoteAllFiles(ftpPath, pattern);
                // 判断该目录下是否有文件
                if (fs == null || fs.size() == 0) {
                    logger.error(ftpPath + DIR_CONTAINS_NO_FILE);
                    return null;
                }
                for (String ff : fs) {
                    String ftpName = new String(ff.getBytes(serverCharset), localCharset);
                    File file = new File(savePath + '/' + ftpName);
                    OutputStream os = null;
                    try {
                        os = new FileOutputStream(file);
                        ftpClient.retrieveFile(ff, os);
                        os.flush();
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                        return null;
                    } finally {
                        os.close();
                    }
                    downLoadFiles.add(file);
                }
            } catch (Exception e) {
                logger.error("下载文件失败", e);
                return null;
            } finally {
                this.closeConnect();
            }

        }
        return downLoadFiles;
    }


    /**
     * FTP服务器路径编码转换
     *
     * @param ftpPath FTP服务器路径
     * @return String
     */
    private String changeEncoding(String ftpPath) {
        String directory = null;
        try {
            if (FTPReply.isPositiveCompletion(ftpClient.sendCommand(OPTS_UTF8, "ON"))) {
                localCharset = CHARSET_UTF8;
            }
            directory = new String(ftpPath.getBytes(localCharset), serverCharset);
        } catch (Exception e) {
            logger.error("路径编码转换失败", e);
        }
        return directory;
    }

    //切换目录到下载目录
    public void movePath(String directory) {
        if (directory != null && !"".equals(directory)) {
            try {
                ftpClient.changeWorkingDirectory(directory);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
