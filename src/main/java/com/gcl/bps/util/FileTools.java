package com.gcl.bps.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class FileTools {
    private static Logger logger = LoggerFactory.getLogger(FileTools.class);


    public static boolean deleteDir(File dir) {
        if(!dir.exists()){
            return true;
        }

        if (dir.isDirectory()) {
            String[] children = dir.list();
            //递归删除目录中的子目录下
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        // 目录此时为空，可以删除
        return dir.delete();
    }

    public static void CreateDir(String path) {
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    public static void createDirectories(String... path) {
        for (int i = 0; i < path.length; i++) {
            File file = new File(Paths.get(System.getProperty("user.dir"), path[i]).toString());
            if (!file.isDirectory()) {
                file.mkdirs();
            }
        }
    }

    /**
     * @param  stream:
     * @Author ： zhangQiang
     * @Date : 2018/7/5-14:32
     * @Desc : 读取text文件
     */
    public static String readInputStream(InputStream stream) {
        StringBuilder result = new StringBuilder();
        try {
            //构造一个BufferedReader类来读取文件
            BufferedReader br = new BufferedReader(new InputStreamReader(stream,"UTF-8"));
            String s;
            while ((s = br.readLine()) != null) {//使用readLine方法，一次读一行
                result.append(s);
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }


    public static String readFileContent(String fileName){
        String encoding = "UTF-8";
        File file = new File(fileName);
        Long filelength = file.length();
        byte[] filecontent = new byte[filelength.intValue()];
        try {
            FileInputStream in = new FileInputStream(file);
            in.read(filecontent);
            in.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            return new String(filecontent, encoding);
        } catch (UnsupportedEncodingException e) {
            System.err.println("The OS does not support " + encoding);
            e.printStackTrace();
            return null;
        }
    }


    //写入文本
    public static void writeToTxt(String outTxt,String fileName) {
        FileOutputStream out = null;
        BufferedOutputStream buff = null;
        File file=new File(fileName);
        if(file.exists()){
            file.delete();
        }
        try {
            out = new FileOutputStream(file);
            buff = new BufferedOutputStream(out);

            buff.write(outTxt.getBytes("gbk"));

            buff.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                buff.close();
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static Boolean writeToTxtEncoding(String outTxt,File file,String encoding) {
        FileOutputStream out = null;
        BufferedOutputStream buff = null;
        if(file.exists()){
            file.delete();
        }
        try {
            out = new FileOutputStream(file,false);
            buff = new BufferedOutputStream(out);
            buff.write(outTxt.getBytes(encoding));
            buff.flush();
            return true;
        } catch (Exception e) {
            logger.error(" writeToTxtEncoding create file fail "+file.getName().toString()+",{}",e);
            return false;
        } finally {
            try {
                buff.close();
                out.close();
            } catch (Exception e) {
                logger.error("ju bing guan bi shi bai ,{}",e);
                return false;
            }
        }
    }




    /**
     * @param  stream:
     * @Author ： zhangQiang
     * @Date : 2018/7/5-14:32
     * @Desc : 读取text文件
     */
    public static String readText(InputStream stream) {
        StringBuilder result = new StringBuilder();
        try {
            //构造一个BufferedReader类来读取文件
            BufferedReader br = new BufferedReader(new InputStreamReader(stream));
            String s;
            while ((s = br.readLine()) != null) {//使用readLine方法，一次读一行
                result.append(s);
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }


    public static void copyFile(String oldPath, String newPath) {
        try {
            int bytesum = 0;
            int byteread = 0;
            File oldfile = new File(oldPath);
            if (oldfile.exists()) { //文件存在时
                InputStream inStream = new FileInputStream(oldPath); //读入原文件

                FileOutputStream fs = new FileOutputStream(newPath);
                byte[] buffer = new byte[1444];

                while ((byteread = inStream.read(buffer)) != -1) {
                    bytesum += byteread; //字节数 文件大小
//                    System.out.println(bytesum);
                    fs.write(buffer, 0, byteread);
                }
                inStream.close();
            } else {
                logger.info("mer account file not exist fileName:" + oldPath);
            }
        } catch (Exception e) {
            System.out.println("复制单个文件操作出错" + ",oldPath=" + oldPath + ",newPath=" + newPath);
            e.printStackTrace();
        }

    }

    public static File[] listFiles(File dir,String name,String axt) {
        FileFilter fileFilter=new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                if(pathname.isFile()&&pathname.getName().endsWith(axt)&&pathname.getName().startsWith(name)){
                    return true;
                }else
                return false;
            }
        };
        File[] fileArray = dir.listFiles(fileFilter);

        return fileArray;
    }

    public static List<Path> listFilesByPattern(String dir, String regular) {
        Path path = Paths.get(dir);
        Pattern pattern = Pattern.compile(regular);
        List<Path> paths = null;
        try {
            paths = Files.walk(path).filter(p -> {
                //如果不是普通的文件，则过滤掉
                if(!Files.isRegularFile(p)) {
                    return false;
                }
                Matcher matcher = pattern.matcher(p.toFile().getName());
                return matcher.matches();
            }).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return paths;
    }


}
