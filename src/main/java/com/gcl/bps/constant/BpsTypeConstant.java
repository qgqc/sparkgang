package com.gcl.bps.constant;

/*****
 * @author wjw
 * @date 2022-01-18
 *
 * 定义BPS系统业务类型常量
 */
public class BpsTypeConstant {

    /***
     * CWFS 钱包商户开立
     */
    public static final String CWFS_TYPE = "CWFS";


    /***
     * PPFS 私人钱包开立
     */
    public static final String PPFS_TYPE = "PPFS";

    /***
	 * PWLS人行共建APP白名单
	 */
    public static final String PWLS_TYPE = "PWLS";

    /***
     *  WLFS 用户商户白名单
     */
    public static final String WLFS_TYPE = "WLFS";


    /***
     *  BWLS 钱包注册白名单
     */
    public static final String BWLS_TYPE = "BWLS";



}
