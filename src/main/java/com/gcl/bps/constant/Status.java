package com.gcl.bps.constant;

/**
 * @program:bps
 * @description:
 * @author: shaowu.ni
 * @create:2021-11-25 10:32
 **/
public class Status {
    public static String SUCCESS="1";
    public static String FAIL="2";
}
