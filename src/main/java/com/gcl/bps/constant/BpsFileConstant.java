package com.gcl.bps.constant;

public class BpsFileConstant {

    //商户类型（收币）
    public static final String MERCHANT_TYPE_RECEIVE = "0000";
    //商户类型（代收币）
    public static final String MERCHANT_TYPE_REPLACE_RECEIVE = "0002";

    //商户结算标志关闭
    public static final String MERCHANT_SETTLEMENT_CLOSE = "0";



}
