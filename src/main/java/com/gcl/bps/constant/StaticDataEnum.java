package com.gcl.bps.constant;

/**
 * @program:bps
 * @description: 静态数据枚举类型
 * @author: shaowu.ni
 * @create:2021-12-22 16:06
 **/
public enum StaticDataEnum {

    MER_CWFS_WALLET("1", "对公钱包固定值"),
    PRIVATE_WALLET("2", "私人钱包开立固定值"),
    APP_LIST_WALLET("3", "共建app白名单"),
    WALLET_REGISTER_LIST_WALLET("4", "钱包注册白名单");


    private String status;
    private String value;

    StaticDataEnum(String status, String value) {
        this.status = status;
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
