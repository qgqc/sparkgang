package com.gcl.bps.constant;

public enum BusPersonInfoStatus {
    INITIALIZATION("0","数据初始化"),
    SUBMIT_REVIEW("1","待审核"),
    REVIEW_PASSED("2","审核通过"),
    REVIEW_REJECTION("3","审核退回"),

    /**
     *  人行共建app钱包开立状态
     */
    PWLS_SYSTEM_PROCESSING("4","人行共建APP白名单系统处理中"),
    PWLS_SUCCESSFUL_OPENING("5","人行共建APP白名单开立成功"),
    PWLS_OPENING_FAILED("6","人行共建APP白名单开立失败"),

    /**
     *  钱包注册白名单开立状态
     */
    BWLS_SYSTEM_PROCESSING("7","钱包注册白名单开立系统处理中"),
    BWLS_SUCCESSFUL_OPENING("8","钱包注册白名单开立成功"),
    BWLS_OPENING_FAILED("9","钱包注册白名单开立失败"),

    /**
     * ppfs 私人钱包开立状态
     */
    PPFS_SYSTEM_PROCESSING("10","私人钱包批量开立系统处理中"),
    PPFS_SUCCESSFUL_OPENING("11","私人钱包批量开立成功"),
    PPFS_OPENING_FAILED("12","私人钱包批量开立失败"),

    SYSTEM_PROCESSING("99", "私人钱包开立系统生成文件时进行打标");

    /**
     * bps处理时，各中业务生成上传文件所需要记录的中间状态
     */

    private String status;
    private String value;

    BusPersonInfoStatus(String status, String value){
        this.status = status;
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
