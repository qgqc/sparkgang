package com.gcl.bps.constant;

/**
 * @program:bps
 * @description:bps 文件的执行步骤
 * @author: shaowu.ni
 * @create:2021-11-25 10:23
 **/
public enum FileStatus {
    CREATE_FILE("1","生成文件"),
    UP_FILE_FILE("2","上传文件"),
    DOWNLOAD_FILE("3","下载文件"),
    RETURN_FILE("4","更新文件");

    private String status;
    private String value;

    FileStatus(String status, String value){
        this.status = status;
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
