package com.gcl.bps.constant;

public class ExecuteStepsAndResult {

	/***
	 * 执行步骤,0待生成, 1已生成，2已上传，3已下载，4已回盘更新
	 */
	public static final String TO_BE_GENERATED = "0";

	public static final String GENERATED = "1";

	public static final String UPLOADED = "2";

	public static final String DOWNLOADED = "3";

	public static final String REVERTED_TO_UPDATE = "4";

	/**
	 * 执行结果,执行步骤的执行结果 0:成功，1:失败
	 */
	public static final String SUCCESS = "0";

	public static final String FAIL = "1";
}
