package com.gcl.bps.constant;

/**
 * @program:bps
 * @description:  状态枚举
 * @author: shaowu.ni
 * @create:2021-11-25 09:59
 **/
public enum DataStatusEnum {
    /**
     * 数据状态
     */
    INITIAL_DATA("1","初始数据"),
    DATA_CONFIRMED("2","数据已确认"),
    DATA_UPLOADED("3","资料已上传"),
    SUBMIT_REVIEW("4","待审核"),
    REVIEW_PASSED("5","审核通过"),
    REVIEW_REJECTION("6","审核退回"),
    SYSTEM_PROCESSING("7","系统处理中"),
    SUCCESSFUL_OPENING("8","开立成功"),
    OPENING_FAILED("9","开立失败"),

    /**
     * bps处理状态
     */
    CREATE_SUCCESS("10","生成文件"),
    UP_FILE_FILE("11","上传文件"),
    DOWNLOAD_FILE("12","下载文件"),
    RETURN_FILE_SUCCESS("13","更新文件"),
    CREATE_FILE_FAIL("14","生成文件失败"),
    RETURN_FILE_FAIL("15","回盘文件更新失败");


    private String status;
    private String value;

    DataStatusEnum(String status, String value){
        this.status = status;
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
