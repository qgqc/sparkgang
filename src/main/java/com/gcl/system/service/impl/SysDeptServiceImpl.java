package com.gcl.system.service.impl;

import com.gcl.common.annotation.DataScope;
import com.gcl.common.constant.UserConstants;
import com.gcl.common.core.domain.TreeSelect;
import com.gcl.common.core.domain.entity.SysDept;
import com.gcl.common.core.domain.entity.SysRole;
import com.gcl.common.core.domain.entity.SysUser;
import com.gcl.common.core.text.Convert;
import com.gcl.common.exception.ServiceException;
import com.gcl.common.utils.SecurityUtils;
import com.gcl.common.utils.StringUtils;
import com.gcl.common.utils.bean.BeanValidators;
import com.gcl.common.utils.spring.SpringUtils;
import com.gcl.system.mapper.SysDeptMapper;
import com.gcl.system.mapper.SysRoleMapper;
import com.gcl.system.service.ISysDeptService;
import com.gcl.system.service.ISysUserService;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Validator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 部门管理 服务实现
 * 
 * @author ruoyi
 */
@Service
public class SysDeptServiceImpl implements ISysDeptService
{
    private static final Logger log = LoggerFactory.getLogger(SysUserServiceImpl.class);

    @Autowired
    private ISysUserService sysUserService;

    @Autowired
    private SysDeptMapper deptMapper;

    @Autowired
    private SysRoleMapper roleMapper;


    @Autowired
    protected Validator validator;
    /**
     * 查询部门管理数据
     * 
     * @param dept 部门信息
     * @return 部门信息集合
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<SysDept> selectDeptList(SysDept dept)
    {
        return deptMapper.selectDeptList(dept);
    }

    /**
     * 构建前端所需要树结构
     * 
     * @param depts 部门列表
     * @return 树结构列表
     */
    @Override
    public List<SysDept> buildDeptTree(List<SysDept> depts)
    {
        List<SysDept> returnList = new ArrayList<SysDept>();
        List<String> tempList = new ArrayList<String>();
        for (SysDept dept : depts)
        {
            tempList.add(dept.getDeptNo());
        }
        for (Iterator<SysDept> iterator = depts.iterator(); iterator.hasNext();)
        {
            SysDept dept = (SysDept) iterator.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dept.getParentNo()))
            {
                recursionFn(depts, dept);
                returnList.add(dept);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = depts;
        }
        return returnList;
    }

    /**
     * 构建前端所需要下拉树结构
     * 
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    @Override
    public List<TreeSelect> buildDeptTreeSelect(List<SysDept> depts)
    {
        List<SysDept> deptTrees = buildDeptTree(depts);
        return deptTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
    }

    /**
     * 根据角色ID查询部门树信息
     * 
     * @param roleId 角色ID
     * @return 选中部门列表
     */
    @Override
    public List<Long> selectDeptListByRoleId(Long roleId)
    {
        SysRole role = roleMapper.selectRoleById(roleId);
        return deptMapper.selectDeptListByRoleId(roleId, role.isDeptCheckStrictly());
    }

    /**
     * 根据部门ID查询信息
     * 
     * @param deptId 部门ID
     * @return 部门信息
     */
    @Override
    public SysDept selectDeptById(Long deptId)
    {
        return deptMapper.selectDeptById(deptId);
    }

    /**
     * 根据ID查询所有子部门（正常状态）
     * 
     * @param deptId 部门ID
     * @return 子部门数
     */
    @Override
    public int selectNormalChildrenDeptById(Long deptId)
    {
        return deptMapper.selectNormalChildrenDeptById(deptId);
    }

    /**
     * 是否存在子节点
     * 
     * @param deptId 部门ID
     * @return 结果
     */
    @Override
    public boolean hasChildByDeptId(Long deptId)
    {
        int result = deptMapper.hasChildByDeptId(deptId);
        return result > 0;
    }

    /**
     * 查询部门是否存在用户
     * 
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    @Override
    public boolean checkDeptExistUser(Long deptId)
    {
        int result = deptMapper.checkDeptExistUser(deptId);
        return result > 0;
    }

    /**
     * 校验部门名称是否唯一
     * 
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public String checkDeptNameUnique(SysDept dept)
    {
        Long deptId = StringUtils.isNull(dept.getDeptId()) ? -1L : dept.getDeptId();
        SysDept info = deptMapper.checkDeptNameUnique(dept.getDeptName(), dept.getParentId());
        if (StringUtils.isNotNull(info) && info.getDeptId().longValue() != deptId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    @Override
    public String checkDeptNameUniqueByParentNo(SysDept dept) {
        Long deptId = StringUtils.isNull(dept.getDeptId()) ? -1L : dept.getDeptId();
        SysDept info = deptMapper.checkDeptNameUniqueByParentNo(dept.getDeptName(), dept.getParentNo());
        if (StringUtils.isNotNull(info) && info.getDeptId().longValue() != deptId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    @Override
    public String checkDeptNoUniqueByParentNo(SysDept dept) {
        Long deptId = StringUtils.isNull(dept.getDeptId()) ? -1L : dept.getDeptId();
        SysDept info = deptMapper.checkDeptNoUniqueByParentNo(dept.getDeptNo(), dept.getParentNo());
        if (StringUtils.isNotNull(info) && info.getDeptId().longValue() != deptId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验部门是否有数据权限
     * 
     * @param deptId 部门id
     */
    @Override
    public void checkDeptDataScope(Long deptId)
    {
        if (!SysUser.isAdmin(SecurityUtils.getUserId()))
        {
            SysDept dept = new SysDept();
            dept.setDeptId(deptId);
            List<SysDept> depts = SpringUtils.getAopProxy(this).selectDeptList(dept);
            if (StringUtils.isEmpty(depts))
            {
                throw new ServiceException("没有权限访问部门数据！");
            }
        }
    }

    /**
     * 新增保存部门信息
     * 
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public int insertDept(SysDept dept)
    {
        //如果机构号或机构名称已存在，不允许新增
        if(StringUtils.isBlank(dept.getDeptNo()) || StringUtils.isBlank(dept.getDeptName()) ){
            throw new ServiceException("机构号或机构名称为空，不允许新增");
        }

        SysDept deptNoDb =  deptMapper.selectDeptByDeptNo(dept.getDeptNo());
        if(deptNoDb != null){
            throw new ServiceException("机构号已存在，不允许新增");
        }

/*        SysDept deptNameDb =  deptMapper.selectDeptByDeptName(dept.getDeptName());
        if(deptNameDb != null){
            throw new ServiceException("机构名称已存在，不允许新增");
        }*/

        //SysDept info = deptMapper.selectDeptById(dept.getParentId());
        if(StringUtils.isBlank(dept.getParentNo())){
            //如果没有父部门，直接插入
            dept.setAncestors(SysDept.NOT_HAVE_ANCESTOR);
            dept.setParentNo(SysDept.NO_PARENT);

        }else{
            SysDept info = deptMapper.selectDeptByDeptNo(dept.getParentNo());

         /*   if(info == null){
                throw new ServiceException("上级部门不存在，不允许新增！请确定上级部门机构号正确，再进行新增，上级部门不存在机构号为：" + dept.getParentNo());

            }*/
            //设置本条部门信息的父机构信息ID
            if(info != null){
                dept.setParentId(info.getDeptId());
                // 如果父节点不为正常状态,则不允许新增子节点
                if (!UserConstants.DEPT_NORMAL.equals(info.getStatus()))
                {
                    throw new ServiceException("部门停用，不允许新增");
                }
                String infoAncestors = info.getAncestors();
                if(StringUtils.isBlank(infoAncestors)){
                    infoAncestors = SysDept.NOT_KNOW_ANCESTOR;
                }
                dept.setAncestors(infoAncestors + "," + dept.getParentId());
            }else{
                dept.setAncestors(SysDept.NOT_KNOW_ANCESTOR);
            }


        }

        //设置插入的部门信息默认为正常状态
        String  normalStatus = "0";
        dept.setStatus(normalStatus);

        return deptMapper.insertDept(dept);
    }

    /**
     * 修改保存部门信息
     * 
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    @Transactional
    public int updateDept(SysDept dept)
    {
        SysDept newParentDept = deptMapper.selectDeptByDeptNo(dept.getParentNo());
        SysDept oldDept = deptMapper.selectDeptByDeptNo(dept.getDeptNo());
        if (StringUtils.isNotNull(newParentDept) && StringUtils.isNotNull(oldDept))
        {
            String newAncestors = newParentDept.getAncestors() + "," + newParentDept.getDeptId();
            String oldAncestors = oldDept.getAncestors();
            dept.setAncestors(newAncestors);
            updateDeptChildren(dept.getDeptId(), newAncestors, oldAncestors);
        }
        int result = deptMapper.updateDept(dept);
        if (UserConstants.DEPT_NORMAL.equals(dept.getStatus()) && StringUtils.isNotEmpty(dept.getAncestors())
                && !StringUtils.equals("0", dept.getAncestors()))
        {
            // 如果该部门是启用状态，则启用该部门的所有上级部门
            updateParentDeptStatusNormal(dept);
        }
        return result;
    }

    /**
     * 修改该部门的父级部门状态
     * 
     * @param dept 当前部门
     */
    private void updateParentDeptStatusNormal(SysDept dept)
    {
        String ancestors = dept.getAncestors();
        Long[] deptIds = Convert.toLongArray(ancestors);
        deptMapper.updateDeptStatusNormal(deptIds);
    }

    /**
     * 修改子元素关系
     * 
     * @param deptId 被修改的部门ID
     * @param newAncestors 新的父ID集合
     * @param oldAncestors 旧的父ID集合
     */
    public void updateDeptChildren(Long deptId, String newAncestors, String oldAncestors)
    {
        List<SysDept> children = deptMapper.selectChildrenDeptById(deptId);
        for (SysDept child : children)
        {
            child.setAncestors(child.getAncestors().replaceFirst(oldAncestors, newAncestors));
        }
        if (children.size() > 0)
        {
            deptMapper.updateDeptChildren(children);
        }
    }

    /**
     * 删除部门管理信息
     * 
     * @param deptId 部门ID
     * @return 结果
     */
    @Override
    public int deleteDeptById(Long deptId)
    {
        return deptMapper.deleteDeptById(deptId);
    }

    /**
     * 递归列表
     */
    private void recursionFn
    (List<SysDept> list, SysDept t)
    {
        // 得到子节点列表
        List<SysDept> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysDept tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<SysDept> getChildList(List<SysDept> list, SysDept t)
    {
        List<SysDept> tlist = new ArrayList<SysDept>();
        Iterator<SysDept> it = list.iterator();
        while (it.hasNext())
        {
            SysDept n = (SysDept) it.next();
            if (StringUtils.isNotBlank(n.getParentNo()) && n.getParentNo().equals(t.getDeptNo()))
            {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<SysDept> list, SysDept t)
    {
        return getChildList(list, t).size() > 0;
    }
    /**
     * 导入用户数据
     *
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    @Override
    public String importDept(List<SysDept> userList, Boolean isUpdateSupport, String operName)
    {
        if (StringUtils.isNull(userList) || userList.size() == 0)
        {
            throw new ServiceException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
    //    String password = configService.selectConfigByKey("sys.user.initPassword");
        for (SysDept dept : userList)
        {
            try
            {

                    BeanValidators.validateWithException(validator, dept);
                   // user.setPassword(SecurityUtils.encryptPassword(password));
                    dept.setCreateBy(operName);
                    this.insertDept(dept);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、部门 " + dept.getDeptName() + " 导入成功");


            }
            catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、部门 " + dept.getDeptName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0)
        {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        }
        else
        {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

    @Override
    public SysDept selectDeptByDeptNo(String deptNo) {
        return deptMapper.selectDeptByDeptNo(deptNo);
    }

    @Override
    public List<SysDept> getAllSonDeptByParentDeptNo(Long parentDeptId, List<SysDept> depts) {
        if(CollectionUtils.isEmpty(depts)){
            depts = new ArrayList<>();
        }
        List<SysDept> sonDepts =  deptMapper.selectChildrenDeptByIdNoFunction(parentDeptId);
        if(!CollectionUtils.isEmpty(sonDepts)){
            depts.addAll(sonDepts);
            for(SysDept dept : sonDepts){
                //儿子如果没有孙子  返回
                List<SysDept> grandsonDepts =  this.getAllSonDeptByParentDeptNo(dept.getDeptId(),depts);
                if(!CollectionUtils.isEmpty(grandsonDepts)){
                    depts.addAll(grandsonDepts);
                }else{
                    continue;
                }
            }
        }

        return depts;
    }

    @Override
    public SysDept selectDeptByDeptName(String deptName) {
        return deptMapper.selectDeptByDeptName(deptName);
    }

    @Override
    public List<SysDept> getCurrentDeptAndAllSonDeptByCurrentLoginUserDept(String loginUserName) {
        List<SysDept>  depts = new ArrayList<>();
        //查询当前登录用户机构信息

        SysUser sysUser = sysUserService.selectUserByUserName(loginUserName);

        if(sysUser.getDeptId() != null){
            SysDept dept = this.selectDeptById(sysUser.getDeptId());
            if(dept != null){
                //返回信息中也要包含本机构信息 所以加上
                depts.add(dept);
                //递归查找所有子机构
                List<SysDept> sonDepts = this.getAllSonDeptByParentDeptNo(dept.getDeptId(),null);
                if(CollectionUtils.isNotEmpty(sonDepts)){
                    depts.addAll(sonDepts);
                }
            }else {
                //如果当前用户不存在机构  则 只能看所有没有机构的数据
                return null;
            }
        }
        return depts;
    }

    @Override
    public List<SysDept> getParentDeptAndAllSonDeptByCurrentLoginUserDept(String loginUserName) {
        List<SysDept>  depts = new ArrayList<>();
        //查询当前登录用户机构信息
        SysUser sysUser = sysUserService.selectUserByUserName(loginUserName);

        if(sysUser.getDeptId() != null){
            SysDept dept = this.selectDeptById(sysUser.getDeptId());
            if(dept != null){
                String parentNo = dept.getParentNo();
                SysDept parentDept  = this.selectDeptByDeptNo(parentNo);
                if(parentDept != null){
                    //有父机构，查找父机构下所有子机构
                }else{
                    //没有父机构 本机构赋值给幅机构  相当于看本机构下所有子机构的数据
                    parentDept =  dept;
                }
                //递归查找所有子机构
                depts = this.getAllSonDeptByParentDeptNo(parentDept.getDeptId(),null);
            }else {
                //如果当前用户不存在机构  则 只能看所有没有机构的数据
                return null;
            }
        }
        return depts;
    }
}
