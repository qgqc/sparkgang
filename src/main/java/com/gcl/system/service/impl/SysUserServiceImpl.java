package com.gcl.system.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.gcl.common.annotation.DataScope;
import com.gcl.common.constant.UserConstants;
import com.gcl.common.core.domain.entity.SysRole;
import com.gcl.common.core.domain.entity.SysUser;
import com.gcl.common.exception.ServiceException;
import com.gcl.common.utils.SecurityUtils;
import com.gcl.common.utils.StringUtils;
import com.gcl.common.utils.bean.BeanValidators;
import com.gcl.common.utils.spring.SpringUtils;
import com.gcl.system.domain.SysPost;
import com.gcl.system.domain.SysUserPost;
import com.gcl.system.domain.SysUserRole;
import com.gcl.system.mapper.SysPostMapper;
import com.gcl.system.mapper.SysRoleMapper;
import com.gcl.system.mapper.SysUserMapper;
import com.gcl.system.mapper.SysUserPostMapper;
import com.gcl.system.mapper.SysUserRoleMapper;
import com.gcl.system.service.ISysConfigService;
import com.gcl.system.service.ISysUserService;

/**
 * 用户 业务层处理
 * 
 * @author ruoyi
 */
@Service
public class SysUserServiceImpl implements ISysUserService {
	private static final Logger log = LoggerFactory.getLogger(SysUserServiceImpl.class);

	@Autowired
	private SysUserMapper userMapper;

	@Autowired
	private SysRoleMapper roleMapper;

	@Autowired
	private SysPostMapper postMapper;

	@Autowired
	private SysUserRoleMapper userRoleMapper;

	@Autowired
	private SysUserPostMapper userPostMapper;

	@Autowired
	private ISysConfigService configService;

	@Autowired
	protected Validator validator;

	/**
	 * 根据条件分页查询用户列表
	 * 
	 * @param user
	 *            用户信息
	 * @return 用户信息集合信息
	 */
	@Override
	@DataScope(deptAlias = "d", userAlias = "u")
	public List<SysUser> selectUserList(SysUser user) {
		return userMapper.selectUserList(user);
	}

	/**
	 * 根据条件分页查询已分配用户角色列表
	 * 
	 * @param user
	 *            用户信息
	 * @return 用户信息集合信息
	 */
	@Override
	@DataScope(deptAlias = "d", userAlias = "u")
	public List<SysUser> selectAllocatedList(SysUser user) {
		return userMapper.selectAllocatedList(user);
	}

	/**
	 * 根据条件分页查询未分配用户角色列表
	 * 
	 * @param user
	 *            用户信息
	 * @return 用户信息集合信息
	 */
	@Override
	@DataScope(deptAlias = "d", userAlias = "u")
	public List<SysUser> selectUnallocatedList(SysUser user) {
		return userMapper.selectUnallocatedList(user);
	}

	/**
	 * 通过用户名查询用户
	 * 
	 * @param userName
	 *            用户名
	 * @return 用户对象信息
	 */
	@Override
	public SysUser selectUserByUserName(String userName) {
		return userMapper.selectUserByUserName(userName);
	}

	/**
	 * 通过用户ID查询用户
	 * 
	 * @param userId
	 *            用户ID
	 * @return 用户对象信息
	 */
	@Override
	public SysUser selectUserById(Long userId) {
		return userMapper.selectUserById(userId);
	}

	@Override
	public List<SysUser> selectUserByDeptId(Long deptId) {
		return userMapper.selectUserByDeptId(deptId);
	}

	/**
	 * 查询用户所属角色组
	 * 
	 * @param userName
	 *            用户名
	 * @return 结果
	 */
	@Override
	public String selectUserRoleGroup(String userName) {
		List<SysRole> list = roleMapper.selectRolesByUserName(userName);
		if (CollectionUtils.isEmpty(list)) {
			return StringUtils.EMPTY;
		}
		return list.stream().map(SysRole::getRoleName).collect(Collectors.joining(","));
	}

	/**
	 * 查询用户所属岗位组
	 * 
	 * @param userName
	 *            用户名
	 * @return 结果
	 */
	@Override
	public String selectUserPostGroup(String userName) {
		List<SysPost> list = postMapper.selectPostsByUserName(userName);
		if (CollectionUtils.isEmpty(list)) {
			return StringUtils.EMPTY;
		}
		return list.stream().map(SysPost::getPostName).collect(Collectors.joining(","));
	}

	/**
	 * 校验用户名称是否唯一
	 * 
	 * @param userName
	 *            用户名称
	 * @return 结果
	 */
	@Override
	public String checkUserNameUnique(String userName) {
		int count = userMapper.checkUserNameUnique(userName);
		if (count > 0) {
			return UserConstants.NOT_UNIQUE;
		}
		return UserConstants.UNIQUE;
	}

	/**
	 * 校验手机号码是否唯一
	 *
	 * @param user
	 *            用户信息
	 * @return
	 */
	@Override
	public String checkPhoneUnique(SysUser user) {
		Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
		SysUser info = userMapper.checkPhoneUnique(user.getPhonenumber());
		if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
			return UserConstants.NOT_UNIQUE;
		}
		return UserConstants.UNIQUE;
	}

	/**
	 * 校验email是否唯一
	 *
	 * @param user
	 *            用户信息
	 * @return
	 */
	@Override
	public String checkEmailUnique(SysUser user) {
		Long userId = StringUtils.isNull(user.getUserId()) ? -1L : user.getUserId();
		SysUser info = userMapper.checkEmailUnique(user.getEmail());
		if (StringUtils.isNotNull(info) && info.getUserId().longValue() != userId.longValue()) {
			return UserConstants.NOT_UNIQUE;
		}
		return UserConstants.UNIQUE;
	}

	/**
	 * 校验用户是否允许操作
	 * 
	 * @param user
	 *            用户信息
	 */
	@Override
	public void checkUserAllowed(SysUser user) {
		if (StringUtils.isNotNull(user.getUserId()) && user.isAdmin()) {
			throw new ServiceException("不允许操作超级管理员用户");
		}
	}

	/**
	 * 校验用户是否有数据权限
	 * 
	 * @param userId
	 *            用户id
	 */
	@Override
	public void checkUserDataScope(Long userId) {
		if (!SysUser.isAdmin(SecurityUtils.getUserId())) {
			SysUser user = new SysUser();
			user.setUserId(userId);
			List<SysUser> users = SpringUtils.getAopProxy(this).selectUserList(user);
			if (StringUtils.isEmpty(users)) {
				throw new ServiceException("没有权限访问用户数据！");
			}
		}
	}

	/**
	 * 新增保存用户信息
	 * 
	 * @param user
	 *            用户信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int insertUser(SysUser user) {
		// 新增用户信息
		int rows = userMapper.insertUser(user);
		//System.out.println("时间21：" + DateFormatUtils.format(new Date(),"HH:mm:ss"));

		// 新增用户岗位关联
		insertUserPost(user);
		//System.out.println("时间22：" + DateFormatUtils.format(new Date(),"HH:mm:ss"));

		// 新增用户与角色管理
		insertUserRole(user);
		//System.out.println("时间23：" + DateFormatUtils.format(new Date(),"HH:mm:ss"));

		return rows;
	}

	@Override
	@Transactional
	public int insertImportUser(SysUser user) {
		// 新增用户信息
		int rows = userMapper.insertUserAndSetDeptId(user);
		//System.out.println("时间21：" + DateFormatUtils.format(new Date(),"HH:mm:ss"));

		// 新增用户岗位关联
		insertUserPost(user);
		//System.out.println("时间22：" + DateFormatUtils.format(new Date(),"HH:mm:ss"));

		// 新增用户与角色管理
		insertUserRole(user);
		//System.out.println("时间23：" + DateFormatUtils.format(new Date(),"HH:mm:ss"));

		return rows;
	}



	@Transactional
	public int insertImportUserList(List<SysUser> users) {
		if(!CollectionUtils.isEmpty(users)){
			//oracle 一次插入数量过多(64k)会报异常，所以将list均分，一千执行一次
			int index = 0;//执行游标
			int groupNum = 1000;
			List<SysUser> insertList = new ArrayList<>(groupNum);
			while(index < users.size()){
				for(int insertListIndex = 0; insertListIndex < groupNum ; insertListIndex++){
					if(index + 1 > users.size()){
						break;
					}
					insertList.add(users.get(index));
					index++;
				}
				// 新增用户信息
				userMapper.insertBatchUserAndSetDeptId(insertList);
				insertList = new ArrayList<>(groupNum);
			}


			//System.out.println("时间21：" + DateFormatUtils.format(new Date(),"HH:mm:ss"));

			for(SysUser user : users){
				// 新增用户岗位关联
				insertUserPost(user);
				//System.out.println("时间22：" + DateFormatUtils.format(new Date(),"HH:mm:ss"));

				// 新增用户与角色管理
				insertUserRole(user);
				//System.out.println("时间23：" + DateFormatUtils.format(new Date(),"HH:mm:ss"));

			}
			return users.size();
		}

		return 0;
	}


	/**
	 * 注册用户信息
	 * 
	 * @param user
	 *            用户信息
	 * @return 结果
	 */
	@Override
	public boolean registerUser(SysUser user) {
		return userMapper.insertUser(user) > 0;
	}

	/**
	 * 修改保存用户信息
	 * 
	 * @param user
	 *            用户信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int updateUser(SysUser user) {
		Long userId = user.getUserId();
		// 删除用户与角色关联
		userRoleMapper.deleteUserRoleByUserId(userId);
		// 新增用户与角色管理
		insertUserRole(user);
		// 删除用户与岗位关联
		userPostMapper.deleteUserPostByUserId(userId);
		// 新增用户与岗位管理
		insertUserPost(user);
		return userMapper.updateUser(user);
	}

	/**
	 * 用户授权角色
	 * 
	 * @param userId
	 *            用户ID
	 * @param roleIds
	 *            角色组
	 */
	@Override
	@Transactional
	public void insertUserAuth(Long userId, Long[] roleIds) {
		userRoleMapper.deleteUserRoleByUserId(userId);
		insertUserRole(userId, roleIds);
	}

	/**
	 * 修改用户状态
	 * 
	 * @param user
	 *            用户信息
	 * @return 结果
	 */
	@Override
	public int updateUserStatus(SysUser user) {
		return userMapper.updateUser(user);
	}

	/**
	 * 修改用户基本信息
	 * 
	 * @param user
	 *            用户信息
	 * @return 结果
	 */
	@Override
	public int updateUserProfile(SysUser user) {
		return userMapper.updateUser(user);
	}

	/**
	 * 修改用户头像
	 * 
	 * @param userName
	 *            用户名
	 * @param avatar
	 *            头像地址
	 * @return 结果
	 */
	@Override
	public boolean updateUserAvatar(String userName, String avatar) {
		return userMapper.updateUserAvatar(userName, avatar) > 0;
	}

	/**
	 * 重置用户密码
	 * 
	 * @param user
	 *            用户信息
	 * @return 结果
	 */
	@Override
	public int resetPwd(SysUser user) {
		return userMapper.updateUser(user);
	}

	/**
	 * 重置用户密码
	 * 
	 * @param userName
	 *            用户名
	 * @param password
	 *            密码
	 * @return 结果
	 */
	@Override
	public int resetUserPwd(String userName, String password) {
		return userMapper.resetUserPwd(userName, password);
	}

	/**
	 * 新增用户角色信息
	 * 
	 * @param user
	 *            用户对象
	 */
	public void insertUserRole(SysUser user) {
		Long[] roles = user.getRoleIds();
		if (StringUtils.isNotNull(roles)) {
			// 新增用户与角色管理
			List<SysUserRole> list = new ArrayList<SysUserRole>();
			for (Long roleId : roles) {
				SysUserRole ur = new SysUserRole();
				ur.setUserId(user.getUserId());
				ur.setRoleId(roleId);
				list.add(ur);
			}
			if (list.size() > 0) {
				userRoleMapper.batchUserRole(list);
			}
		}
	}

	/**
	 * 新增用户岗位信息
	 * 
	 * @param user
	 *            用户对象
	 */
	public void insertUserPost(SysUser user) {
		Long[] posts = user.getPostIds();
		if (StringUtils.isNotNull(posts)) {
			// 新增用户与岗位管理
			List<SysUserPost> list = new ArrayList<SysUserPost>();
			for (Long postId : posts) {
				SysUserPost up = new SysUserPost();
				up.setUserId(user.getUserId());
				up.setPostId(postId);
				list.add(up);
			}
			if (list.size() > 0) {
				userPostMapper.batchUserPost(list);
			}
		}
	}

	/**
	 * 新增用户角色信息
	 * 
	 * @param userId
	 *            用户ID
	 * @param roleIds
	 *            角色组
	 */
	public void insertUserRole(Long userId, Long[] roleIds) {
		if (StringUtils.isNotNull(roleIds)) {
			// 新增用户与角色管理
			List<SysUserRole> list = new ArrayList<SysUserRole>();
			for (Long roleId : roleIds) {
				SysUserRole ur = new SysUserRole();
				ur.setUserId(userId);
				ur.setRoleId(roleId);
				list.add(ur);
			}
			if (list.size() > 0) {
				userRoleMapper.batchUserRole(list);
			}
		}
	}

	/**
	 * 通过用户ID删除用户
	 * 
	 * @param userId
	 *            用户ID
	 * @return 结果
	 */
	@Override
	@Transactional
	public int deleteUserById(Long userId) {
		// 删除用户与角色关联
		userRoleMapper.deleteUserRoleByUserId(userId);
		// 删除用户与岗位表
		userPostMapper.deleteUserPostByUserId(userId);
		return userMapper.deleteUserById(userId);
	}

	/**
	 * 批量删除用户信息
	 * 
	 * @param userIds
	 *            需要删除的用户ID
	 * @return 结果
	 */
	@Override
	@Transactional
	public int deleteUserByIds(Long[] userIds) {
		for (Long userId : userIds) {
			checkUserAllowed(new SysUser(userId));
		}
		// 删除用户与角色关联
		userRoleMapper.deleteUserRole(userIds);
		// 删除用户与岗位关联
		userPostMapper.deleteUserPost(userIds);
		return userMapper.deleteUserByIds(userIds);
	}

	/**
	 * 导入用户数据
	 * 
	 * @param userList
	 *            用户数据列表
	 * @param isUpdateSupport
	 *            是否更新支持，如果已存在，则进行更新数据
	 * @param operName
	 *            操作用户
	 * @return 结果
	 */
	@Override
	@Transactional
	public String importUser(List<SysUser> userList, Boolean isUpdateSupport, String operName) {
		if (StringUtils.isNull(userList) || userList.size() == 0) {
			throw new ServiceException("导入用户数据不能为空！");
		}
		int successNum = 0;
		int failureNum = 0;
        List<SysUser> successList = new ArrayList<SysUser>();

		StringBuilder successMsg = new StringBuilder();
		StringBuilder failureMsg = new StringBuilder();
		String password = configService.selectConfigByKey("sys.user.initPassword");
		String defaultPassword = SecurityUtils.encryptPassword(password);
		Set<String> nowBatchUserNames = new HashSet<>();
		int count = 0;
		for (SysUser user : userList) {
			count++;
			//System.out.println("共有数据：" + userList.size() + "条，当前准备处理" + count + "条。");
			try {
				// 验证是否存在这个用户
				//System.out.println("时间1：" + DateFormatUtils.format(new Date(),"HH:mm:ss"));
				SysUser u = userMapper.selectUserByUserName(user.getUserName());
				if (StringUtils.isNull(u) && !nowBatchUserNames.contains(user.getUserName())) {
					// BeanValidators.validateWithException(validator, user);
					user.setPassword(defaultPassword);
					//user.setNickName(user.getUserName());
					user.setCreateBy(operName);
					//System.out.println("时间2：" + DateFormatUtils.format(new Date(),"HH:mm:ss"));

					//this.insertImportUser(user);
					//System.out.println("时间3：" + DateFormatUtils.format(new Date(),"HH:mm:ss"));
					//导入用户需更新机构号
					//userMapper.updateSynchUserOrganInfo(user.getUserId());
					nowBatchUserNames.add(user.getUserName());
					successList.add(user);
					successNum++;
					successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 导入成功");
				} else if (isUpdateSupport) {
					BeanValidators.validateWithException(validator, user);
					user.setUpdateBy(operName);
					this.updateUser(user);
					successNum++;
					successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 更新成功");
				} else {
					failureNum++;
					failureMsg.append("<br/>" + failureNum + "、账号 " + user.getUserName() + " 已存在");
				}
			} catch (Exception e) {
				failureNum++;
				String msg = "<br/>" + failureNum + "、账号 " + user.getUserName() + " 导入失败：";
				failureMsg.append(msg + e.getMessage());
				log.error(msg, e);
			}
		}
		if (failureNum > 0) {
			failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
			throw new ServiceException(failureMsg.toString());
		} else {
			//一个错没有 全部导入，有一个错，就不导入

			this.insertImportUserList(successList);
			successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
		}

 /*       if(!CollectionUtils.isEmpty(successList)){
            // 执行一次同步用户机构的操作
			userMapper.updateBatchSynchUserOrganInfo(successList);
            //this.updateSynchUserOrganInfo(successList);
        }*/
		return successMsg.toString();
	}





    /******
     * 用户导入后，由于有机构号信息，需要做一次同步机构信息的操作，将机构id设置给用户信息
     */
    public void updateSynchUserOrganInfo(List<SysUser> successList){
        for(SysUser user : successList){
            if(user != null && user.getUserId() != null){
                System.out.println("更新userId=" + user.getUserId());
                userMapper.updateSynchUserOrganInfo(user.getUserId());

            }

        }
    }
}
