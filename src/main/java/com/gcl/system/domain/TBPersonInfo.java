package com.gcl.system.domain;

import com.gcl.common.annotation.Excel;
import com.gcl.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 【请填写功能名称】对象 t_b_person_info
 * 
 * @author ruoyi
 * @date 2022-01-05
 */
public class TBPersonInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String id;

    /** 邮箱地址 */
    @Excel(name = "邮箱地址")
    private String contactEmail;

    /** 手机国际区号 */
    @Excel(name = "手机国际区号")
    private Long mobileInternationalAreaCode;

    /** 手机号城市区号 */
    @Excel(name = "手机号城市区号")
    private String mobileNumberCityAreaCode;

    /** 手机号 */
    @Excel(name = "手机号")
    private String mobileNumber;

    /** 姓名 */
    @Excel(name = "姓名")
    private String custName;

    /** 证件类型 */
    @Excel(name = "证件类型")
    private String idCardType;

    /** 证件号 */
    @Excel(name = "证件号")
    private String idCard;

    /** 用户所属国家编码 */
    @Excel(name = "用户所属国家编码")
    private String countryOfUser;

    /** 用户所属省编码 */
    @Excel(name = "用户所属省编码")
    private String provinceCode;

    /** 用户所属地区编码 */
    @Excel(name = "用户所属地区编码")
    private String provinceOfUser;

    /** 钱包昵称 */
    @Excel(name = "钱包昵称")
    private String nickName;

    /** 预留信息 */
    @Excel(name = "预留信息")
    private String reservedInfo;

    /** 客户类型 */
    @Excel(name = "客户类型")
    private String clientType;

    /** 联系方式类型 */
    @Excel(name = "联系方式类型")
    private String contactwayType;

    /** 变更类型 */
    @Excel(name = "变更类型")
    private String alterationType;

    /** 用户标签 */
    @Excel(name = "用户标签")
    private String userLabel;

    /** 系统类型 */
    @Excel(name = "系统类型")
    private String systemType;

    /** 核心机构号(机构号) */
    @Excel(name = "核心机构号(机构号)")
    private String orgId;

    /** 员工号 */
    @Excel(name = "员工号")
    private String staffId;

    /** 员工姓名(创建人) */
    @Excel(name = "员工姓名(创建人)")
    private String staffName;

    /** 创建人 */
    @Excel(name = "创建人")
    private String createdBycreatedBy;

    /** 创建时间 */
    @Excel(name = "创建时间")
    private String createdDate;

    /** 更新人 */
    @Excel(name = "更新人")
    private String lastUpdatedBy;

    /** 最后更新时间 */
    @Excel(name = "最后更新时间")
    private String lastUpdatedDate;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setContactEmail(String contactEmail) 
    {
        this.contactEmail = contactEmail;
    }

    public String getContactEmail() 
    {
        return contactEmail;
    }
    public void setMobileInternationalAreaCode(Long mobileInternationalAreaCode) 
    {
        this.mobileInternationalAreaCode = mobileInternationalAreaCode;
    }

    public Long getMobileInternationalAreaCode() 
    {
        return mobileInternationalAreaCode;
    }
    public void setMobileNumberCityAreaCode(String mobileNumberCityAreaCode) 
    {
        this.mobileNumberCityAreaCode = mobileNumberCityAreaCode;
    }

    public String getMobileNumberCityAreaCode() 
    {
        return mobileNumberCityAreaCode;
    }
    public void setMobileNumber(String mobileNumber) 
    {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber() 
    {
        return mobileNumber;
    }
    public void setCustName(String custName) 
    {
        this.custName = custName;
    }

    public String getCustName() 
    {
        return custName;
    }
    public void setIdCardType(String idCardType) 
    {
        this.idCardType = idCardType;
    }

    public String getIdCardType() 
    {
        return idCardType;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setCountryOfUser(String countryOfUser) 
    {
        this.countryOfUser = countryOfUser;
    }

    public String getCountryOfUser() 
    {
        return countryOfUser;
    }
    public void setProvinceCode(String provinceCode) 
    {
        this.provinceCode = provinceCode;
    }

    public String getProvinceCode() 
    {
        return provinceCode;
    }
    public void setProvinceOfUser(String provinceOfUser) 
    {
        this.provinceOfUser = provinceOfUser;
    }

    public String getProvinceOfUser() 
    {
        return provinceOfUser;
    }
    public void setNickName(String nickName) 
    {
        this.nickName = nickName;
    }

    public String getNickName() 
    {
        return nickName;
    }
    public void setReservedInfo(String reservedInfo) 
    {
        this.reservedInfo = reservedInfo;
    }

    public String getReservedInfo() 
    {
        return reservedInfo;
    }
    public void setClientType(String clientType) 
    {
        this.clientType = clientType;
    }

    public String getClientType() 
    {
        return clientType;
    }
    public void setContactwayType(String contactwayType) 
    {
        this.contactwayType = contactwayType;
    }

    public String getContactwayType() 
    {
        return contactwayType;
    }
    public void setAlterationType(String alterationType) 
    {
        this.alterationType = alterationType;
    }

    public String getAlterationType() 
    {
        return alterationType;
    }
    public void setUserLabel(String userLabel) 
    {
        this.userLabel = userLabel;
    }

    public String getUserLabel() 
    {
        return userLabel;
    }
    public void setSystemType(String systemType) 
    {
        this.systemType = systemType;
    }

    public String getSystemType() 
    {
        return systemType;
    }
    public void setOrgId(String orgId) 
    {
        this.orgId = orgId;
    }

    public String getOrgId() 
    {
        return orgId;
    }
    public void setStaffId(String staffId) 
    {
        this.staffId = staffId;
    }

    public String getStaffId() 
    {
        return staffId;
    }
    public void setStaffName(String staffName) 
    {
        this.staffName = staffName;
    }

    public String getStaffName() 
    {
        return staffName;
    }
    public void setCreatedBycreatedBy(String createdBycreatedBy) 
    {
        this.createdBycreatedBy = createdBycreatedBy;
    }

    public String getCreatedBycreatedBy() 
    {
        return createdBycreatedBy;
    }
    public void setCreatedDate(String createdDate) 
    {
        this.createdDate = createdDate;
    }

    public String getCreatedDate() 
    {
        return createdDate;
    }
    public void setLastUpdatedBy(String lastUpdatedBy) 
    {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public String getLastUpdatedBy() 
    {
        return lastUpdatedBy;
    }
    public void setLastUpdatedDate(String lastUpdatedDate) 
    {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getLastUpdatedDate() 
    {
        return lastUpdatedDate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("contactEmail", getContactEmail())
            .append("mobileInternationalAreaCode", getMobileInternationalAreaCode())
            .append("mobileNumberCityAreaCode", getMobileNumberCityAreaCode())
            .append("mobileNumber", getMobileNumber())
            .append("custName", getCustName())
            .append("idCardType", getIdCardType())
            .append("idCard", getIdCard())
            .append("countryOfUser", getCountryOfUser())
            .append("provinceCode", getProvinceCode())
            .append("provinceOfUser", getProvinceOfUser())
            .append("nickName", getNickName())
            .append("reservedInfo", getReservedInfo())
            .append("clientType", getClientType())
            .append("contactwayType", getContactwayType())
            .append("alterationType", getAlterationType())
            .append("userLabel", getUserLabel())
            .append("systemType", getSystemType())
            .append("orgId", getOrgId())
            .append("staffId", getStaffId())
            .append("staffName", getStaffName())
            .append("createdBycreatedBy", getCreatedBycreatedBy())
            .append("createdDate", getCreatedDate())
            .append("lastUpdatedBy", getLastUpdatedBy())
            .append("lastUpdatedDate", getLastUpdatedDate())
            .toString();
    }
}
