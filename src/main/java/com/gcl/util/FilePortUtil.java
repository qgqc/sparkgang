package com.gcl.util;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.poi.hssf.usermodel.*;

import javax.servlet.http.HttpServletResponse;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @author 吴昊天
 * @Description 导出表格工具类
 * 基本思路：创建表格对象--->将数据set进表格--->将表格流写入response返回
 * @date 2020/5/15 11:06
 */
public class FilePortUtil {

    /**
     * 方法描述：导出功能
     * 注意：泛型T类字段名和containBean集合里字段名字的一致性
     *
     * @param response
     * @param title       表名
     * @param headers     表头
     * @param list        数据集
     * @param containBean 数据集类型字段
     * @param <T>
     * @return
     * @throws Exception
     * @author Harry
     * @date 2020/5/15  11:13
     **/
    public static <T> void exportExcel(HttpServletResponse response, String title, String[] headers, List<T> list, List<String> containBean) throws Exception {
        HSSFWorkbook workbook = null;
        try {
            Map<Integer, Integer> maxWidth = new HashMap<Integer, Integer>();
            workbook = new HSSFWorkbook();
            HSSFSheet sheet = workbook.createSheet(title);
            HSSFRow row = sheet.createRow(0);
            //创建第一行表头
            for (int i = 0; i < headers.length; i++) {
                HSSFCell cell = row.createCell(i);
                HSSFRichTextString textString = new HSSFRichTextString(headers[(short)i]);
                cell.setCellValue(textString);
                int length = cell.getStringCellValue().getBytes().length * 256 + 200;
                if (length > 15000) {
                    length = 15000;
                }
                maxWidth.put(i, length);
            }
            Iterator<T> it = list.iterator();
            int index = 0;
            while (it.hasNext()) {
                index++;
                row = sheet.createRow(index);
                T t = (T) it.next();
                //通过反射得到字段
                Field[] fields = t.getClass().getDeclaredFields();
                //如果需要匹配
                if (CollectionUtils.isNotEmpty(containBean)) {
                    for (int j = 0; j < containBean.size(); j++) {
                        for (int i = 0; i < fields.length; i++) {
                            Field field = fields[i];
                            if (!field.getName().equals(containBean.get(j)))
                                continue;
                            setCellValue(t, field, row, j, maxWidth);
                        }
                    }
                } else {
                    for (int i = 0; i < fields.length; i++) {
                        Field field = fields[i];
                        setCellValue(t, field, row, i, maxWidth);
                    }
                }
            }
            for (int i = 0; i < headers.length; i++) {
                sheet.setColumnWidth(i,maxWidth.get(i));
            }
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.addHeader("Content-Disposition", "attachment;filename=" + new String((title).getBytes(), "ISO8859-1") + ".xls");
            workbook.write(response.getOutputStream());
        } finally {
            if (workbook != null) {
                workbook.close();
            }
        }
    }

    /**
     * 方法描述：设置每一行中的列
     *
     * @param t
     * @param field
     * @param row
     * @param index
     * @return T
     * @author Harry
     * @date 2020/5/15  11:44
     **/
    private static <T> void setCellValue(T t, Field field, HSSFRow row, int index, Map<Integer, Integer> maxWidth) {
        HSSFCell cell = row.createCell(index);
        Object value = invoke(t, field);
        String textValue = null;
        if (value != null) {
            if (value instanceof Date) {
                Date date = (Date) value;
                textValue = DateFormatUtils.format(date, "yyyy-MM-dd HH:mm:ss");
            } else {
                textValue = value.toString();
            }
        }
        if (textValue != null) {
            cell.setCellValue(textValue);
        }
        int length = cell.getStringCellValue().getBytes().length  * 256 + 200;
        //这里把宽度最大限制到15000
        if (length > 15000) {
            length = 15000;
        }
        maxWidth.put(index, Math.max(length, maxWidth.get(index)));
    }

    /**
     * 方法描述：通过反射获取数据集字段
     *
     * @param t     泛型
     * @param field
     * @return T
     * @author Harry
     * @date 2020/5/15  11:42
     **/
    private static <T> Object invoke(T t, Field field) {
        try {
            String fieldName = field.getName();
            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, t.getClass());
            Method readMethod = propertyDescriptor.getReadMethod();
            return readMethod.invoke(t);
        } catch (Exception e) {
            return null;
        }
    }
}