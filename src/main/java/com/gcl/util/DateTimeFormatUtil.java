package com.gcl.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

/**
 * 日期格式化工具类
 *
 * @author quhan
 */
public class DateTimeFormatUtil {


    private static final DateTimeFormatter YYYY_MM_DD_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMdd").withLocale(Locale.CHINA);
    private static final DateTimeFormatter YYYY = DateTimeFormatter.ofPattern("yyyy").withLocale(Locale.CHINA);
    private static final DateTimeFormatter YYYY_MM_DD_HH_MM_SS_DATE_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyyMMddHHmmss").withLocale(Locale.CHINA);
    private static final DateTimeFormatter HH_MM_SS_TIME_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("HHmmss").withLocale(Locale.CHINA);
    private static final DateTimeFormatter MM_DD_DATE_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("MMdd").withLocale(Locale.CHINA);
    private static final DateTimeFormatter YY_MM_DD_DATE_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyMMdd").withLocale(Locale.CHINA);

    /**
     * 获取8583报文需要的日期字符串，格式为yyyyMMdd
     *
     * @return 日期字符串
     */
    public static String getYYYYMMDDDate() {
        return YYYY_MM_DD_DATE_TIME_FORMATTER.format(LocalDate.now());
    }

    /**
     * 获取日期字符串，只有年份
     *
     * @return 日期字符串
     */
    public static String getYYYYDate() {
        return YYYY.format(LocalDate.now());
    }

    /**
     * 获取8583报文需要的日期字符串，格式为yyyyMMdd
     *
     * @return 日期字符串
     */
    public static String getMMDDDate() {
        return MM_DD_DATE_DATE_TIME_FORMATTER.format(LocalDate.now());
    }

    /**
     * 获取8583报文需要的时间字符串，格式为HHmmss
     *
     * @return 时间字符串
     */
    public static String getHHMMSSTime() {
        return HH_MM_SS_TIME_DATE_TIME_FORMATTER.format(LocalTime.now());
    }

    /**
     * 获取8583报文需要的时间字符串，格式为HHmmss
     *
     * @return 时间字符串
     */
    public static String getYYYYMMDDHHMMSSDate() {
        return YYYY_MM_DD_HH_MM_SS_DATE_DATE_TIME_FORMATTER.format(LocalDateTime.now());
    }

    public static String getYYYYMMDDHHMMSSDate(Date date) {
        SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyyMMddHHmmss");
        return yyyyMMddHHmmss.format(date);
    }

    /**
     * 获取8583报文需要的时间字符串，格式为HHmmss
     *
     * @return 时间字符串
     */
    public static String getYYMMDDDate() {
        return YY_MM_DD_DATE_DATE_TIME_FORMATTER.format(LocalDate.now());
    }


    /**
     * 获取当天0点时间
     *
     * @return 当天0点时间戳
     */
    public static long getToday0Clock() {
        long currentTimestamps = System.currentTimeMillis();
        long oneDayTimestamps = (long) (60 * 60 * 24 * 1000);
        return currentTimestamps - (currentTimestamps + 60 * 60 * 8 * 1000) % oneDayTimestamps;
    }

    /**
     * 获取指定几天以后0点的时间
     *
     * @param days 几天
     * @return 指定天数的时间戳
     */
    public static long getDaysLaterTime(long days) {
        long today0Clock = getToday0Clock();
        return today0Clock + (60 * 60 * 24 * 1000 * days);
    }

    public static Date getYYYYMMDDHHSSMMDate(String date)  {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
