package com.gcl.personalwalletmanagement.domain;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcl.common.annotation.Excel;
import com.gcl.common.core.domain.BaseEntity;

/**
 * 对私批量注册白名单文件头对象 bps_bwls_head
 * 
 * @author yada
 * @date 2022-02-18
 */
public class BpsBwlsHead extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String id;

    /** 记录标识 */
    @Excel(name = "记录标识")
    private String recordId;

    /** 顺序号 */
    @Excel(name = "顺序号")
    private Long sequenceNumber;

    /** 源文件名称 */
    @Excel(name = "源文件名称")
    private String sourceFileName;

    /** 交易机构号 */
    @Excel(name = "交易机构号")
	private String tradingOrganizationNo;

    /** 明细合计笔数 */
    @Excel(name = "明细合计笔数")
	private Integer detailTotal;

    /** 渠道号 */
    @Excel(name = "渠道号")
    private String channelNo;

    /** 交易柜员号 */
    @Excel(name = "交易柜员号")
	private String transactionTellerNumber;

    /** 交易终端号 */
    @Excel(name = "交易终端号")
	private String tradingTerminalNumber;

    /** 数据批次号 */
    @Excel(name = "数据批次号")
	private Integer dataBatchNumber;

    /** 文件处理类型 */
    @Excel(name = "文件处理类型")
    private String fileHandlingType;

    /** 明细成功笔数 */
    @Excel(name = "明细成功笔数")
	private Integer detailSuccessCount;

    /** 明细失败笔数 */
    @Excel(name = "明细失败笔数")
	private Integer detailFailedCount;

    /** 明细未明笔数 */
    @Excel(name = "明细未明笔数")
	private Integer detailUnknownCount;

    /** 文件返回信息码 */
    @Excel(name = "文件返回信息码")
    private String fileReturnCode;

    /** 文件返回信息 */
    @Excel(name = "文件返回信息")
    private String fileReturnMsg;

    /** 冗余项 */
    @Excel(name = "冗余项")
    private String redundantInfo;

    /** 执行步骤 */
    @Excel(name = "执行步骤")
    private String executeSteps;

    /** 执行结果 */
    @Excel(name = "执行结果")
    private String executeResult;

    /** 文件创建时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Excel(name = "文件创建时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date fileCreationTime;

    /** 文件上传FTP时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Excel(name = "文件上传FTP时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date fileUploadFtpTime;

    /** 文件下载FTP时间
 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Excel(name = "文件下载FTP时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date fileDownloadFtpTime;

    /** 文件更新回盘时间
 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@Excel
	(name = "文件更新回盘时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date fileUpdateReturnTime;

    /** 文件批次号 */
    @Excel(name = "文件批次号")
    private String fileBatchNo;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setRecordId(String recordId) 
    {
        this.recordId = recordId;
    }

    public String getRecordId() 
    {
        return recordId;
    }
    public void setSequenceNumber(Long sequenceNumber) 
    {
        this.sequenceNumber = sequenceNumber;
    }

    public Long getSequenceNumber() 
    {
        return sequenceNumber;
    }
    public void setSourceFileName(String sourceFileName) 
    {
        this.sourceFileName = sourceFileName;
    }

    public String getSourceFileName() 
    {
        return sourceFileName;
    }



	public void setDetailTotal(Integer detailTotal)
    {
        this.detailTotal = detailTotal;
    }

	public Integer getDetailTotal()
    {
        return detailTotal;
    }
    public void setChannelNo(String channelNo) 
    {
        this.channelNo = channelNo;
    }

    public String getChannelNo() 
    {
        return channelNo;
    }


    public String getTradingOrganizationNo() {
        return tradingOrganizationNo;
    }

    public void setTradingOrganizationNo(String tradingOrganizationNo) {
        this.tradingOrganizationNo = tradingOrganizationNo;
    }

    public String getTransactionTellerNumber() {
        return transactionTellerNumber;
    }

    public void setTransactionTellerNumber(String transactionTellerNumber) {
        this.transactionTellerNumber = transactionTellerNumber;
    }

    public String getTradingTerminalNumber() {
        return tradingTerminalNumber;
    }

    public void setTradingTerminalNumber(String tradingTerminalNumber) {
        this.tradingTerminalNumber = tradingTerminalNumber;
    }

    public void setDataBatchNumber(Integer dataBatchNumber)
    {
        this.dataBatchNumber = dataBatchNumber;
    }

	public Integer getDataBatchNumber()
    {
        return dataBatchNumber;
    }
    public void setFileHandlingType(String fileHandlingType) 
    {
        this.fileHandlingType = fileHandlingType;
    }

    public String getFileHandlingType() 
    {
        return fileHandlingType;
    }

	public void setDetailSuccessCount(Integer detailSuccessCount)
    {
        this.detailSuccessCount = detailSuccessCount;
    }

	public Integer getDetailSuccessCount()
    {
        return detailSuccessCount;
    }

	public void setDetailFailedCount(Integer detailFailedCount)
    {
        this.detailFailedCount = detailFailedCount;
    }

	public Integer getDetailFailedCount()
    {
        return detailFailedCount;
    }

	public void setDetailUnknownCount(Integer detailUnknownCount)
    {
        this.detailUnknownCount = detailUnknownCount;
    }

	public Integer getDetailUnknownCount() 
    {
        return detailUnknownCount;
    }
    public void setFileReturnCode(String fileReturnCode) 
    {
        this.fileReturnCode = fileReturnCode;
    }

    public String getFileReturnCode() 
    {
        return fileReturnCode;
    }
    public void setFileReturnMsg(String fileReturnMsg) 
    {
        this.fileReturnMsg = fileReturnMsg;
    }

    public String getFileReturnMsg() 
    {
        return fileReturnMsg;
    }
    public void setRedundantInfo(String redundantInfo) 
    {
        this.redundantInfo = redundantInfo;
    }

    public String getRedundantInfo() 
    {
        return redundantInfo;
    }
    public void setExecuteSteps(String executeSteps) 
    {
        this.executeSteps = executeSteps;
    }

    public String getExecuteSteps() 
    {
        return executeSteps;
    }
    public void setExecuteResult(String executeResult) 
    {
        this.executeResult = executeResult;
    }

    public String getExecuteResult() 
    {
        return executeResult;
    }
    public void setFileCreationTime(Date fileCreationTime) 
    {
        this.fileCreationTime = fileCreationTime;
    }

    public Date getFileCreationTime() 
    {
        return fileCreationTime;
    }
    public void setFileUploadFtpTime(Date fileUploadFtpTime) 
    {
        this.fileUploadFtpTime = fileUploadFtpTime;
    }

    public Date getFileUploadFtpTime() 
    {
        return fileUploadFtpTime;
    }
    public void setFileDownloadFtpTime(Date fileDownloadFtpTime) 
    {
        this.fileDownloadFtpTime = fileDownloadFtpTime;
    }

    public Date getFileDownloadFtpTime() 
    {
        return fileDownloadFtpTime;
    }
    public void setFileUpdateReturnTime(Date fileUpdateReturnTime) 
    {
        this.fileUpdateReturnTime = fileUpdateReturnTime;
    }

    public Date getFileUpdateReturnTime() 
    {
        return fileUpdateReturnTime;
    }
    public void setFileBatchNo(String fileBatchNo) 
    {
        this.fileBatchNo = fileBatchNo;
    }

    public String getFileBatchNo() 
    {
        return fileBatchNo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("recordId", getRecordId())
            .append("sequenceNumber", getSequenceNumber())
            .append("sourceFileName", getSourceFileName())
            .append("tradingOrganizationNo", getTradingOrganizationNo())
            .append("detailTotal", getDetailTotal())
            .append("channelNo", getChannelNo())
            .append("transactionTellerNumber", getTransactionTellerNumber())
            .append("tradingTerminalNumber", getTradingTerminalNumber())
            .append("dataBatchNumber", getDataBatchNumber())
            .append("fileHandlingType", getFileHandlingType())
            .append("detailSuccessCount", getDetailSuccessCount())
            .append("detailFailedCount", getDetailFailedCount())
            .append("detailUnknownCount", getDetailUnknownCount())
            .append("fileReturnCode", getFileReturnCode())
            .append("fileReturnMsg", getFileReturnMsg())
            .append("remark", getRemark())
            .append("redundantInfo", getRedundantInfo())
            .append("executeSteps", getExecuteSteps())
            .append("executeResult", getExecuteResult())
            .append("fileCreationTime", getFileCreationTime())
            .append("fileUploadFtpTime", getFileUploadFtpTime())
            .append("fileDownloadFtpTime", getFileDownloadFtpTime())
            .append("fileUpdateReturnTime", getFileUpdateReturnTime())
            .append("updateTime", getUpdateTime())
            .append("fileBatchNo", getFileBatchNo())
            .toString();
    }
}
