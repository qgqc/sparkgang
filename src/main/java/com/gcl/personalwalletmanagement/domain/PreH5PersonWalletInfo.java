package com.gcl.personalwalletmanagement.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.gcl.common.annotation.Excel;
import com.gcl.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * H5个人信息收集报表对象 pre_h5_person_wallet_info
 * 
 * @author yada
 * @date 2022-02-25
 */
public class PreH5PersonWalletInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 客户姓名 */
    @Excel(name = "客户姓名")
    private String custName;

    /** 证件号码 */
    @Excel(name = "证件号码")
    private String idCard;

    /** 证件类型 */
    @Excel(name = "证件类型")
    private String idCardType;

    /** 客户手机 */
    @Excel(name = "客户手机")
    private String mobileNumber;

    /** 核心机构号 */
    @Excel(name = "核心机构号")
    private String orgId;

    /** 员工号 */
    @Excel(name = "员工号")
    private String staffId;

    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "提交时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date createdDate;


    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setCustName(String custName) 
    {
        this.custName = custName;
    }

    public String getCustName() 
    {
        return custName;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setIdCardType(String idCardType) 
    {
        this.idCardType = idCardType;
    }

    public String getIdCardType() 
    {
        return idCardType;
    }
    public void setMobileNumber(String mobileNumber) 
    {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber() 
    {
        return mobileNumber;
    }
    public void setOrgId(String orgId) 
    {
        this.orgId = orgId;
    }

    public String getOrgId() 
    {
        return orgId;
    }
    public void setStaffId(String staffId) 
    {
        this.staffId = staffId;
    }

    public String getStaffId() 
    {
        return staffId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("custName", getCustName())
            .append("idCard", getIdCard())
            .append("idCardType", getIdCardType())
            .append("mobileNumber", getMobileNumber())
            .append("orgId", getOrgId())
            .append("staffId", getStaffId())
            .toString();
    }
}
