package com.gcl.personalwalletmanagement.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.gcl.common.annotation.Excel;
import com.gcl.common.core.domain.BaseEntity;

/**
 * 个人信息导入对象 bus_person_info
 * 
 * @author yada
 * @date 2022-02-18
 */
public class BusPersonInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String id;

    /** 邮箱地址 */
    private String contactEmail;

    /** 手机国际区号 */
    private String mobileInternationalAreaCode;

    /** 手机号城市区号 */
    private String mobileNumberCityAreaCode;

    /** 手机号 */
    @Excel(name = "手机号")
    private String mobileNumber;

    /** 用户姓名 */
    @Excel(name = "用户姓名")
    private String custName;

    /** 证件类型 */
	@Excel(name = "证件类型")
    private String idCardType;

    /** 证件号 */
    @Excel(name = "证件号")
    private String idCard;

    /** 用户所属国家编码 */
    private String countryOfUser;

    /** 用户所属省编码 */
	@Excel(name = "用户所属省编码")
    private String provinceCode;

    /** 用户所属地区编码 */
	@Excel(name = "用户所属地区编码")
    private String provinceOfUser;

    /** 钱包昵称 */
    private String nickName;

    /** 预留信息 */
    @Excel(name = "预留信息")
    private String reservedInfo;

    /** 客户类型 */
    private String clientType;

    /** 联系方式类型 */
    private String contactwayType;

    /** 变更类型 */
    private String alterationType;

    /** 用户标签 */
    private String userLabel;

    /** 系统类型 */
	// @Excel(name = "系统类型")
    private String systemType;

    /** 数据状态 */
    private String dataStatus;

    /** 核心机构号(机构号) */
    @Excel(name = "核心机构号(机构号)")
    private String orgId;

    /** 员工号 */
    @Excel(name = "员工号")
    private String staffId;

    /** 员工姓名(创建人) */
    @Excel(name = "员工姓名(创建人)")
    private String staffName;

	/**
	 * 交易柜员号
	 */
	@Excel(name = "交易柜员号")
    private String transactionTellerNumber;

	/**
	 * 交易终端号
	 */
	@Excel(name = "交易终端号")
	private String tradingTerminalNumber;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setContactEmail(String contactEmail) 
    {
        this.contactEmail = contactEmail;
    }

    public String getContactEmail() 
    {
        return contactEmail;
    }
    public void setMobileInternationalAreaCode(String mobileInternationalAreaCode) 
    {
        this.mobileInternationalAreaCode = mobileInternationalAreaCode;
    }

    public String getMobileInternationalAreaCode() 
    {
        return mobileInternationalAreaCode;
    }
    public void setMobileNumberCityAreaCode(String mobileNumberCityAreaCode) 
    {
        this.mobileNumberCityAreaCode = mobileNumberCityAreaCode;
    }

    public String getMobileNumberCityAreaCode() 
    {
        return mobileNumberCityAreaCode;
    }
    public void setMobileNumber(String mobileNumber) 
    {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber() 
    {
        return mobileNumber;
    }
    public void setCustName(String custName) 
    {
        this.custName = custName;
    }

    public String getCustName() 
    {
        return custName;
    }
    public void setIdCardType(String idCardType) 
    {
        this.idCardType = idCardType;
    }

    public String getIdCardType() 
    {
        return idCardType;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setCountryOfUser(String countryOfUser) 
    {
        this.countryOfUser = countryOfUser;
    }

    public String getCountryOfUser() 
    {
        return countryOfUser;
    }
    public void setProvinceCode(String provinceCode) 
    {
        this.provinceCode = provinceCode;
    }

    public String getProvinceCode() 
    {
        return provinceCode;
    }
    public void setProvinceOfUser(String provinceOfUser) 
    {
        this.provinceOfUser = provinceOfUser;
    }

    public String getProvinceOfUser() 
    {
        return provinceOfUser;
    }
    public void setNickName(String nickName) 
    {
        this.nickName = nickName;
    }

    public String getNickName() 
    {
        return nickName;
    }
    public void setReservedInfo(String reservedInfo) 
    {
        this.reservedInfo = reservedInfo;
    }

    public String getReservedInfo() 
    {
        return reservedInfo;
    }
    public void setClientType(String clientType) 
    {
        this.clientType = clientType;
    }

    public String getClientType() 
    {
        return clientType;
    }
    public void setContactwayType(String contactwayType) 
    {
        this.contactwayType = contactwayType;
    }

    public String getContactwayType() 
    {
        return contactwayType;
    }
    public void setAlterationType(String alterationType) 
    {
        this.alterationType = alterationType;
    }

    public String getAlterationType() 
    {
        return alterationType;
    }
    public void setUserLabel(String userLabel) 
    {
        this.userLabel = userLabel;
    }

    public String getUserLabel() 
    {
        return userLabel;
    }
    public void setSystemType(String systemType) 
    {
        this.systemType = systemType;
    }

    public String getSystemType() 
    {
        return systemType;
    }
    public void setDataStatus(String dataStatus) 
    {
        this.dataStatus = dataStatus;
    }

    public String getDataStatus() 
    {
        return dataStatus;
    }
    public void setOrgId(String orgId) 
    {
        this.orgId = orgId;
    }

    public String getOrgId() 
    {
        return orgId;
    }
    public void setStaffId(String staffId) 
    {
        this.staffId = staffId;
    }

    public String getStaffId() 
    {
        return staffId;
    }
    public void setStaffName(String staffName) 
    {
        this.staffName = staffName;
    }

    public String getStaffName() 
    {
        return staffName;
    }

    public String getTransactionTellerNumber() {
        return transactionTellerNumber;
    }

    public void setTransactionTellerNumber(String transactionTellerNumber) {
        this.transactionTellerNumber = transactionTellerNumber;
    }

    public String getTradingTerminalNumber() {
        return tradingTerminalNumber;
    }

    public void setTradingTerminalNumber(String tradingTerminalNumber) {
        this.tradingTerminalNumber = tradingTerminalNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("contactEmail", getContactEmail())
            .append("mobileInternationalAreaCode", getMobileInternationalAreaCode())
            .append("mobileNumberCityAreaCode", getMobileNumberCityAreaCode())
            .append("mobileNumber", getMobileNumber())
            .append("custName", getCustName())
            .append("idCardType", getIdCardType())
            .append("idCard", getIdCard())
            .append("countryOfUser", getCountryOfUser())
            .append("provinceCode", getProvinceCode())
            .append("provinceOfUser", getProvinceOfUser())
            .append("nickName", getNickName())
            .append("reservedInfo", getReservedInfo())
            .append("clientType", getClientType())
            .append("contactwayType", getContactwayType())
            .append("alterationType", getAlterationType())
            .append("userLabel", getUserLabel())
            .append("systemType", getSystemType())
            .append("dataStatus", getDataStatus())
            .append("orgId", getOrgId())
            .append("staffId", getStaffId())
            .append("staffName", getStaffName())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
				.append("transactionTellerNumber", getTransactionTellerNumber())
				.append("tradingTerminalNumber", getTradingTerminalNumber())
            .toString();
    }
}
