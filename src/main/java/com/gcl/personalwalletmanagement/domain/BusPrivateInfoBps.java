package com.gcl.personalwalletmanagement.domain;

import com.gcl.bps.domain.Base;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.gcl.common.annotation.Excel;

import java.beans.Transient;
import java.io.UnsupportedEncodingException;
import java.sql.Timestamp;

/**
 * 个人钱包对象 bus_private_info_bps
 * 原T_B_PRIVATE_WALLET_INFO表
 * 
 * @author wjw
 * @date 2022-02-10
 */
public class BusPrivateInfoBps extends Base
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 顺序号，从2开始；十二位数字 */
    @Excel(name = "顺序号，从2开始；十二位数字")
    private Long sequenceNumber;

    /** 交易机构号，固定:00012 */
    @Excel(name = "交易机构号，固定:00012")
    private String tradingOrganizationNo;

    /** 交易码 */
    @Excel(name = "交易码")
    private String transactionCode;

    /** 手机国际区号 */
    @Excel(name = "手机国际区号")
    private String mobileInternationalAreaCode;

    /** 手机号城市区号 */
    @Excel(name = "手机号城市区号")
    private String mobileNumberCityAreaCode;

    /** 手机号 */
    @Excel(name = "手机号")
    private String mobileNumber;

    /** 用户所属国家编码 */
    @Excel(name = "用户所属国家编码")
    private String countryOfUser;

    /** 用户所属省编码 */
    @Excel(name = "用户所属省编码")
    private String provinceOfUser;

    /** 钱包昵称 */
    @Excel(name = "钱包昵称")
    private String nickName;

    /** 用户姓名 */
    @Excel(name = "用户姓名")
    private String userName;

    /** 身份证件类型 */
    @Excel(name = "身份证件类型")
    private String idCardType;

    /** 身份证件号码 */
    @Excel(name = "身份证件号码")
    private String idCard;

    /** 对私钱包开立交易返回信息码 */
    @Excel(name = "对私钱包开立交易返回信息码")
    private String returnCode;

    /** 对私钱包开立交易返回信息 */
    @Excel(name = "对私钱包开立交易返回信息")
    private String returnMsg;

    /** 钱包id */
    @Excel(name = "钱包id")
    private String walletId;

    /** 审核成功日期 */
    @Excel(name = "审核成功日期")
    private String examineSuccessDate;

    /** 数据状态，1：存量信息导入"2：新增信息导入"3：申请表已打印"4：资料已上传"5：提交复核"6：复核通过"7：复核退回"8：生成BPS文件"9：上传BPS文件成功"10：下载BPS回盘成功"11：更新ABC处理结果 */
    @Excel(name = "数据状态，1：存量信息导入 2：新增信息导入 3：申请表已打印 4：资料已上传 5：提交复核 6：复核通过 7：复核退回 8：生成BPS文件 9：上传BPS文件成功 10：下载BPS回盘成功 11：更新ABC处理结果")
    private String dataStatus;

    /** 幂等流水（扩位）。选输项， */
    @Excel(name = "幂等流水", readConverterExp = "扩=位")
    private String miDengLiuShui;

    /** 头部id */
    @Excel(name = "头部id")
    private String headId;

    /** 备注信息 */
    @Excel(name = "备注信息")
    private String remarkInfo;

    /** 冗余域 */
    @Excel(name = "冗余域")
    private String redundantInfo;

    /** 柜员号 */
    @Excel(name = "柜员号")
    private String tellerNo;

    /** bps生成文件批次号 */
    @Excel(name = "bps生成文件批次号")
    private String fileBatchNo;

    /** 商户id */
    @Excel(name = "商户id")
    private String merchantId;

    /** 商户全称 */
    @Excel(name = "商户全称")
    private String merchantFullName;

    /** 商户类型 */
    @Excel(name = "商户类型")
    private String merchantType;

    /** 商户简称 */
    @Excel(name = "商户简称")
    private String merchantShortlName;

    /** 所属代理机构编码 */
    @Excel(name = "所属代理机构编码")
    private String agencyCode;

    /** 外部商户号，代收币一级商户必输，二级商户选输，收币商户不输 */
    @Excel(name = "外部商户号，代收币一级商户必输，二级商户选输，收币商户不输")
    private String externalMerchantNo;

    /** 外部商户名称，代收币一级商户必输，二级商户选输，收币商户不输 */
    @Excel(name = "外部商户名称，代收币一级商户必输，二级商户选输，收币商户不输")
    private String externalMerchantName;

    /** 商户等级，必输项 */
    @Excel(name = "商户等级，必输项")
    private String merchantLevel;

    /** 上级商户号，当选择二级商户时，上级商户号必填 */
    @Excel(name = "上级商户号，当选择二级商户时，上级商户号必填")
    private String upperLevelMerchantNo;

    /** 人行商户号 */
    @Excel(name = "人行商户号")
    private String pbocMerchantNo;

    /** 商户属性/标识，商户属性/标识不能为空。0-中行内部 1-电信内部 2-联通内部 3-系统内部（如：党组织） 4-外部，固定填写0 */
    @Excel(name = "商户属性/标识，商户属性/标识不能为空。0-中行内部 1-电信内部 2-联通内部 3-系统内部", readConverterExp = "如=：党组织")
    private String merchantAttributes;

    /** MCC编码，参见现有银联MCC规则 */
    @Excel(name = "MCC编码，参见现有银联MCC规则")
    private String mccCode;

    /** 商户结算标志 */
    @Excel(name = "商户结算标志")
    private String merchantSettlementSign;

    /** 客户经理 */
    @Excel(name = "客户经理")
    private String accountManager;

    /** 省编码，省编码不能为空，长度6位数字 */
    @Excel(name = "省编码，省编码不能为空，长度6位数字")
    private String provinceCode;

    /** 市编码 */
    @Excel(name = "市编码")
    private String cityCode;

    /** 营业地址 */
    @Excel(name = "营业地址")
    private String businessAddress;

    /** 接入标识，01-web收银台，02-移动收银台，03-所有 */
    @Excel(name = "接入标识，01-web收银台，02-移动收银台，03-所有")
    private String accessId;

    /** 商户责任人 */
    @Excel(name = "商户责任人")
    private String merchantResponsiblePerson;

    /** 商户开户联系人姓名 */
    @Excel(name = "商户开户联系人姓名")
    private String merchantAcctOpenContName;

    /** 商户开户联系人邮箱 */
    @Excel(name = "商户开户联系人邮箱")
    private String merchantAcctOpenContEmail;

    /** 商户开户联系人电话 */
    @Excel(name = "商户开户联系人电话")
    private String merchantAcctOpenContPhone;

    /** 商户连锁类型(01-非连锁 */
    @Excel(name = "商户连锁类型(01-非连锁")
    private String merchantChainType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantCallbackUrl;

    /** 是否支持协议代扣（01-支持，02-不支持） */
    @Excel(name = "是否支持协议代扣", readConverterExp = "0=1-支持，02-不支持")
    private String supAgreeWithholding;

    /** 纸质合同号，合同号(系统自动创建，不是与商户签订的纸面合同） */
    @Excel(name = "纸质合同号，合同号(系统自动创建，不是与商户签订的纸面合同）")
    private String paperContractNo;

    /** 合同生效日期YYYYMMDD */
    @Excel(name = "合同生效日期YYYYMMDD")
    private String contractEffectiveDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String contractExpireDate;

    /** 结算周期类型，01:T+N， 02：D+N，03：周结，04：月结：05-季度结算.商户结算标志：0-关闭。此栏位置空 */
    @Excel(name = "结算周期类型，01:T+N， 02：D+N，03：周结，04：月结：05-季度结算.商户结算标志：0-关闭。此栏位置空")
    private String settleCycleType;

    /** 账户类型(01：对公，02：对私，03：中行内部账户) */
    @Excel(name = "账户类型(01：对公，02：对私，03：中行内部账户)")
    private String accountType;

    /** 账户名(对公填写帐户名，对私填写持卡人姓名) */
    @Excel(name = "账户名(对公填写帐户名，对私填写持卡人姓名)")
    private String accountName;

    /** 收款银行代码 */
    @Excel(name = "收款银行代码")
    private String receivePaymentBankCode;

    /** 分支行机构号 */
    @Excel(name = "分支行机构号")
    private String bankBranchNo;

    /** 分支行名称 */
    @Excel(name = "分支行名称")
    private String bankBranchName;

    /** 开户行省编码(6位数字 参见 国家行政区划编码) */
    @Excel(name = "开户行省编码(6位数字 参见 国家行政区划编码)")
    private String accountOpeningProvinceCode;

    /** 开户行市编码 */
    @Excel(name = "开户行市编码")
    private String accountOpeningCityCode;

    /** 结算最小金额（单位：分） */
    @Excel(name = "结算最小金额", readConverterExp = "单=位：分")
    private String minimumSettlementAmount;

    /** 结算类型:01-定期结算02-手工结算03-银行代扣 */
    @Excel(name = "结算类型:01-定期结算02-手工结算03-银行代扣")
    private String settlementType;

    /** 打款时间点，10代表10:00 */
    @Excel(name = "打款时间点，10代表10:00")
    private String paymentTime;

    /** 收款银行名称 */
    @Excel(name = "收款银行名称")
    private String receivingBankName;

    /** 收款银行账号:对公填写收款银行账号，对私填写银行卡号 */
    @Excel(name = "收款银行账号:对公填写收款银行账号，对私填写银行卡号")
    private String receivingBankAccount;

    /** 系统间过渡账户 */
    @Excel(name = "系统间过渡账户")
    private String interSystemTranAccount;

    /** 结算周期 */
    @Excel(name = "结算周期")
    private String settleCycle;

    /** 结算处理方式 */
    @Excel(name = "结算处理方式")
    private String settleTreatmentMethod;

    /** 商户注册渠道 */
    @Excel(name = "商户注册渠道")
    private String merchantRegisterChannels;

    /** 签约人 */
    @Excel(name = "签约人")
    private String signatory;

    /** CNAPS编号(商户结算标志：0-关闭。此栏位，置空;当结算处理方式选择余额归集时，此栏位选填) */
    @Excel(name = "CNAPS编号(商户结算标志：0-关闭。此栏位，置空;当结算处理方式选择余额归集时，此栏位选填)")
    private String cnapsNo;

    /** 商户开立交易返回信息码(00000000－交易成功 */
    @Excel(name = "商户开立交易返回信息码(00000000－交易成功")
    private String merchantOpenTranReturnCode;

    /** 商户开立交易返回信息，返回成功或错误信息 */
    @Excel(name = "商户开立交易返回信息，返回成功或错误信息")
    private String merchantOpenTranReturnMsg;

    /** 用户所属地区编码 */
    @Excel(name = "用户所属地区编码")
    private String areaOfUser;

    /** 导入文件批次号 */
    @Excel(name = "导入文件批次号")
    private String batchNo;

    /** 商户回调地址 */
    @Excel(name = "商户回调地址")
    private String merchantCallbackurl;

    @Excel(name = "更新时间")
    private Timestamp updateTime;
    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String v2Id;

    public BusPrivateInfoBps(){}

    public BusPrivateInfoBps(String returnCode, String returnMsg, String merchantId, String walletId,
                             String merchantOpenTranReturnCode, String merchantOpenTranReturnMsg,
                             String dataStatus, String miDengLiuShui, String remarkInfo,
                             String redundantInfo, Long sequenceNumber, String headId) {
        this.returnCode = returnCode;
        this.returnMsg = returnMsg;
        this.merchantId = merchantId;
        this.walletId = walletId;
        this.merchantOpenTranReturnCode = merchantOpenTranReturnCode;
        this.merchantOpenTranReturnMsg = merchantOpenTranReturnMsg;
        this.dataStatus = dataStatus;
        this.miDengLiuShui = miDengLiuShui;
        this.remarkInfo = remarkInfo;
        this.redundantInfo = redundantInfo;
        this.sequenceNumber = sequenceNumber;
        this.headId = headId;

    }

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setSequenceNumber(Long sequenceNumber) 
    {
        this.sequenceNumber = sequenceNumber;
    }

    public Long getSequenceNumber() 
    {
        return sequenceNumber;
    }
    public void setTradingOrganizationNo(String tradingOrganizationNo) 
    {
        this.tradingOrganizationNo = tradingOrganizationNo;
    }

    public String getTradingOrganizationNo() 
    {
        return tradingOrganizationNo;
    }
    public void setTransactionCode(String transactionCode) 
    {
        this.transactionCode = transactionCode;
    }

    public String getTransactionCode() 
    {
        return transactionCode;
    }
    public void setMobileInternationalAreaCode(String mobileInternationalAreaCode) 
    {
        this.mobileInternationalAreaCode = mobileInternationalAreaCode;
    }

    public String getMobileInternationalAreaCode() 
    {
        return mobileInternationalAreaCode;
    }
    public void setMobileNumberCityAreaCode(String mobileNumberCityAreaCode) 
    {
        this.mobileNumberCityAreaCode = mobileNumberCityAreaCode;
    }

    public String getMobileNumberCityAreaCode() 
    {
        return mobileNumberCityAreaCode;
    }
    public void setMobileNumber(String mobileNumber) 
    {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber() 
    {
        return mobileNumber;
    }
    public void setCountryOfUser(String countryOfUser) 
    {
        this.countryOfUser = countryOfUser;
    }

    public String getCountryOfUser() 
    {
        return countryOfUser;
    }
    public void setProvinceOfUser(String provinceOfUser) 
    {
        this.provinceOfUser = provinceOfUser;
    }

    public String getProvinceOfUser() 
    {
        return provinceOfUser;
    }
    public void setNickName(String nickName) 
    {
        this.nickName = nickName;
    }

    public String getNickName() 
    {
        return nickName;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setIdCardType(String idCardType) 
    {
        this.idCardType = idCardType;
    }

    public String getIdCardType() 
    {
        return idCardType;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setReturnCode(String returnCode) 
    {
        this.returnCode = returnCode;
    }

    public String getReturnCode() 
    {
        return returnCode;
    }
    public void setReturnMsg(String returnMsg) 
    {
        this.returnMsg = returnMsg;
    }

    public String getReturnMsg() 
    {
        return returnMsg;
    }
    public void setWalletId(String walletId) 
    {
        this.walletId = walletId;
    }

    public String getWalletId() 
    {
        return walletId;
    }
    public void setExamineSuccessDate(String examineSuccessDate) 
    {
        this.examineSuccessDate = examineSuccessDate;
    }

    public String getExamineSuccessDate() 
    {
        return examineSuccessDate;
    }
    public void setDataStatus(String dataStatus) 
    {
        this.dataStatus = dataStatus;
    }

    public String getDataStatus() 
    {
        return dataStatus;
    }
    public void setMiDengLiuShui(String miDengLiuShui) 
    {
        this.miDengLiuShui = miDengLiuShui;
    }

    public String getMiDengLiuShui() 
    {
        return miDengLiuShui;
    }
    public void setHeadId(String headId) 
    {
        this.headId = headId;
    }

    public String getHeadId() 
    {
        return headId;
    }
    public void setRemarkInfo(String remarkInfo) 
    {
        this.remarkInfo = remarkInfo;
    }

    public String getRemarkInfo() 
    {
        return remarkInfo;
    }
    public void setRedundantInfo(String redundantInfo) 
    {
        this.redundantInfo = redundantInfo;
    }

    public String getRedundantInfo() 
    {
        return redundantInfo;
    }
    public void setTellerNo(String tellerNo) 
    {
        this.tellerNo = tellerNo;
    }

    public String getTellerNo() 
    {
        return tellerNo;
    }
    public void setFileBatchNo(String fileBatchNo) 
    {
        this.fileBatchNo = fileBatchNo;
    }

    public String getFileBatchNo() 
    {
        return fileBatchNo;
    }
    public void setMerchantId(String merchantId) 
    {
        this.merchantId = merchantId;
    }

    public String getMerchantId() 
    {
        return merchantId;
    }
    public void setMerchantFullName(String merchantFullName) 
    {
        this.merchantFullName = merchantFullName;
    }

    public String getMerchantFullName() 
    {
        return merchantFullName;
    }
    public void setMerchantType(String merchantType) 
    {
        this.merchantType = merchantType;
    }

    public String getMerchantType() 
    {
        return merchantType;
    }
    public void setMerchantShortlName(String merchantShortlName) 
    {
        this.merchantShortlName = merchantShortlName;
    }

    public String getMerchantShortlName() 
    {
        return merchantShortlName;
    }
    public void setAgencyCode(String agencyCode) 
    {
        this.agencyCode = agencyCode;
    }

    public String getAgencyCode() 
    {
        return agencyCode;
    }
    public void setExternalMerchantNo(String externalMerchantNo) 
    {
        this.externalMerchantNo = externalMerchantNo;
    }

    public String getExternalMerchantNo() 
    {
        return externalMerchantNo;
    }
    public void setExternalMerchantName(String externalMerchantName) 
    {
        this.externalMerchantName = externalMerchantName;
    }

    public String getExternalMerchantName() 
    {
        return externalMerchantName;
    }
    public void setMerchantLevel(String merchantLevel) 
    {
        this.merchantLevel = merchantLevel;
    }

    public String getMerchantLevel() 
    {
        return merchantLevel;
    }
    public void setUpperLevelMerchantNo(String upperLevelMerchantNo) 
    {
        this.upperLevelMerchantNo = upperLevelMerchantNo;
    }

    public String getUpperLevelMerchantNo() 
    {
        return upperLevelMerchantNo;
    }
    public void setPbocMerchantNo(String pbocMerchantNo) 
    {
        this.pbocMerchantNo = pbocMerchantNo;
    }

    public String getPbocMerchantNo() 
    {
        return pbocMerchantNo;
    }
    public void setMerchantAttributes(String merchantAttributes) 
    {
        this.merchantAttributes = merchantAttributes;
    }

    public String getMerchantAttributes() 
    {
        return merchantAttributes;
    }
    public void setMccCode(String mccCode) 
    {
        this.mccCode = mccCode;
    }

    public String getMccCode() 
    {
        return mccCode;
    }
    public void setMerchantSettlementSign(String merchantSettlementSign) 
    {
        this.merchantSettlementSign = merchantSettlementSign;
    }

    public String getMerchantSettlementSign() 
    {
        return merchantSettlementSign;
    }
    public void setAccountManager(String accountManager) 
    {
        this.accountManager = accountManager;
    }

    public String getAccountManager() 
    {
        return accountManager;
    }
    public void setProvinceCode(String provinceCode) 
    {
        this.provinceCode = provinceCode;
    }

    public String getProvinceCode() 
    {
        return provinceCode;
    }
    public void setCityCode(String cityCode) 
    {
        this.cityCode = cityCode;
    }

    public String getCityCode() 
    {
        return cityCode;
    }
    public void setBusinessAddress(String businessAddress) 
    {
        this.businessAddress = businessAddress;
    }

    public String getBusinessAddress() 
    {
        return businessAddress;
    }
    public void setAccessId(String accessId) 
    {
        this.accessId = accessId;
    }

    public String getAccessId() 
    {
        return accessId;
    }
    public void setMerchantResponsiblePerson(String merchantResponsiblePerson) 
    {
        this.merchantResponsiblePerson = merchantResponsiblePerson;
    }

    public String getMerchantResponsiblePerson() 
    {
        return merchantResponsiblePerson;
    }
    public void setMerchantAcctOpenContName(String merchantAcctOpenContName) 
    {
        this.merchantAcctOpenContName = merchantAcctOpenContName;
    }

    public String getMerchantAcctOpenContName() 
    {
        return merchantAcctOpenContName;
    }
    public void setMerchantAcctOpenContEmail(String merchantAcctOpenContEmail) 
    {
        this.merchantAcctOpenContEmail = merchantAcctOpenContEmail;
    }

    public String getMerchantAcctOpenContEmail() 
    {
        return merchantAcctOpenContEmail;
    }
    public void setMerchantAcctOpenContPhone(String merchantAcctOpenContPhone) 
    {
        this.merchantAcctOpenContPhone = merchantAcctOpenContPhone;
    }

    public String getMerchantAcctOpenContPhone() 
    {
        return merchantAcctOpenContPhone;
    }
    public void setMerchantChainType(String merchantChainType) 
    {
        this.merchantChainType = merchantChainType;
    }

    public String getMerchantChainType() 
    {
        return merchantChainType;
    }
    public void setMerchantCallbackUrl(String merchantCallbackUrl) 
    {
        this.merchantCallbackUrl = merchantCallbackUrl;
    }

    public String getMerchantCallbackUrl() 
    {
        return merchantCallbackUrl;
    }
    public void setSupAgreeWithholding(String supAgreeWithholding) 
    {
        this.supAgreeWithholding = supAgreeWithholding;
    }

    public String getSupAgreeWithholding() 
    {
        return supAgreeWithholding;
    }
    public void setPaperContractNo(String paperContractNo) 
    {
        this.paperContractNo = paperContractNo;
    }

    public String getPaperContractNo() 
    {
        return paperContractNo;
    }
    public void setContractEffectiveDate(String contractEffectiveDate) 
    {
        this.contractEffectiveDate = contractEffectiveDate;
    }

    public String getContractEffectiveDate() 
    {
        return contractEffectiveDate;
    }
    public void setContractExpireDate(String contractExpireDate) 
    {
        this.contractExpireDate = contractExpireDate;
    }

    public String getContractExpireDate() 
    {
        return contractExpireDate;
    }
    public void setSettleCycleType(String settleCycleType) 
    {
        this.settleCycleType = settleCycleType;
    }

    public String getSettleCycleType() 
    {
        return settleCycleType;
    }
    public void setAccountType(String accountType) 
    {
        this.accountType = accountType;
    }

    public String getAccountType() 
    {
        return accountType;
    }
    public void setAccountName(String accountName) 
    {
        this.accountName = accountName;
    }

    public String getAccountName() 
    {
        return accountName;
    }
    public void setReceivePaymentBankCode(String receivePaymentBankCode) 
    {
        this.receivePaymentBankCode = receivePaymentBankCode;
    }

    public String getReceivePaymentBankCode() 
    {
        return receivePaymentBankCode;
    }
    public void setBankBranchNo(String bankBranchNo) 
    {
        this.bankBranchNo = bankBranchNo;
    }

    public String getBankBranchNo() 
    {
        return bankBranchNo;
    }
    public void setBankBranchName(String bankBranchName) 
    {
        this.bankBranchName = bankBranchName;
    }

    public String getBankBranchName() 
    {
        return bankBranchName;
    }
    public void setAccountOpeningProvinceCode(String accountOpeningProvinceCode) 
    {
        this.accountOpeningProvinceCode = accountOpeningProvinceCode;
    }

    public String getAccountOpeningProvinceCode() 
    {
        return accountOpeningProvinceCode;
    }
    public void setAccountOpeningCityCode(String accountOpeningCityCode) 
    {
        this.accountOpeningCityCode = accountOpeningCityCode;
    }

    public String getAccountOpeningCityCode() 
    {
        return accountOpeningCityCode;
    }
    public void setMinimumSettlementAmount(String minimumSettlementAmount) 
    {
        this.minimumSettlementAmount = minimumSettlementAmount;
    }

    public String getMinimumSettlementAmount() 
    {
        return minimumSettlementAmount;
    }
    public void setSettlementType(String settlementType) 
    {
        this.settlementType = settlementType;
    }

    public String getSettlementType() 
    {
        return settlementType;
    }
    public void setPaymentTime(String paymentTime) 
    {
        this.paymentTime = paymentTime;
    }

    public String getPaymentTime() 
    {
        return paymentTime;
    }
    public void setReceivingBankName(String receivingBankName) 
    {
        this.receivingBankName = receivingBankName;
    }

    public String getReceivingBankName() 
    {
        return receivingBankName;
    }
    public void setReceivingBankAccount(String receivingBankAccount) 
    {
        this.receivingBankAccount = receivingBankAccount;
    }

    public String getReceivingBankAccount() 
    {
        return receivingBankAccount;
    }
    public void setInterSystemTranAccount(String interSystemTranAccount) 
    {
        this.interSystemTranAccount = interSystemTranAccount;
    }

    public String getInterSystemTranAccount() 
    {
        return interSystemTranAccount;
    }
    public void setSettleCycle(String settleCycle) 
    {
        this.settleCycle = settleCycle;
    }

    public String getSettleCycle() 
    {
        return settleCycle;
    }
    public void setSettleTreatmentMethod(String settleTreatmentMethod) 
    {
        this.settleTreatmentMethod = settleTreatmentMethod;
    }

    public String getSettleTreatmentMethod() 
    {
        return settleTreatmentMethod;
    }
    public void setMerchantRegisterChannels(String merchantRegisterChannels) 
    {
        this.merchantRegisterChannels = merchantRegisterChannels;
    }

    public String getMerchantRegisterChannels() 
    {
        return merchantRegisterChannels;
    }
    public void setSignatory(String signatory) 
    {
        this.signatory = signatory;
    }

    public String getSignatory() 
    {
        return signatory;
    }
    public void setCnapsNo(String cnapsNo) 
    {
        this.cnapsNo = cnapsNo;
    }

    public String getCnapsNo() 
    {
        return cnapsNo;
    }
    public void setMerchantOpenTranReturnCode(String merchantOpenTranReturnCode) 
    {
        this.merchantOpenTranReturnCode = merchantOpenTranReturnCode;
    }

    public String getMerchantOpenTranReturnCode() 
    {
        return merchantOpenTranReturnCode;
    }
    public void setMerchantOpenTranReturnMsg(String merchantOpenTranReturnMsg) 
    {
        this.merchantOpenTranReturnMsg = merchantOpenTranReturnMsg;
    }

    public String getMerchantOpenTranReturnMsg() 
    {
        return merchantOpenTranReturnMsg;
    }
    public void setAreaOfUser(String areaOfUser) 
    {
        this.areaOfUser = areaOfUser;
    }

    public String getAreaOfUser() 
    {
        return areaOfUser;
    }
    public void setBatchNo(String batchNo) 
    {
        this.batchNo = batchNo;
    }

    public String getBatchNo() 
    {
        return batchNo;
    }
    public void setMerchantCallbackurl(String merchantCallbackurl) 
    {
        this.merchantCallbackurl = merchantCallbackurl;
    }

    public String getMerchantCallbackurl() 
    {
        return merchantCallbackurl;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public void setV2Id(String v2Id)
    {
        this.v2Id = v2Id;
    }

    public String getV2Id() 
    {
        return v2Id;
    }


    @Transient
    public String getDetailFileRow() throws UnsupportedEncodingException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("D");
        stringBuilder.append(getNumberFormat(12, String.valueOf(sequenceNumber)));
        stringBuilder.append(getOneVsSpace(5, tradingOrganizationNo));
        stringBuilder.append(getOneVsSpace(6, transactionCode));
        stringBuilder.append(getOneVsSpace(10, mobileInternationalAreaCode));
        stringBuilder.append(getOneVsSpace(10, mobileNumberCityAreaCode));
        stringBuilder.append(getOneVsSpace(16, mobileNumber));
        stringBuilder.append(getOneVsSpace(2, countryOfUser));
        stringBuilder.append(getOneVsSpace(6, provinceOfUser));
        stringBuilder.append(getOneVsSpace(6, areaOfUser));
        stringBuilder.append(getOneVsSpace(60, userName));
        stringBuilder.append(getOneVsSpace(40, userName));
        stringBuilder.append(getOneVsSpace(4, idCardType));
        stringBuilder.append(getOneVsSpace(32, idCard));
        if ("TF0014".equals(transactionCode)) {
            stringBuilder.append(getOneVsSpace(1826, " "));
            stringBuilder.append("\n");
            return stringBuilder.toString();
        }
        stringBuilder.append(getOneVsSpace(32, merchantId));
        stringBuilder.append(getOneVsSpace(60, merchantFullName));
        stringBuilder.append(getOneVsSpace(4, merchantType));
        stringBuilder.append(getOneVsSpace(30, merchantShortlName));
        stringBuilder.append(getOneVsSpace(4, agencyCode));
        stringBuilder.append(getOneVsSpace(16, walletId));
        stringBuilder.append(getOneVsSpace(32, externalMerchantNo));
        stringBuilder.append(getOneVsSpace(60, externalMerchantName));
        stringBuilder.append(getOneVsSpace(4, merchantLevel));
        stringBuilder.append(getOneVsSpace(23, upperLevelMerchantNo));
        stringBuilder.append(getOneVsSpace(8, pbocMerchantNo));
        stringBuilder.append(getOneVsSpace(1, merchantAttributes));
        stringBuilder.append(getOneVsSpace(4, mccCode));
        stringBuilder.append(getOneVsSpace(1, merchantSettlementSign));
        stringBuilder.append(getOneVsSpace(40, accountManager));
        stringBuilder.append(getOneVsSpace(6, provinceCode));
        stringBuilder.append(getOneVsSpace(6, cityCode));
        stringBuilder.append(getOneVsSpace(80, businessAddress));
        stringBuilder.append(getOneVsSpace(2, accessId));
        stringBuilder.append(getOneVsSpace(40, merchantResponsiblePerson));
        stringBuilder.append(getOneVsSpace(40, merchantAcctOpenContName));
        stringBuilder.append(getOneVsSpace(50, merchantAcctOpenContEmail));
        stringBuilder.append(getOneVsSpace(18, merchantAcctOpenContPhone));
        stringBuilder.append(getOneVsSpace(2, merchantChainType));
        stringBuilder.append(getOneVsSpace(100, merchantCallbackUrl));
        stringBuilder.append(getOneVsSpace(2, supAgreeWithholding));
        stringBuilder.append(getOneVsSpace(40, paperContractNo));
        stringBuilder.append(getOneVsSpace(8, contractEffectiveDate));
        stringBuilder.append(getOneVsSpace(8, contractExpireDate));
        stringBuilder.append(getOneVsSpace(2, settleCycleType));
        stringBuilder.append(getOneVsSpace(2, accountType));
        stringBuilder.append(getOneVsSpace(240, accountName));
        stringBuilder.append(getOneVsSpace(16, receivePaymentBankCode));
        stringBuilder.append(getOneVsSpace(5, bankBranchNo));
        stringBuilder.append(getOneVsSpace(64, bankBranchName));
        stringBuilder.append(getOneVsSpace(6, accountOpeningProvinceCode));
        stringBuilder.append(getOneVsSpace(6, accountOpeningCityCode));
        stringBuilder.append(getOneVsSpace(19, minimumSettlementAmount));
        stringBuilder.append(getOneVsSpace(2, settlementType));
        stringBuilder.append(getOneVsSpace(2, paymentTime));
        stringBuilder.append(getOneVsSpace(99, receivingBankName));
        stringBuilder.append(getOneVsSpace(68, receivingBankAccount));
        stringBuilder.append(getOneVsSpace(2, interSystemTranAccount));
        stringBuilder.append(getOneVsSpace(2, settleCycle));
        stringBuilder.append(getOneVsSpace(2, settleTreatmentMethod));
        stringBuilder.append(getOneVsSpace(2, merchantRegisterChannels));
        stringBuilder.append(getOneVsSpace(40, signatory));
        stringBuilder.append(getOneVsSpace(14, cnapsNo));
        stringBuilder.append(getOneVsSpace(8, merchantOpenTranReturnCode));
        stringBuilder.append(getOneVsSpace(60, merchantOpenTranReturnMsg));
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sequenceNumber", getSequenceNumber())
            .append("tradingOrganizationNo", getTradingOrganizationNo())
            .append("transactionCode", getTransactionCode())
            .append("mobileInternationalAreaCode", getMobileInternationalAreaCode())
            .append("mobileNumberCityAreaCode", getMobileNumberCityAreaCode())
            .append("mobileNumber", getMobileNumber())
            .append("countryOfUser", getCountryOfUser())
            .append("provinceOfUser", getProvinceOfUser())
            .append("nickName", getNickName())
            .append("userName", getUserName())
            .append("idCardType", getIdCardType())
            .append("idCard", getIdCard())
            .append("returnCode", getReturnCode())
            .append("returnMsg", getReturnMsg())
            .append("walletId", getWalletId())
            .append("examineSuccessDate", getExamineSuccessDate())
            .append("dataStatus", getDataStatus())
            .append("miDengLiuShui", getMiDengLiuShui())
            .append("headId", getHeadId())
            .append("remarkInfo", getRemarkInfo())
            .append("redundantInfo", getRedundantInfo())
            .append("tellerNo", getTellerNo())
            .append("fileBatchNo", getFileBatchNo())
            .append("merchantId", getMerchantId())
            .append("merchantFullName", getMerchantFullName())
            .append("merchantType", getMerchantType())
            .append("merchantShortlName", getMerchantShortlName())
            .append("agencyCode", getAgencyCode())
            .append("externalMerchantNo", getExternalMerchantNo())
            .append("externalMerchantName", getExternalMerchantName())
            .append("merchantLevel", getMerchantLevel())
            .append("upperLevelMerchantNo", getUpperLevelMerchantNo())
            .append("pbocMerchantNo", getPbocMerchantNo())
            .append("merchantAttributes", getMerchantAttributes())
            .append("mccCode", getMccCode())
            .append("merchantSettlementSign", getMerchantSettlementSign())
            .append("accountManager", getAccountManager())
            .append("provinceCode", getProvinceCode())
            .append("cityCode", getCityCode())
            .append("businessAddress", getBusinessAddress())
            .append("accessId", getAccessId())
            .append("merchantResponsiblePerson", getMerchantResponsiblePerson())
            .append("merchantAcctOpenContName", getMerchantAcctOpenContName())
            .append("merchantAcctOpenContEmail", getMerchantAcctOpenContEmail())
            .append("merchantAcctOpenContPhone", getMerchantAcctOpenContPhone())
            .append("merchantChainType", getMerchantChainType())
            .append("merchantCallbackUrl", getMerchantCallbackUrl())
            .append("supAgreeWithholding", getSupAgreeWithholding())
            .append("paperContractNo", getPaperContractNo())
            .append("contractEffectiveDate", getContractEffectiveDate())
            .append("contractExpireDate", getContractExpireDate())
            .append("settleCycleType", getSettleCycleType())
            .append("accountType", getAccountType())
            .append("accountName", getAccountName())
            .append("receivePaymentBankCode", getReceivePaymentBankCode())
            .append("bankBranchNo", getBankBranchNo())
            .append("bankBranchName", getBankBranchName())
            .append("accountOpeningProvinceCode", getAccountOpeningProvinceCode())
            .append("accountOpeningCityCode", getAccountOpeningCityCode())
            .append("minimumSettlementAmount", getMinimumSettlementAmount())
            .append("settlementType", getSettlementType())
            .append("paymentTime", getPaymentTime())
            .append("receivingBankName", getReceivingBankName())
            .append("receivingBankAccount", getReceivingBankAccount())
            .append("interSystemTranAccount", getInterSystemTranAccount())
            .append("settleCycle", getSettleCycle())
            .append("settleTreatmentMethod", getSettleTreatmentMethod())
            .append("merchantRegisterChannels", getMerchantRegisterChannels())
            .append("signatory", getSignatory())
            .append("cnapsNo", getCnapsNo())
            .append("merchantOpenTranReturnCode", getMerchantOpenTranReturnCode())
            .append("merchantOpenTranReturnMsg", getMerchantOpenTranReturnMsg())
            .append("areaOfUser", getAreaOfUser())
            .append("batchNo", getBatchNo())
            .append("merchantCallbackurl", getMerchantCallbackurl())
            .append("updateTime", getUpdateTime())
            .append("v2Id", getV2Id())
            .toString();
    }
}
