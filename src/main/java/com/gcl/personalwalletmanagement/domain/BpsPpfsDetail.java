package com.gcl.personalwalletmanagement.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.gcl.common.annotation.Excel;
import com.gcl.common.core.domain.BaseEntity;

/**
 * 批量开立对私钱包明细对象 bps_ppfs_detail
 * 
 * @author yada
 * @date 2022-02-20
 */
public class BpsPpfsDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String id;

    /** 记录标识 */
    @Excel(name = "记录标识")
    private String recordId;

    /** 顺序号 */
    @Excel(name = "顺序号")
    private Long sequenceNumber;

    /** 文件批次号 */
    @Excel(name = "文件批次号")
    private String fileBatchNo;

    /** 交易机构号 */
    @Excel(name = "交易机构号")
    private String tradingOrganizationNo;

    /** 交易码 */
    @Excel(name = "交易码")
    private String transactionCode;

    /** 手机国际区号 */
    @Excel(name = "手机国际区号")
    private String phoneAreaCode;

    /** 手机号城市区号 */
    @Excel(name = "手机号城市区号")
    private String phoneCityCode;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;

    /** 用户所属国家编码 */
    @Excel(name = "用户所属国家编码")
    private String custCountryCode;

    /** 用户所属省编码 */
    @Excel(name = "用户所属省编码")
    private Integer custProvinceCode;

    /** 用户所属地区编码 */
    @Excel(name = "用户所属地区编码")
	private Integer custCityCode;

    /** 钱包昵称 */
    @Excel(name = "钱包昵称")
    private String walletNickname;

    /** 用户姓名 */
    @Excel(name = "用户姓名")
    private String custName;

    /** 身份证件类型 */
    @Excel(name = "身份证件类型")
    private String idCardType;

    /** 身份证件号码 */
    @Excel(name = "身份证件号码")
    private String idCard;

    /** 对私钱包开立交易返回信息码 */
    @Excel(name = "对私钱包开立交易返回信息码")
    private String returnInfoCodeForWallet;

    /** 对私钱包开立交易返回信息 */
    @Excel(name = "对私钱包开立交易返回信息")
    private String returnInformationForWallet;

    /** 商户号 */
    @Excel(name = "商户号")
    private String merchantId;

    /** 商户全称 */
    @Excel(name = "商户全称")
    private String merchantFullName;

    /** 商户类型 */
    @Excel(name = "商户类型")
    private String merchantType;

    /** 商户简称 */
    @Excel(name = "商户简称")
    private String merchantShortlName;

    /** 所属代理机构编码 */
    @Excel(name = "所属代理机构编码")
    private String agencyCode;

    /** 钱包id */
    @Excel(name = "钱包id")
    private String walletId;

    /** 外部商户号 */
    @Excel(name = "外部商户号")
    private String externalMerchantNo;

    /** 外部商户名称 */
    @Excel(name = "外部商户名称")
    private String externalMerchantName;

    /** 商户等级 */
    @Excel(name = "商户等级")
    private String merchantLevel;

    /** 上级商户号 */
    @Excel(name = "上级商户号")
    private String upperLevelMerchantNo;

    /** 人行商户号 */
    @Excel(name = "人行商户号")
    private String pbocmerchantNo;

    /** 商户属性/标识 */
    @Excel(name = "商户属性/标识")
    private Integer merchantAttributes;

    /** MCC编码 */
    @Excel(name = "MCC编码")
    private String mccCode;

    /** 商户结算标志 */
    @Excel(name = "商户结算标志")
    private Integer merchantSettlement;

    /** 客户经理 */
    @Excel(name = "客户经理")
    private String accountManager;

    /** 省编码 */
    @Excel(name = "省编码")
    private Integer provinceCode;

    /** 市编码 */
    @Excel(name = "市编码")
    private Integer cityCode;

    /** 营业地址 */
    @Excel(name = "营业地址")
    private String businessAddress;

    /** 接入标识 */
    @Excel(name = "接入标识")
    private Integer accessId;

    /** 商户责任人 */
    @Excel(name = "商户责任人")
    private String merchantResponsiblePerson;

    /** 商户开户联系人姓名 */
    @Excel(name = "商户开户联系人姓名")
    private String merchantAcctOpenContName;

    /** 商户开户联系人邮箱 */
    @Excel(name = "商户开户联系人邮箱")
    private String merchantAcctOpenContEmail;

    /** 商户开户联系人电话 */
    @Excel(name = "商户开户联系人电话")
    private String merchantAcctOpenContPhone;

    /** 商户连锁类型 */
    @Excel(name = "商户连锁类型")
    private Integer merchantChainType;

    /** 商户回调地址 */
    @Excel(name = "商户回调地址")
    private String merchantCallbackurl;

    /** 是否支持协议代扣 */
    @Excel(name = "是否支持协议代扣")
    private Integer supAgreeWithholding;

    /** 纸质合同号 */
    @Excel(name = "纸质合同号")
    private String paperContractNo;

    /** 合同生效日期 */
    @Excel(name = "合同生效日期")
    private String contractEffectiveDate;

    /** 合同失效日期 */
    @Excel(name = "合同失效日期")
    private String contractExpireDate;

    /** 结算周期 */
    @Excel(name = "结算周期")
    private String settleCycle;

    /** 账户类型 */
    @Excel(name = "账户类型")
    private Integer accountType;

    /** 账户名 */
    @Excel(name = "账户名")
    private String accountName;

    /** 收款银行代码 */
    @Excel(name = "收款银行代码")
    private String receivePaymentBankCode;

    /** 分支行机构号 */
    @Excel(name = "分支行机构号")
    private String bankBranchNo;

    /** 分支行名称 */
    @Excel(name = "分支行名称")
    private String bankBranchName;

    /** 开户行省编码 */
    @Excel(name = "开户行省编码")
    private Integer accountOpeningProvinceCode;

    /** 开户行市编码 */
    @Excel(name = "开户行市编码")
    private Integer accountOpeningCityCode;

    /** 结算最小金额 */
    @Excel(name = "结算最小金额")
    private String minimumSettlementAmount;

    /** 结算类型 */
    @Excel(name = "结算类型")
    private String settleCycleType;

    /** 打款时间点 */
    @Excel(name = "打款时间点")
    private String paymentTime;

    /** 收款银行名称 */
    @Excel(name = "收款银行名称")
    private String receivingBankName;

    /** 收款银行账号 */
    @Excel(name = "收款银行账号")
    private String receivingBankAccount;

    /** 系统间过渡账户 */
    @Excel(name = "系统间过渡账户")
    private String interSystemTranAccount;

    /** 结算处理方式 */
    @Excel(name = "结算处理方式")
    private String settleTreatmentMethod;

    /** 商户注册渠道 */
    @Excel(name = "商户注册渠道")
    private String merchantRegisterChannels;

    /** 签约人 */
    @Excel(name = "签约人")
    private String signatory;

    /** CNAPS编号 */
    @Excel(name = "CNAPS编号")
    private String cnapsno;

    /** 商户开立交易返回信息码 */
    @Excel(name = "商户开立交易返回信息码")
    private String merchantOpenTranReturnCode;

    /** 商户开立交易返回信息 */
    @Excel(name = "商户开立交易返回信息")
    private String merchantOpenTranReturnMsg;

    /** 幂等流水 */
    @Excel(name = "幂等流水")
    private String miDengLiuShui;

    /** 备注 */
    @Excel(name = "备注")
    private String remarkInfo;

    /** 冗余项 */
    @Excel(name = "冗余项")
    private String redundantInfo;

    /** 交易状态 */
    @Excel(name = "交易状态")
    private String tradeStatus;

    /** 文件头主键 */
    @Excel(name = "文件头主键")
    private String bpsPpfsHeadId;

    /** 交易柜员号 */
    @Excel(name = "交易柜员号")
    private String transactionTellerNumber;

    /** 交易终端号 */
    @Excel(name = "交易终端号")
    private String tradingTerminalNumber;

	/** 个人信息表主键 */
	@Excel(name = "个人信息表主键")
	private String busPersonInfoId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setRecordId(String recordId) 
    {
        this.recordId = recordId;
    }

    public String getRecordId() 
    {
        return recordId;
    }
    public void setSequenceNumber(Long sequenceNumber) 
    {
        this.sequenceNumber = sequenceNumber;
    }

    public Long getSequenceNumber() 
    {
        return sequenceNumber;
    }
    public void setFileBatchNo(String fileBatchNo) 
    {
        this.fileBatchNo = fileBatchNo;
    }

    public String getFileBatchNo() 
    {
        return fileBatchNo;
    }
    public void setTransactionCode(String transactionCode)
    {
        this.transactionCode = transactionCode;
    }

    public String getTradingOrganizationNo() {
        return tradingOrganizationNo;
    }

    public void setTradingOrganizationNo(String tradingOrganizationNo) {
        this.tradingOrganizationNo = tradingOrganizationNo;
    }

    public String getTransactionTellerNumber() {
        return transactionTellerNumber;
    }

    public void setTransactionTellerNumber(String transactionTellerNumber) {
        this.transactionTellerNumber = transactionTellerNumber;
    }

    public String getTradingTerminalNumber() {
        return tradingTerminalNumber;
    }

    public void setTradingTerminalNumber(String tradingTerminalNumber) {
        this.tradingTerminalNumber = tradingTerminalNumber;
    }

    public String getTransactionCode()
    {
        return transactionCode;
    }
    public void setPhoneAreaCode(String phoneAreaCode) 
    {
        this.phoneAreaCode = phoneAreaCode;
    }

    public String getPhoneAreaCode() 
    {
        return phoneAreaCode;
    }
    public void setPhoneCityCode(String phoneCityCode) 
    {
        this.phoneCityCode = phoneCityCode;
    }

    public String getPhoneCityCode() 
    {
        return phoneCityCode;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setCustCountryCode(String custCountryCode) 
    {
        this.custCountryCode = custCountryCode;
    }

    public String getCustCountryCode() 
    {
        return custCountryCode;
    }
    public void setCustProvinceCode(Integer custProvinceCode) 
    {
        this.custProvinceCode = custProvinceCode;
    }

    public Integer getCustProvinceCode() 
    {
        return custProvinceCode;
    }

	public void setCustCityCode(Integer custCityCode)
    {
        this.custCityCode = custCityCode;
    }

	public Integer getCustCityCode()
    {
        return custCityCode;
    }
    public void setWalletNickname(String walletNickname) 
    {
        this.walletNickname = walletNickname;
    }

    public String getWalletNickname() 
    {
        return walletNickname;
    }
    public void setCustName(String custName) 
    {
        this.custName = custName;
    }

    public String getCustName() 
    {
        return custName;
    }
    public void setIdCardType(String idCardType) 
    {
        this.idCardType = idCardType;
    }

    public String getIdCardType() 
    {
        return idCardType;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setReturnInfoCodeForWallet(String returnInfoCodeForWallet) 
    {
        this.returnInfoCodeForWallet = returnInfoCodeForWallet;
    }

    public String getReturnInfoCodeForWallet() 
    {
        return returnInfoCodeForWallet;
    }
    public void setReturnInformationForWallet(String returnInformationForWallet) 
    {
        this.returnInformationForWallet = returnInformationForWallet;
    }

    public String getReturnInformationForWallet() 
    {
        return returnInformationForWallet;
    }
    public void setMerchantId(String merchantId) 
    {
        this.merchantId = merchantId;
    }

    public String getMerchantId() 
    {
        return merchantId;
    }
    public void setMerchantFullName(String merchantFullName) 
    {
        this.merchantFullName = merchantFullName;
    }

    public String getMerchantFullName() 
    {
        return merchantFullName;
    }
    public void setMerchantType(String merchantType) 
    {
        this.merchantType = merchantType;
    }

    public String getMerchantType() 
    {
        return merchantType;
    }
    public void setMerchantShortlName(String merchantShortlName) 
    {
        this.merchantShortlName = merchantShortlName;
    }

    public String getMerchantShortlName() 
    {
        return merchantShortlName;
    }
    public void setAgencyCode(String agencyCode) 
    {
        this.agencyCode = agencyCode;
    }

    public String getAgencyCode() 
    {
        return agencyCode;
    }
    public void setWalletId(String walletId) 
    {
        this.walletId = walletId;
    }

    public String getWalletId() 
    {
        return walletId;
    }
    public void setExternalMerchantNo(String externalMerchantNo) 
    {
        this.externalMerchantNo = externalMerchantNo;
    }

    public String getExternalMerchantNo() 
    {
        return externalMerchantNo;
    }
    public void setExternalMerchantName(String externalMerchantName) 
    {
        this.externalMerchantName = externalMerchantName;
    }

    public String getExternalMerchantName() 
    {
        return externalMerchantName;
    }
    public void setMerchantLevel(String merchantLevel) 
    {
        this.merchantLevel = merchantLevel;
    }

    public String getMerchantLevel() 
    {
        return merchantLevel;
    }
    public void setUpperLevelMerchantNo(String upperLevelMerchantNo) 
    {
        this.upperLevelMerchantNo = upperLevelMerchantNo;
    }

    public String getUpperLevelMerchantNo() 
    {
        return upperLevelMerchantNo;
    }
    public void setPbocmerchantNo(String pbocmerchantNo) 
    {
        this.pbocmerchantNo = pbocmerchantNo;
    }

    public String getPbocmerchantNo() 
    {
        return pbocmerchantNo;
    }
    public void setMerchantAttributes(Integer merchantAttributes) 
    {
        this.merchantAttributes = merchantAttributes;
    }

    public Integer getMerchantAttributes() 
    {
        return merchantAttributes;
    }
    public void setMccCode(String mccCode) 
    {
        this.mccCode = mccCode;
    }

    public String getMccCode() 
    {
        return mccCode;
    }
    public void setMerchantSettlement(Integer merchantSettlement) 
    {
        this.merchantSettlement = merchantSettlement;
    }

    public Integer getMerchantSettlement() 
    {
        return merchantSettlement;
    }
    public void setAccountManager(String accountManager) 
    {
        this.accountManager = accountManager;
    }

    public String getAccountManager() 
    {
        return accountManager;
    }
    public void setProvinceCode(Integer provinceCode) 
    {
        this.provinceCode = provinceCode;
    }

    public Integer getProvinceCode() 
    {
        return provinceCode;
    }
    public void setCityCode(Integer cityCode) 
    {
        this.cityCode = cityCode;
    }

    public Integer getCityCode() 
    {
        return cityCode;
    }
    public void setBusinessAddress(String businessAddress) 
    {
        this.businessAddress = businessAddress;
    }

    public String getBusinessAddress() 
    {
        return businessAddress;
    }
    public void setAccessId(Integer accessId) 
    {
        this.accessId = accessId;
    }

    public Integer getAccessId() 
    {
        return accessId;
    }
    public void setMerchantResponsiblePerson(String merchantResponsiblePerson) 
    {
        this.merchantResponsiblePerson = merchantResponsiblePerson;
    }

    public String getMerchantResponsiblePerson() 
    {
        return merchantResponsiblePerson;
    }
    public void setMerchantAcctOpenContName(String merchantAcctOpenContName) 
    {
        this.merchantAcctOpenContName = merchantAcctOpenContName;
    }

    public String getMerchantAcctOpenContName() 
    {
        return merchantAcctOpenContName;
    }
    public void setMerchantAcctOpenContEmail(String merchantAcctOpenContEmail) 
    {
        this.merchantAcctOpenContEmail = merchantAcctOpenContEmail;
    }

    public String getMerchantAcctOpenContEmail() 
    {
        return merchantAcctOpenContEmail;
    }
    public void setMerchantAcctOpenContPhone(String merchantAcctOpenContPhone) 
    {
        this.merchantAcctOpenContPhone = merchantAcctOpenContPhone;
    }

    public String getMerchantAcctOpenContPhone() 
    {
        return merchantAcctOpenContPhone;
    }
    public void setMerchantChainType(Integer merchantChainType) 
    {
        this.merchantChainType = merchantChainType;
    }

    public Integer getMerchantChainType() 
    {
        return merchantChainType;
    }
    public void setMerchantCallbackurl(String merchantCallbackurl) 
    {
        this.merchantCallbackurl = merchantCallbackurl;
    }

    public String getMerchantCallbackurl() 
    {
        return merchantCallbackurl;
    }
    public void setSupAgreeWithholding(Integer supAgreeWithholding) 
    {
        this.supAgreeWithholding = supAgreeWithholding;
    }

    public Integer getSupAgreeWithholding() 
    {
        return supAgreeWithholding;
    }
    public void setPaperContractNo(String paperContractNo) 
    {
        this.paperContractNo = paperContractNo;
    }

    public String getPaperContractNo() 
    {
        return paperContractNo;
    }
    public void setContractEffectiveDate(String contractEffectiveDate) 
    {
        this.contractEffectiveDate = contractEffectiveDate;
    }

    public String getContractEffectiveDate() 
    {
        return contractEffectiveDate;
    }
    public void setContractExpireDate(String contractExpireDate) 
    {
        this.contractExpireDate = contractExpireDate;
    }

    public String getContractExpireDate() 
    {
        return contractExpireDate;
    }
    public void setSettleCycle(String settleCycle) 
    {
        this.settleCycle = settleCycle;
    }

    public String getSettleCycle() 
    {
        return settleCycle;
    }
    public void setAccountType(Integer accountType) 
    {
        this.accountType = accountType;
    }

    public Integer getAccountType() 
    {
        return accountType;
    }
    public void setAccountName(String accountName) 
    {
        this.accountName = accountName;
    }

    public String getAccountName() 
    {
        return accountName;
    }
    public void setReceivePaymentBankCode(String receivePaymentBankCode) 
    {
        this.receivePaymentBankCode = receivePaymentBankCode;
    }

    public String getReceivePaymentBankCode() 
    {
        return receivePaymentBankCode;
    }
    public void setBankBranchNo(String bankBranchNo) 
    {
        this.bankBranchNo = bankBranchNo;
    }

    public String getBankBranchNo() 
    {
        return bankBranchNo;
    }
    public void setBankBranchName(String bankBranchName) 
    {
        this.bankBranchName = bankBranchName;
    }

    public String getBankBranchName() 
    {
        return bankBranchName;
    }
    public void setAccountOpeningProvinceCode(Integer accountOpeningProvinceCode) 
    {
        this.accountOpeningProvinceCode = accountOpeningProvinceCode;
    }

    public Integer getAccountOpeningProvinceCode() 
    {
        return accountOpeningProvinceCode;
    }
    public void setAccountOpeningCityCode(Integer accountOpeningCityCode) 
    {
        this.accountOpeningCityCode = accountOpeningCityCode;
    }

    public Integer getAccountOpeningCityCode() 
    {
        return accountOpeningCityCode;
    }
    public void setMinimumSettlementAmount(String minimumSettlementAmount) 
    {
        this.minimumSettlementAmount = minimumSettlementAmount;
    }

    public String getMinimumSettlementAmount() 
    {
        return minimumSettlementAmount;
    }
    public void setSettleCycleType(String settleCycleType) 
    {
        this.settleCycleType = settleCycleType;
    }

    public String getSettleCycleType() 
    {
        return settleCycleType;
    }
    public void setPaymentTime(String paymentTime) 
    {
        this.paymentTime = paymentTime;
    }

    public String getPaymentTime() 
    {
        return paymentTime;
    }
    public void setReceivingBankName(String receivingBankName) 
    {
        this.receivingBankName = receivingBankName;
    }

    public String getReceivingBankName() 
    {
        return receivingBankName;
    }
    public void setReceivingBankAccount(String receivingBankAccount) 
    {
        this.receivingBankAccount = receivingBankAccount;
    }

    public String getReceivingBankAccount() 
    {
        return receivingBankAccount;
    }
    public void setInterSystemTranAccount(String interSystemTranAccount) 
    {
        this.interSystemTranAccount = interSystemTranAccount;
    }

    public String getInterSystemTranAccount() 
    {
        return interSystemTranAccount;
    }
    public void setSettleTreatmentMethod(String settleTreatmentMethod) 
    {
        this.settleTreatmentMethod = settleTreatmentMethod;
    }

    public String getSettleTreatmentMethod() 
    {
        return settleTreatmentMethod;
    }
    public void setMerchantRegisterChannels(String merchantRegisterChannels) 
    {
        this.merchantRegisterChannels = merchantRegisterChannels;
    }

    public String getMerchantRegisterChannels() 
    {
        return merchantRegisterChannels;
    }
    public void setSignatory(String signatory) 
    {
        this.signatory = signatory;
    }

    public String getSignatory() 
    {
        return signatory;
    }
    public void setCnapsno(String cnapsno) 
    {
        this.cnapsno = cnapsno;
    }

    public String getCnapsno() 
    {
        return cnapsno;
    }
    public void setMerchantOpenTranReturnCode(String merchantOpenTranReturnCode) 
    {
        this.merchantOpenTranReturnCode = merchantOpenTranReturnCode;
    }

    public String getMerchantOpenTranReturnCode() 
    {
        return merchantOpenTranReturnCode;
    }
    public void setMerchantOpenTranReturnMsg(String merchantOpenTranReturnMsg) 
    {
        this.merchantOpenTranReturnMsg = merchantOpenTranReturnMsg;
    }

    public String getMerchantOpenTranReturnMsg() 
    {
        return merchantOpenTranReturnMsg;
    }
    public void setMiDengLiuShui(String miDengLiuShui) 
    {
        this.miDengLiuShui = miDengLiuShui;
    }

    public String getMiDengLiuShui() 
    {
        return miDengLiuShui;
    }
    public void setRemarkInfo(String remarkInfo) 
    {
        this.remarkInfo = remarkInfo;
    }

    public String getRemarkInfo() 
    {
        return remarkInfo;
    }
    public void setRedundantInfo(String redundantInfo) 
    {
        this.redundantInfo = redundantInfo;
    }

    public String getRedundantInfo() 
    {
        return redundantInfo;
    }
    public void setTradeStatus(String tradeStatus) 
    {
        this.tradeStatus = tradeStatus;
    }

    public String getTradeStatus() 
    {
        return tradeStatus;
    }
    public void setBpsPpfsHeadId(String bpsPpfsHeadId) 
    {
        this.bpsPpfsHeadId = bpsPpfsHeadId;
    }

    public String getBpsPpfsHeadId() 
    {
        return bpsPpfsHeadId;
    }

	public void setBusPersonInfoId(String busPersonInfoId) {
		this.busPersonInfoId = busPersonInfoId;
	}

	public String getBusPersonInfoId() {
		return busPersonInfoId;
	}

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("recordId", getRecordId())
            .append("sequenceNumber", getSequenceNumber())
            .append("fileBatchNo", getFileBatchNo())
            .append("tradingOrganizationNo", getTradingOrganizationNo())
            .append("transactionCode", getTransactionCode())
            .append("phoneAreaCode", getPhoneAreaCode())
            .append("phoneCityCode", getPhoneCityCode())
            .append("phone", getPhone())
            .append("custCountryCode", getCustCountryCode())
            .append("custProvinceCode", getCustProvinceCode())
            .append("custCityCode", getCustCityCode())
            .append("walletNickname", getWalletNickname())
            .append("custName", getCustName())
            .append("idCardType", getIdCardType())
            .append("idCard", getIdCard())
            .append("returnInfoCodeForWallet", getReturnInfoCodeForWallet())
            .append("returnInformationForWallet", getReturnInformationForWallet())
            .append("merchantId", getMerchantId())
            .append("merchantFullName", getMerchantFullName())
            .append("merchantType", getMerchantType())
            .append("merchantShortlName", getMerchantShortlName())
            .append("agencyCode", getAgencyCode())
            .append("walletId", getWalletId())
            .append("externalMerchantNo", getExternalMerchantNo())
            .append("externalMerchantName", getExternalMerchantName())
            .append("merchantLevel", getMerchantLevel())
            .append("upperLevelMerchantNo", getUpperLevelMerchantNo())
            .append("pbocmerchantNo", getPbocmerchantNo())
            .append("merchantAttributes", getMerchantAttributes())
            .append("mccCode", getMccCode())
            .append("merchantSettlement", getMerchantSettlement())
            .append("accountManager", getAccountManager())
            .append("provinceCode", getProvinceCode())
            .append("cityCode", getCityCode())
            .append("businessAddress", getBusinessAddress())
            .append("accessId", getAccessId())
            .append("merchantResponsiblePerson", getMerchantResponsiblePerson())
            .append("merchantAcctOpenContName", getMerchantAcctOpenContName())
            .append("merchantAcctOpenContEmail", getMerchantAcctOpenContEmail())
            .append("merchantAcctOpenContPhone", getMerchantAcctOpenContPhone())
            .append("merchantChainType", getMerchantChainType())
            .append("merchantCallbackurl", getMerchantCallbackurl())
            .append("supAgreeWithholding", getSupAgreeWithholding())
            .append("paperContractNo", getPaperContractNo())
            .append("contractEffectiveDate", getContractEffectiveDate())
            .append("contractExpireDate", getContractExpireDate())
            .append("settleCycle", getSettleCycle())
            .append("accountType", getAccountType())
            .append("accountName", getAccountName())
            .append("receivePaymentBankCode", getReceivePaymentBankCode())
            .append("bankBranchNo", getBankBranchNo())
            .append("bankBranchName", getBankBranchName())
            .append("accountOpeningProvinceCode", getAccountOpeningProvinceCode())
            .append("accountOpeningCityCode", getAccountOpeningCityCode())
            .append("minimumSettlementAmount", getMinimumSettlementAmount())
            .append("settleCycleType", getSettleCycleType())
            .append("paymentTime", getPaymentTime())
            .append("receivingBankName", getReceivingBankName())
            .append("receivingBankAccount", getReceivingBankAccount())
            .append("interSystemTranAccount", getInterSystemTranAccount())
            .append("settleTreatmentMethod", getSettleTreatmentMethod())
            .append("merchantRegisterChannels", getMerchantRegisterChannels())
            .append("signatory", getSignatory())
            .append("cnapsno", getCnapsno())
            .append("merchantOpenTranReturnCode", getMerchantOpenTranReturnCode())
            .append("merchantOpenTranReturnMsg", getMerchantOpenTranReturnMsg())
            .append("miDengLiuShui", getMiDengLiuShui())
            .append("remarkInfo", getRemarkInfo())
            .append("redundantInfo", getRedundantInfo())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("tradeStatus", getTradeStatus())
            .append("bpsPpfsHeadId", getBpsPpfsHeadId())
            .append("transactionTellerNumber", getTransactionTellerNumber())
            .append("tradingTerminalNumber", getTradingTerminalNumber())
				.append("busPersonInfoId", getBusPersonInfoId())
            .toString();
    }
}
