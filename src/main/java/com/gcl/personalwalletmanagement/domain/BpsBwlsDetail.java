package com.gcl.personalwalletmanagement.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.gcl.common.annotation.Excel;
import com.gcl.common.core.domain.BaseEntity;

/**
 * 对私批量注册白名单文件明细对象 bps_bwls_detail
 * 
 * @author yada
 * @date 2022-02-18
 */
public class BpsBwlsDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String id;

    /** 记录标识 */
    private String recordId;

    /** 顺序号 */
    @Excel(name = "顺序号")
    private Long sequenceNumber;

    /** 交易机构号 */
    @Excel(name = "交易机构号")
	private String tradingOrganizationNo;

    /** 交易码 */
    @Excel(name = "交易码")
    private String transactionCode;

    /** 邮箱地址
 */
	@Excel(name = "邮箱地址")
    private String contactEmail;

    /** 国际电话区号 */
    @Excel(name = "国际电话区号")
    private String phoneAreaCode;

    /** 电话城市区号 */
    @Excel(name = "电话城市区号")
    private String phoneCityCode;

    /** 电话号码 */
    @Excel(name = "电话号码")
    private String phone;

    /** 用户姓名 */
    @Excel(name = "用户姓名")
    private String custName;

    /** 证件类型
 */
	@Excel
	(name="证件类型")
    private String idCardType;

    /** 证件号
 */
	@Excel
	(name="证件号")
    private String idCard;

    /** 预留信息
 */
	@Excel
	(name="预留信息")
    private String reservedInfo;

    /** 员工姓名(创建人)
 */
	@Excel
	(name="员工姓名(创建人)")
    private String staffName;

    /** 客户类型 */
    @Excel(name = "客户类型")
    private String customerType;

    /** 机构号 */
    @Excel(name = "机构号")
    private String coreOrgNo;

    /** 用户所属省编码 */
    @Excel(name = "用户所属省编码")
    private String custProvinceCode;

    /** 用户所属市码 */
    @Excel(name = "用户所属市码")
    private String custCityType;

    /** 变更类型 */
    @Excel(name = "变更类型")
    private String changeType;

    /** 返回信息码 */
    @Excel(name = "返回信息码")
    private String returnCode;

    /** 返回信息 */
    @Excel(name = "返回信息")
    private String returnMsg;

    /** 幂等流水 */
    @Excel(name = "幂等流水")
    private String miDengLiuShui;

    /** 备注 */
    @Excel(name = "备注")
    private String remarkInfo;

    /** 冗余域 */
    @Excel(name = "冗余域")
    private String redundantInfo;

    /** 交易状态 */
    @Excel(name = "交易状态")
    private String tradeStatus;

    /** 文件头主键 */
    @Excel(name = "文件头主键")
    private String bpsPwlsHeadId;

    /** 文件批次号 */
    @Excel(name = "文件批次号")
    private String fileBatchNo;

    /** 交易柜员号
 */
	@Excel(name = "交易柜员号")
	private String transactionTellerNumber;

    /** 交易终端号
 */
	@Excel(name = "交易终端号")
	private String tradingTerminalNumber;

	/** 个人信息表主键 */
	@Excel(name = "个人信息表主键")
	private String busPersonInfoId;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setRecordId(String recordId) 
    {
        this.recordId = recordId;
    }

    public String getRecordId() 
    {
        return recordId;
    }
    public void setSequenceNumber(Long sequenceNumber) 
    {
        this.sequenceNumber = sequenceNumber;
    }

    public Long getSequenceNumber() 
    {
        return sequenceNumber;
    }


    public void setTransactionCode(String transactionCode)
    {
        this.transactionCode = transactionCode;
    }

    public String getTransactionCode() 
    {
        return transactionCode;
    }
    public void setContactEmail(String contactEmail) 
    {
        this.contactEmail = contactEmail;
    }

    public String getContactEmail() 
    {
        return contactEmail;
    }
    public void setPhoneAreaCode(String phoneAreaCode) 
    {
        this.phoneAreaCode = phoneAreaCode;
    }

    public String getPhoneAreaCode() 
    {
        return phoneAreaCode;
    }
    public void setPhoneCityCode(String phoneCityCode) 
    {
        this.phoneCityCode = phoneCityCode;
    }

    public String getPhoneCityCode() 
    {
        return phoneCityCode;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setCustName(String custName) 
    {
        this.custName = custName;
    }

    public String getCustName() 
    {
        return custName;
    }
    public void setIdCardType(String idCardType) 
    {
        this.idCardType = idCardType;
    }

    public String getIdCardType() 
    {
        return idCardType;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setReservedInfo(String reservedInfo) 
    {
        this.reservedInfo = reservedInfo;
    }

    public String getReservedInfo() 
    {
        return reservedInfo;
    }
    public void setStaffName(String staffName) 
    {
        this.staffName = staffName;
    }

    public String getStaffName() 
    {
        return staffName;
    }
    public void setCustomerType(String customerType) 
    {
        this.customerType = customerType;
    }

    public String getCustomerType() 
    {
        return customerType;
    }
    public void setCoreOrgNo(String coreOrgNo) 
    {
        this.coreOrgNo = coreOrgNo;
    }

    public String getCoreOrgNo() 
    {
        return coreOrgNo;
    }
    public void setCustProvinceCode(String custProvinceCode) 
    {
        this.custProvinceCode = custProvinceCode;
    }

    public String getCustProvinceCode() 
    {
        return custProvinceCode;
    }
    public void setCustCityType(String custCityType) 
    {
        this.custCityType = custCityType;
    }

    public String getCustCityType() 
    {
        return custCityType;
    }
    public void setChangeType(String changeType) 
    {
        this.changeType = changeType;
    }

    public String getChangeType() 
    {
        return changeType;
    }
    public void setReturnCode(String returnCode) 
    {
        this.returnCode = returnCode;
    }

    public String getReturnCode() 
    {
        return returnCode;
    }
    public void setReturnMsg(String returnMsg) 
    {
        this.returnMsg = returnMsg;
    }

    public String getReturnMsg() 
    {
        return returnMsg;
    }
    public void setMiDengLiuShui(String miDengLiuShui) 
    {
        this.miDengLiuShui = miDengLiuShui;
    }

    public String getMiDengLiuShui() 
    {
        return miDengLiuShui;
    }
    public void setRemarkInfo(String remarkInfo) 
    {
        this.remarkInfo = remarkInfo;
    }

    public String getRemarkInfo() 
    {
        return remarkInfo;
    }
    public void setRedundantInfo(String redundantInfo) 
    {
        this.redundantInfo = redundantInfo;
    }

    public String getRedundantInfo() 
    {
        return redundantInfo;
    }
    public void setTradeStatus(String tradeStatus) 
    {
        this.tradeStatus = tradeStatus;
    }

    public String getTradeStatus() 
    {
        return tradeStatus;
    }
    public void setBpsPwlsHeadId(String bpsPwlsHeadId) 
    {
        this.bpsPwlsHeadId = bpsPwlsHeadId;
    }

    public String getBpsPwlsHeadId() 
    {
        return bpsPwlsHeadId;
    }
    public void setFileBatchNo(String fileBatchNo) 
    {
        this.fileBatchNo = fileBatchNo;
    }

    public String getFileBatchNo() 
    {
        return fileBatchNo;
    }



	public void setBusPersonInfoId(String busPersonInfoId) {
		this.busPersonInfoId = busPersonInfoId;
	}

	public String getBusPersonInfoId() {
		return busPersonInfoId;
	}


    public String getTradingOrganizationNo() {
        return tradingOrganizationNo;
    }

    public void setTradingOrganizationNo(String tradingOrganizationNo) {
        this.tradingOrganizationNo = tradingOrganizationNo;
    }

    public String getTransactionTellerNumber() {
        return transactionTellerNumber;
    }

    public void setTransactionTellerNumber(String transactionTellerNumber) {
        this.transactionTellerNumber = transactionTellerNumber;
    }

    public String getTradingTerminalNumber() {
        return tradingTerminalNumber;
    }

    public void setTradingTerminalNumber(String tradingTerminalNumber) {
        this.tradingTerminalNumber = tradingTerminalNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("recordId", getRecordId())
            .append("sequenceNumber", getSequenceNumber())
            .append("tradingOrganizationNo", getTradingOrganizationNo())
            .append("transactionCode", getTransactionCode())
            .append("contactEmail", getContactEmail())
            .append("phoneAreaCode", getPhoneAreaCode())
            .append("phoneCityCode", getPhoneCityCode())
            .append("phone", getPhone())
            .append("custName", getCustName())
            .append("idCardType", getIdCardType())
            .append("idCard", getIdCard())
            .append("reservedInfo", getReservedInfo())
            .append("staffName", getStaffName())
            .append("customerType", getCustomerType())
            .append("coreOrgNo", getCoreOrgNo())
            .append("custProvinceCode", getCustProvinceCode())
            .append("custCityType", getCustCityType())
            .append("changeType", getChangeType())
            .append("returnCode", getReturnCode())
            .append("returnMsg", getReturnMsg())
            .append("miDengLiuShui", getMiDengLiuShui())
            .append("remarkInfo", getRemarkInfo())
            .append("redundantInfo", getRedundantInfo())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("tradeStatus", getTradeStatus())
            .append("bpsPwlsHeadId", getBpsPwlsHeadId())
            .append("fileBatchNo", getFileBatchNo())
            .append("transactionTellerNumber", getTransactionTellerNumber())
            .append("tradingTerminalNumber", getTradingTerminalNumber())
				.append("busPersonInfoId", getBusPersonInfoId())
            .toString();
    }
}
