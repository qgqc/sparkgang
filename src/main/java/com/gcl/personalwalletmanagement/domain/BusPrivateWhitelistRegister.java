package com.gcl.personalwalletmanagement.domain;

import java.beans.Transient;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.gcl.bps.domain.Base;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.gcl.common.annotation.Excel;

/**
 * 钱包注册白名单信息对象 bus_private_whitelist_register
 * 
 * @author wjw
 * @date 2022-02-07
 */
public class BusPrivateWhitelistRegister extends Base
{
    private static final long serialVersionUID = 1L;

    /** 主键Id */
    private String id;

    /** 顺序号 */
    @Excel(name = "顺序号")
    private String sequenceNumber;

    /** 交易机构号 */
    @Excel(name = "交易机构号")
    private String tradingOrganizationNo;

    /** 交易码 */
    @Excel(name = "交易码")
    private String transactionCode;

    /** email地址 */
    @Excel(name = "email地址")
    private String email;

    /** 手机号 */
    @Excel(name = "手机号")
    private String phone;

    /** 手机号国际区号 */
    @Excel(name = "手机号国际区号")
    private String phoneGlobalRoaming;

    /** 手机号城市区号 */
    @Excel(name = "手机号城市区号")
    private String phoneCityCode;

    /** 姓名 */
    @Excel(name = "姓名")
    private String userName;

    /** 证件类型 */
    @Excel(name = "证件类型")
    private String documentType;

    /** 证件号 */
    @Excel(name = "证件号")
    private String idCard;

    /** 预留信息 */
    @Excel(name = "预留信息")
    private String reservedInformation;

    /** 创建人 */
    @Excel(name = "创建人")
    private String creator;

    /** 客户类型 */
    @Excel(name = "客户类型")
    private String customerType;

    /** 用户所属市 */
    @Excel(name = "用户所属市")
    private String countryOfUser;

    /** 用户所属省 */
    @Excel(name = "用户所属省")
    private String provinceOfUser;

    /** 变更类型CC00 */
    @Excel(name = "变更类型CC00")
    private String changeType;

    /** 数据状态 */
    @Excel(name = "数据状态")
    private String dataStatus;

    /** 开立返回码 */
    @Excel(name = "开立返回码")
    private String returnCode;

    /** 开立返回信息 */
    @Excel(name = "开立返回信息")
    private String returnMsg;

    /** 返回信息更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "返回信息更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date returnTime;

    /** 幂等流水 */
    @Excel(name = "幂等流水")
    private String miDengLiuShui;

    /** 备注 */
    @Excel(name = "备注")
    private String remarkInfo;

    /** 冗余域 */
    @Excel(name = "冗余域")
    private String redundantInfo;

    /** 时间戳，最后更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "时间戳，最后更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date ts;

    /** 审核成功时间 */
    @Excel(name = "审核成功时间")
    private String examineDate;

    /** 所属bps上送文件的文件名 */
    @Excel(name = "所属bps上送文件的文件名")
    private String sourceFileName;

    /** 柜员号 */
    @Excel(name = "柜员号")
    private String tellerNo;

    /** 所属bps上送文件的文件名的id */
    @Excel(name = "所属bps上送文件的文件名的id")
    private String headId;

    /** 核心机构号 */
    @Excel(name = "核心机构号")
    private String coreOrgNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String fileBatchNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String v2Id;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setSequenceNumber(String sequenceNumber) 
    {
        this.sequenceNumber = sequenceNumber;
    }

    public String getSequenceNumber() 
    {
        return sequenceNumber;
    }
    public void setTradingOrganizationNo(String tradingOrganizationNo) 
    {
        this.tradingOrganizationNo = tradingOrganizationNo;
    }

    public String getTradingOrganizationNo() 
    {
        return tradingOrganizationNo;
    }
    public void setTransactionCode(String transactionCode) 
    {
        this.transactionCode = transactionCode;
    }

    public String getTransactionCode() 
    {
        return transactionCode;
    }
    public void setEmail(String email) 
    {
        this.email = email;
    }

    public String getEmail() 
    {
        return email;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setPhoneGlobalRoaming(String phoneGlobalRoaming) 
    {
        this.phoneGlobalRoaming = phoneGlobalRoaming;
    }

    public String getPhoneGlobalRoaming() 
    {
        return phoneGlobalRoaming;
    }
    public void setPhoneCityCode(String phoneCityCode) 
    {
        this.phoneCityCode = phoneCityCode;
    }

    public String getPhoneCityCode() 
    {
        return phoneCityCode;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setDocumentType(String documentType) 
    {
        this.documentType = documentType;
    }

    public String getDocumentType() 
    {
        return documentType;
    }
    public void setIdCard(String idCard) 
    {
        this.idCard = idCard;
    }

    public String getIdCard() 
    {
        return idCard;
    }
    public void setReservedInformation(String reservedInformation) 
    {
        this.reservedInformation = reservedInformation;
    }

    public String getReservedInformation() 
    {
        return reservedInformation;
    }
    public void setCreator(String creator) 
    {
        this.creator = creator;
    }

    public String getCreator() 
    {
        return creator;
    }
    public void setCustomerType(String customerType) 
    {
        this.customerType = customerType;
    }

    public String getCustomerType() 
    {
        return customerType;
    }
    public void setCountryOfUser(String countryOfUser) 
    {
        this.countryOfUser = countryOfUser;
    }

    public String getCountryOfUser() 
    {
        return countryOfUser;
    }
    public void setProvinceOfUser(String provinceOfUser) 
    {
        this.provinceOfUser = provinceOfUser;
    }

    public String getProvinceOfUser() 
    {
        return provinceOfUser;
    }
    public void setChangeType(String changeType) 
    {
        this.changeType = changeType;
    }

    public String getChangeType() 
    {
        return changeType;
    }
    public void setDataStatus(String dataStatus) 
    {
        this.dataStatus = dataStatus;
    }

    public String getDataStatus() 
    {
        return dataStatus;
    }
    public void setReturnCode(String returnCode) 
    {
        this.returnCode = returnCode;
    }

    public String getReturnCode() 
    {
        return returnCode;
    }
    public void setReturnMsg(String returnMsg) 
    {
        this.returnMsg = returnMsg;
    }

    public String getReturnMsg() 
    {
        return returnMsg;
    }
    public void setReturnTime(Date returnTime) 
    {
        this.returnTime = returnTime;
    }

    public Date getReturnTime() 
    {
        return returnTime;
    }
    public void setMiDengLiuShui(String miDengLiuShui) 
    {
        this.miDengLiuShui = miDengLiuShui;
    }

    public String getMiDengLiuShui() 
    {
        return miDengLiuShui;
    }
    public void setRemarkInfo(String remarkInfo) 
    {
        this.remarkInfo = remarkInfo;
    }

    public String getRemarkInfo() 
    {
        return remarkInfo;
    }
    public void setRedundantInfo(String redundantInfo) 
    {
        this.redundantInfo = redundantInfo;
    }

    public String getRedundantInfo() 
    {
        return redundantInfo;
    }
    public void setTs(Date ts) 
    {
        this.ts = ts;
    }

    public Date getTs() 
    {
        return ts;
    }
    public void setExamineDate(String examineDate) 
    {
        this.examineDate = examineDate;
    }

    public String getExamineDate() 
    {
        return examineDate;
    }
    public void setSourceFileName(String sourceFileName) 
    {
        this.sourceFileName = sourceFileName;
    }

    public String getSourceFileName() 
    {
        return sourceFileName;
    }
    public void setTellerNo(String tellerNo) 
    {
        this.tellerNo = tellerNo;
    }

    public String getTellerNo() 
    {
        return tellerNo;
    }
    public void setHeadId(String headId) 
    {
        this.headId = headId;
    }

    public String getHeadId() 
    {
        return headId;
    }
    public void setCoreOrgNo(String coreOrgNo) 
    {
        this.coreOrgNo = coreOrgNo;
    }

    public String getCoreOrgNo() 
    {
        return coreOrgNo;
    }
    public void setFileBatchNo(String fileBatchNo) 
    {
        this.fileBatchNo = fileBatchNo;
    }

    public String getFileBatchNo() 
    {
        return fileBatchNo;
    }
    public void setV2Id(String v2Id) 
    {
        this.v2Id = v2Id;
    }

    public String getV2Id() 
    {
        return v2Id;
    }



    @Transient
    public String getDetailFileRow() throws UnsupportedEncodingException {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("D");
        stringBuilder.append(getNumberFormat(12,this.sequenceNumber.toString()));
        stringBuilder.append(getNumberFormat(5,this.tradingOrganizationNo));
        stringBuilder.append(getOneVsSpace(6,this.transactionCode));
        stringBuilder.append(getOneVsSpace(64,this.email));
        stringBuilder.append(getOneVsSpace(64,this.phone));
        stringBuilder.append(getOneVsSpace(5,this.phoneGlobalRoaming));
        stringBuilder.append(getOneVsSpace(5,this.phoneCityCode));
        stringBuilder.append(getOneVsSpace(64,this.userName));
        stringBuilder.append(getOneVsSpace(10,this.documentType));
        stringBuilder.append(getOneVsSpace(50,this.idCard));
        stringBuilder.append(getOneVsSpace(75,this.reservedInformation));
        stringBuilder.append(getOneVsSpace(75,this.creator));
        stringBuilder.append(getOneVsSpace(2,this.customerType));
        stringBuilder.append(getOneVsSpace(12,this.coreOrgNo));
        stringBuilder.append(getOneVsSpace(6,this.provinceOfUser));
        stringBuilder.append(getOneVsSpace(6,this.countryOfUser));
        stringBuilder.append(getOneVsSpace(4,this.changeType));
        stringBuilder.append(getOneVsSpace(8,this.returnCode));
        stringBuilder.append(getOneVsSpace(60,this.returnMsg));
        stringBuilder.append(getOneVsSpace(47,this.miDengLiuShui));
        stringBuilder.append(getOneVsSpace(64,this.remarkInfo));
        stringBuilder.append(getOneVsSpace(100,this.redundantInfo));
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("sequenceNumber", getSequenceNumber())
            .append("tradingOrganizationNo", getTradingOrganizationNo())
            .append("transactionCode", getTransactionCode())
            .append("email", getEmail())
            .append("phone", getPhone())
            .append("phoneGlobalRoaming", getPhoneGlobalRoaming())
            .append("phoneCityCode", getPhoneCityCode())
            .append("userName", getUserName())
            .append("documentType", getDocumentType())
            .append("idCard", getIdCard())
            .append("reservedInformation", getReservedInformation())
            .append("creator", getCreator())
            .append("customerType", getCustomerType())
            .append("countryOfUser", getCountryOfUser())
            .append("provinceOfUser", getProvinceOfUser())
            .append("changeType", getChangeType())
            .append("dataStatus", getDataStatus())
            .append("returnCode", getReturnCode())
            .append("returnMsg", getReturnMsg())
            .append("returnTime", getReturnTime())
            .append("miDengLiuShui", getMiDengLiuShui())
            .append("remarkInfo", getRemarkInfo())
            .append("redundantInfo", getRedundantInfo())
            .append("ts", getTs())
            .append("examineDate", getExamineDate())
            .append("sourceFileName", getSourceFileName())
            .append("tellerNo", getTellerNo())
            .append("headId", getHeadId())
            .append("coreOrgNo", getCoreOrgNo())
            .append("fileBatchNo", getFileBatchNo())
            .append("v2Id", getV2Id())
            .toString();
    }
}
