package com.gcl.personalwalletmanagement.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.gcl.common.annotation.Excel;
import com.gcl.common.core.domain.BaseEntity;

/**
 * 人行共建APP白名单批量文件明细对象 bps_pwls_detail
 * 
 * @author yada
 * @date 2022-02-09
 */
public class BpsPwlsDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private String id;

    /** 记录标识 */
    @Excel(name = "记录标识")
    private String recordId;

    /** 顺序号 */
    @Excel(name = "顺序号")
    private Long sequenceNumber;

    /** 交易机构号 */
    @Excel(name = "交易机构号")
    private Long tradingOrganizationNo;

    /** 交易码 */
    @Excel(name = "交易码")
    private String transactionCode;

    /** 联系方式类型 */
    @Excel(name = "联系方式类型")
    private String contactType;

    /** 手机国际区号 */
    @Excel(name = "手机国际区号")
    private String phoneAreaCode;

    /** 手机号城市区号 */
    @Excel(name = "手机号城市区号")
    private String phoneCityCode;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String phone;

    /** 变更类型 */
    @Excel(name = "变更类型")
    private String changeType;

    /** 用户姓名 */
    @Excel(name = "用户姓名")
    private String userName;

    /** 用户标签 */
    @Excel(name = "用户标签")
    private String userLabel;

    /** 系统类型 */
    @Excel(name = "系统类型")
    private String systemType;

    /** 客户类型 */
    @Excel(name = "客户类型")
    private String customerType;

    /** 机构号 */
    @Excel(name = "机构号")
    private String coreOrgNo;

    /** 用户所属省编码 */
    @Excel(name = "用户所属省编码")
    private String custProvinceCode;

    /** 用户所属市码 */
    @Excel(name = "用户所属市码")
    private String custCityType;

    /** 返回信息码 */
    @Excel(name = "返回信息码")
    private String returnCode;

    /** 返回信息 */
    @Excel(name = "返回信息")
    private String returnMsg;

    /** 幂等流水 */
    @Excel(name = "幂等流水")
    private String miDengLiuShui;

    /** 备注 */
    @Excel(name = "备注")
    private String remarkInfo;

    /** 冗余域 */
    @Excel(name = "冗余域")
    private String redundantInfo;

    /** 交易状态 */
    @Excel(name = "交易状态")
    private String tradeStatus;

	/** 文件头主键 */
	@Excel(name = "文件头主键")
	private String bpsPwlsHeadId;

	/** 文件批次号 */
	@Excel(name = "文件批次号")
	private String fileBatchNo;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setRecordId(String recordId) 
    {
        this.recordId = recordId;
    }

    public String getRecordId() 
    {
        return recordId;
    }
    public void setSequenceNumber(Long sequenceNumber) 
    {
        this.sequenceNumber = sequenceNumber;
    }

    public Long getSequenceNumber() 
    {
        return sequenceNumber;
    }
    public void setTradingOrganizationNo(Long tradingOrganizationNo) 
    {
        this.tradingOrganizationNo = tradingOrganizationNo;
    }

    public Long getTradingOrganizationNo() 
    {
        return tradingOrganizationNo;
    }
    public void setTransactionCode(String transactionCode) 
    {
        this.transactionCode = transactionCode;
    }

    public String getTransactionCode() 
    {
        return transactionCode;
    }
    public void setContactType(String contactType) 
    {
        this.contactType = contactType;
    }

    public String getContactType() 
    {
        return contactType;
    }
    public void setPhoneAreaCode(String phoneAreaCode) 
    {
        this.phoneAreaCode = phoneAreaCode;
    }

    public String getPhoneAreaCode() 
    {
        return phoneAreaCode;
    }
    public void setPhoneCityCode(String phoneCityCode) 
    {
        this.phoneCityCode = phoneCityCode;
    }

    public String getPhoneCityCode() 
    {
        return phoneCityCode;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setChangeType(String changeType) 
    {
        this.changeType = changeType;
    }

    public String getChangeType() 
    {
        return changeType;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setUserLabel(String userLabel) 
    {
        this.userLabel = userLabel;
    }

    public String getUserLabel() 
    {
        return userLabel;
    }
    public void setSystemType(String systemType) 
    {
        this.systemType = systemType;
    }

    public String getSystemType() 
    {
        return systemType;
    }
    public void setCustomerType(String customerType) 
    {
        this.customerType = customerType;
    }

    public String getCustomerType() 
    {
        return customerType;
    }
    public void setCoreOrgNo(String coreOrgNo) 
    {
        this.coreOrgNo = coreOrgNo;
    }

    public String getCoreOrgNo() 
    {
        return coreOrgNo;
    }
    public void setCustProvinceCode(String custProvinceCode) 
    {
        this.custProvinceCode = custProvinceCode;
    }

    public String getCustProvinceCode() 
    {
        return custProvinceCode;
    }
    public void setCustCityType(String custCityType) 
    {
        this.custCityType = custCityType;
    }

    public String getCustCityType() 
    {
        return custCityType;
    }
    public void setReturnCode(String returnCode) 
    {
        this.returnCode = returnCode;
    }

    public String getReturnCode() 
    {
        return returnCode;
    }
    public void setReturnMsg(String returnMsg) 
    {
        this.returnMsg = returnMsg;
    }

    public String getReturnMsg() 
    {
        return returnMsg;
    }
    public void setMiDengLiuShui(String miDengLiuShui) 
    {
        this.miDengLiuShui = miDengLiuShui;
    }

    public String getMiDengLiuShui() 
    {
        return miDengLiuShui;
    }
    public void setRemarkInfo(String remarkInfo) 
    {
        this.remarkInfo = remarkInfo;
    }

    public String getRemarkInfo() 
    {
        return remarkInfo;
    }
    public void setRedundantInfo(String redundantInfo) 
    {
        this.redundantInfo = redundantInfo;
    }

    public String getRedundantInfo() 
    {
        return redundantInfo;
    }
    public void setTradeStatus(String tradeStatus) 
    {
        this.tradeStatus = tradeStatus;
    }

    public String getTradeStatus() 
    {
        return tradeStatus;
    }

	public void setBpsPwlsHeadId(String bpsPwlsHeadId) {
		this.bpsPwlsHeadId = bpsPwlsHeadId;
	}

	public String getBpsPwlsHeadId() {
		return bpsPwlsHeadId;
	}

	public void setFileBatchNo(String fileBatchNo) {
		this.fileBatchNo = fileBatchNo;
	}

	public String getFileBatchNo() {
		return fileBatchNo;
	}

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("recordId", getRecordId())
            .append("sequenceNumber", getSequenceNumber())
            .append("tradingOrganizationNo", getTradingOrganizationNo())
            .append("transactionCode", getTransactionCode())
            .append("contactType", getContactType())
            .append("phoneAreaCode", getPhoneAreaCode())
            .append("phoneCityCode", getPhoneCityCode())
            .append("phone", getPhone())
            .append("changeType", getChangeType())
            .append("userName", getUserName())
            .append("userLabel", getUserLabel())
            .append("systemType", getSystemType())
            .append("customerType", getCustomerType())
            .append("coreOrgNo", getCoreOrgNo())
            .append("custProvinceCode", getCustProvinceCode())
            .append("custCityType", getCustCityType())
            .append("returnCode", getReturnCode())
            .append("returnMsg", getReturnMsg())
            .append("miDengLiuShui", getMiDengLiuShui())
            .append("remarkInfo", getRemarkInfo())
            .append("redundantInfo", getRedundantInfo())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("tradeStatus", getTradeStatus())
				.append("bpsPwlsHeadId", getBpsPwlsHeadId()).append("fileBatchNo", getFileBatchNo())
            .toString();
    }
}
