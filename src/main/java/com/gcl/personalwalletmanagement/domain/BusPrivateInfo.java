package com.gcl.personalwalletmanagement.domain;

import java.util.Date;

import com.gcl.common.annotation.Excel;
import com.gcl.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 【请填写功能名称】对象 t_b_private_info
 *
 * @author ruoyi
 * @date 2022-01-13
 */
public class BusPrivateInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String id;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String accessId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String accountManager;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String accountName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String accountOpeningCityCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String accountOpeningProvinceCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String accountType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String agencyCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String alterationType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String areaOfUser;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String bankBranchName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String bankBranchNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String batchNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String businessAddress;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String cityCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String clientType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String cnapsNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String contactEmail;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String contactwayType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String contractEffectiveDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String countryOfUser;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String createdBycreatedBy;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date createdDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String dataStatus;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String examineSuccessDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String externalMerchantName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String externalMerchantNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String fileBatchNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String headId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String idCard;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String idCardType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String interSystemTranAccount;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String lastUpdatedBy;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Date lastUpdatedDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String mccCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantAcctOpenContEmail;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantAcctOpenContName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantAcctOpenContPhone;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantAttributes;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantCallbackurl;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantChainType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantFullName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantLevel;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantOpenTranReturnCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantOpenTranReturnMsg;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantRegisterChannels;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantResponsiblePerson;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantSettlementSign;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantShortlName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String miDengLiuShui;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String minimumSettlementAmount;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String mobileInternationalAreaCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String mobileNumber;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String mobileNumberCityAreaCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String nickName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String orgId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String paperContractNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String paymentTime;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String pbocMerchantNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String provinceCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String provinceOfUser;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String receivePaymentBankCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String receivingBankAccount;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String receivingBankName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String redundantInfo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String remarkInfo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String reservedInfo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String returnCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String returnMsg;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long sequenceNumber;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String settleCycle;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String settleCycleType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String settleTreatmentMethod;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String settlementType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String signatory;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String staffId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String staffName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String supAgreeWithholding;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String systemType;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String tellerNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String tradingOrganizationNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String transactionCode;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String upperLevelMerchantNo;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String userLabel;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String userName;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String walletId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String contractExpireDate;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private String merchantCallbackUrl;

    public void setId(String id)
    {
        this.id = id;
    }

    public String getId()
    {
        return id;
    }
    public void setAccessId(String accessId)
    {
        this.accessId = accessId;
    }

    public String getAccessId()
    {
        return accessId;
    }
    public void setAccountManager(String accountManager)
    {
        this.accountManager = accountManager;
    }

    public String getAccountManager()
    {
        return accountManager;
    }
    public void setAccountName(String accountName)
    {
        this.accountName = accountName;
    }

    public String getAccountName()
    {
        return accountName;
    }
    public void setAccountOpeningCityCode(String accountOpeningCityCode)
    {
        this.accountOpeningCityCode = accountOpeningCityCode;
    }

    public String getAccountOpeningCityCode()
    {
        return accountOpeningCityCode;
    }
    public void setAccountOpeningProvinceCode(String accountOpeningProvinceCode)
    {
        this.accountOpeningProvinceCode = accountOpeningProvinceCode;
    }

    public String getAccountOpeningProvinceCode()
    {
        return accountOpeningProvinceCode;
    }
    public void setAccountType(String accountType)
    {
        this.accountType = accountType;
    }

    public String getAccountType()
    {
        return accountType;
    }
    public void setAgencyCode(String agencyCode)
    {
        this.agencyCode = agencyCode;
    }

    public String getAgencyCode()
    {
        return agencyCode;
    }
    public void setAlterationType(String alterationType)
    {
        this.alterationType = alterationType;
    }

    public String getAlterationType()
    {
        return alterationType;
    }
    public void setAreaOfUser(String areaOfUser)
    {
        this.areaOfUser = areaOfUser;
    }

    public String getAreaOfUser()
    {
        return areaOfUser;
    }
    public void setBankBranchName(String bankBranchName)
    {
        this.bankBranchName = bankBranchName;
    }

    public String getBankBranchName()
    {
        return bankBranchName;
    }
    public void setBankBranchNo(String bankBranchNo)
    {
        this.bankBranchNo = bankBranchNo;
    }

    public String getBankBranchNo()
    {
        return bankBranchNo;
    }
    public void setBatchNo(String batchNo)
    {
        this.batchNo = batchNo;
    }

    public String getBatchNo()
    {
        return batchNo;
    }
    public void setBusinessAddress(String businessAddress)
    {
        this.businessAddress = businessAddress;
    }

    public String getBusinessAddress()
    {
        return businessAddress;
    }
    public void setCityCode(String cityCode)
    {
        this.cityCode = cityCode;
    }

    public String getCityCode()
    {
        return cityCode;
    }
    public void setClientType(String clientType)
    {
        this.clientType = clientType;
    }

    public String getClientType()
    {
        return clientType;
    }
    public void setCnapsNo(String cnapsNo)
    {
        this.cnapsNo = cnapsNo;
    }

    public String getCnapsNo()
    {
        return cnapsNo;
    }
    public void setContactEmail(String contactEmail)
    {
        this.contactEmail = contactEmail;
    }

    public String getContactEmail()
    {
        return contactEmail;
    }
    public void setContactwayType(String contactwayType)
    {
        this.contactwayType = contactwayType;
    }

    public String getContactwayType()
    {
        return contactwayType;
    }
    public void setContractEffectiveDate(String contractEffectiveDate)
    {
        this.contractEffectiveDate = contractEffectiveDate;
    }

    public String getContractEffectiveDate()
    {
        return contractEffectiveDate;
    }
    public void setCountryOfUser(String countryOfUser)
    {
        this.countryOfUser = countryOfUser;
    }

    public String getCountryOfUser()
    {
        return countryOfUser;
    }
    public void setCreatedBycreatedBy(String createdBycreatedBy)
    {
        this.createdBycreatedBy = createdBycreatedBy;
    }

    public String getCreatedBycreatedBy()
    {
        return createdBycreatedBy;
    }
    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }
    public void setDataStatus(String dataStatus)
    {
        this.dataStatus = dataStatus;
    }

    public String getDataStatus()
    {
        return dataStatus;
    }
    public void setExamineSuccessDate(String examineSuccessDate)
    {
        this.examineSuccessDate = examineSuccessDate;
    }

    public String getExamineSuccessDate()
    {
        return examineSuccessDate;
    }
    public void setExternalMerchantName(String externalMerchantName)
    {
        this.externalMerchantName = externalMerchantName;
    }

    public String getExternalMerchantName()
    {
        return externalMerchantName;
    }
    public void setExternalMerchantNo(String externalMerchantNo)
    {
        this.externalMerchantNo = externalMerchantNo;
    }

    public String getExternalMerchantNo()
    {
        return externalMerchantNo;
    }
    public void setFileBatchNo(String fileBatchNo)
    {
        this.fileBatchNo = fileBatchNo;
    }

    public String getFileBatchNo()
    {
        return fileBatchNo;
    }
    public void setHeadId(String headId)
    {
        this.headId = headId;
    }

    public String getHeadId()
    {
        return headId;
    }
    public void setIdCard(String idCard)
    {
        this.idCard = idCard;
    }

    public String getIdCard()
    {
        return idCard;
    }
    public void setIdCardType(String idCardType)
    {
        this.idCardType = idCardType;
    }

    public String getIdCardType()
    {
        return idCardType;
    }
    public void setInterSystemTranAccount(String interSystemTranAccount)
    {
        this.interSystemTranAccount = interSystemTranAccount;
    }

    public String getInterSystemTranAccount()
    {
        return interSystemTranAccount;
    }
    public void setLastUpdatedBy(String lastUpdatedBy)
    {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public String getLastUpdatedBy()
    {
        return lastUpdatedBy;
    }
    public void setLastUpdatedDate(Date lastUpdatedDate)
    {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public Date getLastUpdatedDate()
    {
        return lastUpdatedDate;
    }
    public void setMccCode(String mccCode)
    {
        this.mccCode = mccCode;
    }

    public String getMccCode()
    {
        return mccCode;
    }
    public void setMerchantAcctOpenContEmail(String merchantAcctOpenContEmail)
    {
        this.merchantAcctOpenContEmail = merchantAcctOpenContEmail;
    }

    public String getMerchantAcctOpenContEmail()
    {
        return merchantAcctOpenContEmail;
    }
    public void setMerchantAcctOpenContName(String merchantAcctOpenContName)
    {
        this.merchantAcctOpenContName = merchantAcctOpenContName;
    }

    public String getMerchantAcctOpenContName()
    {
        return merchantAcctOpenContName;
    }
    public void setMerchantAcctOpenContPhone(String merchantAcctOpenContPhone)
    {
        this.merchantAcctOpenContPhone = merchantAcctOpenContPhone;
    }

    public String getMerchantAcctOpenContPhone()
    {
        return merchantAcctOpenContPhone;
    }
    public void setMerchantAttributes(String merchantAttributes)
    {
        this.merchantAttributes = merchantAttributes;
    }

    public String getMerchantAttributes()
    {
        return merchantAttributes;
    }
    public void setMerchantCallbackurl(String merchantCallbackurl)
    {
        this.merchantCallbackurl = merchantCallbackurl;
    }

    public String getMerchantCallbackurl()
    {
        return merchantCallbackurl;
    }
    public void setMerchantChainType(String merchantChainType)
    {
        this.merchantChainType = merchantChainType;
    }

    public String getMerchantChainType()
    {
        return merchantChainType;
    }
    public void setMerchantFullName(String merchantFullName)
    {
        this.merchantFullName = merchantFullName;
    }

    public String getMerchantFullName()
    {
        return merchantFullName;
    }
    public void setMerchantId(String merchantId)
    {
        this.merchantId = merchantId;
    }

    public String getMerchantId()
    {
        return merchantId;
    }
    public void setMerchantLevel(String merchantLevel)
    {
        this.merchantLevel = merchantLevel;
    }

    public String getMerchantLevel()
    {
        return merchantLevel;
    }
    public void setMerchantOpenTranReturnCode(String merchantOpenTranReturnCode)
    {
        this.merchantOpenTranReturnCode = merchantOpenTranReturnCode;
    }

    public String getMerchantOpenTranReturnCode()
    {
        return merchantOpenTranReturnCode;
    }
    public void setMerchantOpenTranReturnMsg(String merchantOpenTranReturnMsg)
    {
        this.merchantOpenTranReturnMsg = merchantOpenTranReturnMsg;
    }

    public String getMerchantOpenTranReturnMsg()
    {
        return merchantOpenTranReturnMsg;
    }
    public void setMerchantRegisterChannels(String merchantRegisterChannels)
    {
        this.merchantRegisterChannels = merchantRegisterChannels;
    }

    public String getMerchantRegisterChannels()
    {
        return merchantRegisterChannels;
    }
    public void setMerchantResponsiblePerson(String merchantResponsiblePerson)
    {
        this.merchantResponsiblePerson = merchantResponsiblePerson;
    }

    public String getMerchantResponsiblePerson()
    {
        return merchantResponsiblePerson;
    }
    public void setMerchantSettlementSign(String merchantSettlementSign)
    {
        this.merchantSettlementSign = merchantSettlementSign;
    }

    public String getMerchantSettlementSign()
    {
        return merchantSettlementSign;
    }
    public void setMerchantShortlName(String merchantShortlName)
    {
        this.merchantShortlName = merchantShortlName;
    }

    public String getMerchantShortlName()
    {
        return merchantShortlName;
    }
    public void setMerchantType(String merchantType)
    {
        this.merchantType = merchantType;
    }

    public String getMerchantType()
    {
        return merchantType;
    }
    public void setMiDengLiuShui(String miDengLiuShui)
    {
        this.miDengLiuShui = miDengLiuShui;
    }

    public String getMiDengLiuShui()
    {
        return miDengLiuShui;
    }
    public void setMinimumSettlementAmount(String minimumSettlementAmount)
    {
        this.minimumSettlementAmount = minimumSettlementAmount;
    }

    public String getMinimumSettlementAmount()
    {
        return minimumSettlementAmount;
    }
    public void setMobileInternationalAreaCode(String mobileInternationalAreaCode)
    {
        this.mobileInternationalAreaCode = mobileInternationalAreaCode;
    }

    public String getMobileInternationalAreaCode()
    {
        return mobileInternationalAreaCode;
    }
    public void setMobileNumber(String mobileNumber)
    {
        this.mobileNumber = mobileNumber;
    }

    public String getMobileNumber()
    {
        return mobileNumber;
    }
    public void setMobileNumberCityAreaCode(String mobileNumberCityAreaCode)
    {
        this.mobileNumberCityAreaCode = mobileNumberCityAreaCode;
    }

    public String getMobileNumberCityAreaCode()
    {
        return mobileNumberCityAreaCode;
    }
    public void setNickName(String nickName)
    {
        this.nickName = nickName;
    }

    public String getNickName()
    {
        return nickName;
    }
    public void setOrgId(String orgId)
    {
        this.orgId = orgId;
    }

    public String getOrgId()
    {
        return orgId;
    }
    public void setPaperContractNo(String paperContractNo)
    {
        this.paperContractNo = paperContractNo;
    }

    public String getPaperContractNo()
    {
        return paperContractNo;
    }
    public void setPaymentTime(String paymentTime)
    {
        this.paymentTime = paymentTime;
    }

    public String getPaymentTime()
    {
        return paymentTime;
    }
    public void setPbocMerchantNo(String pbocMerchantNo)
    {
        this.pbocMerchantNo = pbocMerchantNo;
    }

    public String getPbocMerchantNo()
    {
        return pbocMerchantNo;
    }
    public void setProvinceCode(String provinceCode)
    {
        this.provinceCode = provinceCode;
    }

    public String getProvinceCode()
    {
        return provinceCode;
    }
    public void setProvinceOfUser(String provinceOfUser)
    {
        this.provinceOfUser = provinceOfUser;
    }

    public String getProvinceOfUser()
    {
        return provinceOfUser;
    }
    public void setReceivePaymentBankCode(String receivePaymentBankCode)
    {
        this.receivePaymentBankCode = receivePaymentBankCode;
    }

    public String getReceivePaymentBankCode()
    {
        return receivePaymentBankCode;
    }
    public void setReceivingBankAccount(String receivingBankAccount)
    {
        this.receivingBankAccount = receivingBankAccount;
    }

    public String getReceivingBankAccount()
    {
        return receivingBankAccount;
    }
    public void setReceivingBankName(String receivingBankName)
    {
        this.receivingBankName = receivingBankName;
    }

    public String getReceivingBankName()
    {
        return receivingBankName;
    }
    public void setRedundantInfo(String redundantInfo)
    {
        this.redundantInfo = redundantInfo;
    }

    public String getRedundantInfo()
    {
        return redundantInfo;
    }
    public void setRemarkInfo(String remarkInfo)
    {
        this.remarkInfo = remarkInfo;
    }

    public String getRemarkInfo()
    {
        return remarkInfo;
    }
    public void setReservedInfo(String reservedInfo)
    {
        this.reservedInfo = reservedInfo;
    }

    public String getReservedInfo()
    {
        return reservedInfo;
    }
    public void setReturnCode(String returnCode)
    {
        this.returnCode = returnCode;
    }

    public String getReturnCode()
    {
        return returnCode;
    }
    public void setReturnMsg(String returnMsg)
    {
        this.returnMsg = returnMsg;
    }

    public String getReturnMsg()
    {
        return returnMsg;
    }
    public void setSequenceNumber(Long sequenceNumber)
    {
        this.sequenceNumber = sequenceNumber;
    }

    public Long getSequenceNumber()
    {
        return sequenceNumber;
    }
    public void setSettleCycle(String settleCycle)
    {
        this.settleCycle = settleCycle;
    }

    public String getSettleCycle()
    {
        return settleCycle;
    }
    public void setSettleCycleType(String settleCycleType)
    {
        this.settleCycleType = settleCycleType;
    }

    public String getSettleCycleType()
    {
        return settleCycleType;
    }
    public void setSettleTreatmentMethod(String settleTreatmentMethod)
    {
        this.settleTreatmentMethod = settleTreatmentMethod;
    }

    public String getSettleTreatmentMethod()
    {
        return settleTreatmentMethod;
    }
    public void setSettlementType(String settlementType)
    {
        this.settlementType = settlementType;
    }

    public String getSettlementType()
    {
        return settlementType;
    }
    public void setSignatory(String signatory)
    {
        this.signatory = signatory;
    }

    public String getSignatory()
    {
        return signatory;
    }
    public void setStaffId(String staffId)
    {
        this.staffId = staffId;
    }

    public String getStaffId()
    {
        return staffId;
    }
    public void setStaffName(String staffName)
    {
        this.staffName = staffName;
    }

    public String getStaffName()
    {
        return staffName;
    }
    public void setSupAgreeWithholding(String supAgreeWithholding)
    {
        this.supAgreeWithholding = supAgreeWithholding;
    }

    public String getSupAgreeWithholding()
    {
        return supAgreeWithholding;
    }
    public void setSystemType(String systemType)
    {
        this.systemType = systemType;
    }

    public String getSystemType()
    {
        return systemType;
    }
    public void setTellerNo(String tellerNo)
    {
        this.tellerNo = tellerNo;
    }

    public String getTellerNo()
    {
        return tellerNo;
    }
    public void setTradingOrganizationNo(String tradingOrganizationNo)
    {
        this.tradingOrganizationNo = tradingOrganizationNo;
    }

    public String getTradingOrganizationNo()
    {
        return tradingOrganizationNo;
    }
    public void setTransactionCode(String transactionCode)
    {
        this.transactionCode = transactionCode;
    }

    public String getTransactionCode()
    {
        return transactionCode;
    }
    public void setUpperLevelMerchantNo(String upperLevelMerchantNo)
    {
        this.upperLevelMerchantNo = upperLevelMerchantNo;
    }

    public String getUpperLevelMerchantNo()
    {
        return upperLevelMerchantNo;
    }
    public void setUserLabel(String userLabel)
    {
        this.userLabel = userLabel;
    }

    public String getUserLabel()
    {
        return userLabel;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setWalletId(String walletId)
    {
        this.walletId = walletId;
    }

    public String getWalletId()
    {
        return walletId;
    }
    public void setContractExpireDate(String contractExpireDate)
    {
        this.contractExpireDate = contractExpireDate;
    }

    public String getContractExpireDate()
    {
        return contractExpireDate;
    }
    public void setMerchantCallbackUrl(String merchantCallbackUrl)
    {
        this.merchantCallbackUrl = merchantCallbackUrl;
    }

    public String getMerchantCallbackUrl()
    {
        return merchantCallbackUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("accessId", getAccessId())
                .append("accountManager", getAccountManager())
                .append("accountName", getAccountName())
                .append("accountOpeningCityCode", getAccountOpeningCityCode())
                .append("accountOpeningProvinceCode", getAccountOpeningProvinceCode())
                .append("accountType", getAccountType())
                .append("agencyCode", getAgencyCode())
                .append("alterationType", getAlterationType())
                .append("areaOfUser", getAreaOfUser())
                .append("bankBranchName", getBankBranchName())
                .append("bankBranchNo", getBankBranchNo())
                .append("batchNo", getBatchNo())
                .append("businessAddress", getBusinessAddress())
                .append("cityCode", getCityCode())
                .append("clientType", getClientType())
                .append("cnapsNo", getCnapsNo())
                .append("contactEmail", getContactEmail())
                .append("contactwayType", getContactwayType())
                .append("contractEffectiveDate", getContractEffectiveDate())
                .append("countryOfUser", getCountryOfUser())
                .append("createdBycreatedBy", getCreatedBycreatedBy())
                .append("createdDate", getCreatedDate())
                .append("dataStatus", getDataStatus())
                .append("examineSuccessDate", getExamineSuccessDate())
                .append("externalMerchantName", getExternalMerchantName())
                .append("externalMerchantNo", getExternalMerchantNo())
                .append("fileBatchNo", getFileBatchNo())
                .append("headId", getHeadId())
                .append("idCard", getIdCard())
                .append("idCardType", getIdCardType())
                .append("interSystemTranAccount", getInterSystemTranAccount())
                .append("lastUpdatedBy", getLastUpdatedBy())
                .append("lastUpdatedDate", getLastUpdatedDate())
                .append("mccCode", getMccCode())
                .append("merchantAcctOpenContEmail", getMerchantAcctOpenContEmail())
                .append("merchantAcctOpenContName", getMerchantAcctOpenContName())
                .append("merchantAcctOpenContPhone", getMerchantAcctOpenContPhone())
                .append("merchantAttributes", getMerchantAttributes())
                .append("merchantCallbackurl", getMerchantCallbackurl())
                .append("merchantChainType", getMerchantChainType())
                .append("merchantFullName", getMerchantFullName())
                .append("merchantId", getMerchantId())
                .append("merchantLevel", getMerchantLevel())
                .append("merchantOpenTranReturnCode", getMerchantOpenTranReturnCode())
                .append("merchantOpenTranReturnMsg", getMerchantOpenTranReturnMsg())
                .append("merchantRegisterChannels", getMerchantRegisterChannels())
                .append("merchantResponsiblePerson", getMerchantResponsiblePerson())
                .append("merchantSettlementSign", getMerchantSettlementSign())
                .append("merchantShortlName", getMerchantShortlName())
                .append("merchantType", getMerchantType())
                .append("miDengLiuShui", getMiDengLiuShui())
                .append("minimumSettlementAmount", getMinimumSettlementAmount())
                .append("mobileInternationalAreaCode", getMobileInternationalAreaCode())
                .append("mobileNumber", getMobileNumber())
                .append("mobileNumberCityAreaCode", getMobileNumberCityAreaCode())
                .append("nickName", getNickName())
                .append("orgId", getOrgId())
                .append("paperContractNo", getPaperContractNo())
                .append("paymentTime", getPaymentTime())
                .append("pbocMerchantNo", getPbocMerchantNo())
                .append("provinceCode", getProvinceCode())
                .append("provinceOfUser", getProvinceOfUser())
                .append("receivePaymentBankCode", getReceivePaymentBankCode())
                .append("receivingBankAccount", getReceivingBankAccount())
                .append("receivingBankName", getReceivingBankName())
                .append("redundantInfo", getRedundantInfo())
                .append("remarkInfo", getRemarkInfo())
                .append("reservedInfo", getReservedInfo())
                .append("returnCode", getReturnCode())
                .append("returnMsg", getReturnMsg())
                .append("sequenceNumber", getSequenceNumber())
                .append("settleCycle", getSettleCycle())
                .append("settleCycleType", getSettleCycleType())
                .append("settleTreatmentMethod", getSettleTreatmentMethod())
                .append("settlementType", getSettlementType())
                .append("signatory", getSignatory())
                .append("staffId", getStaffId())
                .append("staffName", getStaffName())
                .append("supAgreeWithholding", getSupAgreeWithholding())
                .append("systemType", getSystemType())
                .append("tellerNo", getTellerNo())
                .append("tradingOrganizationNo", getTradingOrganizationNo())
                .append("transactionCode", getTransactionCode())
                .append("upperLevelMerchantNo", getUpperLevelMerchantNo())
                .append("userLabel", getUserLabel())
                .append("userName", getUserName())
                .append("walletId", getWalletId())
                .append("contractExpireDate", getContractExpireDate())
                .append("merchantCallbackUrl", getMerchantCallbackUrl())
                .toString();
    }
}

