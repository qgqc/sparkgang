package com.gcl.personalwalletmanagement.controller;



import java.util.ArrayList;
import java.util.List;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.personalwalletmanagement.domain.BusPrivateInfo;
import com.gcl.personalwalletmanagement.enums.PriWalletDataStatus;
import com.gcl.personalwalletmanagement.service.IBusPrivateInfoService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 个人钱包Controller
 *
 * @author ruoyi
 * @date 2022-01-13
 */
@RestController
@RequestMapping("/private/info")
public class BusPrivateInfoController extends BaseController
{
    @Autowired
    private IBusPrivateInfoService busPrivateInfoService;

    /**
     * 查询个人钱包列表
     */
    @PreAuthorize("@ss.hasPermi('private:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusPrivateInfo busPrivateInfo)
    {
        startPage();
        List<BusPrivateInfo> list = busPrivateInfoService.selectBusPrivateInfoList(busPrivateInfo);
        return getDataTable(list);
    }
    @PreAuthorize("@ss.hasPermi('private:info:reviewList')")
    @GetMapping("/reviewList")
    public TableDataInfo reviewList(BusPrivateInfo busPrivateInfo)
    {
        startPage();
        List<BusPrivateInfo> list = busPrivateInfoService.selectBusPrivateInfoList(busPrivateInfo);
        ArrayList<BusPrivateInfo> objects = new ArrayList<>();
        for (BusPrivateInfo busPrivateInfo1:list) {
            if(busPrivateInfo1.getDataStatus().equals(PriWalletDataStatus.SUBMIT_REVIEW.getStatus())){
                objects.add(busPrivateInfo1);
            }
        }
        return getDataTable(objects);
    }
    /**
     * 导出个人钱包列表
     */
    @PreAuthorize("@ss.hasPermi('private:info:export')")
    @Log(title = "个人钱包", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BusPrivateInfo busPrivateInfo)
    {
        List<BusPrivateInfo> list = busPrivateInfoService.selectBusPrivateInfoList(busPrivateInfo);
        ExcelUtil<BusPrivateInfo> util = new ExcelUtil<BusPrivateInfo>(BusPrivateInfo.class);
        return util.exportExcel(list, "个人钱包数据");
    }

    /**
     * 获取个人钱包详细信息
     */
    @PreAuthorize("@ss.hasPermi('private:info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(busPrivateInfoService.selectBusPrivateInfoById(id));
    }

    /**
     * 新增个人钱包
     */
    @PreAuthorize("@ss.hasPermi('private:info:add')")
    @Log(title = "个人钱包", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusPrivateInfo busPrivateInfo)
    {
        return toAjax(busPrivateInfoService.insertBusPrivateInfo(busPrivateInfo));
    }

    /**
     * 修改个人钱包
     */
    @PreAuthorize("@ss.hasPermi('private:info:edit')")
    @Log(title = "个人钱包", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusPrivateInfo busPrivateInfo)
    {
        return toAjax(busPrivateInfoService.updateBusPrivateInfo(busPrivateInfo));
    }

    /**
     * 修改状态
     */
    @PreAuthorize("@ss.hasPermi('private:info:submitAudit')")
    @Log(title = "个人钱包", businessType = BusinessType.UPDATE)
    @DeleteMapping("/submitAudit/{idss}")
    public AjaxResult submitAudit(@PathVariable String[] idss)
    {
        return toAjax(busPrivateInfoService.updateDataStatus(idss));
    }

    /**
     * 修改状态
     */
    @PreAuthorize("@ss.hasPermi('private:info:auditSuccess')")
    @Log(title = "个人钱包", businessType = BusinessType.UPDATE)
    @DeleteMapping("/auditSuccess/{idss}")
    public AjaxResult auditSuccess(@PathVariable String[] idss)
    {
        return toAjax(busPrivateInfoService.updateDataStatusReview(idss));
    }
    /**
     * 删除个人钱包
     */
    @PreAuthorize("@ss.hasPermi('private:info:remove')")
    @Log(title = "个人钱包", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(busPrivateInfoService.deleteBusPrivateInfoByIds(ids));
    }





}
