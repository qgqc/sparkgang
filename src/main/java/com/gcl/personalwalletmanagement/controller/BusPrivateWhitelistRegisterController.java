package com.gcl.personalwalletmanagement.controller;

import java.util.List;
import java.util.UUID;

import com.gcl.personalwalletmanagement.domain.BusPrivateWhitelistRegister;
import com.gcl.personalwalletmanagement.service.IBusPrivateWhitelistRegisterService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;

/**
 * 钱包注册白名单信息Controller
 * 
 * @author wjw
 * @date 2022-02-07
 */
@RestController
@RequestMapping("/redenvelopemanagement/register")
public class BusPrivateWhitelistRegisterController extends BaseController
{
    @Autowired
    private IBusPrivateWhitelistRegisterService busPrivateWhitelistRegisterService;

    /**
     * 查询钱包注册白名单信息列表
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:register:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusPrivateWhitelistRegister busPrivateWhitelistRegister)
    {
        startPage();
        List<BusPrivateWhitelistRegister> list = busPrivateWhitelistRegisterService.selectBusPrivateWhitelistRegisterList(busPrivateWhitelistRegister);
        return getDataTable(list);
    }

    /**
     * 导出钱包注册白名单信息列表
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:register:export')")
    @Log(title = "钱包注册白名单信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BusPrivateWhitelistRegister busPrivateWhitelistRegister)
    {
        List<BusPrivateWhitelistRegister> list = busPrivateWhitelistRegisterService.selectBusPrivateWhitelistRegisterList(busPrivateWhitelistRegister);
        ExcelUtil<BusPrivateWhitelistRegister> util = new ExcelUtil<BusPrivateWhitelistRegister>(BusPrivateWhitelistRegister.class);
        return util.exportExcel(list, "钱包注册白名单信息数据");
    }

    /**
     * 获取钱包注册白名单信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:register:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(busPrivateWhitelistRegisterService.selectBusPrivateWhitelistRegisterById(id));
    }

    /**
     * 新增钱包注册白名单信息
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:register:add')")
    @Log(title = "钱包注册白名单信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusPrivateWhitelistRegister busPrivateWhitelistRegister)
    {
        String id = UUID.randomUUID().toString().replace("-", "");
        busPrivateWhitelistRegister.setId(id);
        return toAjax(busPrivateWhitelistRegisterService.insertBusPrivateWhitelistRegister(busPrivateWhitelistRegister));
    }

    /**
     * 修改钱包注册白名单信息
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:register:edit')")
    @Log(title = "钱包注册白名单信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusPrivateWhitelistRegister busPrivateWhitelistRegister)
    {
        return toAjax(busPrivateWhitelistRegisterService.updateBusPrivateWhitelistRegister(busPrivateWhitelistRegister));
    }

    /**
     * 删除钱包注册白名单信息
     */
    @PreAuthorize("@ss.hasPermi('redenvelopemanagement:register:remove')")
    @Log(title = "钱包注册白名单信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(busPrivateWhitelistRegisterService.deleteBusPrivateWhitelistRegisterByIds(ids));
    }
}
