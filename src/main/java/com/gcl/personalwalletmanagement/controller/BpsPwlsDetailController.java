package com.gcl.personalwalletmanagement.controller;

import java.util.List;
import java.util.UUID;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.personalwalletmanagement.domain.BpsPwlsDetail;
import com.gcl.personalwalletmanagement.service.IBpsPwlsDetailService;

/**
 * 人行共建APP白名单批量文件明细Controller
 * 
 * @author yada
 * @date 2022-02-09
 */
@RestController
@RequestMapping("/personalwalletmanagement/BpsPwlsDetail")
public class BpsPwlsDetailController extends BaseController
{
    @Autowired
    private IBpsPwlsDetailService bpsPwlsDetailService;

    /**
     * 查询人行共建APP白名单批量文件明细列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPwlsDetail:list')")
    @GetMapping("/list")
    public TableDataInfo list(BpsPwlsDetail bpsPwlsDetail)
    {
        startPage();
        List<BpsPwlsDetail> list = bpsPwlsDetailService.selectBpsPwlsDetailList(bpsPwlsDetail);
        return getDataTable(list);
    }

    /**
     * 导出人行共建APP白名单批量文件明细列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPwlsDetail:export')")
    @Log(title = "人行共建APP白名单批量文件明细", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BpsPwlsDetail bpsPwlsDetail)
    {
        List<BpsPwlsDetail> list = bpsPwlsDetailService.selectBpsPwlsDetailList(bpsPwlsDetail);
        ExcelUtil<BpsPwlsDetail> util = new ExcelUtil<BpsPwlsDetail>(BpsPwlsDetail.class);
        return util.exportExcel(list, "人行共建APP白名单批量文件明细数据");
    }

    /**
     * 获取人行共建APP白名单批量文件明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPwlsDetail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(bpsPwlsDetailService.selectBpsPwlsDetailById(id));
    }

    /**
     * 新增人行共建APP白名单批量文件明细
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPwlsDetail:add')")
    @Log(title = "人行共建APP白名单批量文件明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BpsPwlsDetail bpsPwlsDetail)
    {
        String id = UUID.randomUUID().toString().replace("-", "");
        bpsPwlsDetail.setId(id);
        return toAjax(bpsPwlsDetailService.insertBpsPwlsDetail(bpsPwlsDetail));
    }

    /**
     * 修改人行共建APP白名单批量文件明细
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPwlsDetail:edit')")
    @Log(title = "人行共建APP白名单批量文件明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BpsPwlsDetail bpsPwlsDetail)
    {
        return toAjax(bpsPwlsDetailService.updateBpsPwlsDetail(bpsPwlsDetail));
    }

    /**
     * 删除人行共建APP白名单批量文件明细
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPwlsDetail:remove')")
    @Log(title = "人行共建APP白名单批量文件明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(bpsPwlsDetailService.deleteBpsPwlsDetailByIds(ids));
    }
}
