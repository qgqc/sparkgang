package com.gcl.personalwalletmanagement.controller;

import java.util.List;
import java.util.UUID;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.personalwalletmanagement.domain.BpsPwlsHead;
import com.gcl.personalwalletmanagement.service.IBpsPwlsHeadService;

/**
 * 人行共建APP白名单批量文件头Controller
 * 
 * @author yada
 * @date 2022-02-09
 */
@RestController
@RequestMapping("/personalwalletmanagement/BpsPwlsHead")
public class BpsPwlsHeadController extends BaseController
{
    @Autowired
    private IBpsPwlsHeadService bpsPwlsHeadService;

    /**
     * 查询人行共建APP白名单批量文件头列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPwlsHead:list')")
    @GetMapping("/list")
    public TableDataInfo list(BpsPwlsHead bpsPwlsHead)
    {
        startPage();
        List<BpsPwlsHead> list = bpsPwlsHeadService.selectBpsPwlsHeadList(bpsPwlsHead);
        return getDataTable(list);
    }

    /**
     * 导出人行共建APP白名单批量文件头列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPwlsHead:export')")
    @Log(title = "人行共建APP白名单批量文件头", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BpsPwlsHead bpsPwlsHead)
    {
        List<BpsPwlsHead> list = bpsPwlsHeadService.selectBpsPwlsHeadList(bpsPwlsHead);
        ExcelUtil<BpsPwlsHead> util = new ExcelUtil<BpsPwlsHead>(BpsPwlsHead.class);
        return util.exportExcel(list, "人行共建APP白名单批量文件头数据");
    }

    /**
     * 获取人行共建APP白名单批量文件头详细信息
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPwlsHead:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(bpsPwlsHeadService.selectBpsPwlsHeadById(id));
    }

    /**
     * 新增人行共建APP白名单批量文件头
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPwlsHead:add')")
    @Log(title = "人行共建APP白名单批量文件头", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BpsPwlsHead bpsPwlsHead)
    {
        String id = UUID.randomUUID().toString().replace("-", "");
        bpsPwlsHead.setId(id);
        return toAjax(bpsPwlsHeadService.insertBpsPwlsHead(bpsPwlsHead));
    }

    /**
     * 修改人行共建APP白名单批量文件头
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPwlsHead:edit')")
    @Log(title = "人行共建APP白名单批量文件头", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BpsPwlsHead bpsPwlsHead)
    {
        return toAjax(bpsPwlsHeadService.updateBpsPwlsHead(bpsPwlsHead));
    }

    /**
     * 删除人行共建APP白名单批量文件头
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPwlsHead:remove')")
    @Log(title = "人行共建APP白名单批量文件头", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(bpsPwlsHeadService.deleteBpsPwlsHeadByIds(ids));
    }
}
