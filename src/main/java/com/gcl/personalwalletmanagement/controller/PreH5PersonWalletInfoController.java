package com.gcl.personalwalletmanagement.controller;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.personalwalletmanagement.domain.PreH5PersonWalletInfo;
import com.gcl.personalwalletmanagement.service.IPreH5PersonWalletInfoService;

/**
 * H5个人信息收集报表Controller
 * 
 * @author yada
 * @date 2022-02-25
 */
@RestController
@RequestMapping("/personalwalletmanagement/PreH5PersonWalletInfo")
public class PreH5PersonWalletInfoController extends BaseController
{
    @Autowired
    private IPreH5PersonWalletInfoService preH5PersonWalletInfoService;

    /**
     * 查询H5个人信息收集报表列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:PreH5PersonWalletInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(PreH5PersonWalletInfo preH5PersonWalletInfo)
    {
        startPage();
        List<PreH5PersonWalletInfo> list = preH5PersonWalletInfoService.selectPreH5PersonWalletInfoList(preH5PersonWalletInfo);
        return getDataTable(list);
    }

    /**
     * 导出H5个人信息收集报表列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:PreH5PersonWalletInfo:export')")
    @Log(title = "H5个人信息收集报表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(PreH5PersonWalletInfo preH5PersonWalletInfo)
    {
        List<PreH5PersonWalletInfo> list = preH5PersonWalletInfoService.selectPreH5PersonWalletInfoList(preH5PersonWalletInfo);
        ExcelUtil<PreH5PersonWalletInfo> util = new ExcelUtil<PreH5PersonWalletInfo>(PreH5PersonWalletInfo.class);
        return util.exportExcel(list, "H5个人信息收集报表数据");
    }

    /**
     * 获取H5个人信息收集报表详细信息
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:PreH5PersonWalletInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(preH5PersonWalletInfoService.selectPreH5PersonWalletInfoById(id));
    }

    /**
     * 新增H5个人信息收集报表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:PreH5PersonWalletInfo:add')")
    @Log(title = "H5个人信息收集报表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PreH5PersonWalletInfo preH5PersonWalletInfo)
    {
        String id = UUID.randomUUID().toString().replace("-", "");
        preH5PersonWalletInfo.setId(id);
        preH5PersonWalletInfo.setCreatedDate(new Date());
        return toAjax(preH5PersonWalletInfoService.insertPreH5PersonWalletInfo(preH5PersonWalletInfo));
    }

    /**
     * 修改H5个人信息收集报表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:PreH5PersonWalletInfo:edit')")
    @Log(title = "H5个人信息收集报表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PreH5PersonWalletInfo preH5PersonWalletInfo)
    {
        return toAjax(preH5PersonWalletInfoService.updatePreH5PersonWalletInfo(preH5PersonWalletInfo));
    }

    /**
     * 删除H5个人信息收集报表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:PreH5PersonWalletInfo:remove')")
    @Log(title = "H5个人信息收集报表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(preH5PersonWalletInfoService.deletePreH5PersonWalletInfoByIds(ids));
    }
}
