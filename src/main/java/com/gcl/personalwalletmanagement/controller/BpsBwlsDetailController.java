package com.gcl.personalwalletmanagement.controller;

import java.util.List;
import java.util.UUID;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.personalwalletmanagement.domain.BpsBwlsDetail;
import com.gcl.personalwalletmanagement.service.IBpsBwlsDetailService;

/**
 * 对私批量注册白名单文件明细Controller
 * 
 * @author yada
 * @date 2022-02-15
 */
@RestController
@RequestMapping("/personalwalletmanagement/BpsBwlsDetail")
public class BpsBwlsDetailController extends BaseController
{
    @Autowired
    private IBpsBwlsDetailService bpsBwlsDetailService;

    /**
     * 查询对私批量注册白名单文件明细列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsBwlsDetail:list')")
    @GetMapping("/list")
    public TableDataInfo list(BpsBwlsDetail bpsBwlsDetail)
    {
        startPage();
        List<BpsBwlsDetail> list = bpsBwlsDetailService.selectBpsBwlsDetailList(bpsBwlsDetail);
        return getDataTable(list);
    }

    /**
     * 导出对私批量注册白名单文件明细列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsBwlsDetail:export')")
    @Log(title = "对私批量注册白名单文件明细", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BpsBwlsDetail bpsBwlsDetail)
    {
        List<BpsBwlsDetail> list = bpsBwlsDetailService.selectBpsBwlsDetailList(bpsBwlsDetail);
        ExcelUtil<BpsBwlsDetail> util = new ExcelUtil<BpsBwlsDetail>(BpsBwlsDetail.class);
        return util.exportExcel(list, "对私批量注册白名单文件明细数据");
    }

    /**
     * 获取对私批量注册白名单文件明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsBwlsDetail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(bpsBwlsDetailService.selectBpsBwlsDetailById(id));
    }

    /**
     * 新增对私批量注册白名单文件明细
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsBwlsDetail:add')")
    @Log(title = "对私批量注册白名单文件明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BpsBwlsDetail bpsBwlsDetail)
    {
        String id = UUID.randomUUID().toString().replace("-", "");
        bpsBwlsDetail.setId(id);
        return toAjax(bpsBwlsDetailService.insertBpsBwlsDetail(bpsBwlsDetail));
    }

    /**
     * 修改对私批量注册白名单文件明细
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsBwlsDetail:edit')")
    @Log(title = "对私批量注册白名单文件明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BpsBwlsDetail bpsBwlsDetail)
    {
        return toAjax(bpsBwlsDetailService.updateBpsBwlsDetail(bpsBwlsDetail));
    }

    /**
     * 删除对私批量注册白名单文件明细
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsBwlsDetail:remove')")
    @Log(title = "对私批量注册白名单文件明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(bpsBwlsDetailService.deleteBpsBwlsDetailByIds(ids));
    }
}
