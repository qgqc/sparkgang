package com.gcl.personalwalletmanagement.controller;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.personalwalletmanagement.domain.BusPersonInfo;
import com.gcl.personalwalletmanagement.service.IBusPersonInfoService;
import com.gcl.system.service.ISysConfigService;

/**
 * 个人信息导入Controller
 * 
 * @author yada
 * @date 2022-01-18
 */
@RestController
@RequestMapping("/personalwalletmanagement/BusPersonInfo")
public class BusPersonInfoController extends BaseController
{
    @Autowired
    private IBusPersonInfoService busPersonInfoService;
    //注入参数设置
    @Autowired
    private ISysConfigService sysConfigService;

    /**
     * 查询个人信息导入列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BusPersonInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(BusPersonInfo busPersonInfo)
    {
        startPage();
        List<BusPersonInfo> list = busPersonInfoService.selectBusPersonInfoList(busPersonInfo);
        return getDataTable(list);
    }

    /**
     * 导出个人信息导入列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BusPersonInfo:export')")
    @Log(title = "个人信息导入", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BusPersonInfo busPersonInfo)
    {
        List<BusPersonInfo> list = busPersonInfoService.selectBusPersonInfoList(busPersonInfo);
        ExcelUtil<BusPersonInfo> util = new ExcelUtil<BusPersonInfo>(BusPersonInfo.class);
        return util.exportExcel(list, "个人信息导入数据");
    }

    /**
     * 获取个人信息导入详细信息
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BusPersonInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(busPersonInfoService.selectBusPersonInfoById(id));
    }

    /**
     * 新增个人信息导入
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BusPersonInfo:add')")
    @Log(title = "个人信息导入", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BusPersonInfo busPersonInfo)
    {
        String id = UUID.randomUUID().toString().replace("-", "");
        busPersonInfo.setId(id);
        //插入固定值过程
        //手机国际区号 bus.person.info.mobileInternationalAreaCode 86
        busPersonInfo.setMobileInternationalAreaCode(sysConfigService.selectConfigByKey("bus.person.info.mobileInternationalAreaCode"));
        //用户所属国家编码  bus.person.info.countryOfUser CN
        busPersonInfo.setCountryOfUser(sysConfigService.selectConfigByKey("bus.person.info.countryOfUser"));
        //钱包昵称 bus.person.info.nickName 前四位 中国银行
        busPersonInfo.setNickName(sysConfigService.selectConfigByKey("bus.person.info.nickName")+busPersonInfo.getMobileNumber().substring(busPersonInfo.getMobileNumber().length()-4));
        //客户类型 bus.person.info.clientType 00
        busPersonInfo.setClientType(sysConfigService.selectConfigByKey("bus.person.info.clientType"));
        //联系方式类型  bus.person.info.contactwayType 00
        busPersonInfo.setContactwayType(sysConfigService.selectConfigByKey("bus.person.info.contactwayType"));
        //变更类型  bus.person.info.alterationType CC00
        busPersonInfo.setAlterationType(sysConfigService.selectConfigByKey("bus.person.info.alterationType"));
        busPersonInfo.setCreateBy(getUsername());
        busPersonInfo.setCreateTime(new Date());
        busPersonInfo.setUpdateBy(getUsername());
        busPersonInfo.setUpdateTime(new Date());
        return toAjax(busPersonInfoService.insertBusPersonInfo(busPersonInfo));
    }
    
    @Log(title = "个人信息导入数据", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BusPersonInfo:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<BusPersonInfo> util = new ExcelUtil<BusPersonInfo>(BusPersonInfo.class);
        List<BusPersonInfo> busPersonInfoList = util.importExcel(file.getInputStream());
        String operName = getUsername();
        String message = busPersonInfoService.importBusPersonInfo(busPersonInfoList, updateSupport, operName);
        return AjaxResult.success(message);
    }

    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<BusPersonInfo> util = new ExcelUtil<BusPersonInfo>(BusPersonInfo.class);
        util.importTemplateExcel(response, "个人信息导入数据");
    }

    /**
     * 修改个人信息
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BusPersonInfo:edit')")
    @Log(title = "个人信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BusPersonInfo busPersonInfo)
    {
        return toAjax(busPersonInfoService.updateBusPersonInfo(busPersonInfo));
    }

    /**
     * 删除个人信息导入
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BusPersonInfo:remove')")
    @Log(title = "个人信息导入", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(busPersonInfoService.deleteBusPersonInfoByIds(ids));
    }	
    /**
     * 批量提交审核 个人信息导入
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BusPersonInfo:submitAudit')")
    @Log(title = "个人信息导入", businessType = BusinessType.SUBMITAUDIT)
    @PutMapping("/submitAudit/{ids}")
    public AjaxResult submitAudit(@PathVariable String[] ids)
    {
    	return toAjax(busPersonInfoService.submitAudit(ids));
    }
    /**
     * 批量审核通过
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BusPersonInfo:auditPassed')")
    @Log(title = "个人信息导入", businessType = BusinessType.UPDATE)
    @PutMapping("/auditPassed/{ids}")
    public AjaxResult auditPassed(@PathVariable String[] ids)
    {
    	return toAjax(busPersonInfoService.auditPassed(ids));
    }
    /**
     * 批量审核退回
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BusPersonInfo:auditReturn')")
    @Log(title = "个人信息导入", businessType = BusinessType.UPDATE)
    @PutMapping("/auditReturn/{ids}")
    public AjaxResult auditReturn(@PathVariable String[] ids)
    {
    	return toAjax(busPersonInfoService.auditReturn(ids));
    }
}
