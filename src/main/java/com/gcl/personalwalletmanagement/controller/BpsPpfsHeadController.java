package com.gcl.personalwalletmanagement.controller;

import java.util.List;
import java.util.UUID;

import com.gcl.bps.constant.BpsTypeConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.personalwalletmanagement.domain.BpsPpfsHead;
import com.gcl.personalwalletmanagement.service.IBpsPpfsHeadService;

import javax.servlet.http.HttpServletResponse;

/**
 * 批量开立对私钱包文件头Controller
 * 
 * @author yada
 * @date 2022-02-17
 */
@Api(value = "批量开立对私钱包文件头",description = "批量开立对私钱包文件头")
@RestController
@RequestMapping("/personalwalletmanagement/BpsPpfsHead")
public class BpsPpfsHeadController extends BaseController
{
    @Autowired
    private IBpsPpfsHeadService bpsPpfsHeadService;

    /**
     * 查询批量开立对私钱包文件头列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPpfsHead:list')")
    @GetMapping("/list")
    public TableDataInfo list(BpsPpfsHead bpsPpfsHead)
    {
        startPage();
        List<BpsPpfsHead> list = bpsPpfsHeadService.selectBpsPpfsHeadList(bpsPpfsHead);
        return getDataTable(list);
    }

    /**
     * 导出批量开立对私钱包文件头列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPpfsHead:export')")
    @Log(title = "批量开立对私钱包文件头", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BpsPpfsHead bpsPpfsHead)
    {
        List<BpsPpfsHead> list = bpsPpfsHeadService.selectBpsPpfsHeadList(bpsPpfsHead);
        ExcelUtil<BpsPpfsHead> util = new ExcelUtil<BpsPpfsHead>(BpsPpfsHead.class);
        return util.exportExcel(list, "批量开立对私钱包文件头数据");
    }

    /**
     * 获取批量开立对私钱包文件头详细信息
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPpfsHead:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(bpsPpfsHeadService.selectBpsPpfsHeadById(id));
    }

    /**
     * 新增批量开立对私钱包文件头
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPpfsHead:add')")
    @Log(title = "批量开立对私钱包文件头", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BpsPpfsHead bpsPpfsHead)
    {
        String id = UUID.randomUUID().toString().replace("-", "");
        bpsPpfsHead.setId(id);
        return toAjax(bpsPpfsHeadService.insertBpsPpfsHead(bpsPpfsHead));
    }

    /**
     * 修改批量开立对私钱包文件头
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPpfsHead:edit')")
    @Log(title = "批量开立对私钱包文件头", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BpsPpfsHead bpsPpfsHead)
    {
        return toAjax(bpsPpfsHeadService.updateBpsPpfsHead(bpsPpfsHead));
    }

    /**
     * 删除批量开立对私钱包文件头
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPpfsHead:remove')")
    @Log(title = "批量开立对私钱包文件头", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(bpsPpfsHeadService.deleteBpsPpfsHeadByIds(ids));
    }




    @ApiOperation("下载BPS-DAT文件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "申请信息ID", required = true, dataType = "String", paramType = "query"),
    })
    @PostMapping(value = "/downloadDATFile")
    public void downloadDATFile(BpsPpfsHead bpsPpfsHead, HttpServletResponse response) throws Exception {

        bpsPpfsHeadService.downloadDATFile(bpsPpfsHead.getId(), BpsTypeConstant.PPFS_TYPE,response);

    }
}
