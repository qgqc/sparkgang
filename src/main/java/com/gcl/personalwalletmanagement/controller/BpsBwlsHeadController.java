package com.gcl.personalwalletmanagement.controller;

import java.util.List;
import java.util.UUID;

import com.gcl.bps.constant.BpsTypeConstant;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.personalwalletmanagement.domain.BpsBwlsHead;
import com.gcl.personalwalletmanagement.service.IBpsBwlsHeadService;

import javax.servlet.http.HttpServletResponse;

/**
 * 对私批量注册白名单文件头Controller
 * 
 * @author yada
 * @date 2022-02-15
 */
@Api(value = "对私批量注册白名单文件头",description = "对私批量注册白名单文件头")
@RestController
@RequestMapping("/personalwalletmanagement/BpsBwlsHead")
public class BpsBwlsHeadController extends BaseController
{
    @Autowired
    private IBpsBwlsHeadService bpsBwlsHeadService;

    /**
     * 查询对私批量注册白名单文件头列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsBwlsHead:list')")
    @GetMapping("/list")
    public TableDataInfo list(BpsBwlsHead bpsBwlsHead)
    {
        startPage();
        List<BpsBwlsHead> list = bpsBwlsHeadService.selectBpsBwlsHeadList(bpsBwlsHead);
        return getDataTable(list);
    }

    /**
     * 导出对私批量注册白名单文件头列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsBwlsHead:export')")
    @Log(title = "对私批量注册白名单文件头", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BpsBwlsHead bpsBwlsHead)
    {
        List<BpsBwlsHead> list = bpsBwlsHeadService.selectBpsBwlsHeadList(bpsBwlsHead);
        ExcelUtil<BpsBwlsHead> util = new ExcelUtil<BpsBwlsHead>(BpsBwlsHead.class);
        return util.exportExcel(list, "对私批量注册白名单文件头数据");
    }

    /**
     * 获取对私批量注册白名单文件头详细信息
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsBwlsHead:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(bpsBwlsHeadService.selectBpsBwlsHeadById(id));
    }

    /**
     * 新增对私批量注册白名单文件头
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsBwlsHead:add')")
    @Log(title = "对私批量注册白名单文件头", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BpsBwlsHead bpsBwlsHead)
    {
        String id = UUID.randomUUID().toString().replace("-", "");
        bpsBwlsHead.setId(id);
        return toAjax(bpsBwlsHeadService.insertBpsBwlsHead(bpsBwlsHead));
    }

    /**
     * 修改对私批量注册白名单文件头
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsBwlsHead:edit')")
    @Log(title = "对私批量注册白名单文件头", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BpsBwlsHead bpsBwlsHead)
    {
        return toAjax(bpsBwlsHeadService.updateBpsBwlsHead(bpsBwlsHead));
    }

    /**
     * 删除对私批量注册白名单文件头
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsBwlsHead:remove')")
    @Log(title = "对私批量注册白名单文件头", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(bpsBwlsHeadService.deleteBpsBwlsHeadByIds(ids));
    }




    @ApiOperation("下载BPS-DAT文件")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "申请信息ID", required = true, dataType = "String", paramType = "query"),
    })
    @PostMapping(value = "/downloadDATFile")
    public void downloadDATFile(BpsBwlsHead bpsBwlsHead, HttpServletResponse response) throws Exception {

       bpsBwlsHeadService.downloadDATFile(bpsBwlsHead.getId(), BpsTypeConstant.BWLS_TYPE,response);

    }
}
