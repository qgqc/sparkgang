package com.gcl.personalwalletmanagement.controller;

import java.util.List;
import java.util.UUID;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gcl.common.annotation.Log;
import com.gcl.common.core.controller.BaseController;
import com.gcl.common.core.domain.AjaxResult;
import com.gcl.common.core.page.TableDataInfo;
import com.gcl.common.enums.BusinessType;
import com.gcl.common.utils.poi.ExcelUtil;
import com.gcl.personalwalletmanagement.domain.BpsPpfsDetail;
import com.gcl.personalwalletmanagement.service.IBpsPpfsDetailService;

/**
 * 批量开立对私钱包明细Controller
 * 
 * @author yada
 * @date 2022-02-17
 */
@RestController
@RequestMapping("/personalwalletmanagement/BpsPpfsDetail")
public class BpsPpfsDetailController extends BaseController
{
    @Autowired
    private IBpsPpfsDetailService bpsPpfsDetailService;

    /**
     * 查询批量开立对私钱包明细列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPpfsDetail:list')")
    @GetMapping("/list")
    public TableDataInfo list(BpsPpfsDetail bpsPpfsDetail)
    {
        startPage();
        List<BpsPpfsDetail> list = bpsPpfsDetailService.selectBpsPpfsDetailList(bpsPpfsDetail);
        return getDataTable(list);
    }

    /**
     * 导出批量开立对私钱包明细列表
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPpfsDetail:export')")
    @Log(title = "批量开立对私钱包明细", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(BpsPpfsDetail bpsPpfsDetail)
    {
        List<BpsPpfsDetail> list = bpsPpfsDetailService.selectBpsPpfsDetailList(bpsPpfsDetail);
        ExcelUtil<BpsPpfsDetail> util = new ExcelUtil<BpsPpfsDetail>(BpsPpfsDetail.class);
        return util.exportExcel(list, "批量开立对私钱包明细数据");
    }

    /**
     * 获取批量开立对私钱包明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPpfsDetail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(bpsPpfsDetailService.selectBpsPpfsDetailById(id));
    }

    /**
     * 新增批量开立对私钱包明细
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPpfsDetail:add')")
    @Log(title = "批量开立对私钱包明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody BpsPpfsDetail bpsPpfsDetail)
    {
        String id = UUID.randomUUID().toString().replace("-", "");
        bpsPpfsDetail.setId(id);
        return toAjax(bpsPpfsDetailService.insertBpsPpfsDetail(bpsPpfsDetail));
    }

    /**
     * 修改批量开立对私钱包明细
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPpfsDetail:edit')")
    @Log(title = "批量开立对私钱包明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody BpsPpfsDetail bpsPpfsDetail)
    {
        return toAjax(bpsPpfsDetailService.updateBpsPpfsDetail(bpsPpfsDetail));
    }

    /**
     * 删除批量开立对私钱包明细
     */
    @PreAuthorize("@ss.hasPermi('personalwalletmanagement:BpsPpfsDetail:remove')")
    @Log(title = "批量开立对私钱包明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(bpsPpfsDetailService.deleteBpsPpfsDetailByIds(ids));
    }
}
