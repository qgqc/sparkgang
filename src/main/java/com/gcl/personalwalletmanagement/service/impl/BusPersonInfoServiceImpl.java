package com.gcl.personalwalletmanagement.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcl.common.exception.ServiceException;
import com.gcl.common.utils.StringUtils;
import com.gcl.common.utils.bean.BeanValidators;
import com.gcl.personalwalletmanagement.domain.BusPersonInfo;
import com.gcl.personalwalletmanagement.mapper.BusPersonInfoMapper;
import com.gcl.personalwalletmanagement.service.IBusPersonInfoService;
import com.gcl.system.service.ISysConfigService;

/**
 * 个人信息导入Service业务层处理
 * 
 * @author yada
 * @date 2022-01-18
 */
@Service
public class BusPersonInfoServiceImpl implements IBusPersonInfoService 
{
	private static final Logger log = LoggerFactory.getLogger(BusPersonInfoServiceImpl.class);
	
    @Autowired
    private BusPersonInfoMapper busPersonInfoMapper;
    
    /**
     * 参数表
     */
    @Autowired
    private ISysConfigService sysConfigService;
    
    @Autowired
    protected Validator validator;

    /**
     * 查询个人信息导入
     * 
     * @param id 个人信息导入主键
     * @return 个人信息导入
     */
    @Override
    public BusPersonInfo selectBusPersonInfoById(String id)
    {
        return busPersonInfoMapper.selectBusPersonInfoById(id);
    }

    /**
     * 查询个人信息导入列表
     * 
     * @param busPersonInfo 个人信息导入
     * @return 个人信息导入
     */
    @Override
    public List<BusPersonInfo> selectBusPersonInfoList(BusPersonInfo busPersonInfo)
    {
        return busPersonInfoMapper.selectBusPersonInfoList(busPersonInfo);
    }

    /**
     * 新增个人信息导入
     * 
     * @param busPersonInfo 个人信息导入
     * @return 结果
     */
    @Override
    public int insertBusPersonInfo(BusPersonInfo busPersonInfo)
    {
        return busPersonInfoMapper.insertBusPersonInfo(busPersonInfo);
    }

    /**
     * 修改个人信息导入
     * 
     * @param busPersonInfo 个人信息导入
     * @return 结果
     */
    @Override
    public int updateBusPersonInfo(BusPersonInfo busPersonInfo)
    {
        return busPersonInfoMapper.updateBusPersonInfo(busPersonInfo);
    }

    /**
     * 批量删除个人信息导入
     * 
     * @param ids 需要删除的个人信息导入主键
     * @return 结果
     */
    @Override
    public int deleteBusPersonInfoByIds(String[] ids)
    {
        return busPersonInfoMapper.deleteBusPersonInfoByIds(ids);
    }

    /**
     * 删除个人信息导入信息
     * 
     * @param id 个人信息导入主键
     * @return 结果
     */
    @Override
    public int deleteBusPersonInfoById(String id)
    {
        return busPersonInfoMapper.deleteBusPersonInfoById(id);
    }

	/* 
	 * @see com.yada.personalwalletmanagement.service.IBusPersonInfoService#importBusPersonInfo(java.util.List, java.lang.Boolean, java.lang.String)
	 */
	@Override
	public String importBusPersonInfo(List<BusPersonInfo> busPersonInfoList, Boolean isUpdateSupport, String operName) {
		
		if(StringUtils.isNull(busPersonInfoList) || busPersonInfoList.size() == 0) {
			throw new ServiceException("导入个人信息数据不能为空！");
		}
		int successNum = 0;
		int failureNum = 0;
		StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        //插入固定值过程
        //手机国际区号 bus.person.info.mobileInternationalAreaCode 86
        String mobileInternationalAreaCode = sysConfigService.selectConfigByKey("bus.person.info.mobileInternationalAreaCode");
        //用户所属国家编码  bus.person.info.countryOfUser CN
        String countryOfUser = sysConfigService.selectConfigByKey("bus.person.info.countryOfUser");
        //钱包昵称 bus.person.info.nickName 前四位 中国银行
        String nickName = sysConfigService.selectConfigByKey("bus.person.info.nickName");
//        		+busPersonInfo.getMobileNumber().substring(busPersonInfo.getMobileNumber().length()-4));
        //客户类型 bus.person.info.clientType 00
        String clientType = sysConfigService.selectConfigByKey("bus.person.info.clientType");
        //联系方式类型  bus.person.info.contactwayType 00
        String contactwayType = sysConfigService.selectConfigByKey("bus.person.info.contactwayType");
        //变更类型  bus.person.info.alterationType CC00
        String alterationType = sysConfigService.selectConfigByKey("bus.person.info.alterationType");
        for(BusPersonInfo busPersonInfo : busPersonInfoList) {
        	try {
        	//验证是否存在个人信息，通过证件号码来确定唯一性，后续可改ID_CARD selectBusPersonInfoByIdCard
        	BusPersonInfo b = busPersonInfoMapper.selectBusPersonInfoByIdCard(busPersonInfo.getIdCard());
        	if(StringUtils.isNull(b)) {
        		BeanValidators.validateWithException(validator, busPersonInfo);
        		String id = UUID.randomUUID().toString().replace("-", "");
                busPersonInfo.setId(id);
                //插入固定值过程
                //手机国际区号 bus.person.info.mobileInternationalAreaCode 86
                busPersonInfo.setMobileInternationalAreaCode(mobileInternationalAreaCode);
                //用户所属国家编码  bus.person.info.countryOfUser CN
                busPersonInfo.setCountryOfUser(countryOfUser);
                //钱包昵称 bus.person.info.nickName 前四位 中国银行
                busPersonInfo.setNickName(nickName + busPersonInfo.getMobileNumber().substring(busPersonInfo.getMobileNumber().length()-4));
                //客户类型 bus.person.info.clientType 00
                busPersonInfo.setClientType(clientType);
                //联系方式类型  bus.person.info.contactwayType 00
                busPersonInfo.setContactwayType(contactwayType);
                //变更类型  bus.person.info.alterationType CC00
                busPersonInfo.setAlterationType(alterationType);
                busPersonInfo.setCreateBy(operName);
                busPersonInfo.setCreateTime(new Date());
                busPersonInfo.setUpdateBy(operName);
                busPersonInfo.setUpdateTime(new Date());
                this.insertBusPersonInfo(busPersonInfo);
                successNum++;
                successMsg.append("<br/>" + successNum + "、个人信息： " + busPersonInfo.getCustName() + " 导入成功");
        	}else if(isUpdateSupport) {
        		BeanValidators.validateWithException(validator, busPersonInfo);
        		busPersonInfo.setUpdateBy(operName);
        		busPersonInfo.setUpdateTime(new Date());
                this.updateBusPersonInfo(busPersonInfo);
                successNum++;
                successMsg.append("<br/>" + successNum + "、个人信息： " + busPersonInfo.getCustName() + " 更新成功");
        	}else {
        		failureNum++;
        		successMsg.append("<br/>" + successNum + "、个人信息： " + busPersonInfo.getCustName() + "已存在");
        	}
        }catch (Exception e) {
        	 failureNum++;
             String msg = "<br/>" + failureNum + "、、个人信息： " + busPersonInfo.getCustName() + " 导入失败：";
             failureMsg.append(msg + e.getMessage());
             log.error(msg, e);
		}
        }
        if (failureNum > 0)
        {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        }
        else
        {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
	}

	/**
     * 批量提交审核 个人信息导入
     * 
     * @param ids 需要提交审核的个人信息导入主键集合
     * @return 结果
     */
    public int submitAudit(String[] ids) {
    	HashMap map = new HashMap<Object,Object>();
    	//1:待审核
    	String dataStatus = "1";
    	map.put("ids", ids);
    	map.put("dataStatus", dataStatus);
    	return busPersonInfoMapper.updateDataStatusByIds(map);
    }

	@Override
	public int auditPassed(String[] ids) {
		HashMap map = new HashMap<Object,Object>();
		//2:审核通过
    	String dataStatus = "2";
    	map.put("ids", ids);
    	map.put("dataStatus", dataStatus);
    	return busPersonInfoMapper.updateDataStatusByIds(map);
	}

	@Override
	public int auditReturn(String[] ids) {
		HashMap map = new HashMap<Object,Object>();
		//2:审核退回
    	String dataStatus = "3";
    	map.put("ids", ids);
    	map.put("dataStatus", dataStatus);
    	return busPersonInfoMapper.updateDataStatusByIds(map);
	};

	public Long getSeqBpsSequenceNumbeValue() {
		return busPersonInfoMapper.getSeqBpsSequenceNumbeValue();
	};
}
