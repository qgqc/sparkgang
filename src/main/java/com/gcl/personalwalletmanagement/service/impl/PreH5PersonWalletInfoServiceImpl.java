package com.gcl.personalwalletmanagement.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gcl.personalwalletmanagement.mapper.PreH5PersonWalletInfoMapper;
import com.gcl.personalwalletmanagement.domain.PreH5PersonWalletInfo;
import com.gcl.personalwalletmanagement.service.IPreH5PersonWalletInfoService;

/**
 * H5个人信息收集报表Service业务层处理
 * 
 * @author yada
 * @date 2022-02-25
 */
@Service
public class PreH5PersonWalletInfoServiceImpl implements IPreH5PersonWalletInfoService 
{
    @Autowired
    private PreH5PersonWalletInfoMapper preH5PersonWalletInfoMapper;

    /**
     * 查询H5个人信息收集报表
     * 
     * @param id H5个人信息收集报表主键
     * @return H5个人信息收集报表
     */
    @Override
    public PreH5PersonWalletInfo selectPreH5PersonWalletInfoById(String id)
    {
        return preH5PersonWalletInfoMapper.selectPreH5PersonWalletInfoById(id);
    }

    /**
     * 查询H5个人信息收集报表列表
     * 
     * @param preH5PersonWalletInfo H5个人信息收集报表
     * @return H5个人信息收集报表
     */
    @Override
    public List<PreH5PersonWalletInfo> selectPreH5PersonWalletInfoList(PreH5PersonWalletInfo preH5PersonWalletInfo)
    {
        return preH5PersonWalletInfoMapper.selectPreH5PersonWalletInfoList(preH5PersonWalletInfo);
    }

    /**
     * 新增H5个人信息收集报表
     * 
     * @param preH5PersonWalletInfo H5个人信息收集报表
     * @return 结果
     */
    @Override
    public int insertPreH5PersonWalletInfo(PreH5PersonWalletInfo preH5PersonWalletInfo)
    {
        return preH5PersonWalletInfoMapper.insertPreH5PersonWalletInfo(preH5PersonWalletInfo);
    }

    /**
     * 修改H5个人信息收集报表
     * 
     * @param preH5PersonWalletInfo H5个人信息收集报表
     * @return 结果
     */
    @Override
    public int updatePreH5PersonWalletInfo(PreH5PersonWalletInfo preH5PersonWalletInfo)
    {
        return preH5PersonWalletInfoMapper.updatePreH5PersonWalletInfo(preH5PersonWalletInfo);
    }

    /**
     * 批量删除H5个人信息收集报表
     * 
     * @param ids 需要删除的H5个人信息收集报表主键
     * @return 结果
     */
    @Override
    public int deletePreH5PersonWalletInfoByIds(String[] ids)
    {
        return preH5PersonWalletInfoMapper.deletePreH5PersonWalletInfoByIds(ids);
    }

    /**
     * 删除H5个人信息收集报表信息
     * 
     * @param id H5个人信息收集报表主键
     * @return 结果
     */
    @Override
    public int deletePreH5PersonWalletInfoById(String id)
    {
        return preH5PersonWalletInfoMapper.deletePreH5PersonWalletInfoById(id);
    }
}
