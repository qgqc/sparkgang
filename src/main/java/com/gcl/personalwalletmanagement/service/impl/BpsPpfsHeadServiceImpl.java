package com.gcl.personalwalletmanagement.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import com.gcl.common.utils.DateUtils;
import com.gcl.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.gcl.personalwalletmanagement.mapper.BpsPpfsHeadMapper;
import com.gcl.personalwalletmanagement.domain.BpsPpfsHead;
import com.gcl.personalwalletmanagement.service.IBpsPpfsHeadService;

import javax.servlet.http.HttpServletResponse;

/**
 * 批量开立对私钱包文件头Service业务层处理
 * 
 * @author yada
 * @date 2022-02-17
 */
@Service
public class BpsPpfsHeadServiceImpl implements IBpsPpfsHeadService 
{
    @Autowired
    private BpsPpfsHeadMapper bpsPpfsHeadMapper;

    @Value("${bpsCreatLocalPath}")
    private String bpsDatFilePath;

    /**
     * 查询批量开立对私钱包文件头
     * 
     * @param id 批量开立对私钱包文件头主键
     * @return 批量开立对私钱包文件头
     */
    @Override
    public BpsPpfsHead selectBpsPpfsHeadById(String id)
    {
        return bpsPpfsHeadMapper.selectBpsPpfsHeadById(id);
    }

    /**
     * 查询批量开立对私钱包文件头列表
     * 
     * @param bpsPpfsHead 批量开立对私钱包文件头
     * @return 批量开立对私钱包文件头
     */
    @Override
    public List<BpsPpfsHead> selectBpsPpfsHeadList(BpsPpfsHead bpsPpfsHead)
    {
        return bpsPpfsHeadMapper.selectBpsPpfsHeadList(bpsPpfsHead);
    }

    /**
     * 新增批量开立对私钱包文件头
     * 
     * @param bpsPpfsHead 批量开立对私钱包文件头
     * @return 结果
     */
    @Override
    public int insertBpsPpfsHead(BpsPpfsHead bpsPpfsHead)
    {
        return bpsPpfsHeadMapper.insertBpsPpfsHead(bpsPpfsHead);
    }

    /**
     * 修改批量开立对私钱包文件头
     * 
     * @param bpsPpfsHead 批量开立对私钱包文件头
     * @return 结果
     */
    @Override
    public int updateBpsPpfsHead(BpsPpfsHead bpsPpfsHead)
    {
        bpsPpfsHead.setUpdateTime(DateUtils.getNowDate());
        return bpsPpfsHeadMapper.updateBpsPpfsHead(bpsPpfsHead);
    }

    /**
     * 批量删除批量开立对私钱包文件头
     * 
     * @param ids 需要删除的批量开立对私钱包文件头主键
     * @return 结果
     */
    @Override
    public int deleteBpsPpfsHeadByIds(String[] ids)
    {
        return bpsPpfsHeadMapper.deleteBpsPpfsHeadByIds(ids);
    }

    /**
     * 删除批量开立对私钱包文件头信息
     * 
     * @param id 批量开立对私钱包文件头主键
     * @return 结果
     */
    @Override
    public int deleteBpsPpfsHeadById(String id)
    {
        return bpsPpfsHeadMapper.deleteBpsPpfsHeadById(id);
    }

    @Override
    public void downloadDATFile(String id, String ppfsType, HttpServletResponse response) throws Exception {
        BpsPpfsHead head = bpsPpfsHeadMapper.selectBpsPpfsHeadById(id);
        if(head != null && StringUtils.isNotBlank(head.getSourceFileName())){
            String datFilePath = bpsDatFilePath + ppfsType + File.separator + head.getSourceFileName();
            File file = new File(datFilePath);
            if (!file.exists()) {
                throw new Exception("文件不存在");
            }
            InputStream inputStream = new FileInputStream(datFilePath);
            OutputStream outputStream = response.getOutputStream();
            byte[] b = new byte[4096];
            int len;
            while ((len = inputStream.read(b)) > 0) {
                outputStream.write(b, 0, len);
            }
            inputStream.close();
        }else {
            throw new Exception("head信息不存在");
        }
    }
}
