package com.gcl.personalwalletmanagement.service.impl;


import java.util.Date;
import java.util.List;

import com.gcl.common.utils.DateUtils;
import com.gcl.personalwalletmanagement.domain.BusPrivateInfo;
import com.gcl.personalwalletmanagement.enums.PriWalletDataStatus;
import com.gcl.personalwalletmanagement.mapper.BusPrivateInfoMapper;
import com.gcl.personalwalletmanagement.service.IBusPrivateInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.gcl.common.utils.SecurityUtils.getLoginUser;


/**
 * 个人钱包Service业务层处理
 *
 * @author ruoyi
 * @date 2022-01-13
 */
@Service
public class BusPrivateInfoServiceImpl implements IBusPrivateInfoService
{
    @Autowired
    private BusPrivateInfoMapper busPrivateInfoMapper;

    /**
     * 查询个人钱包
     *
     * @param id 个人钱包主键
     * @return 个人钱包
     */
    @Override
    public BusPrivateInfo selectBusPrivateInfoById(String id)
    {
        return busPrivateInfoMapper.selectBusPrivateInfoById(id);
    }

    /**
     * 查询个人钱包列表
     *
     * @param busPrivateInfo 个人钱包
     * @return 个人钱包
     */
    @Override
    public List<BusPrivateInfo> selectBusPrivateInfoList(BusPrivateInfo busPrivateInfo)
    {
        return busPrivateInfoMapper.selectBusPrivateInfoList(busPrivateInfo);
    }

    /**
     * 新增个人钱包
     *
     * @param busPrivateInfo 个人钱包
     * @return 结果
     */
    @Override
    public int insertBusPrivateInfo(BusPrivateInfo busPrivateInfo)
    {
        init(busPrivateInfo);
        return busPrivateInfoMapper.insertBusPrivateInfo(busPrivateInfo);
    }
    /**
     * 初始化个人钱包信息（固定值）
     *
     * @param perWalletInfo 个人钱包信息
     */
    //    todo 确定后需要进行更改
    public void init(BusPrivateInfo perWalletInfo) {
        perWalletInfo.setDataStatus(PriWalletDataStatus.INITIALIZATION.getStatus());
        perWalletInfo.setMobileInternationalAreaCode("86");
        perWalletInfo.setIdCardType("IT01");
        perWalletInfo.setAlterationType("CC00");
        perWalletInfo.setClientType("00");
        perWalletInfo.setContactwayType("00");
        perWalletInfo.setCountryOfUser("CN");
        //创建人
//        Session session = SecurityUtils.getSubject().getSession();
//        User user = (User) session.getAttribute("curUser");
        perWalletInfo.setCreatedBycreatedBy(getLoginUser().getUsername());
        perWalletInfo.setCreatedDate(new Date());
        String mobileNumber = perWalletInfo.getMobileNumber();
        String substring = mobileNumber.substring(7, mobileNumber.length());
        perWalletInfo.setNickName("中国银行" + substring);
        perWalletInfo.setMobileInternationalAreaCode("86");
        perWalletInfo.setMobileNumberCityAreaCode("123");
        perWalletInfo.setLastUpdatedBy(getLoginUser().getUsername());
        perWalletInfo.setTradingOrganizationNo("27005");
//        if (perWalletInfo.getSystemType().equals("苹果")) {
//            perWalletInfo.setSystemType("00");
//        } else {
//            perWalletInfo.setSystemType("01");
//        }

    }
    /**
     * 修改个人钱包
     *
     * @param busPrivateInfo 个人钱包
     * @return 结果
     */
    @Override
    public int updateBusPrivateInfo(BusPrivateInfo busPrivateInfo)
    {
        return busPrivateInfoMapper.updateBusPrivateInfo(busPrivateInfo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateDataStatus(String[] ids) {
       // LOG.info("{},更新数据状态：{}-{}",FUNC,id,dataStatusEnum.getValue());


        for (int i = 0; i < ids.length; i++) {

            if (ids[i] != null && !"".equals(ids[i] .trim())) {
                //PerWalletV2Info perWalletV2Info = perWalletInfoV2Dao.findById(ids.get(i)).orElse(null);
                BusPrivateInfo busPrivateInfo = busPrivateInfoMapper.selectBusPrivateInfoById(ids[i]);

                if (busPrivateInfo.getDataStatus().equals(PriWalletDataStatus.INITIALIZATION.getStatus())) {
                    busPrivateInfo.setExamineSuccessDate(DateUtils.dateTime());
                    busPrivateInfo.setLastUpdatedDate(new Date());
                    busPrivateInfo.setLastUpdatedBy(getLoginUser().getUsername());
                    busPrivateInfo.setDataStatus(PriWalletDataStatus.SUBMIT_REVIEW.getStatus());
                    busPrivateInfoMapper.updateBusPrivateInfo(busPrivateInfo);
                }
            }
        }
        return 1;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateDataStatusReview(String[] ids) {



        for (int i = 0; i < ids.length; i++) {

            if (ids[i] != null && !"".equals(ids[i] .trim())) {
                //PerWalletV2Info perWalletV2Info = perWalletInfoV2Dao.findById(ids.get(i)).orElse(null);
                BusPrivateInfo busPrivateInfo = busPrivateInfoMapper.selectBusPrivateInfoById(ids[i]);

                if (busPrivateInfo.getDataStatus().equals(PriWalletDataStatus.SUBMIT_REVIEW.getStatus())) {
                    busPrivateInfo.setExamineSuccessDate(DateUtils.dateTime());
                    busPrivateInfo.setLastUpdatedDate(new Date());
                    busPrivateInfo.setLastUpdatedBy(getLoginUser().getUsername());
                    busPrivateInfo.setDataStatus(PriWalletDataStatus.REVIEW_PASSED.getStatus());
                    busPrivateInfoMapper.updateBusPrivateInfo(busPrivateInfo);
                }
            }
        }
        return 1;
    }
    /**
     * 批量删除个人钱包
     *
     * @param ids 需要删除的个人钱包主键
     * @return 结果
     */
    @Override
    public int deleteBusPrivateInfoByIds(String[] ids)
    {
        return busPrivateInfoMapper.deleteBusPrivateInfoByIds(ids);
    }

    /**
     * 删除个人钱包信息
     *
     * @param id 个人钱包主键
     * @return 结果
     */
    @Override
    public int deleteBusPrivateInfoById(String id)
    {
        return busPrivateInfoMapper.deleteBusPrivateInfoById(id);
    }
}
