package com.gcl.personalwalletmanagement.service.impl;

import java.sql.Timestamp;
import java.util.List;
import com.gcl.common.utils.DateUtils;
import com.gcl.personalwalletmanagement.domain.BusPrivateInfoBps;
import com.gcl.personalwalletmanagement.mapper.BusPrivateInfoBpsMapper;
import com.gcl.personalwalletmanagement.service.IBusPrivateInfoBpsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 个人钱包Service业务层处理
 * 
 * @author wjw
 * @date 2022-02-10
 */
@Service
public class BusPrivateInfoBpsServiceImpl implements IBusPrivateInfoBpsService
{
    @Autowired
    private BusPrivateInfoBpsMapper busPrivateInfoBpsMapper;

    /**
     * 查询个人钱包
     * 
     * @param id 个人钱包主键
     * @return 个人钱包
     */
    @Override
    public BusPrivateInfoBps selectBusPrivateInfoBpsById(String id)
    {
        return busPrivateInfoBpsMapper.selectBusPrivateInfoBpsById(id);
    }

    /**
     * 查询个人钱包列表
     * 
     * @param busPrivateInfoBps 个人钱包
     * @return 个人钱包
     */
    @Override
    public List<BusPrivateInfoBps> selectBusPrivateInfoBpsList(BusPrivateInfoBps busPrivateInfoBps)
    {
        return busPrivateInfoBpsMapper.selectBusPrivateInfoBpsList(busPrivateInfoBps);
    }

    /**
     * 新增个人钱包
     * 
     * @param busPrivateInfoBps 个人钱包
     * @return 结果
     */
    @Override
    public int insertBusPrivateInfoBps(BusPrivateInfoBps busPrivateInfoBps)
    {
        return busPrivateInfoBpsMapper.insertBusPrivateInfoBps(busPrivateInfoBps);
    }

    /**
     * 修改个人钱包
     * 
     * @param busPrivateInfoBps 个人钱包
     * @return 结果
     */
    @Override
    public int updateBusPrivateInfoBps(BusPrivateInfoBps busPrivateInfoBps)
    {
        busPrivateInfoBps.setUpdateTime( new Timestamp(DateUtils.getNowDate().getTime()));
        return busPrivateInfoBpsMapper.updateBusPrivateInfoBps(busPrivateInfoBps);
    }

    /**
     * 批量删除个人钱包
     * 
     * @param ids 需要删除的个人钱包主键
     * @return 结果
     */
    @Override
    public int deleteBusPrivateInfoBpsByIds(String[] ids)
    {
        return busPrivateInfoBpsMapper.deleteBusPrivateInfoBpsByIds(ids);
    }

    /**
     * 删除个人钱包信息
     * 
     * @param id 个人钱包主键
     * @return 结果
     */
    @Override
    public int deleteBusPrivateInfoBpsById(String id)
    {
        return busPrivateInfoBpsMapper.deleteBusPrivateInfoBpsById(id);
    }
}
