package com.gcl.personalwalletmanagement.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import com.gcl.common.utils.DateUtils;
import com.gcl.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.gcl.personalwalletmanagement.mapper.BpsBwlsHeadMapper;
import com.gcl.personalwalletmanagement.domain.BpsBwlsHead;
import com.gcl.personalwalletmanagement.service.IBpsBwlsHeadService;

import javax.servlet.http.HttpServletResponse;

/**
 * 对私批量注册白名单文件头Service业务层处理
 * 
 * @author yada
 * @date 2022-02-15
 */
@Service
public class BpsBwlsHeadServiceImpl implements IBpsBwlsHeadService 
{
    @Autowired
    private BpsBwlsHeadMapper bpsBwlsHeadMapper;

    @Value("${bpsCreatLocalPath}")
    private String bpsDatFilePath;
    /**
     * 查询对私批量注册白名单文件头
     * 
     * @param id 对私批量注册白名单文件头主键
     * @return 对私批量注册白名单文件头
     */
    @Override
    public BpsBwlsHead selectBpsBwlsHeadById(String id)
    {
        return bpsBwlsHeadMapper.selectBpsBwlsHeadById(id);
    }

    /**
     * 查询对私批量注册白名单文件头列表
     * 
     * @param bpsBwlsHead 对私批量注册白名单文件头
     * @return 对私批量注册白名单文件头
     */
    @Override
    public List<BpsBwlsHead> selectBpsBwlsHeadList(BpsBwlsHead bpsBwlsHead)
    {
        return bpsBwlsHeadMapper.selectBpsBwlsHeadList(bpsBwlsHead);
    }

    /**
     * 新增对私批量注册白名单文件头
     * 
     * @param bpsBwlsHead 对私批量注册白名单文件头
     * @return 结果
     */
    @Override
    public int insertBpsBwlsHead(BpsBwlsHead bpsBwlsHead)
    {
        return bpsBwlsHeadMapper.insertBpsBwlsHead(bpsBwlsHead);
    }

    /**
     * 修改对私批量注册白名单文件头
     * 
     * @param bpsBwlsHead 对私批量注册白名单文件头
     * @return 结果
     */
    @Override
    public int updateBpsBwlsHead(BpsBwlsHead bpsBwlsHead)
    {
        bpsBwlsHead.setUpdateTime(DateUtils.getNowDate());
        return bpsBwlsHeadMapper.updateBpsBwlsHead(bpsBwlsHead);
    }

    /**
     * 批量删除对私批量注册白名单文件头
     * 
     * @param ids 需要删除的对私批量注册白名单文件头主键
     * @return 结果
     */
    @Override
    public int deleteBpsBwlsHeadByIds(String[] ids)
    {
        return bpsBwlsHeadMapper.deleteBpsBwlsHeadByIds(ids);
    }

    /**
     * 删除对私批量注册白名单文件头信息
     * 
     * @param id 对私批量注册白名单文件头主键
     * @return 结果
     */
    @Override
    public int deleteBpsBwlsHeadById(String id)
    {
        return bpsBwlsHeadMapper.deleteBpsBwlsHeadById(id);
    }

    @Override
    public void downloadDATFile(String id, String bwlsType, HttpServletResponse response)  throws Exception{
        BpsBwlsHead head = bpsBwlsHeadMapper.selectBpsBwlsHeadById(id);
        if(head != null && StringUtils.isNotBlank(head.getSourceFileName())){
            String datFilePath = bpsDatFilePath + bwlsType + File.separator + head.getSourceFileName();
            File file = new File(datFilePath);
            if (!file.exists()) {
                throw new Exception("文件不存在");
            }
            InputStream inputStream = new FileInputStream(datFilePath);
            OutputStream outputStream = response.getOutputStream();
            byte[] b = new byte[4096];
            int len;
            while ((len = inputStream.read(b)) > 0) {
                outputStream.write(b, 0, len);
            }
            inputStream.close();
        }else {
            throw new Exception("head信息不存在");
        }
    }
}
