package com.gcl.personalwalletmanagement.service.impl;

import java.util.List;

import com.gcl.personalwalletmanagement.domain.BusPrivateWhitelistRegister;
import com.gcl.personalwalletmanagement.mapper.BusPrivateWhitelistRegisterMapper;
import com.gcl.personalwalletmanagement.service.IBusPrivateWhitelistRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 钱包注册白名单信息Service业务层处理
 * 
 * @author yada
 * @date 2022-02-07
 */
@Service
public class BusPrivateWhitelistRegisterServiceImpl implements IBusPrivateWhitelistRegisterService
{
    @Autowired
    private BusPrivateWhitelistRegisterMapper busPrivateWhitelistRegisterMapper;

    /**
     * 查询钱包注册白名单信息
     * 
     * @param id 钱包注册白名单信息主键
     * @return 钱包注册白名单信息
     */
    @Override
    public BusPrivateWhitelistRegister selectBusPrivateWhitelistRegisterById(String id)
    {
        return busPrivateWhitelistRegisterMapper.selectBusPrivateWhitelistRegisterById(id);
    }

    /**
     * 查询钱包注册白名单信息列表
     * 
     * @param busPrivateWhitelistRegister 钱包注册白名单信息
     * @return 钱包注册白名单信息
     */
    @Override
    public List<BusPrivateWhitelistRegister> selectBusPrivateWhitelistRegisterList(BusPrivateWhitelistRegister busPrivateWhitelistRegister)
    {
        return busPrivateWhitelistRegisterMapper.selectBusPrivateWhitelistRegisterList(busPrivateWhitelistRegister);
    }

    /**
     * 新增钱包注册白名单信息
     * 
     * @param busPrivateWhitelistRegister 钱包注册白名单信息
     * @return 结果
     */
    @Override
    public int insertBusPrivateWhitelistRegister(BusPrivateWhitelistRegister busPrivateWhitelistRegister)
    {
        return busPrivateWhitelistRegisterMapper.insertBusPrivateWhitelistRegister(busPrivateWhitelistRegister);
    }

    /**
     * 修改钱包注册白名单信息
     * 
     * @param busPrivateWhitelistRegister 钱包注册白名单信息
     * @return 结果
     */
    @Override
    public int updateBusPrivateWhitelistRegister(BusPrivateWhitelistRegister busPrivateWhitelistRegister)
    {
        return busPrivateWhitelistRegisterMapper.updateBusPrivateWhitelistRegister(busPrivateWhitelistRegister);
    }

    /**
     * 批量删除钱包注册白名单信息
     * 
     * @param ids 需要删除的钱包注册白名单信息主键
     * @return 结果
     */
    @Override
    public int deleteBusPrivateWhitelistRegisterByIds(String[] ids)
    {
        return busPrivateWhitelistRegisterMapper.deleteBusPrivateWhitelistRegisterByIds(ids);
    }

    /**
     * 删除钱包注册白名单信息信息
     * 
     * @param id 钱包注册白名单信息主键
     * @return 结果
     */
    @Override
    public int deleteBusPrivateWhitelistRegisterById(String id)
    {
        return busPrivateWhitelistRegisterMapper.deleteBusPrivateWhitelistRegisterById(id);
    }
}
