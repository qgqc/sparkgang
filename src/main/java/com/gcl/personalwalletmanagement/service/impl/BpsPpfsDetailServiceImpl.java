package com.gcl.personalwalletmanagement.service.impl;

import java.util.List;
import com.gcl.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gcl.personalwalletmanagement.mapper.BpsPpfsDetailMapper;
import com.gcl.personalwalletmanagement.domain.BpsPpfsDetail;
import com.gcl.personalwalletmanagement.service.IBpsPpfsDetailService;

/**
 * 批量开立对私钱包明细Service业务层处理
 * 
 * @author yada
 * @date 2022-02-17
 */
@Service
public class BpsPpfsDetailServiceImpl implements IBpsPpfsDetailService 
{
    @Autowired
    private BpsPpfsDetailMapper bpsPpfsDetailMapper;

    /**
     * 查询批量开立对私钱包明细
     * 
     * @param id 批量开立对私钱包明细主键
     * @return 批量开立对私钱包明细
     */
    @Override
    public BpsPpfsDetail selectBpsPpfsDetailById(String id)
    {
        return bpsPpfsDetailMapper.selectBpsPpfsDetailById(id);
    }

    /**
     * 查询批量开立对私钱包明细列表
     * 
     * @param bpsPpfsDetail 批量开立对私钱包明细
     * @return 批量开立对私钱包明细
     */
    @Override
    public List<BpsPpfsDetail> selectBpsPpfsDetailList(BpsPpfsDetail bpsPpfsDetail)
    {
        return bpsPpfsDetailMapper.selectBpsPpfsDetailList(bpsPpfsDetail);
    }

    /**
     * 新增批量开立对私钱包明细
     * 
     * @param bpsPpfsDetail 批量开立对私钱包明细
     * @return 结果
     */
    @Override
    public int insertBpsPpfsDetail(BpsPpfsDetail bpsPpfsDetail)
    {
        bpsPpfsDetail.setCreateTime(DateUtils.getNowDate());
        return bpsPpfsDetailMapper.insertBpsPpfsDetail(bpsPpfsDetail);
    }

    /**
     * 修改批量开立对私钱包明细
     * 
     * @param bpsPpfsDetail 批量开立对私钱包明细
     * @return 结果
     */
    @Override
    public int updateBpsPpfsDetail(BpsPpfsDetail bpsPpfsDetail)
    {
        bpsPpfsDetail.setUpdateTime(DateUtils.getNowDate());
        return bpsPpfsDetailMapper.updateBpsPpfsDetail(bpsPpfsDetail);
    }

    /**
     * 批量删除批量开立对私钱包明细
     * 
     * @param ids 需要删除的批量开立对私钱包明细主键
     * @return 结果
     */
    @Override
    public int deleteBpsPpfsDetailByIds(String[] ids)
    {
        return bpsPpfsDetailMapper.deleteBpsPpfsDetailByIds(ids);
    }

    /**
     * 删除批量开立对私钱包明细信息
     * 
     * @param id 批量开立对私钱包明细主键
     * @return 结果
     */
    @Override
    public int deleteBpsPpfsDetailById(String id)
    {
        return bpsPpfsDetailMapper.deleteBpsPpfsDetailById(id);
    }
}
