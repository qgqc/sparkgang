package com.gcl.personalwalletmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gcl.bps.domain.OrgNoAndTellerNo;
import com.gcl.common.utils.DateUtils;
import com.gcl.personalwalletmanagement.domain.BpsBwlsDetail;
import com.gcl.personalwalletmanagement.mapper.BpsBwlsDetailMapper;
import com.gcl.personalwalletmanagement.service.IBpsBwlsDetailService;

/**
 * 对私批量注册白名单文件明细Service业务层处理
 * 
 * @author yada
 * @date 2022-02-15
 */
@Service
public class BpsBwlsDetailServiceImpl implements IBpsBwlsDetailService 
{
    @Autowired
    private BpsBwlsDetailMapper bpsBwlsDetailMapper;

    /**
     * 查询对私批量注册白名单文件明细
     * 
     * @param id 对私批量注册白名单文件明细主键
     * @return 对私批量注册白名单文件明细
     */
    @Override
    public BpsBwlsDetail selectBpsBwlsDetailById(String id)
    {
        return bpsBwlsDetailMapper.selectBpsBwlsDetailById(id);
    }

    /**
     * 查询对私批量注册白名单文件明细列表
     * 
     * @param bpsBwlsDetail 对私批量注册白名单文件明细
     * @return 对私批量注册白名单文件明细
     */
    @Override
    public List<BpsBwlsDetail> selectBpsBwlsDetailList(BpsBwlsDetail bpsBwlsDetail)
    {
        return bpsBwlsDetailMapper.selectBpsBwlsDetailList(bpsBwlsDetail);
    }

    /**
     * 新增对私批量注册白名单文件明细
     * 
     * @param bpsBwlsDetail 对私批量注册白名单文件明细
     * @return 结果
     */
    @Override
    public int insertBpsBwlsDetail(BpsBwlsDetail bpsBwlsDetail)
    {
        bpsBwlsDetail.setCreateTime(DateUtils.getNowDate());
        return bpsBwlsDetailMapper.insertBpsBwlsDetail(bpsBwlsDetail);
    }

    /**
     * 修改对私批量注册白名单文件明细
     * 
     * @param bpsBwlsDetail 对私批量注册白名单文件明细
     * @return 结果
     */
    @Override
    public int updateBpsBwlsDetail(BpsBwlsDetail bpsBwlsDetail)
    {
        bpsBwlsDetail.setUpdateTime(DateUtils.getNowDate());
        return bpsBwlsDetailMapper.updateBpsBwlsDetail(bpsBwlsDetail);
    }

    /**
     * 批量删除对私批量注册白名单文件明细
     * 
     * @param ids 需要删除的对私批量注册白名单文件明细主键
     * @return 结果
     */
    @Override
    public int deleteBpsBwlsDetailByIds(String[] ids)
    {
        return bpsBwlsDetailMapper.deleteBpsBwlsDetailByIds(ids);
    }

    /**
     * 删除对私批量注册白名单文件明细信息
     * 
     * @param id 对私批量注册白名单文件明细主键
     * @return 结果
     */
    @Override
    public int deleteBpsBwlsDetailById(String id)
    {
        return bpsBwlsDetailMapper.deleteBpsBwlsDetailById(id);
    }

	/**
	 * 通过文件批次号查询机构号和柜员号的列表
	 * 
	 * @param fileBatchNo
	 * @return
	 */
	public List<OrgNoAndTellerNo> selectOrgNoAndTellerNoListByFileBatchNo(String fileBatchNo) {
		return bpsBwlsDetailMapper.selectOrgNoAndTellerNoListByFileBatchNo(fileBatchNo);
	}
}
