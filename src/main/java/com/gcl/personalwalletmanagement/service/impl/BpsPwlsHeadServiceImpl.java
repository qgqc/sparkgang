package com.gcl.personalwalletmanagement.service.impl;

import java.util.List;
import com.gcl.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gcl.personalwalletmanagement.mapper.BpsPwlsHeadMapper;
import com.gcl.personalwalletmanagement.domain.BpsPwlsHead;
import com.gcl.personalwalletmanagement.service.IBpsPwlsHeadService;

/**
 * 人行共建APP白名单批量文件头Service业务层处理
 * 
 * @author yada
 * @date 2022-02-09
 */
@Service
public class BpsPwlsHeadServiceImpl implements IBpsPwlsHeadService 
{
    @Autowired
    private BpsPwlsHeadMapper bpsPwlsHeadMapper;

    /**
     * 查询人行共建APP白名单批量文件头
     * 
     * @param id 人行共建APP白名单批量文件头主键
     * @return 人行共建APP白名单批量文件头
     */
    @Override
    public BpsPwlsHead selectBpsPwlsHeadById(String id)
    {
        return bpsPwlsHeadMapper.selectBpsPwlsHeadById(id);
    }

    /**
     * 查询人行共建APP白名单批量文件头列表
     * 
     * @param bpsPwlsHead 人行共建APP白名单批量文件头
     * @return 人行共建APP白名单批量文件头
     */
    @Override
    public List<BpsPwlsHead> selectBpsPwlsHeadList(BpsPwlsHead bpsPwlsHead)
    {
        return bpsPwlsHeadMapper.selectBpsPwlsHeadList(bpsPwlsHead);
    }

    /**
     * 新增人行共建APP白名单批量文件头
     * 
     * @param bpsPwlsHead 人行共建APP白名单批量文件头
     * @return 结果
     */
    @Override
    public int insertBpsPwlsHead(BpsPwlsHead bpsPwlsHead)
    {
        return bpsPwlsHeadMapper.insertBpsPwlsHead(bpsPwlsHead);
    }

    /**
     * 修改人行共建APP白名单批量文件头
     * 
     * @param bpsPwlsHead 人行共建APP白名单批量文件头
     * @return 结果
     */
    @Override
    public int updateBpsPwlsHead(BpsPwlsHead bpsPwlsHead)
    {
        bpsPwlsHead.setUpdateTime(DateUtils.getNowDate());
        return bpsPwlsHeadMapper.updateBpsPwlsHead(bpsPwlsHead);
    }

    /**
     * 批量删除人行共建APP白名单批量文件头
     * 
     * @param ids 需要删除的人行共建APP白名单批量文件头主键
     * @return 结果
     */
    @Override
    public int deleteBpsPwlsHeadByIds(String[] ids)
    {
        return bpsPwlsHeadMapper.deleteBpsPwlsHeadByIds(ids);
    }

    /**
     * 删除人行共建APP白名单批量文件头信息
     * 
     * @param id 人行共建APP白名单批量文件头主键
     * @return 结果
     */
    @Override
    public int deleteBpsPwlsHeadById(String id)
    {
        return bpsPwlsHeadMapper.deleteBpsPwlsHeadById(id);
    }
}
