package com.gcl.personalwalletmanagement.service.impl;

import java.util.List;
import com.gcl.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gcl.personalwalletmanagement.mapper.BpsPwlsDetailMapper;
import com.gcl.personalwalletmanagement.domain.BpsPwlsDetail;
import com.gcl.personalwalletmanagement.service.IBpsPwlsDetailService;

/**
 * 人行共建APP白名单批量文件明细Service业务层处理
 * 
 * @author yada
 * @date 2022-02-09
 */
@Service
public class BpsPwlsDetailServiceImpl implements IBpsPwlsDetailService 
{
    @Autowired
    private BpsPwlsDetailMapper bpsPwlsDetailMapper;

    /**
     * 查询人行共建APP白名单批量文件明细
     * 
     * @param id 人行共建APP白名单批量文件明细主键
     * @return 人行共建APP白名单批量文件明细
     */
    @Override
    public BpsPwlsDetail selectBpsPwlsDetailById(String id)
    {
        return bpsPwlsDetailMapper.selectBpsPwlsDetailById(id);
    }

    /**
     * 查询人行共建APP白名单批量文件明细列表
     * 
     * @param bpsPwlsDetail 人行共建APP白名单批量文件明细
     * @return 人行共建APP白名单批量文件明细
     */
    @Override
    public List<BpsPwlsDetail> selectBpsPwlsDetailList(BpsPwlsDetail bpsPwlsDetail)
    {
        return bpsPwlsDetailMapper.selectBpsPwlsDetailList(bpsPwlsDetail);
    }

    /**
     * 新增人行共建APP白名单批量文件明细
     * 
     * @param bpsPwlsDetail 人行共建APP白名单批量文件明细
     * @return 结果
     */
    @Override
    public int insertBpsPwlsDetail(BpsPwlsDetail bpsPwlsDetail)
    {
        bpsPwlsDetail.setCreateTime(DateUtils.getNowDate());
        return bpsPwlsDetailMapper.insertBpsPwlsDetail(bpsPwlsDetail);
    }

    /**
     * 修改人行共建APP白名单批量文件明细
     * 
     * @param bpsPwlsDetail 人行共建APP白名单批量文件明细
     * @return 结果
     */
    @Override
    public int updateBpsPwlsDetail(BpsPwlsDetail bpsPwlsDetail)
    {
        bpsPwlsDetail.setUpdateTime(DateUtils.getNowDate());
        return bpsPwlsDetailMapper.updateBpsPwlsDetail(bpsPwlsDetail);
    }

    /**
     * 批量删除人行共建APP白名单批量文件明细
     * 
     * @param ids 需要删除的人行共建APP白名单批量文件明细主键
     * @return 结果
     */
    @Override
    public int deleteBpsPwlsDetailByIds(String[] ids)
    {
        return bpsPwlsDetailMapper.deleteBpsPwlsDetailByIds(ids);
    }

    /**
     * 删除人行共建APP白名单批量文件明细信息
     * 
     * @param id 人行共建APP白名单批量文件明细主键
     * @return 结果
     */
    @Override
    public int deleteBpsPwlsDetailById(String id)
    {
        return bpsPwlsDetailMapper.deleteBpsPwlsDetailById(id);
    }
}
