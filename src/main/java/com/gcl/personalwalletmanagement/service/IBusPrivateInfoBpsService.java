package com.gcl.personalwalletmanagement.service;

import com.gcl.personalwalletmanagement.domain.BusPrivateInfoBps;

import java.util.List;

/**
 * 个人钱包Service接口
 * 
 * @author yada
 * @date 2022-02-10
 */
public interface IBusPrivateInfoBpsService 
{
    /**
     * 查询个人钱包
     * 
     * @param id 个人钱包主键
     * @return 个人钱包
     */
    public BusPrivateInfoBps selectBusPrivateInfoBpsById(String id);

    /**
     * 查询个人钱包列表
     * 
     * @param busPrivateInfoBps 个人钱包
     * @return 个人钱包集合
     */
    public List<BusPrivateInfoBps> selectBusPrivateInfoBpsList(BusPrivateInfoBps busPrivateInfoBps);

    /**
     * 新增个人钱包
     * 
     * @param busPrivateInfoBps 个人钱包
     * @return 结果
     */
    public int insertBusPrivateInfoBps(BusPrivateInfoBps busPrivateInfoBps);

    /**
     * 修改个人钱包
     * 
     * @param busPrivateInfoBps 个人钱包
     * @return 结果
     */
    public int updateBusPrivateInfoBps(BusPrivateInfoBps busPrivateInfoBps);

    /**
     * 批量删除个人钱包
     * 
     * @param ids 需要删除的个人钱包主键集合
     * @return 结果
     */
    public int deleteBusPrivateInfoBpsByIds(String[] ids);

    /**
     * 删除个人钱包信息
     * 
     * @param id 个人钱包主键
     * @return 结果
     */
    public int deleteBusPrivateInfoBpsById(String id);
}
