package com.gcl.personalwalletmanagement.service;

import java.util.List;
import com.gcl.personalwalletmanagement.domain.BpsPpfsHead;

import javax.servlet.http.HttpServletResponse;

/**
 * 批量开立对私钱包文件头Service接口
 * 
 * @author yada
 * @date 2022-02-17
 */
public interface IBpsPpfsHeadService 
{
    /**
     * 查询批量开立对私钱包文件头
     * 
     * @param id 批量开立对私钱包文件头主键
     * @return 批量开立对私钱包文件头
     */
    public BpsPpfsHead selectBpsPpfsHeadById(String id);

    /**
     * 查询批量开立对私钱包文件头列表
     * 
     * @param bpsPpfsHead 批量开立对私钱包文件头
     * @return 批量开立对私钱包文件头集合
     */
    public List<BpsPpfsHead> selectBpsPpfsHeadList(BpsPpfsHead bpsPpfsHead);

    /**
     * 新增批量开立对私钱包文件头
     * 
     * @param bpsPpfsHead 批量开立对私钱包文件头
     * @return 结果
     */
    public int insertBpsPpfsHead(BpsPpfsHead bpsPpfsHead);

    /**
     * 修改批量开立对私钱包文件头
     * 
     * @param bpsPpfsHead 批量开立对私钱包文件头
     * @return 结果
     */
    public int updateBpsPpfsHead(BpsPpfsHead bpsPpfsHead);

    /**
     * 批量删除批量开立对私钱包文件头
     * 
     * @param ids 需要删除的批量开立对私钱包文件头主键集合
     * @return 结果
     */
    public int deleteBpsPpfsHeadByIds(String[] ids);

    /**
     * 删除批量开立对私钱包文件头信息
     * 
     * @param id 批量开立对私钱包文件头主键
     * @return 结果
     */
    public int deleteBpsPpfsHeadById(String id);

    void downloadDATFile(String id, String bwlsType, HttpServletResponse response) throws Exception;
}
