package com.gcl.personalwalletmanagement.service;

import java.util.List;
import com.gcl.personalwalletmanagement.domain.BpsPwlsHead;

/**
 * 人行共建APP白名单批量文件头Service接口
 * 
 * @author yada
 * @date 2022-02-09
 */
public interface IBpsPwlsHeadService 
{
    /**
     * 查询人行共建APP白名单批量文件头
     * 
     * @param id 人行共建APP白名单批量文件头主键
     * @return 人行共建APP白名单批量文件头
     */
    public BpsPwlsHead selectBpsPwlsHeadById(String id);

    /**
     * 查询人行共建APP白名单批量文件头列表
     * 
     * @param bpsPwlsHead 人行共建APP白名单批量文件头
     * @return 人行共建APP白名单批量文件头集合
     */
    public List<BpsPwlsHead> selectBpsPwlsHeadList(BpsPwlsHead bpsPwlsHead);

    /**
     * 新增人行共建APP白名单批量文件头
     * 
     * @param bpsPwlsHead 人行共建APP白名单批量文件头
     * @return 结果
     */
    public int insertBpsPwlsHead(BpsPwlsHead bpsPwlsHead);

    /**
     * 修改人行共建APP白名单批量文件头
     * 
     * @param bpsPwlsHead 人行共建APP白名单批量文件头
     * @return 结果
     */
    public int updateBpsPwlsHead(BpsPwlsHead bpsPwlsHead);

    /**
     * 批量删除人行共建APP白名单批量文件头
     * 
     * @param ids 需要删除的人行共建APP白名单批量文件头主键集合
     * @return 结果
     */
    public int deleteBpsPwlsHeadByIds(String[] ids);

    /**
     * 删除人行共建APP白名单批量文件头信息
     * 
     * @param id 人行共建APP白名单批量文件头主键
     * @return 结果
     */
    public int deleteBpsPwlsHeadById(String id);
}
