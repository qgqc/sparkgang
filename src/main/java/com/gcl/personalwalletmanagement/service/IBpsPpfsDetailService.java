package com.gcl.personalwalletmanagement.service;

import java.util.List;
import com.gcl.personalwalletmanagement.domain.BpsPpfsDetail;

/**
 * 批量开立对私钱包明细Service接口
 * 
 * @author yada
 * @date 2022-02-17
 */
public interface IBpsPpfsDetailService 
{
    /**
     * 查询批量开立对私钱包明细
     * 
     * @param id 批量开立对私钱包明细主键
     * @return 批量开立对私钱包明细
     */
    public BpsPpfsDetail selectBpsPpfsDetailById(String id);

    /**
     * 查询批量开立对私钱包明细列表
     * 
     * @param bpsPpfsDetail 批量开立对私钱包明细
     * @return 批量开立对私钱包明细集合
     */
    public List<BpsPpfsDetail> selectBpsPpfsDetailList(BpsPpfsDetail bpsPpfsDetail);

    /**
     * 新增批量开立对私钱包明细
     * 
     * @param bpsPpfsDetail 批量开立对私钱包明细
     * @return 结果
     */
    public int insertBpsPpfsDetail(BpsPpfsDetail bpsPpfsDetail);

    /**
     * 修改批量开立对私钱包明细
     * 
     * @param bpsPpfsDetail 批量开立对私钱包明细
     * @return 结果
     */
    public int updateBpsPpfsDetail(BpsPpfsDetail bpsPpfsDetail);

    /**
     * 批量删除批量开立对私钱包明细
     * 
     * @param ids 需要删除的批量开立对私钱包明细主键集合
     * @return 结果
     */
    public int deleteBpsPpfsDetailByIds(String[] ids);

    /**
     * 删除批量开立对私钱包明细信息
     * 
     * @param id 批量开立对私钱包明细主键
     * @return 结果
     */
    public int deleteBpsPpfsDetailById(String id);
}
