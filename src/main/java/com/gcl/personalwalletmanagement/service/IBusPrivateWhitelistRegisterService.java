package com.gcl.personalwalletmanagement.service;

import com.gcl.personalwalletmanagement.domain.BusPrivateWhitelistRegister;

import java.util.List;

/**
 * 钱包注册白名单信息Service接口
 * 
 * @author yada
 * @date 2022-02-07
 */
public interface IBusPrivateWhitelistRegisterService 
{
    /**
     * 查询钱包注册白名单信息
     * 
     * @param id 钱包注册白名单信息主键
     * @return 钱包注册白名单信息
     */
    public BusPrivateWhitelistRegister selectBusPrivateWhitelistRegisterById(String id);

    /**
     * 查询钱包注册白名单信息列表
     * 
     * @param busPrivateWhitelistRegister 钱包注册白名单信息
     * @return 钱包注册白名单信息集合
     */
    public List<BusPrivateWhitelistRegister> selectBusPrivateWhitelistRegisterList(BusPrivateWhitelistRegister busPrivateWhitelistRegister);

    /**
     * 新增钱包注册白名单信息
     * 
     * @param busPrivateWhitelistRegister 钱包注册白名单信息
     * @return 结果
     */
    public int insertBusPrivateWhitelistRegister(BusPrivateWhitelistRegister busPrivateWhitelistRegister);

    /**
     * 修改钱包注册白名单信息
     * 
     * @param busPrivateWhitelistRegister 钱包注册白名单信息
     * @return 结果
     */
    public int updateBusPrivateWhitelistRegister(BusPrivateWhitelistRegister busPrivateWhitelistRegister);

    /**
     * 批量删除钱包注册白名单信息
     * 
     * @param ids 需要删除的钱包注册白名单信息主键集合
     * @return 结果
     */
    public int deleteBusPrivateWhitelistRegisterByIds(String[] ids);

    /**
     * 删除钱包注册白名单信息信息
     * 
     * @param id 钱包注册白名单信息主键
     * @return 结果
     */
    public int deleteBusPrivateWhitelistRegisterById(String id);
}
