package com.gcl.personalwalletmanagement.service;

import java.util.List;
import com.gcl.personalwalletmanagement.domain.BpsBwlsHead;

import javax.servlet.http.HttpServletResponse;

/**
 * 对私批量注册白名单文件头Service接口
 * 
 * @author yada
 * @date 2022-02-15
 */
public interface IBpsBwlsHeadService 
{
    /**
     * 查询对私批量注册白名单文件头
     * 
     * @param id 对私批量注册白名单文件头主键
     * @return 对私批量注册白名单文件头
     */
    public BpsBwlsHead selectBpsBwlsHeadById(String id);

    /**
     * 查询对私批量注册白名单文件头列表
     * 
     * @param bpsBwlsHead 对私批量注册白名单文件头
     * @return 对私批量注册白名单文件头集合
     */
    public List<BpsBwlsHead> selectBpsBwlsHeadList(BpsBwlsHead bpsBwlsHead);

    /**
     * 新增对私批量注册白名单文件头
     * 
     * @param bpsBwlsHead 对私批量注册白名单文件头
     * @return 结果
     */
    public int insertBpsBwlsHead(BpsBwlsHead bpsBwlsHead);

    /**
     * 修改对私批量注册白名单文件头
     * 
     * @param bpsBwlsHead 对私批量注册白名单文件头
     * @return 结果
     */
    public int updateBpsBwlsHead(BpsBwlsHead bpsBwlsHead);

    /**
     * 批量删除对私批量注册白名单文件头
     * 
     * @param ids 需要删除的对私批量注册白名单文件头主键集合
     * @return 结果
     */
    public int deleteBpsBwlsHeadByIds(String[] ids);

    /**
     * 删除对私批量注册白名单文件头信息
     * 
     * @param id 对私批量注册白名单文件头主键
     * @return 结果
     */
    public int deleteBpsBwlsHeadById(String id);

    void downloadDATFile(String id, String bwlsType, HttpServletResponse response) throws Exception;
}
