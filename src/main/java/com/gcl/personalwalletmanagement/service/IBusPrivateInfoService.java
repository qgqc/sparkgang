package com.gcl.personalwalletmanagement.service;


import com.gcl.personalwalletmanagement.domain.BusPrivateInfo;

import java.util.List;


/**
 * 个人钱包Service接口
 *
 * @author ruoyi
 * @date 2022-01-13
 */
public interface IBusPrivateInfoService
{
    /**
     * 查询个人钱包
     *
     * @param id 个人钱包主键
     * @return 个人钱包
     */
    public BusPrivateInfo selectBusPrivateInfoById(String id);

    /**
     * 查询个人钱包列表
     *
     * @param busPrivateInfo 个人钱包
     * @return 个人钱包集合
     */
    public List<BusPrivateInfo> selectBusPrivateInfoList(BusPrivateInfo busPrivateInfo);

    /**
     * 新增个人钱包
     *
     * @param busPrivateInfo 个人钱包
     * @return 结果
     */
    public int insertBusPrivateInfo(BusPrivateInfo busPrivateInfo);

    /**
     * 修改个人钱包
     *
     * @param busPrivateInfo 个人钱包
     * @return 结果
     */
    public int updateBusPrivateInfo(BusPrivateInfo busPrivateInfo);
    public int updateDataStatus(String[] ids);
    public int updateDataStatusReview(String[] ids);
    /**
     * 批量删除个人钱包
     *
     * @param ids 需要删除的个人钱包主键集合
     * @return 结果
     */
    public int deleteBusPrivateInfoByIds(String[] ids);

    /**
     * 删除个人钱包信息
     *
     * @param id 个人钱包主键
     * @return 结果
     */
    public int deleteBusPrivateInfoById(String id);
}
