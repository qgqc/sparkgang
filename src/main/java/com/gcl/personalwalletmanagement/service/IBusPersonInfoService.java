package com.gcl.personalwalletmanagement.service;

import java.util.List;

import com.gcl.personalwalletmanagement.domain.BusPersonInfo;

/**
 * 个人信息导入Service接口
 * 
 * @author yada
 * @date 2022-01-18
 */
public interface IBusPersonInfoService 
{
    /**
     * 查询个人信息导入
     * 
     * @param id 个人信息导入主键
     * @return 个人信息导入
     */
    public BusPersonInfo selectBusPersonInfoById(String id);

    /**
     * 查询个人信息导入列表
     * 
     * @param busPersonInfo 个人信息导入
     * @return 个人信息导入集合
     */
    public List<BusPersonInfo> selectBusPersonInfoList(BusPersonInfo busPersonInfo);

    /**
     * 新增个人信息导入
     * 
     * @param busPersonInfo 个人信息导入
     * @return 结果
     */
    public int insertBusPersonInfo(BusPersonInfo busPersonInfo);

    /**
     * 修改个人信息导入
     * 
     * @param busPersonInfo 个人信息导入
     * @return 结果
     */
    public int updateBusPersonInfo(BusPersonInfo busPersonInfo);

    /**
     * 批量删除个人信息导入
     * 
     * @param ids 需要删除的个人信息导入主键集合
     * @return 结果
     */
    public int deleteBusPersonInfoByIds(String[] ids);
    
    /**
     * 批量提交审核 个人信息导入
     * 
     * @param ids 需要提交审核的个人信息导入主键集合
     * @return 结果
     */
    public int submitAudit(String[] ids);
    
    /**
     * 批量审核通过
     * 
     * @param ids 主键集合
     * @return 结果
     */
    public int auditPassed(String[] ids);
    /**
     * 批量审核退回
     * 
     * @param ids 主键集合
     * @return 结果
     */
    public int auditReturn(String[] ids);

    /**
     * 删除个人信息导入信息
     * 
     * @param id 个人信息导入主键
     * @return 结果
     */
    public int deleteBusPersonInfoById(String id);
    
    /**
     * @param busPersonInfoList 个人信息数据列表
     * @param isUpdateSupport	是否更新支持，如果已存在，则进行更新数据
     * @param operName	操作用户
     * @return结果
     */
    public String importBusPersonInfo(List<BusPersonInfo> busPersonInfoList, Boolean isUpdateSupport, String operName);

	public Long getSeqBpsSequenceNumbeValue();
}
