package com.gcl.personalwalletmanagement.service;

import java.util.List;

import com.gcl.bps.domain.OrgNoAndTellerNo;
import com.gcl.personalwalletmanagement.domain.BpsBwlsDetail;

/**
 * 对私批量注册白名单文件明细Service接口
 * 
 * @author yada
 * @date 2022-02-15
 */
public interface IBpsBwlsDetailService 
{
    /**
     * 查询对私批量注册白名单文件明细
     * 
     * @param id 对私批量注册白名单文件明细主键
     * @return 对私批量注册白名单文件明细
     */
    public BpsBwlsDetail selectBpsBwlsDetailById(String id);

    /**
     * 查询对私批量注册白名单文件明细列表
     * 
     * @param bpsBwlsDetail 对私批量注册白名单文件明细
     * @return 对私批量注册白名单文件明细集合
     */
    public List<BpsBwlsDetail> selectBpsBwlsDetailList(BpsBwlsDetail bpsBwlsDetail);

    /**
     * 新增对私批量注册白名单文件明细
     * 
     * @param bpsBwlsDetail 对私批量注册白名单文件明细
     * @return 结果
     */
    public int insertBpsBwlsDetail(BpsBwlsDetail bpsBwlsDetail);

    /**
     * 修改对私批量注册白名单文件明细
     * 
     * @param bpsBwlsDetail 对私批量注册白名单文件明细
     * @return 结果
     */
    public int updateBpsBwlsDetail(BpsBwlsDetail bpsBwlsDetail);

    /**
     * 批量删除对私批量注册白名单文件明细
     * 
     * @param ids 需要删除的对私批量注册白名单文件明细主键集合
     * @return 结果
     */
    public int deleteBpsBwlsDetailByIds(String[] ids);

    /**
     * 删除对私批量注册白名单文件明细信息
     * 
     * @param id 对私批量注册白名单文件明细主键
     * @return 结果
     */
    public int deleteBpsBwlsDetailById(String id);

	/**
	 * 通过文件批次号查询机构号和柜员号的列表
	 * 
	 * @param fileBatchNo
	 * @return
	 */
	public List<OrgNoAndTellerNo> selectOrgNoAndTellerNoListByFileBatchNo(String fileBatchNo);
}
