package com.gcl.personalwalletmanagement.enums;

/**
 * @program:DigitalRMBManagementPlatform
 * @description:
 * @author: shaowu.ni
 * @create:2021-12-03 09:35
 **/
public enum PriWalletDataStatus {
    INITIALIZATION("0","数据初始化"),
    SUBMIT_REVIEW("1","待审核"),
    REVIEW_PASSED("2","审核通过"),
    REVIEW_REJECTION("3","审核退回"),
    SYSTEM_PROCESSING("4","系统处理中(包括bps的处理状态)"),
    SUCCESSFUL_OPENING("5","开立成功"),
    OPENING_FAILED("6","开立失败"),
    BWLS_SYSTEM_PROCESSING("7","系统处理中(包括bps的处理状态)"),
    BWLS_SUCCESSFUL_OPENING("8","开立成功"),
    BWLS_OPENING_FAILED("9","开立失败"),
    PPFS_SYSTEM_PROCESSING("10","系统处理中(包括bps的处理状态)"),
    PPFS_SUCCESSFUL_OPENING("11","开立成功"),
    PPFS_OPENING_FAILED("12","开立失败"),
    SYSTEM_PROCESSINGS("99", "私人钱包开立系统生成文件时进行打标");

    private String status;
    private String value;

    PriWalletDataStatus(String status, String value){
        this.status = status;
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
