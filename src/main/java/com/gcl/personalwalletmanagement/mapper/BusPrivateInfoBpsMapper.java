package com.gcl.personalwalletmanagement.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.gcl.bps.domain.OrgAndNum;
import com.gcl.personalwalletmanagement.domain.BusPrivateInfoBps;

/**
 * 个人钱包Mapper接口
 * 
 * @author yada
 * @date 2022-02-10
 */
public interface BusPrivateInfoBpsMapper 
{
    /**
     * 查询个人钱包
     * 
     * @param id 个人钱包主键
     * @return 个人钱包
     */
    public BusPrivateInfoBps selectBusPrivateInfoBpsById(String id);

    /**
     * 查询个人钱包列表
     * 
     * @param busPrivateInfoBps 个人钱包
     * @return 个人钱包集合
     */
    public List<BusPrivateInfoBps> selectBusPrivateInfoBpsList(BusPrivateInfoBps busPrivateInfoBps);

    /**
     * 新增个人钱包
     * 
     * @param busPrivateInfoBps 个人钱包
     * @return 结果
     */
    public int insertBusPrivateInfoBps(BusPrivateInfoBps busPrivateInfoBps);

    /**
     * 修改个人钱包
     * 
     * @param busPrivateInfoBps 个人钱包
     * @return 结果
     */
    public int updateBusPrivateInfoBps(BusPrivateInfoBps busPrivateInfoBps);

    /**
     * 删除个人钱包
     * 
     * @param id 个人钱包主键
     * @return 结果
     */
    public int deleteBusPrivateInfoBpsById(String id);

    /**
     * 批量删除个人钱包
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusPrivateInfoBpsByIds(String[] ids);

    void updateAll(ArrayList<BusPrivateInfoBps> list);

    void insertByStatus(Map insertObj);

    int countByDataStatusAndExamineSuccessDateGreaterThanEqual(Map queryParam);

    void updatefileBatchNoByStatus(Map queryParam);

    List<OrgAndNum> selectCount(String fileBatchNo);

    ArrayList<BusPrivateInfoBps> selectByFileBatchNo(Map queryParam);

    void updateBatchBySequenceNumberAndHeadId(ArrayList<BusPrivateInfoBps> list);
}
