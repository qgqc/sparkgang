package com.gcl.personalwalletmanagement.mapper;

import java.util.HashMap;
import java.util.List;

import com.gcl.personalwalletmanagement.domain.BusPersonInfo;

/**
 * 个人信息导入Mapper接口
 * 
 * @author yada
 * @date 2022-01-18
 */
public interface BusPersonInfoMapper 
{
    /**
     * 查询个人信息导入
     * 
     * @param id 个人信息导入主键
     * @return 个人信息导入
     */
    public BusPersonInfo selectBusPersonInfoById(String id);

    /**
     * 查询个人信息导入列表
     * 
     * @param busPersonInfo 个人信息导入
     * @return 个人信息导入集合
     */
    public List<BusPersonInfo> selectBusPersonInfoList(BusPersonInfo busPersonInfo);

    /**
     * 新增个人信息导入
     * 
     * @param busPersonInfo 个人信息导入
     * @return 结果
     */
    public int insertBusPersonInfo(BusPersonInfo busPersonInfo);

    /**
     * 修改个人信息导入
     * 
     * @param busPersonInfo 个人信息导入
     * @return 结果
     */
    public int updateBusPersonInfo(BusPersonInfo busPersonInfo);
    
    /**
     * 根据id数组批量更新数据状态
     * @param ids 需要提交审核的个人信息导入主键集合
     * @param dataStatus 数据状态 0:数据初始化1:待审核2:审核通过3:审核退回
     * 4:人行共建app钱包系统处理中5:人行共建app钱包开立成功 6:人行共建app钱包开立失败
     * 7:钱包注册白名单系统处理中8:钱包注册白名单开立成功9:钱包注册白名单开立失败
     * 10:私人钱包系统处理中11:私人钱包开立成功12:私人钱包开立失败
     * @return 更新成功条数
     */
    public int updateDataStatusByIds(HashMap<Object,Object> map);
    
    /**
     * 删除个人信息导入
     * 
     * @param id 个人信息导入主键
     * @return 结果
     */
    public int deleteBusPersonInfoById(String id);

    /**
     * 批量删除个人信息导入
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusPersonInfoByIds(String[] ids);
    
    
    /**
     * 通过证件号查询个人信息
     * @param idCard
     * @return
     */
    public BusPersonInfo selectBusPersonInfoByIdCard(String idCard);

	public Long getSeqBpsSequenceNumbeValue();
}
