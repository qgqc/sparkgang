package com.gcl.personalwalletmanagement.mapper;

import java.util.List;
import com.gcl.personalwalletmanagement.domain.BpsPwlsDetail;

/**
 * 人行共建APP白名单批量文件明细Mapper接口
 * 
 * @author yada
 * @date 2022-02-09
 */
public interface BpsPwlsDetailMapper 
{
    /**
     * 查询人行共建APP白名单批量文件明细
     * 
     * @param id 人行共建APP白名单批量文件明细主键
     * @return 人行共建APP白名单批量文件明细
     */
    public BpsPwlsDetail selectBpsPwlsDetailById(String id);

    /**
     * 查询人行共建APP白名单批量文件明细列表
     * 
     * @param bpsPwlsDetail 人行共建APP白名单批量文件明细
     * @return 人行共建APP白名单批量文件明细集合
     */
    public List<BpsPwlsDetail> selectBpsPwlsDetailList(BpsPwlsDetail bpsPwlsDetail);

    /**
     * 新增人行共建APP白名单批量文件明细
     * 
     * @param bpsPwlsDetail 人行共建APP白名单批量文件明细
     * @return 结果
     */
    public int insertBpsPwlsDetail(BpsPwlsDetail bpsPwlsDetail);

    /**
     * 修改人行共建APP白名单批量文件明细
     * 
     * @param bpsPwlsDetail 人行共建APP白名单批量文件明细
     * @return 结果
     */
    public int updateBpsPwlsDetail(BpsPwlsDetail bpsPwlsDetail);

    /**
     * 删除人行共建APP白名单批量文件明细
     * 
     * @param id 人行共建APP白名单批量文件明细主键
     * @return 结果
     */
    public int deleteBpsPwlsDetailById(String id);

    /**
     * 批量删除人行共建APP白名单批量文件明细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBpsPwlsDetailByIds(String[] ids);
}
