package com.gcl.personalwalletmanagement.mapper;

import java.util.List;
import com.gcl.personalwalletmanagement.domain.PreH5PersonWalletInfo;

/**
 * H5个人信息收集报表Mapper接口
 * 
 * @author yada
 * @date 2022-02-25
 */
public interface PreH5PersonWalletInfoMapper 
{
    /**
     * 查询H5个人信息收集报表
     * 
     * @param id H5个人信息收集报表主键
     * @return H5个人信息收集报表
     */
    public PreH5PersonWalletInfo selectPreH5PersonWalletInfoById(String id);

    /**
     * 查询H5个人信息收集报表列表
     * 
     * @param preH5PersonWalletInfo H5个人信息收集报表
     * @return H5个人信息收集报表集合
     */
    public List<PreH5PersonWalletInfo> selectPreH5PersonWalletInfoList(PreH5PersonWalletInfo preH5PersonWalletInfo);

    /**
     * 新增H5个人信息收集报表
     * 
     * @param preH5PersonWalletInfo H5个人信息收集报表
     * @return 结果
     */
    public int insertPreH5PersonWalletInfo(PreH5PersonWalletInfo preH5PersonWalletInfo);

    /**
     * 修改H5个人信息收集报表
     * 
     * @param preH5PersonWalletInfo H5个人信息收集报表
     * @return 结果
     */
    public int updatePreH5PersonWalletInfo(PreH5PersonWalletInfo preH5PersonWalletInfo);

    /**
     * 删除H5个人信息收集报表
     * 
     * @param id H5个人信息收集报表主键
     * @return 结果
     */
    public int deletePreH5PersonWalletInfoById(String id);

    /**
     * 批量删除H5个人信息收集报表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePreH5PersonWalletInfoByIds(String[] ids);
}
