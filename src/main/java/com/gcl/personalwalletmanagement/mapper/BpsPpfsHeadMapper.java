package com.gcl.personalwalletmanagement.mapper;

import java.util.List;

import com.gcl.personalwalletmanagement.domain.BpsPpfsHead;

/**
 * 批量开立对私钱包文件头Mapper接口
 * 
 * @author yada
 * @date 2022-02-17
 */
public interface BpsPpfsHeadMapper 
{
    /**
     * 查询批量开立对私钱包文件头
     * 
     * @param id 批量开立对私钱包文件头主键
     * @return 批量开立对私钱包文件头
     */
    public BpsPpfsHead selectBpsPpfsHeadById(String id);

    /**
     * 查询批量开立对私钱包文件头列表
     * 
     * @param bpsPpfsHead 批量开立对私钱包文件头
     * @return 批量开立对私钱包文件头集合
     */
    public List<BpsPpfsHead> selectBpsPpfsHeadList(BpsPpfsHead bpsPpfsHead);

    /**
     * 新增批量开立对私钱包文件头
     * 
     * @param bpsPpfsHead 批量开立对私钱包文件头
     * @return 结果
     */
    public int insertBpsPpfsHead(BpsPpfsHead bpsPpfsHead);

    /**
     * 修改批量开立对私钱包文件头
     * 
     * @param bpsPpfsHead 批量开立对私钱包文件头
     * @return 结果
     */
    public int updateBpsPpfsHead(BpsPpfsHead bpsPpfsHead);

    /**
     * 删除批量开立对私钱包文件头
     * 
     * @param id 批量开立对私钱包文件头主键
     * @return 结果
     */
    public int deleteBpsPpfsHeadById(String id);

    /**
     * 批量删除批量开立对私钱包文件头
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBpsPpfsHeadByIds(String[] ids);

	public BpsPpfsHead findMaxBatchNoByCurrentDateAndOrgNo(BpsPpfsHead bpsPpfsHead);
}
