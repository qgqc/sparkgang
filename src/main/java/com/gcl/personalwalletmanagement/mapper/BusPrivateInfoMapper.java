package com.gcl.personalwalletmanagement.mapper;


import com.gcl.personalwalletmanagement.domain.BusPrivateInfo;

import java.util.List;
import java.util.Map;

/**
 * 个人钱包Mapper接口
 * wjw:这个Mapper接口的对应xml文件为什么命名为PrivateFileUploadMapper.xml呢，看不懂，不知道谁做的？
 * @author ruoyi
 * @date 2022-01-13
 */
public interface BusPrivateInfoMapper
{
    /**
     * 查询个人钱包
     *
     * @param id 个人钱包主键
     * @return 个人钱包
     */
    public BusPrivateInfo selectBusPrivateInfoById(String id);

    /**
     * 查询个人钱包列表
     *
     * @param busPrivateInfo 个人钱包
     * @return 个人钱包集合
     */
    public List<BusPrivateInfo> selectBusPrivateInfoList(BusPrivateInfo busPrivateInfo);

    /**
     * 新增个人钱包
     *
     * @param busPrivateInfo 个人钱包
     * @return 结果
     */
    public int insertBusPrivateInfo(BusPrivateInfo busPrivateInfo);

    /**
     * 修改个人钱包
     *
     * @param busPrivateInfo 个人钱包
     * @return 结果
     */
    public int updateBusPrivateInfo(BusPrivateInfo busPrivateInfo);

    /**
     * 删除个人钱包
     *
     * @param id 个人钱包主键
     * @return 结果
     */
    public int deleteBusPrivateInfoById(String id);

    /**
     * 批量删除个人钱包
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusPrivateInfoByIds(String[] ids);

    void updatePwlsUpdateById(Map queryParam);

    void updatePwlsStatusByHeadId(Map queryParam);

    void updatePwlsBusinessTable(Map param);



    void updatePPFSUpdateById(Map queryParam);

    void updatePPFSStatus(Map queryParam);

    void updatePPFSBusinessTable(Map param);

}
