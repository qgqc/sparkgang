package com.gcl.personalwalletmanagement.mapper;

import java.util.List;

import com.gcl.bps.domain.OrgNoAndTellerNo;
import com.gcl.personalwalletmanagement.domain.BpsPpfsDetail;

/**
 * 批量开立对私钱包明细Mapper接口
 * 
 * @author yada
 * @date 2022-02-17
 */
public interface BpsPpfsDetailMapper 
{
    /**
     * 查询批量开立对私钱包明细
     * 
     * @param id 批量开立对私钱包明细主键
     * @return 批量开立对私钱包明细
     */
    public BpsPpfsDetail selectBpsPpfsDetailById(String id);

    /**
     * 查询批量开立对私钱包明细列表
     * 
     * @param bpsPpfsDetail 批量开立对私钱包明细
     * @return 批量开立对私钱包明细集合
     */
    public List<BpsPpfsDetail> selectBpsPpfsDetailList(BpsPpfsDetail bpsPpfsDetail);

    /**
     * 新增批量开立对私钱包明细
     * 
     * @param bpsPpfsDetail 批量开立对私钱包明细
     * @return 结果
     */
    public int insertBpsPpfsDetail(BpsPpfsDetail bpsPpfsDetail);

    /**
     * 修改批量开立对私钱包明细
     * 
     * @param bpsPpfsDetail 批量开立对私钱包明细
     * @return 结果
     */
    public int updateBpsPpfsDetail(BpsPpfsDetail bpsPpfsDetail);

    /**
     * 删除批量开立对私钱包明细
     * 
     * @param id 批量开立对私钱包明细主键
     * @return 结果
     */
    public int deleteBpsPpfsDetailById(String id);

    /**
     * 批量删除批量开立对私钱包明细
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBpsPpfsDetailByIds(String[] ids);

	/**
	 * 通过文件批次号查询机构号和柜员号的列表
	 * 
	 * @param fileBatchNo
	 * @return
	 */
	public List<OrgNoAndTellerNo> selectOrgNoAndTellerNoListByFileBatchNo(String fileBatchNo);
}
