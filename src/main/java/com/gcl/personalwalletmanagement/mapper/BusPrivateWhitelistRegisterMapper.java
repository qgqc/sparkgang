package com.gcl.personalwalletmanagement.mapper;

import com.gcl.bps.domain.OrgAndNum;
import com.gcl.personalwalletmanagement.domain.BusPrivateWhitelistRegister;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 钱包注册白名单信息Mapper接口
 * 
 * @author wjw
 * @date 2022-02-07
 */
public interface BusPrivateWhitelistRegisterMapper 
{
    /**
     * 查询钱包注册白名单信息
     * 
     * @param id 钱包注册白名单信息主键
     * @return 钱包注册白名单信息
     */
    public BusPrivateWhitelistRegister selectBusPrivateWhitelistRegisterById(String id);

    /**
     * 查询钱包注册白名单信息列表
     * 
     * @param busPrivateWhitelistRegister 钱包注册白名单信息
     * @return 钱包注册白名单信息集合
     */
    public List<BusPrivateWhitelistRegister> selectBusPrivateWhitelistRegisterList(BusPrivateWhitelistRegister busPrivateWhitelistRegister);

    /**
     * 新增钱包注册白名单信息
     * 
     * @param busPrivateWhitelistRegister 钱包注册白名单信息
     * @return 结果
     */
    public int insertBusPrivateWhitelistRegister(BusPrivateWhitelistRegister busPrivateWhitelistRegister);

    /**
     * 修改钱包注册白名单信息
     * 
     * @param busPrivateWhitelistRegister 钱包注册白名单信息
     * @return 结果
     */
    public int updateBusPrivateWhitelistRegister(BusPrivateWhitelistRegister busPrivateWhitelistRegister);

    /**
     * 删除钱包注册白名单信息
     * 
     * @param id 钱包注册白名单信息主键
     * @return 结果
     */
    public int deleteBusPrivateWhitelistRegisterById(String id);

    /**
     * 批量删除钱包注册白名单信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteBusPrivateWhitelistRegisterByIds(String[] ids);

    int countByDataStatusAndExamineDateGreaterThanEqual(Map queryParam);

    void updatefileBatchNoByStatus(Map queryParam);

    List<OrgAndNum> selectCount(String fileBatchNo);

    ArrayList<BusPrivateWhitelistRegister> selectByFileBatchNo(Map queryParam);

    void updateAll(ArrayList<BusPrivateWhitelistRegister> list);

    void insertByStatus(Map map);

    void updateBatchBySequenceNumberAndHeadId(ArrayList<Object[]> list);
}
