package com.gcl.common.utils.bean;


import com.gcl.corporatewalletmanagement.domain.BusMerWalletInfo;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * @program:DigitalRMBManagementPlatform
 * @description:
 * @author: shaowu.ni
 * @create:2021-11-26 14:54
 **/

public class VerificationField {
    private static ArrayList<String> entCertType = new ArrayList<String>() {{
        add("IT11");
        add("IT12");
        add("IT13");
        add("IT14");
        add("IT14");
        add("IT15");
        add("IT16");
        add("IT17");
        add("IT99");
    }};
    private static ArrayList<String> entLegalPsCertType = new ArrayList<String>() {{
        add("IT01");
        add("IT02");
        add("IT03");
        add("IT04");
        add("IT04");
        add("IT05");
        add("IT06");
        add("IT07");
        add("IT08");
        add("IT09");
        add("IT10");
        add("IT11");
        add("IT12");
        add("IT31");
        add("IT32");
        add("IT33");
        add("IT99");
    }};

    private static ArrayList<String> bindBankAccountType = new ArrayList<String>() {{
        add("AT04");
        add("AT05");
        add("AT06");
        add("AT07");
    }};

    public static String verificationField(BusMerWalletInfo merFileInfo) {
        StringBuilder stringBuilder = new StringBuilder();
        if(merFileInfo == null){
            stringBuilder.append("<br/>请使用系统提供的模板进行数据导入");
            return stringBuilder.toString();
        }
        //客户号重复不能导入加校验
        if (StringUtils.isBlank(merFileInfo.getCustNo())||merFileInfo.getCustNo().length()>40) {
            stringBuilder.append("<br/>第1列企业客户号为空或长度超过40个字符,");
        }
        if (!StringUtils.isBlank(merFileInfo.getEntName())&& merFileInfo.getEntName().length()>100) {
           stringBuilder.append("<br/>第2列企业名称有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getCustType())&&!"01".equals(merFileInfo.getCustType())&& !"02".equals(merFileInfo.getCustType())) {
           stringBuilder.append("<br/>第3列客户类型有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getEntAddr() ) && merFileInfo.getEntAddr().length() > 125) {
           stringBuilder.append("<br/>第4列企业地址有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getIndustryType())&& (merFileInfo.getIndustryType().length() != 1 || !isLetter(merFileInfo.getIndustryType()))) {
           stringBuilder.append("<br/>第5列行业类型有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getEntCertType()) && !entCertType.contains(merFileInfo.getEntCertType())) {
           stringBuilder.append("<br/>第6列企业证件号类型有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getEntCertNo()) && merFileInfo.getEntCertNo().length() > 32) {
           stringBuilder.append("<br/>第7列企业证件号有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getEntCertTypeExpireDate()) && merFileInfo.getEntCertTypeExpireDate().length() != 8) {
           stringBuilder.append("<br/>第8列企业证件到期日有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getEntCustContName()) && merFileInfo.getEntCustContName().length()>40) {
           stringBuilder.append("<br/>第9列联系人姓名有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getEntCustContPhone()) && merFileInfo.getEntCustContPhone().length()>18) {
           stringBuilder.append("<br/>第10列联系人手机号有误,");
        }
      /*  if (!StringUtils.isEmpty(merFileInfo.getEntCustContCertType())&& !"IT01".equals(merFileInfo.getEntCustContCertType())) {
           stringBuilder.append("<br/>第11列联系人证件类型有误,");
        }*/
        if (!StringUtils.isBlank(merFileInfo.getEntCustContCertNo()) && merFileInfo.getEntCustContCertNo().length()>32) {
           stringBuilder.append("<br/>第12列联系人证件号码有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getEntCustConCertNoExDate()) && merFileInfo.getEntCustConCertNoExDate().length()!= 8) {
           stringBuilder.append("<br/>第13列联系人证件到期日有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getEntLegalPsName()) && merFileInfo.getEntLegalPsName().length()>40) {
           stringBuilder.append("<br/>第14列企业法人姓名有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getEntLegalPsCertType()) && !entLegalPsCertType.contains(merFileInfo.getEntLegalPsCertType())) {
           stringBuilder.append("<br/>第15列企业法人身份证件类型有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getEntLegalPsCertNo()) && merFileInfo.getEntLegalPsCertNo().length()>32) {
           stringBuilder.append("<br/>第16列企业法人证件号码有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getEntLegalPsCertExpireDate()) && merFileInfo.getEntLegalPsCertExpireDate().length()!= 8) {
           stringBuilder.append("<br/>第17列企业法人证件到期日有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getEntCreationDate()) && merFileInfo.getEntCreationDate().length()!= 8) {
           stringBuilder.append("<br/>第18列企业成立日期有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getDurationBusinessLicense()) && merFileInfo.getDurationBusinessLicense().length()!= 8) {
           stringBuilder.append("<br/>第19列营业执照期限有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getBusinessScope()) && merFileInfo.getBusinessScope().length()> 30) {
           stringBuilder.append("<br/>第20列经营范围有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getBusinessLicenseNo()) && merFileInfo.getBusinessLicenseNo().length()>32) {
           stringBuilder.append("<br/>第21列营业执照号码有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getUniformSocialCreditCode()) && merFileInfo.getUniformSocialCreditCode().length()>32) {
           stringBuilder.append("<br/>第22列统一社会信用代码有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getOrganizationCode()) && merFileInfo.getOrganizationCode().length()>32) {
           stringBuilder.append("<br/>第23列组织机构代码有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getTaxRegistrationNo()) && merFileInfo.getTaxRegistrationNo().length()>32) {
           stringBuilder.append("<br/>第24列税务登记号有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getEnterpriseOwnedProvince()) && merFileInfo.getEnterpriseOwnedProvince().length()!=6|| !isInteger(merFileInfo.getEnterpriseOwnedProvince())) {
           stringBuilder.append("<br/>第25列企业所属省份有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getEnterpriseOwnedCity()) && merFileInfo.getEnterpriseOwnedCity().length()!= 6|| !isInteger(merFileInfo.getEnterpriseOwnedCity())) {
           stringBuilder.append("<br/>第26列企业所属城市有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getAccountName()) && merFileInfo.getAccountName().length()> 100) {
           stringBuilder.append("<br/>第27列账户名称有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getAccount()) && merFileInfo.getAccount().length()!=12) {
           stringBuilder.append("<br/>第28列开户账号有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getBindBankAccountType())&&!bindBankAccountType.contains(merFileInfo.getBindBankAccountType())) {
           stringBuilder.append("<br/>第29列开户类型有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getBankBranchNo()) && isInteger(merFileInfo.getBankBranchNo())&&merFileInfo.getBankBranchNo().length()!=5) {
           stringBuilder.append("<br/>第30列开户行机构号有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getBankBranchName()) && merFileInfo.getBankBranchName().length()> 20) {
           stringBuilder.append("<br/>第31列开户行名称有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getMccCode()) && isInteger(merFileInfo.getMccCode()) && merFileInfo.getMccCode().length()!=4) {
           stringBuilder.append("<br/>第32列MCC编码有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getMerchantResponsiblePerson()) && merFileInfo.getMerchantResponsiblePerson().length()>40) {
           stringBuilder.append("<br/>第33列商家责任人有误,");
        }
        if (!StringUtils.isBlank(merFileInfo.getMerchantAcctOpenContName()) && merFileInfo.getMerchantAcctOpenContName().length()>40) {
           stringBuilder.append("<br/>第34列商家开户联系人姓名,");
        }
        if (!StringUtils.isBlank(merFileInfo.getMerchantAcctOpenContPhone()) && isInteger(merFileInfo.getMerchantAcctOpenContPhone())&&merFileInfo.getMerchantAcctOpenContPhone().length()>18) {
           stringBuilder.append("<br/>第35列商家开户联系人电话,");
        }
        if (!StringUtils.isBlank(merFileInfo.getCustMngOrgNo()) && merFileInfo.getCustMngOrgNo().length()!=5) {
           stringBuilder.append("<br/>第36列客户经理所属机构号,");
        }
        if (!StringUtils.isBlank(merFileInfo.getCustMngId()) && merFileInfo.getCustMngId().length()>10) {
           stringBuilder.append("<br/>第37列客户经理员工号,");
        }
        if (!StringUtils.isBlank(merFileInfo.getAccountManager()) && merFileInfo.getAccountManager().length()>40) {
           stringBuilder.append("<br/>第38列客户经理姓名,");
        }
        return stringBuilder.toString();
    }

    public static boolean isInteger(String str) {
        Pattern pattern = Pattern.compile("^[\\d]*$");
        return pattern.matcher(str).matches();
    }


    public static boolean isLetter(String str){
        Pattern pattern = Pattern.compile("^[A-T]+$");
        return pattern.matcher(str).matches();
    }
}
