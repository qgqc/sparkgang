package com.gcl.common.constant;

/**
 *  业务逻辑执行结果，controller接口返回结果值
 * 
 * @author wjw
 */
public class ResultStatus
{
    /**
     * 操作成功
     */
    public static final int SUCCESS = 1;


    /**
     * 操作失败
     */
    public static final int FAIL = 0;


}
