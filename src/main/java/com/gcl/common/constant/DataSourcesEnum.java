package com.gcl.common.constant;

/**
 * 数据来源
 * @author zhb
 */
public enum DataSourcesEnum {
    /**
     * 数据来源
      */
    STOCK_DATA("1","存量数据导入"),
    NEW_DATA("2","新增数据导入"),
    ;

    private String status;
    private String value;

    DataSourcesEnum(String status, String value){
        this.status = status;
        this.value = value;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
