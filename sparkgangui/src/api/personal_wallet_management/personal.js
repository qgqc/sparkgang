import request from '@/utils/request'

// 查询个人钱包列表
export function listInfo(query) {
  return request({
    url: '/private/info/list',
    method: 'get',
    params: query
  })
}

// 查询个人钱包列表
export function reviewList(query) {
  return request({
    url: '/private/info/reviewList',
    method: 'get',
    params: query
  })
}

// 查询个人钱包详细
export function getInfo(id) {
  return request({
    url: '/private/info/' + id,
    method: 'get'
  })
}

// 新增个人钱包
export function addInfo(data) {
  return request({
    url: '/private/info',
    method: 'post',
    data: data
  })
}

// 修改个人钱包
export function updateInfo(data) {
  return request({
    url: '/private/info',
    method: 'put',
    data: data
  })
}

// 删除个人钱包
export function delInfo(id) {
  return request({
    url: '/private/info/' + id,
    method: 'delete'
  })
}

// 审核
export function submitAudit(id) {
  return request({
    url: '/private/info/submitAudit/' + id,
    method: 'delete'
  })
}
// 审核
export function auditSuccess(id) {
  return request({
    url: '/private/info/auditSuccess/' + id,
    method: 'delete'
  })
}
