import request from '@/utils/request'

// 查询智能合约管理列表
export function listBussmartcontractinfo(query) {
  return request({
    url: '/redenvelopemanagement/bussmartcontractinfo/list',
    method: 'get',
    params: query
  })
}

// 查询智能合约管理详细
export function getBussmartcontractinfo(id) {
  return request({
    url: '/redenvelopemanagement/bussmartcontractinfo/' + id,
    method: 'get'
  })
}

// 新增智能合约管理
export function addBussmartcontractinfo(data) {
  return request({
    url: '/redenvelopemanagement/bussmartcontractinfo',
    method: 'post',
    data: data
  })
}

// 修改智能合约管理
export function updateBussmartcontractinfo(data) {
  return request({
    url: '/redenvelopemanagement/bussmartcontractinfo',
    method: 'put',
    data: data
  })
}

// 删除智能合约管理
export function delBussmartcontractinfo(id) {
  return request({
    url: '/redenvelopemanagement/bussmartcontractinfo/' + id,
    method: 'delete'
  })
}
