import request from '@/utils/request'

// 查询生成测试列表
export function listTest(query) {
  return request({
    url: '/redenvelopemanagement/test/list',
    method: 'get',
    params: query
  })
}

// 查询生成测试详细
export function getTest(id) {
  return request({
    url: '/redenvelopemanagement/test/' + id,
    method: 'get'
  })
}

// 新增生成测试
export function addTest(data) {
  return request({
    url: '/redenvelopemanagement/test',
    method: 'post',
    data: data
  })
}

// 修改生成测试
export function updateTest(data) {
  return request({
    url: '/redenvelopemanagement/test',
    method: 'put',
    data: data
  })
}

// 删除生成测试
export function delTest(id) {
  return request({
    url: '/redenvelopemanagement/test/' + id,
    method: 'delete'
  })
}
