import request from '@/utils/request'

// 查询人行共建APP白名单批量文件头列表
export function listBpsPwlsHead(query) {
  return request({
    url: '/personalwalletmanagement/BpsPwlsHead/list',
    method: 'get',
    params: query
  })
}

// 查询人行共建APP白名单批量文件头详细
export function getBpsPwlsHead(id) {
  return request({
    url: '/personalwalletmanagement/BpsPwlsHead/' + id,
    method: 'get'
  })
}

// 新增人行共建APP白名单批量文件头
export function addBpsPwlsHead(data) {
  return request({
    url: '/personalwalletmanagement/BpsPwlsHead',
    method: 'post',
    data: data
  })
}

// 修改人行共建APP白名单批量文件头
export function updateBpsPwlsHead(data) {
  return request({
    url: '/personalwalletmanagement/BpsPwlsHead',
    method: 'put',
    data: data
  })
}

// 删除人行共建APP白名单批量文件头
export function delBpsPwlsHead(id) {
  return request({
    url: '/personalwalletmanagement/BpsPwlsHead/' + id,
    method: 'delete'
  })
}
