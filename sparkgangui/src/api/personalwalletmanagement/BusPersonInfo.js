import request from '@/utils/request'

// 查询个人信息导入列表
export function listBusPersonInfo(query) {
  return request({
    url: '/personalwalletmanagement/BusPersonInfo/list',
    method: 'get',
    params: query
  })
}

// 查询个人信息导入详细
export function getBusPersonInfo(id) {
  return request({
    url: '/personalwalletmanagement/BusPersonInfo/' + id,
    method: 'get'
  })
}

// 新增个人信息导入
export function addBusPersonInfo(data) {
  return request({
    url: '/personalwalletmanagement/BusPersonInfo',
    method: 'post',
    data: data
  })
}

// 修改个人信息导入
export function updateBusPersonInfo(data) {
  return request({
    url: '/personalwalletmanagement/BusPersonInfo',
    method: 'put',
    data: data
  })
}

// 删除个人信息导入
export function delBusPersonInfo(id) {
  return request({
    url: '/personalwalletmanagement/BusPersonInfo/' + id,
    method: 'delete'
  })
}

// 提交审核
export function submitAudit(id) {
  return request({
    url: '/personalwalletmanagement/BusPersonInfo/submitAudit/' + id,
    method: 'put'
  })
}

// 批量审核通过
export function auditPassed(id) {
  return request({
    url: '/personalwalletmanagement/BusPersonInfo/auditPassed/' + id,
    method: 'put'
  })
}

// 批量审核退回
export function auditReturn(id) {
  return request({
    url: '/personalwalletmanagement/BusPersonInfo/auditReturn/' + id,
    method: 'put'
  })
}
