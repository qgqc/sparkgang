import request from '@/utils/request'

// 查询人行共建APP白名单批量文件明细列表
export function listBpsPwlsDetail(query) {
  return request({
    url: '/personalwalletmanagement/BpsPwlsDetail/list',
    method: 'get',
    params: query
  })
}

// 查询人行共建APP白名单批量文件明细详细
export function getBpsPwlsDetail(id) {
  return request({
    url: '/personalwalletmanagement/BpsPwlsDetail/' + id,
    method: 'get'
  })
}

// 新增人行共建APP白名单批量文件明细
export function addBpsPwlsDetail(data) {
  return request({
    url: '/personalwalletmanagement/BpsPwlsDetail',
    method: 'post',
    data: data
  })
}

// 修改人行共建APP白名单批量文件明细
export function updateBpsPwlsDetail(data) {
  return request({
    url: '/personalwalletmanagement/BpsPwlsDetail',
    method: 'put',
    data: data
  })
}

// 删除人行共建APP白名单批量文件明细
export function delBpsPwlsDetail(id) {
  return request({
    url: '/personalwalletmanagement/BpsPwlsDetail/' + id,
    method: 'delete'
  })
}
