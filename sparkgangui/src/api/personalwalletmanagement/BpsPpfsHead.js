import request from '@/utils/request'

// 查询批量开立对私钱包文件头列表
export function listBpsPpfsHead(query) {
  return request({
    url: '/personalwalletmanagement/BpsPpfsHead/list',
    method: 'get',
    params: query
  })
}

// 查询批量开立对私钱包文件头详细
export function getBpsPpfsHead(id) {
  return request({
    url: '/personalwalletmanagement/BpsPpfsHead/' + id,
    method: 'get'
  })
}

// 新增批量开立对私钱包文件头
export function addBpsPpfsHead(data) {
  return request({
    url: '/personalwalletmanagement/BpsPpfsHead',
    method: 'post',
    data: data
  })
}

// 修改批量开立对私钱包文件头
export function updateBpsPpfsHead(data) {
  return request({
    url: '/personalwalletmanagement/BpsPpfsHead',
    method: 'put',
    data: data
  })
}

// 删除批量开立对私钱包文件头
export function delBpsPpfsHead(id) {
  return request({
    url: '/personalwalletmanagement/BpsPpfsHead/' + id,
    method: 'delete'
  })
}
