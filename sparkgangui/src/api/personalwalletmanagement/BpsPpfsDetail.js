import request from '@/utils/request'

// 查询批量开立对私钱包明细列表
export function listBpsPpfsDetail(query) {
  return request({
    url: '/personalwalletmanagement/BpsPpfsDetail/list',
    method: 'get',
    params: query
  })
}

// 查询批量开立对私钱包明细详细
export function getBpsPpfsDetail(id) {
  return request({
    url: '/personalwalletmanagement/BpsPpfsDetail/' + id,
    method: 'get'
  })
}

// 新增批量开立对私钱包明细
export function addBpsPpfsDetail(data) {
  return request({
    url: '/personalwalletmanagement/BpsPpfsDetail',
    method: 'post',
    data: data
  })
}

// 修改批量开立对私钱包明细
export function updateBpsPpfsDetail(data) {
  return request({
    url: '/personalwalletmanagement/BpsPpfsDetail',
    method: 'put',
    data: data
  })
}

// 删除批量开立对私钱包明细
export function delBpsPpfsDetail(id) {
  return request({
    url: '/personalwalletmanagement/BpsPpfsDetail/' + id,
    method: 'delete'
  })
}
