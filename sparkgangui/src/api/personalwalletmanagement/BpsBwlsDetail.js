import request from '@/utils/request'

// 查询对私批量注册白名单文件明细列表
export function listBpsBwlsDetail(query) {
  return request({
    url: '/personalwalletmanagement/BpsBwlsDetail/list',
    method: 'get',
    params: query
  })
}

// 查询对私批量注册白名单文件明细详细
export function getBpsBwlsDetail(id) {
  return request({
    url: '/personalwalletmanagement/BpsBwlsDetail/' + id,
    method: 'get'
  })
}

// 新增对私批量注册白名单文件明细
export function addBpsBwlsDetail(data) {
  return request({
    url: '/personalwalletmanagement/BpsBwlsDetail',
    method: 'post',
    data: data
  })
}

// 修改对私批量注册白名单文件明细
export function updateBpsBwlsDetail(data) {
  return request({
    url: '/personalwalletmanagement/BpsBwlsDetail',
    method: 'put',
    data: data
  })
}

// 删除对私批量注册白名单文件明细
export function delBpsBwlsDetail(id) {
  return request({
    url: '/personalwalletmanagement/BpsBwlsDetail/' + id,
    method: 'delete'
  })
}
