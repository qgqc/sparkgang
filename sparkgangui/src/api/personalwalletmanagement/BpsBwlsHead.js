import request from '@/utils/request'

// 查询对私批量注册白名单文件头列表
export function listBpsBwlsHead(query) {
  return request({
    url: '/personalwalletmanagement/BpsBwlsHead/list',
    method: 'get',
    params: query
  })
}

// 查询对私批量注册白名单文件头详细
export function getBpsBwlsHead(id) {
  return request({
    url: '/personalwalletmanagement/BpsBwlsHead/' + id,
    method: 'get'
  })
}

// 新增对私批量注册白名单文件头
export function addBpsBwlsHead(data) {
  return request({
    url: '/personalwalletmanagement/BpsBwlsHead',
    method: 'post',
    data: data
  })
}

// 修改对私批量注册白名单文件头
export function updateBpsBwlsHead(data) {
  return request({
    url: '/personalwalletmanagement/BpsBwlsHead',
    method: 'put',
    data: data
  })
}

// 删除对私批量注册白名单文件头
export function delBpsBwlsHead(id) {
  return request({
    url: '/personalwalletmanagement/BpsBwlsHead/' + id,
    method: 'delete'
  })
}
