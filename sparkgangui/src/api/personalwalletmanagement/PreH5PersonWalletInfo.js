import request from '@/utils/request'

// 查询H5个人信息收集报表列表
export function listPreH5PersonWalletInfo(query) {
  return request({
    url: '/personalwalletmanagement/PreH5PersonWalletInfo/list',
    method: 'get',
    params: query
  })
}

// 查询H5个人信息收集报表详细
export function getPreH5PersonWalletInfo(id) {
  return request({
    url: '/personalwalletmanagement/PreH5PersonWalletInfo/' + id,
    method: 'get'
  })
}

// 新增H5个人信息收集报表
export function addPreH5PersonWalletInfo(data) {
  return request({
    url: '/personalwalletmanagement/PreH5PersonWalletInfo',
    method: 'post',
    data: data
  })
}

// 修改H5个人信息收集报表
export function updatePreH5PersonWalletInfo(data) {
  return request({
    url: '/personalwalletmanagement/PreH5PersonWalletInfo',
    method: 'put',
    data: data
  })
}

// 删除H5个人信息收集报表
export function delPreH5PersonWalletInfo(id) {
  return request({
    url: '/personalwalletmanagement/PreH5PersonWalletInfo/' + id,
    method: 'delete'
  })
}
