import request from '@/utils/request'

// 查询资料上传日志列表
export function listEnterprisedatalog(query) {
  return request({
    url: '/corporatewalletmanagement/enterprisedatalog/list',
    method: 'get',
    params: query
  })
}

// 查询资料上传日志详细
export function getEnterprisedatalog(id) {
  return request({
    url: '/corporatewalletmanagement/enterprisedatalog/' + id,
    method: 'get'
  })
}

// 新增资料上传日志
export function addEnterprisedatalog(data) {
  return request({
    url: '/corporatewalletmanagement/enterprisedatalog',
    method: 'post',
    data: data
  })
}

// 修改资料上传日志
export function updateEnterprisedatalog(data) {
  return request({
    url: '/corporatewalletmanagement/enterprisedatalog',
    method: 'put',
    data: data
  })
}

// 删除资料上传日志
export function delEnterprisedatalog(id) {
  return request({
    url: '/corporatewalletmanagement/enterprisedatalog/' + id,
    method: 'delete'
  })
}
