import request from '@/utils/request'

// 查询企业资料信息列表
export function listEnterprisefile(query) {
  return request({
    url: '/corporatewalletmanagement/enterprisefile/list',
    method: 'get',
    params: query
  })
}

// 查询企业资料信息详细
export function getEnterprisefile(id) {
  return request({
    url: '/corporatewalletmanagement/enterprisefile/' + id,
    method: 'get'
  })
}

// 新增企业资料信息
export function addEnterprisefile(data) {
  return request({
    url: '/corporatewalletmanagement/enterprisefile',
    method: 'post',
    data: data
  })
}

// 修改企业资料信息
export function updateEnterprisefile(data) {
  return request({
    url: '/corporatewalletmanagement/enterprisefile',
    method: 'put',
    data: data
  })
}

// 删除企业资料信息
export function delEnterprisefile(id) {
  return request({
    url: '/corporatewalletmanagement/enterprisefile/' + id,
    method: 'delete'
  })
}
