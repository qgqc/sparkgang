import request from '@/utils/request'

// 查询BPS文件处理管理列表
export function listFilehandl(query) {
  return request({
    url: '/corporatewalletmanagement/filehandl/list',
    method: 'get',
    params: query
  })
}

// 查询BPS文件处理管理详细
export function getFilehandl(id) {
  return request({
    url: '/corporatewalletmanagement/filehandl/' + id,
    method: 'get'
  })
}

// 新增BPS文件处理管理
export function addFilehandl(data) {
  return request({
    url: '/corporatewalletmanagement/filehandl',
    method: 'post',
    data: data
  })
}

// 修改BPS文件处理管理
export function updateFilehandl(data) {
  return request({
    url: '/corporatewalletmanagement/filehandl',
    method: 'put',
    data: data
  })
}

// 删除BPS文件处理管理
export function delFilehandl(id) {
  return request({
    url: '/corporatewalletmanagement/filehandl/' + id,
    method: 'delete'
  })
}
