import request from '@/utils/request'

// 查询BPS日志列表
export function listBpslog(query) {
  return request({
    url: '/corporatewalletmanagement/bpslog/list',
    method: 'get',
    params: query
  })
}

// 查询BPS日志详细
export function getBpslog(logId) {
  return request({
    url: '/corporatewalletmanagement/bpslog/' + logId,
    method: 'get'
  })
}

// 新增BPS日志
export function addBpslog(data) {
  return request({
    url: '/corporatewalletmanagement/bpslog',
    method: 'post',
    data: data
  })
}

// 修改BPS日志
export function updateBpslog(data) {
  return request({
    url: '/corporatewalletmanagement/bpslog',
    method: 'put',
    data: data
  })
}

// 删除BPS日志
export function delBpslog(logId) {
  return request({
    url: '/corporatewalletmanagement/bpslog/' + logId,
    method: 'delete'
  })
}
