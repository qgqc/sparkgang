import request from "@/utils/request";

// 查询存量信息导入列表
export function stockInfoList(query) {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo/stocklist",
    method: "get",
    params: query,
  });
}

// 新增信息导入列表
export function newlyAddedInfoList(query) {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo/newlyaddedlist",
    method: "get",
    params: query,
  });
}

// 查询对公钱包开立申请列表
export function listBusmerwalletinfo(query) {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo/list",
    method: "get",
    params: query,
  });
}



// 查询审核列表
export function listApplyelistinfo(query) {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo/applyList",
    method: "get",
    params: query,
  });
}


// 查询审核列表
export function listExaminelistinfo(query) {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo/examinelist",
    method: "get",
    params: query,
  });
}

// 查询对公钱包开立申请详细
export function getBusmerwalletinfo(id) {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo/" + id,
    method: "get",
  });
}


// 查询对公钱包开立申请详细
export function getBusmerwalletinfoWithCustInfo(id) {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo/withCustInfo/" + id,
    method: "get",
  });
}

//查询当前登录用户机构信息
export function getLoginOrgInfo() {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo/loginOrgInfo",
    method: "get",
  });
}

// 新增对公钱包开立申请
export function addBusmerwalletinfo(data) {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo",
    method: "post",
    data: data,
  });
}

// 修改对公钱包开立申请
export function updateBusmerwalletinfo(data) {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo",
    method: "put",
    data: data,
  });
}

// 删除对公钱包开立申请
export function delBusmerwalletinfo(id) {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo/" + id,
    method: "delete",
  });
}
//对公钱包开立申请-提交审核
export function submitBusmerwalletinfoAudit(id) {
  console.log("id:" + id);
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo/submitReview",
    data: { id: id },
    method: "post",
  });
}

//审核通过
export function busmerwalletinfoAuditAgree(data) {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo/agreeInf",
    data: data,
    method: "post",
  });
}

//审核拒绝
export function busmerwalletinfoAuditReject(data) {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo/reject",
    data: data,
    method: "post",
  });
}

//对公钱包商户开通
export function submitBusmerwalletinfoOpenForm(data) {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo/open",
    data: data,
    method: "post",
  });
}

//获取已上传文件列表
export function getFileListById(data) {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo/getFileListById",
    data: data,
    method: "post",
  });
}

//下载文件数据
export function downLoadFile(url, data) {
  return request({
    url: url,
    data: data,
    method: "get",
  });
}
//获取已上传文件列表
export function deleteFileLoad(data) {
  return request({
    url: "/corporatewalletmanagement/busmerwalletinfo/deleteFileLoad",
    data: data,
    method: "post",
  });
}

//获取已上传文件列表
export function vertifyLoginname(data) {
  return request({
    url: '/corporatewalletmanagement/busmerwalletinfo/vertifyLoginname',
    data: data,
    method: 'post'
  })
}


export function vertifyEditLoginname(data) {
  return request({
    url: '/corporatewalletmanagement/busmerwalletinfo/vertifyEditLoginname',
    data: data,
    method: 'post'
  })
}

