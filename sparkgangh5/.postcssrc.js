// https://github.com/michael-ciniawsky/postcss-load-config
module.exports = {
  "plugins": {
    "postcss-px-to-viewport": {
      rootValue: 75,
      propList: ['*'],
      exclude: /node_modules/i,
      selectorBlackList: ['.norem', '-vant', '-my'] // 过滤掉.norem-开头的class，不进行rem转换
    }
  }
}




// module.exports = {
//   "plugins": {
//     "postcss-import": {},
//     "postcss-url": {},
//     // to edit target browsers: use "browserslist" field in package.json
//     "autoprefixer": {}
//   }
// }
