import Vue from "vue";
import App from "./App.vue";
// 在src/main.js进行全局引入
import Vant from "vant";
import "vant/lib/index.css";

import "amfe-flexible/index.js";
import router from "./router";
Vue.use(Vant);
Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
