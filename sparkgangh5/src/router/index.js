import Vue from "vue";
import Router from "vue-router";

import TaskIndex  from "../views/taskHall/TaskIndex.vue"
Vue.use(Router);

const routes = [
  {
    path: "/",
    redirect: "/taskIndex"
  },
  // {
  //   path: "/home",
  //   name: "home",
  //   component: Home,
  //   meta: {
  //     title: "首页"
  //   }
  // },
  // {
  //   path: "/hello",
  //   name: "hello",
  //   component: HelloWorld
  // },
  // {
  //   path: "/open",
  //   name: "open",
  //   component: Open
  // },
  // {
  //   path: "/download",
  //   name: "download",
  //   component: Download
  // },
  {
    path: "/taskIndex",
    name: "TaskIndex",
    component: TaskIndex
  }
];

const router = new Router({
  routes
});

// router.beforeEach((to, from, next) => {
// 	document.title = "中行数字人民币在线申请"
// 	next()
// })

export default router;
